package com.godrejproperties.customerapps.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;

import com.godrejproperties.customerapps.model.EmplContDeviceLogDto;

import javax.security.auth.x500.X500Principal;

public class Utils {

    public static void deleteCache(Context context) {
        System.out.println("Clearing Cache");
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    private static final X500Principal DEBUG_DN = new X500Principal("CN=Android Debug,O=Android,C=US");
    private static boolean isDebuggable(Context ctx)
    {
        boolean debuggable = false;

        try
        {
            PackageInfo pinfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature signatures[] = pinfo.signatures;

            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            for ( int i = 0; i < signatures.length;i++)
            {
                ByteArrayInputStream stream = new ByteArrayInputStream(signatures[i].toByteArray());
                X509Certificate cert = (X509Certificate) cf.generateCertificate(stream);
                debuggable = cert.getSubjectX500Principal().equals(DEBUG_DN);
                if (debuggable)
                    break;
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            //debuggable variable will remain false
        }
        catch (CertificateException e)
        {
            //debuggable variable will remain false
        }
        return debuggable;
    }

    public static String getServerURL(Context ctx){
        //return "htt//p://gpl.cp.pixieitsolutions.com/";
        return "https://cp.godrejproperties.com/";

        //return "https://640a6efb3dcd.ngrok.io/";
		/*if(Utils.isDebuggable(ctx)){
			return "https://cput.godrejproperties.com/";p
		}
		else {
			return "https://cp.godrejproperties.com/";
		}*/
    }

    public static String getDeviceLogID(Context ctx){
        AppPreference obj = new AppPreference();
        EmplContDeviceLogDto deviceLog = obj.getEmplContDeviceLog(ctx);
        if(deviceLog.getEmplContDeviceLogID().equals("0")){
            new DeviceRegistrar().Register(ctx);
            return deviceLog.getEmplContDeviceLogID();

        //return "http://69062bc1e730.ngrok.io/";
        }
        else{
            return deviceLog.getEmplContDeviceLogID();
        }
    }

}
