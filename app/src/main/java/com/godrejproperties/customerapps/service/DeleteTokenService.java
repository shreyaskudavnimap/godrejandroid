package com.godrejproperties.customerapps.service;

import android.app.IntentService;
import android.app.TaskInfo;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.godrejproperties.customerapps.R;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

public class DeleteTokenService
{
    public String TAG;
    AppPreference obj = new AppPreference();

    protected void DeleteToken(Context context)
    {
        try
        {
            TAG = context.getString(R.string.tag_id);
            // Check for current token
            String originalToken = obj.getDeviceToken(context);
            Log.d(TAG, "Token before deletion: " + originalToken);

            // Resets Instance ID and revokes all tokens.
            FirebaseInstanceId.getInstance().deleteInstanceId();

            // Clear current saved token
            obj.saveDeviceToken(context, "");

            // Check for success of empty token
            String tokenCheck = obj.getDeviceToken(context);
            Log.d(TAG, "Token deleted. Proof: " + tokenCheck);

            // Now manually call onTokenRefresh()
            Log.d(TAG, "Getting new token");
            FirebaseInstanceId.getInstance().getToken();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
