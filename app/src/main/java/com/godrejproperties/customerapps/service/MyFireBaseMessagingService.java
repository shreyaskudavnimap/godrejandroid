package com.godrejproperties.customerapps.service;

import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.godrejproperties.customerapps.ChannelPartnerActivity;
import com.godrejproperties.customerapps.MyApplication;
import com.godrejproperties.customerapps.R;
import com.godrejproperties.customerapps.model.MsgDataModel;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

public class MyFireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFireBaseMessagingService.class.getSimpleName();
    AppPreference obj = new AppPreference();
    Gson gson = new Gson();

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        obj.saveDeviceToken(getApplicationContext(), s);
        obj.saveIsTokenValid(getApplicationContext(), "Y");
        Log.i(getString(R.string.tag_id), "New Token: "+s);
        new DeviceRegistrar().Register(getApplicationContext());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.i(getString(R.string.tag_id),"Message received");
        String strData = remoteMessage.getData().toString();
        MsgDataModel msgData = gson.fromJson(strData, MsgDataModel.class);
        Log.i(getString(R.string.tag_id),strData);
        //Log.i(getString(R.string.tag_id),getString(R.string.firebase_channel_id));
        Intent intent  = new Intent(this, ChannelPartnerActivity.class);
        intent.putExtra("msgData", strData);

        ((MyApplication)getApplication()).triggerNotificationWithBackStack(ChannelPartnerActivity.class,
                getString(R.string.FBCNoticationID),
                msgData.getTitle() + " Ti",
                msgData.getBody(),
                msgData.getBody(),
                NotificationCompat.PRIORITY_HIGH,
                true,
                Integer.valueOf(msgData.getNotificationLogID()),
                PendingIntent.FLAG_UPDATE_CURRENT,
                intent);
    }


}
