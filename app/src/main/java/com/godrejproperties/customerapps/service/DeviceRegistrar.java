package com.godrejproperties.customerapps.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.godrejproperties.customerapps.R;
import com.godrejproperties.customerapps.model.APIOutput;
import com.godrejproperties.customerapps.model.EmplContDeviceLogDto;
import com.google.gson.Gson;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceRegistrar {
    AppPreference obj = new AppPreference();
    Gson gson = new Gson();
    String Tag;
    private static Retrofit retrofit=null;

    public void Register(final Context context){
        Tag = context.getString(R.string.tag_id);
        Log.i(Tag, "Entered Register");

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utils.getServerURL(context) + "webapi/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        APIService apiService = retrofit.create(APIService.class);
        EmplContDeviceLogDto deviceLog = obj.getEmplContDeviceLog(context);

        String IsTokenValid = obj.getIsTokenValid(context);
        Log.i(Tag, "IsTokenValid=" + IsTokenValid + ", EmplContID = " + deviceLog.getEmplContID());


        if(IsTokenValid.equals("Y") && !deviceLog.getEmplContID().equals("0")){
            deviceLog.setActive("Y");
            String strdevice = gson.toJson(deviceLog);
            Call<EmplContDeviceLogDto> call = apiService.SaveDeviceLog(strdevice);
            call.enqueue(new Callback<EmplContDeviceLogDto>() {
                @Override
                public void onResponse(Call<EmplContDeviceLogDto> call, Response<EmplContDeviceLogDto> response) {
                    Log.i(Tag, "SaveDeviceLog Response");
                    EmplContDeviceLogDto dto = response.body();
                    if(dto != null){
                        Log.i(Tag, "DeviceLogID=" + dto.getEmplContDeviceLogID());
                        obj.saveEmplContDeviceLog(context, dto);
                    }
                }

                @Override
                public void onFailure(Call<EmplContDeviceLogDto> call, Throwable t) {
                    Log.i(Tag, "SaveDeviceLog Response failed");

                }
            });

        }
        else{
            new DeleteTokenService().DeleteToken(context);
        }


    }
    public void DeRegister(final Context context){
        Tag = context.getString(R.string.tag_id);
        Log.i(Tag, "Entered DeRegister");

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utils.getServerURL(context)  + "webapi/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        APIService apiService = retrofit.create(APIService.class);
        final EmplContDeviceLogDto deviceLog = obj.getEmplContDeviceLog(context);
        deviceLog.setActive("N");

        String strdevice = gson.toJson(deviceLog);
        Call<APIOutput> call = apiService.DeactivateDevice(strdevice);

        call.enqueue(new Callback<APIOutput>() {
            @Override
            public void onResponse(Call<APIOutput> call, Response<APIOutput> response) {
                APIOutput output = response.body();
                Log.i(Tag, "StatusCode=" + output.getStatusCode() + ",StatusCode=" + output.getStatusCode());
                EmplContDeviceLogDto dto = deviceLog;
                dto.setEmplContID("0");
                obj.saveEmplContDeviceLog(context, dto);
            }

            @Override
            public void onFailure(Call<APIOutput> call, Throwable t) {

            }
        });
    }

    public void DeviceAccessed(final Context context){
        Tag = context.getString(R.string.tag_id);
        Log.i(Tag, "Device Accessed");

        final EmplContDeviceLogDto deviceLog = obj.getEmplContDeviceLog(context);
        if(deviceLog.getEmplContDeviceLogID().equals("0")){
            Log.i(Tag, "deviceLog.getEmplContDeviceLogID() = 0");
            Register(context);
        }
        else{

            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(Utils.getServerURL(context)  + "webapi/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            APIService apiService = retrofit.create(APIService.class);
            String strdevice = gson.toJson(deviceLog);
            Call<APIOutput> call = apiService.Accessed(strdevice);

            call.enqueue(new Callback<APIOutput>() {
                @Override
                public void onResponse(Call<APIOutput> call, Response<APIOutput> response) {
                    APIOutput output = response.body();
                    Log.i(Tag, "StatusCode=" + output.getStatusCode() + ",StatusCode=" + output.getStatusCode());
                }

                @Override
                public void onFailure(Call<APIOutput> call, Throwable t) {

                }
            });
        }

    }

}
