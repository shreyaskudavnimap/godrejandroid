package com.godrejproperties.customerapps.service;

import com.godrejproperties.customerapps.model.APIOutput;
import com.godrejproperties.customerapps.model.EmplContDeviceLogDto;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIService {

    @POST("SaveEmplContDeviceLog.ashx")
    @FormUrlEncoded
    Call<EmplContDeviceLogDto> SaveDeviceLog(@Field(value = "Device", encoded = false) String Device);

        @POST("DeactivateDevice.ashx")
    @FormUrlEncoded
    Call<APIOutput> DeactivateDevice(@Field(value = "Device", encoded = false) String Device);


    @POST("DeviceAccessed.ashx")
    @FormUrlEncoded
    Call<APIOutput> Accessed(@Field(value = "Device", encoded = false) String Device);

}
