package com.godrejproperties.customerapps.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.godrejproperties.customerapps.model.EmplContDeviceLogDto;
import com.godrejproperties.customerapps.model.UserModel;

public class AppPreference {



    public void saveEmplContDeviceLog(Context c, EmplContDeviceLogDto dto)
    {
        // System.out.println("Project Submission Day" + day);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("EmplContDeviceLogID", dto.getEmplContDeviceLogID());
        editor.putString("DeviceToken", dto.getDeviceToken());
        editor.putString("EmplContID", dto.getEmplContID());
        editor.putString("MobilePlatform", dto.getMobilePlatform());
        editor.putString("Active", dto.getActive());
        editor.commit();
    }

    public EmplContDeviceLogDto getEmplContDeviceLog(Context c)
    {
        EmplContDeviceLogDto dto = new EmplContDeviceLogDto();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        dto.setEmplContDeviceLogID(sharedPreferences.getString("EmplContDeviceLogID", "0"));
        dto.setDeviceToken(sharedPreferences.getString("DeviceToken", ""));
        dto.setEmplContID(sharedPreferences.getString("EmplContID", "0"));
        dto.setMobilePlatform(sharedPreferences.getString("MobilePlatform", "Android"));
        dto.setActive(sharedPreferences.getString("Active", "Y"));

        return dto;
    }


    public void saveIsTokenValid(Context c, String IsTokenValid)
    {
        // System.out.println("Project Submission Day" + day);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("IsTokenValid", IsTokenValid);
        editor.commit();
    }

    public String getIsTokenValid(Context c)
    {
        String IsTokenValid;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        IsTokenValid = sharedPreferences.getString("IsTokenValid", "N");
        System.out.println("IsTokenValid saved---" + IsTokenValid);
        return IsTokenValid;
    }


    public void saveEmplContID(Context c, String EmplContID)
    {
        // System.out.println("Project Submission Day" + day);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("EmplContID", EmplContID);
        editor.commit();
    }

    public String getEmplContID(Context c)
    {
        String EmplContID;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        EmplContID = sharedPreferences.getString("EmplContID", "N");
        System.out.println("EmplContID saved---" + EmplContID);
        return EmplContID;
    }


    public void saveDeviceToken(Context c, String token)
    {
        // System.out.println("Project Submission Day" + day);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("DeviceToken", token);
        editor.commit();
    }

    public String getDeviceToken(Context c)
    {
        String deviceToken;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        deviceToken = sharedPreferences.getString("DeviceToken", "");
        System.out.println("DeviceToken saved---" + deviceToken);
        return deviceToken;
    }


    public void saveNotifNo(Context c, int i)
    {
        // System.out.println("Project Submission Day" + day);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("notifNo", i);
        editor.commit();
    }

    public int getNotifNo(Context c)
    {
        int i;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        i = sharedPreferences.getInt("notifNo", 0);
        System.out.println("notifNo got---" + i);
        return i;
    }

    public UserModel getUserData(Context c)
    {
        UserModel user =new UserModel();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        user.setUser_id(sharedPreferences.getLong("User_id", 0));
        user.setEdpNo(sharedPreferences.getString("EdpNo", ""));
        user.setName(sharedPreferences.getString("Name", ""));
        user.setEmail(sharedPreferences.getString("Email", ""));
        user.setDepartment(sharedPreferences.getString("Department", ""));
        user.setJobTitle(sharedPreferences.getString("JobTitle", ""));
        user.setLocation(sharedPreferences.getString("Location", ""));
        user.setSupervisorEdp(sharedPreferences.getString("SupervisorEdp", ""));
        user.setTeamCount(sharedPreferences.getInt("TeamCount", 0));

        return user;
    }

    public void saveUserData(Context c, UserModel user)
    {
        System.out.println("save Admin data"+user.getEdpNo());
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("User_id", user.getUser_id());
        editor.putString("EdpNo", user.getEdpNo());
        editor.putString("Name", user.getName());
        editor.putString("Email", user.getEmail());
        editor.putString("Department", user.getDepartment());
        editor.putString("JobTitle",  user.getCategory());
        editor.putString("Location",  user.getLocation());
        editor.putString("SupervisorEdp",  user.getSupervisorEdp());
        editor.putInt("TeamCount", user.getTeamCount());
        editor.commit();

    }


}
