package com.godrejproperties.customerapps;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;
import android.util.Log;
import android.widget.Toast;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class CalendarEvent {

	Context context;
	int year, month, day, hour, minute;
	String title, desc, location;

	public CalendarEvent(Context context) {
		super();
		this.context = context;
		// this.year = year;
		// this.month = month;
		// this.day = day;
		// this.title = title;
		// this.desc = desc;
		// this.location = location;
	}

	public void setEvent(String title, String desc, String location, int year,
						 int month, int day) {

		// ArrayList<String> c = CalendarUtility.readCalendarEvent(context);
		// Log.v("CalenderEvents", c.toString());

//		Uri eventUri = Uri.parse("content://com.android.calendar/events"); // or
//																// "content://com.android.calendar/events
//		Cursor cursor = context.getContentResolver().query(eventUri,
//				new String[] { "_id" }, "calendar_id = " + 1, null, null); // calendar_id
//																			// can
//																			// change
//																			// in
//																			// new
//																			// versions
//		while (cursor.moveToNext()) {
//			Uri deleteUri = ContentUris.withAppendedId(eventUri,
//					cursor.getInt(0));
//			int row = context.getContentResolver()
//					.delete(deleteUri, null, null);
//
//			Log.v("Deleted row", String.valueOf(row));
//		}

		boolean eventFlag = false;

		ArrayList<String> nameofEvent = CalendarUtility
				.readCalendarEvent(context);
		for (int i = 0; i < nameofEvent.size(); i++) {
			Log.v("Title", nameofEvent.get(i));
			if (title == nameofEvent.get(i)) {

				eventFlag = true;
			} else {
			}
		}

		if (!eventFlag) {
			// title = "Event Title";
			// desc = "Event description";
			// location = "Event Location";

			// Calendar object

			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getDefault());
			cal.set(year, month - 1, day, 9, 0);

			ContentResolver cr = context.getContentResolver();

			// insert an event into Calendar
			ContentValues values = new ContentValues();

			values.put(Events.EVENT_TIMEZONE, TimeZone
					.getDefault().getID());
			values.put(Events.CALENDAR_ID, 1);
			values.put(Events.DTSTART, cal.getTimeInMillis());
			values.put(Events.DTEND, cal.getTimeInMillis());
			values.put(CalendarContract.EXTRA_EVENT_ALL_DAY, 1);
			values.put(Events.TITLE, title);
			values.put(Events.DESCRIPTION, desc);
			values.put(Events.EVENT_LOCATION, location);
			values.put(Events.RRULE, "");
			values.put(Events.HAS_ALARM, 1);

			if (checkSelfPermission(context, android.Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
				// TODO: Consider calling
				//    Activity#requestPermissions
				// here to request the missing permissions, and then overriding
				//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
				//                                          int[] grantResults)
				// to handle the case where the user grants the permission. See the documentation
				// for Activity#requestPermissions for more details.
				return;
			}
			Uri uri = cr.insert(Events.CONTENT_URI, values);
			// Log.d("Calender URI", uri.toString());

			int id = Integer.parseInt(uri.getLastPathSegment());

			// insert an event into Reminder
			ContentValues reminders = new ContentValues();

			reminders.put(Reminders.EVENT_ID, id);
			reminders.put(Reminders.METHOD, Reminders.METHOD_ALERT);
			reminders.put(Reminders.MINUTES, 10);

			cr.insert(Reminders.CONTENT_URI, reminders);

			Toast.makeText(context, "Reminder set for this event",
					Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(context, "Reminder already present",
					Toast.LENGTH_SHORT).show();
		}

	}
}
