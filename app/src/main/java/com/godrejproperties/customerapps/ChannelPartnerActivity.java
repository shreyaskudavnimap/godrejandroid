package com.godrejproperties.customerapps;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.cordova.CordovaActivity;
import org.jsoup.Jsoup;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.MailTo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Switch;
import android.widget.Toast;

import com.github.javiersantos.appupdater.AppUpdaterUtils;
import com.github.javiersantos.appupdater.enums.AppUpdaterError;
import com.github.javiersantos.appupdater.objects.Update;
import com.godrejproperties.customerapps.model.MsgDataModel;
import com.godrejproperties.customerapps.service.AppPreference;
import com.godrejproperties.customerapps.service.DeviceRegistrar;
import com.godrejproperties.customerapps.service.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;        // shreyas
import com.google.gson.Gson;
//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.android.gcm.GCMRegistrar;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

@SuppressLint("NewApi")
public class ChannelPartnerActivity extends CordovaActivity implements WSCallerVersionListener {
    public Tracker mTracker; // shreyas
    //  Added in Manifest  File  android:name="android.support.multidex.MultiDexApplication"    For Exception - Firebase  Android - java.lang.NoClassDefFoundError: com.google.firebase.FirebaseOptions
    WebView mWebView;
    private final static int ACTIVITY_TAKE_PHOTO = 1, RESULT_CROP = 400;
    private static final String TAG = "Image Pick Activity";
    private Uri outputFileUri;
    public String p1 = "";
    String fname;
    boolean isForceUpdate = true;
    String fileNameWithoutExtn;
    int readExternalStoragePermission;
    private boolean iscamaraFile = false;
    private int SELECT_FILE = 900;

    // Download a File
    private DownloadManager downloadManager;
    private long refid;
    private Uri Download_Uri;
    ArrayList<Long> list = new ArrayList<>();

    public ProgressDialog progressDialog;

    private ProgressDialog pdDownload;


    private String UPLOAD_URL;


    // Log tag that is used to distinguish log info.
    private final static String TAG_BROWSE_PICTURE = "BROWSE_PICTURE";

    // Used when request action Intent.ACTION_GET_CONTENT
    private final static int REQUEST_CODE_BROWSE_PICTURE = 1;
    private final static int REQUEST_CODE_BROWSE_DOCUMENT = 4;
    private final static int REQUEST_CODE_CAMERA_PAN_PICTURE = 5;


    // Used when request read external storage permission.
    private final static int REQUEST_PERMISSION_READ_EXTERNAL = 2;

    // Added by 21.8.2018
    private int GALLERY = 1, CAMERA = 2, CAMERA_NEW = 6;
    private int PICK_PDF_REQUEST = 300;
    public static final int TAKE_PHOTO = 100;
    private static final String IMAGE_DIRECTORY = "/IMGCPAPP";
    public static final int REQUEST_PERMISSION = 200;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 124;

    //Uri to store the image uri
    private Uri filePath;
    String path;

    /***************** APP PREFERENCES ****************/
    public static final String AppPref = "AppPrefs";
    SharedPreferences UserPreferences;

    /****************** GCM CONTROLS **********************/
    public static String GCMPREFERENCES = "GCMPrefs";
    SharedPreferences gcmpreferences;
    public static String gcmregid = "";
    int gcmStatus = 0;
    public static String paramValue;
    private String mCameraFileName;
    Gson gson = new Gson();

    // GoogleAnalyticsTracker tracker;
    ///private EasyTracker easyTracker = null;


    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public ChannelPartnerActivity() {

    }

    Activity act;
    Context c;

    public final int REQ_CODE_READ_PHONE_STATE = 0;
    public final int REQ_CODE_ACCESS_COARSE_LOCATION = 1;
    public final int REQ_CODE_ACCESS_FINE_LOCATION = 2;
    public final int REQ_CODE_CALL_PHONE = 3;
    public final int REQ_CODE_ACCESS_NETWORK_STATE = 4;
    public final int REQ_CODE_INTERNET = 5;
    public final int REQ_CODE_GET_ACCOUNTS = 6;
    public final int REQ_CODE_WAKE_LOCK = 7;
    public final int REQ_CODE_VIBRATE = 8;
    public final int REQ_CODE_WRITE_EXTERNAL_STORAGE = 9;
    public final int REQ_CODE_READ_EXTERNAL_STORAGE = 10;
    public final int REQ_CODE_WRITE_CONTACTS = 11;
    public final int REQ_CODE_READ_CONTACTS = 12;
    public final int REQ_CODE_RECORD_AUDIO = 13;
    public final int REQ_CODE_CAMERA = 14;
    //public final int REQ_CODE_CROP = 15;

    String[] perms = new String[]{

            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.VIBRATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA

    };

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i(getString(R.string.tag_id), "onNewIntent=" + getIntent().getStringExtra("msgData"));
    }

    /**
     * Called when the activity is first created.
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UPLOAD_URL = Utils.getServerURL(getApplicationContext()) + "upload_Docs.aspx";


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.channel_partner_webview);
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        act = ChannelPartnerActivity.this;
        //c = ConsumerSalesActivity.this;
        SharedPreferences prefs2 = PreferenceManager
                .getDefaultSharedPreferences(this);
        if (Build.VERSION.SDK_INT >= 23) {

            if (!prefs2.getBoolean("perms_firstTime", false)) {
                for (int count = 0; count <= perms.length; count++) {
                    ActivityCompat.requestPermissions(
                            (Activity) act,
                            perms,
                            count);
                }

				/*try {
					Thread.sleep(10000);
					for (int count =0;count<=perms.length;count++){
						ActivityCompat.requestPermissions(
								(Activity) act,
								perms,
								count);
					}

				} catch (InterruptedException e) {
					e.printStackTrace();
				}*/


                   /* public static void requestPermissions(
                    final @NonNull Activity activity,
                    final @NonNull String[] permissions,
                    final int requestCode)*/

                // Added By Ajay 27.7.2018
                //	new GooglePlayStoreAppVersionNameLoader(getApplicationContext(), this).execute();

                // mark first time has run.
                SharedPreferences.Editor editor = prefs2.edit();
                editor.putBoolean("perms_firstTime", true);
                editor.commit();
            } else {
                //Toast.makeText(this, "" + "permissions are already granted.", Toast.LENGTH_SHORT).show();
            }
        } else {
            //process();

        }
        //checkNetwork();

        UserPreferences = getSharedPreferences(AppPref, Context.MODE_PRIVATE);

        gcmpreferences = getSharedPreferences(GCMPREFERENCES,
                Context.MODE_PRIVATE);

        /* Analytical code Starts **************************** */

        // Google Analytics tracking
        //easyTracker = EasyTracker.getInstance(ChannelPartnerActivity.this);

        /* Analytical code Ends ********************************** */

        //Log.i(getString(R.string.tag_id),"before msgData="+getIntent().getStringExtra("msgData"));
        initview();

        // Download A File
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        // urlList.add("http://192.168.0.198/upload_pdf_cptest/pdf/2.pdf");

        registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        SendGA("Login Screen");
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {


            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);


            Log.e("IN", "" + referenceId);

            list.remove(referenceId);


            if (list.isEmpty()) {

                if(pdDownload != null) {            // Shreyas
                    pdDownload.dismiss();
                    dialogDownloadFile();
                }

                Log.e("INSIDE", "" + referenceId);
			/*	NotificationCompat.Builder mBuilder =
						new NotificationCompat.Builder(ChannelPartnerActivity.this)
								.setSmallIcon(R.drawable.ic_media_play_dark)
								.setContentTitle("Godrej Partner Connect")
								.setContentText("Download Completed")
								.setContentIntent(pendingIntent);
                ((MyApplication) getApplication()).triggerNotificationWithBackStack(NotificationDetailsActivity.class,
                        getString(R.string.FBCNoticationID),
                        "Godrej Partner Connect",
                        "Download Completed",
                        "Download Completed",
                        NotificationCompat.PRIORITY_DEFAULT,
                        true,
                        getResources().getInteger(R.integer.dwonload_notificationId),
                        PendingIntent.FLAG_UPDATE_CURRENT);

			 */


            }

        }
    };



/*	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

		if (requestCode == REQ_CODE_READ_PHONE_STATE) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_ACCESS_COARSE_LOCATION) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_ACCESS_FINE_LOCATION) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_CALL_PHONE) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_ACCESS_NETWORK_STATE) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_INTERNET) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_GET_ACCOUNTS) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_WAKE_LOCK) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_VIBRATE) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_WRITE_EXTERNAL_STORAGE) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_READ_EXTERNAL_STORAGE) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_WRITE_CONTACTS) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}
		if (requestCode == REQ_CODE_READ_CONTACTS) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}

		if (requestCode == REQ_CODE_RECORD_AUDIO) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "AUDIO denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "AUDIO Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}

		if (requestCode == REQ_CODE_CAMERA) {
			// If request is cancelled, the result arrays are empty.
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

				// process();
				//Toast.makeText(this, "Camera denied" , Toast.LENGTH_SHORT).show();

			} else {

				//Toast.makeText(this, "Camera Permission allowed" , Toast.LENGTH_SHORT).show();
			}
			return;
		}


	}*/

    @JavascriptInterface
    public void SendGA(String page) {
        MyApplication application = (MyApplication) getApplication(); //shreyas
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName(page);      // shreyas
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == REQUEST_PERMISSION || requestCode == TAKE_PHOTO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Thanks for granting Permission", Toast.LENGTH_SHORT).show();

                takePhotoFromCamera();

            }
        } else if (requestCode == REQUEST_PERMISSION || requestCode == REQUEST_CODE_CAMERA_PAN_PICTURE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Thanks for granting Permission", Toast.LENGTH_SHORT).show();

                takePhotoFromCameraPan();

            }
        } else if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
                //showFileChooser();
            } else {
                // Permission Denied
                Toast.makeText(ChannelPartnerActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == REQUEST_PERMISSION_READ_EXTERNAL) {
            if (grantResults.length > 0) {
                int grantResult = grantResults[0];
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    // If user grant the permission then open choose image popup dialog.
                    // openPictureGallery();
                    galleryIntent();
                } else {
                    Toast.makeText(getApplicationContext(), "You denied read external storage permission.", Toast.LENGTH_LONG).show();
                }
            }
        }/*else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission granted
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("application/pdf");
            startActivityForResult(
                    Intent.createChooser(intent, "Select File"),
                    SELECT_FILE);
        }*/

    }

    @SuppressWarnings("deprecation")
    @SuppressLint({"NewApi", "SetJavaScriptEnabled", "JavascriptInterface"})
    @JavascriptInterface
    public void initview() {

        mWebView = (WebView) findViewById(R.id.surfaceview);

        mWebView.addJavascriptInterface(this, "ChannelPartnerActivity");
        //WebView debugging is not affected by the state of the debuggable flag in the application's manifest.
        // If you want to enable WebView debugging only when debuggable is true, test the flag at runtime.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setPluginState(PluginState.ON);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);

        webSettings.setDefaultZoom(ZoomDensity.MEDIUM);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.requestFocusFromTouch();
        mWebView.setWebChromeClient(new WebChromeClient());

        Log.i(getString(R.string.tag_id), "msgData=" + getIntent().getStringExtra("msgData"));
        String strData = getIntent().getStringExtra("msgData");
        if (strData != null) {
            MsgDataModel msgData = gson.fromJson(strData, MsgDataModel.class);
            ((MyApplication) getApplication()).cancelNotification(Integer.valueOf(msgData.getNotificationLogID()));
            if (msgData.getChannelType().equals("notification")) {
                mWebView.loadUrl("file:///android_asset/index.html?reurl=notification_details.html%3Fid%3D" + msgData.getNotificationLogID());
            } else if (msgData.getChannelType().equals("project")) {
                mWebView.loadUrl("file:///android_asset/index.html?reurl=project_details.html%3Fprojectid%3D" + msgData.getId());
            } else if (msgData.getChannelType().equals("offer")) {
                mWebView.loadUrl("file:///android_asset/index.html?reurl=offer_details.html%3Fofferid%3D" + msgData.getId());
            } else if (msgData.getChannelType().equals("event")) {
                mWebView.loadUrl("file:///android_asset/index.html?reurl=event_details.html%3Feventid%3D" + msgData.getId());
            }

        } else {
            mWebView.loadUrl("file:///android_asset/index.html");
        }


        //mWebView.loadUrl("http://192.168.0.15/test/godrej_cp_gpl/index.html");  old

        //mWebView.loadUrl("http://192.168.0.15/test/cp_app_for_anagha_android/index.html");     // new

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("mailto:")) {
                    //final Activity activity = act.get();
                    if (act != null) {
                        MailTo mt = MailTo.parse(url);
                        Intent i = newEmailIntent(act, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                        act.startActivity(i);
                        //view.reload();
                        return true;
                    }
                } else if (url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                    //view.reload();
                    return true;
                } else {
                    view.loadUrl(url);
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                //checkNetwork();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //mWebView.goBack();
    }


    // Commented By Ajay  10.10.2018
/*	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()
				&& !mWebView.getUrl().contains("index.htm")) {
			mWebView.goBack();
		} else {
			showAlertDialog("Channel Partner",
					"Do you want to exit Channel Partner?");
		}

		return true;
	}*/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        System.out.println("Inside onKeyDown");

        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
                || (keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {

            return true;
        }

        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()
                && !mWebView.getUrl().contains("request_for_empanelment_new.html")) {
            mWebView.goBack();
        } else {
            showAlertDialog("Channel Partner",
                    "Do you want to exit Channel Partner?");
        }

        if (mWebView.canGoBack()
                && mWebView.getUrl().contains("index.html")) {
            // wv.loadUrl("about:blank");

            mWebView.loadUrl("file:///android_asset/index.html");

            showAlertDialog("Channel Partner",
                    "Do you want to exit Channel Partner?");
        }

        // if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebV iew.canGoBack()
        // && !mWebView.getUrl().contains("home.html")) {
        // showAlertDialog("Marathon Properties","Do you want to exit Marathon Properties ?");
        // return true;
        // }
        //
        // if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()
        // && !mWebView.getUrl().contains("property_list.html")) {
        // showAlertDialog("Marathon Properties",
        // "Do you want to exit Marathon Properties ?");
        // return true;
        // }

        // if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack())
        //
        // if (!mWebView.getUrl().contains("home.html")
        // || !mWebView.getUrl().contains("property_list.html")) {
        // {
        // mWebView.goBack();
        // }

		/*EasyTracker.getInstance(MarathonActivity.this).send(
				MapBuilder.createEvent("ACTION", "BACK press", "back button",
						null).build());*/

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        //EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    @JavascriptInterface
    public void onResume() {
        super.onResume();

        //storeFCM();

        IntentFilter filter = new IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION);

        registerReceiver(networkReceiver, filter);
		/*if (web_update()) {
			dialog_appUpdate();
		} else {

		}*/


        // App Update Pop Up - Play store Link for Updated App
        //web_update_new();

        // Notification For APP Update on Device
		/*new AppUpdater(this)
				.setDisplay(Display.NOTIFICATION)
				.setIcon(R.drawable.icon2).start(); // Notification icon ;*/
    }

    private void web_update_new() {

        AppUpdaterUtils appUpdaterUtils = new AppUpdaterUtils(this)
                //.setUpdateFrom(UpdateFrom.AMAZON)
                //.setUpdateFrom(UpdateFrom.GITHUB)
                //.setGitHubUserAndRepo("javiersantos", "AppUpdater")
                //...
                .withListener(new AppUpdaterUtils.UpdateListener() {
                    @Override
                    public void onSuccess(Update update, Boolean isUpdateAvailable) {
                        Log.d("Latest Version", update.getLatestVersion());
                        //Log.d("Latest Version Code", update.getLatestVersionCode().toString());
                        Log.d("Release notes", update.getReleaseNotes());
                        Log.d("URL", update.getUrlToDownload().toString());
                        Log.d("Is update available?", Boolean.toString(isUpdateAvailable));


                        if (isUpdateAvailable) {
                            dialog_appUpdate();
                            //showUpdateDialog();
                            //	lay_update.setVisibility(View.VISIBLE);

                        } else {

                            //	lay_update.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailed(AppUpdaterError error) {
                        Log.d("AppUpdater Error", "Something went wrong");
                    }
                });
        appUpdaterUtils.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkReceiver);
        Utils.deleteCache(ChannelPartnerActivity.this);
    }

    @Override
    public void onDestroy() {
        try {
            //unregisterReceiver(mHandleMessageReceiver);
            //GCMRegistrar.onDestroy(this);

            Utils.deleteCache(ChannelPartnerActivity.this);

        } catch (Exception e) {
            // Log.e(TAG, "UnRegister Receiver Error > " + e.getMessage());
        }
        super.onDestroy();
    }


    private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        intent.setType("message/rfc822");
        return intent;
    }

    @JavascriptInterface
    public void showAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setIcon(R.drawable.icon2);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                gcmStatus = 0;
                ChannelPartnerActivity.this.finish();
            }
        });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();
    }

    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getExtras() != null) {
                NetworkInfo ni = (NetworkInfo) intent.getExtras().get(
                        ConnectivityManager.EXTRA_NETWORK_INFO);
                if (ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {
                    // we're connected

                    // Toast.makeText(getApplicationContext(),
                    // "Connected to internet", Toast.LENGTH_LONG).show();
                } else {
                    // we're not connected
                    // Toast.makeText(getApplicationContext(),
                    // "Not connected to internet", Toast.LENGTH_LONG)
                    // .show();
                    if (isNetworkConnected()) {
                    } else {
                        showNetworkDialog();
                    }
                }
            }
        }
    };

    public void checkNetwork() {
        if (isNetworkConnected()) {
        } else {
            showNetworkDialog();
            // showNetworkPopup();
        }
    }

    @JavascriptInterface
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @JavascriptInterface
    public void showNetworkDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No Data Connection");
        builder.setMessage("Enable Data Connection or WiFi");
        builder.setCancelable(false);
        builder.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // checkNetwork(mWebView.getUrl().toString());
                        if (isNetworkConnected()) {

                            Toast.makeText(getApplicationContext(),
                                    "Connected to internet", Toast.LENGTH_SHORT)
                                    .show();

                        } else {
                            Intent intent = new Intent(
                                    android.provider.Settings.ACTION_SETTINGS);

                            startActivity(intent);
                        }

                    }
                });
        builder.setNegativeButton("Exit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

        builder.show();
    }

    @JavascriptInterface
    public void showNetworkPopup() {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Log.d("NO INTERNET CONNECTION", "");
                mWebView.loadUrl("javascript:no_net();");

            }
        });
    }

    @JavascriptInterface
    public void makePhoneCall(String phone_number) {

        System.out.println("phone_number received from JS : " + phone_number);
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone_number));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);

    }


    /*********************************** SEND MAIL INTENT ******************************/
    @JavascriptInterface
    public void composeMail(String mailId) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        String[] recipients = {mailId};
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        //intent.putExtra(Intent.EXTRA_TEXT,"def");
        intent.setType("text/html");
        startActivity(Intent.createChooser(intent, "Send mail"));
    }


    @JavascriptInterface
    public void openURL(final String imgUrl) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Dialog dialog = new Dialog(ChannelPartnerActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_layout2);
                dialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
                        LayoutParams.FILL_PARENT);
                final WebView wv = (WebView) dialog
                        .findViewById(R.id.dialogWebView);
                wv.getSettings().setJavaScriptEnabled(true);
                wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                wv.getSettings().setDomStorageEnabled(true);
                wv.getSettings().setAllowFileAccess(true);
                wv.getSettings().setAllowContentAccess(true);
                wv.getSettings().setAllowFileAccessFromFileURLs(true);
                wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
                wv.getSettings().setSupportZoom(true);
                wv.getSettings().setBuiltInZoomControls(true);
                wv.getSettings().setDefaultZoom(ZoomDensity.MEDIUM);
                wv.setVerticalScrollBarEnabled(false);
                wv.setHorizontalScrollBarEnabled(false);
                wv.requestFocusFromTouch();

                final ProgressDialog pd = new ProgressDialog(
                        ChannelPartnerActivity.this);
                wv.setWebViewClient(new WebViewClient() {


                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if (url.startsWith("mailto:")) {
                            //final Activity activity = act.get();
                            if (act != null) {
                                MailTo mt = MailTo.parse(url);
                                Intent i = newEmailIntent(act, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                                act.startActivity(i);
                                view.reload();
                                return true;
                            }
                        } else if (url.startsWith("tel:")) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                            startActivity(intent);
                            //view.reload();
                            return true;
                        } else {
                            view.loadUrl(url);
                        }
                        return true;
                    }

                    @Override
                    public void onPageStarted(WebView view, String url,
                                              Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);

                        pd.setMessage("Loading..");
                        pd.show();

                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);

                        pd.dismiss();
                    }

                });
                wv.loadUrl(imgUrl);

                dialog.show();

                dialog.findViewById(R.id.btnCancel).setOnClickListener(
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });


                pdDownload = new ProgressDialog(
                        ChannelPartnerActivity.this);


                dialog.findViewById(R.id.btnDownload).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        pdDownload.setMessage("Please Wait File DownLoading.....");
                        pdDownload.setCancelable(false);
                        pdDownload.show();

                        list.clear();

                        String[] parts = imgUrl.split("_");

                        Log.d("Url Image ====", ":=" + parts[1]);

                        Download_Uri = Uri.parse(imgUrl);
                        Log.d("Url Image ====", ":=" + Download_Uri);

                        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                        request.setAllowedOverRoaming(false);
                        //request.setTitle("Download" + ".pdf");
                        request.setDescription("Downloading " + "Sample" + ".jpeg");
                        request.setVisibleInDownloadsUi(true);
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/" + parts[1]);


                        refid = downloadManager.enqueue(request);

                        Log.e("OUT", "" + refid);

                        list.add(refid);


                    }
                });


            }
        });
    }


    @JavascriptInterface
    public void openURLinDialog(final String downloadURL) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                System.out.println("Received link from JS : " + downloadURL);

                String final_url = null;

                if (downloadURL.contains("html") || downloadURL.contains("htm")) {
                    final_url = downloadURL;
                } else if (downloadURL.toLowerCase().contains(".jpg") || downloadURL.toLowerCase().contains(".jpeg") || downloadURL.toLowerCase().contains(".png")) {
                    final_url = downloadURL;

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Log.d("Sending Response", downloadURL);
                            String responseString = "javascript:setImage('"
                                    + downloadURL + "');";

                            mWebView.loadUrl(responseString);

                        }
                    });


                } else {

                    final_url = "http://docs.google.com/gview?embedded=true&url="
                            + downloadURL;

                    Log.i(TAG, "Opening URL: " + final_url);

                }

                final Dialog dialog = new Dialog(ChannelPartnerActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_layout);
                dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
                        LayoutParams.MATCH_PARENT);

                final WebView wv = (WebView) dialog
                        .findViewById(R.id.dialogWebView);
                wv.getSettings().setJavaScriptEnabled(true);
                wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                wv.getSettings().setDomStorageEnabled(true);
                wv.getSettings().setAllowFileAccess(true);
                wv.getSettings().setAllowContentAccess(true);
                wv.getSettings().setAllowFileAccessFromFileURLs(true);
                wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
                wv.getSettings().setSupportZoom(true);
                wv.getSettings().setBuiltInZoomControls(true);
                wv.getSettings().setDefaultZoom(ZoomDensity.MEDIUM);
                wv.setVerticalScrollBarEnabled(false);
                wv.setHorizontalScrollBarEnabled(false);
                wv.requestFocusFromTouch();


                final ProgressDialog pd = new ProgressDialog(
                        ChannelPartnerActivity.this);

                wv.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if (url.startsWith("mailto:")) {
                            //final Activity activity = act.get();
                            if (act != null) {
                                MailTo mt = MailTo.parse(url);
                                Intent i = newEmailIntent(act, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                                act.startActivity(i);
                                //view.reload();
                                return true;
                            }
                        } else if (url.startsWith("tel:")) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                            startActivity(intent);
                            //view.reload();
                            return true;
                        } else {
                            view.loadUrl(url);
                        }
                        return true;
                    }

                    @Override
                    public void onPageStarted(WebView view, String url,
                                              Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);

                        pd.setMessage("Loading..");
                        pd.show();

                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);

                        pd.dismiss();
                    }

                });
                wv.loadUrl(final_url);

                dialog.show();

                dialog.findViewById(R.id.btnCancel).setOnClickListener(
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                pdDownload = new ProgressDialog(
                        ChannelPartnerActivity.this);


                dialog.findViewById(R.id.btnDownload).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        pdDownload.setMessage("Please wait... File downloading.....");
                        pdDownload.setCancelable(false);
                        pdDownload.show();

                        list.clear();

                        String[] parts = downloadURL.split("_");

                        Log.d("Url====", ":=" + parts[1]);

                        String doc_name = parts[1]; // parts[1].contains(".pdf") ? parts[1] : parts[1]+ ".pdf"; // Shreyas 04.08.2020 

                        Download_Uri = Uri.parse(downloadURL);
                        Log.d("Url====", ":=" + Download_Uri);

                        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                        request.setAllowedOverRoaming(false);
                        //request.setTitle("Download" + ".pdf");
                        request.setDescription("Downloading " + "Sample" + ".pdf");
                        request.setVisibleInDownloadsUi(true);
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/" + doc_name);
                        //request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        request.allowScanningByMediaScanner();
                        refid = downloadManager.enqueue(request);

                        Log.e("OUT", "" + refid);

                        list.add(refid);
                        pdDownload.hide();

                    }
                });

            }
        });
    }


    //-----------------------

    @JavascriptInterface
    public void openURLImageinDialog(final String downloadURL) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                System.out.println("Received link from JS : " + downloadURL);

                String final_url = null;

                if (downloadURL.contains("html") || downloadURL.contains("htm")) {
                    final_url = downloadURL;
                } else if (downloadURL.toLowerCase().contains(".jpg") || downloadURL.toLowerCase().contains(".jpeg") || downloadURL.toLowerCase().contains(".png")) {
                    final_url = downloadURL;

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Log.d("Sending Response", downloadURL);
                            String responseString = "javascript:setImage('"
                                    + downloadURL + "');";

                            mWebView.loadUrl(responseString);

                        }
                    });


                } else {

                    final_url = "http://docs.google.com/gview?embedded=true&url="
                            + downloadURL;

                    Log.i(TAG, "Opening URL Image : " + final_url);

                }

                final Dialog dialog = new Dialog(ChannelPartnerActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_layout2);
                dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
                        LayoutParams.MATCH_PARENT);

                final WebView wv = (WebView) dialog
                        .findViewById(R.id.dialogWebView);
                wv.getSettings().setJavaScriptEnabled(true);
                wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                wv.getSettings().setDomStorageEnabled(true);
                wv.getSettings().setAllowFileAccess(true);
                wv.getSettings().setAllowContentAccess(true);
                wv.getSettings().setAllowFileAccessFromFileURLs(true);
                wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
                // wv.getSettings().setSupportZoom(true);
                wv.getSettings().setBuiltInZoomControls(true);
                //  wv.getSettings().setDefaultZoom(ZoomDensity.MEDIUM);
                wv.setVerticalScrollBarEnabled(false);
                wv.setHorizontalScrollBarEnabled(false);
                wv.requestFocusFromTouch();
                wv.getSettings().setLoadWithOverviewMode(true);
                wv.getSettings().setUseWideViewPort(true);


                final ProgressDialog pd = new ProgressDialog(
                        ChannelPartnerActivity.this);

                wv.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if (url.startsWith("mailto:")) {
                            //final Activity activity = act.get();
                            if (act != null) {
                                MailTo mt = MailTo.parse(url);
                                Intent i = newEmailIntent(act, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                                act.startActivity(i);
                                //view.reload();
                                return true;
                            }
                        } else if (url.startsWith("tel:")) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                            startActivity(intent);
                            //view.reload();
                            return true;
                        } else {
                            view.loadUrl(url);
                        }
                        return true;
                    }

                    @Override
                    public void onPageStarted(WebView view, String url,
                                              Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);

                        pd.setMessage("Loading..");
                        pd.show();

                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);

                        pd.dismiss();
                    }

                });
                wv.loadUrl(final_url);

                //	wv.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}</style>" + final_url, "text/html", "UTF-8", null);

                dialog.show();

                dialog.findViewById(R.id.btnCancel).setOnClickListener(
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                pdDownload = new ProgressDialog(
                        ChannelPartnerActivity.this);


                dialog.findViewById(R.id.btnDownload).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        pdDownload.setMessage("Please Wait File DownLoading.....");
                        pdDownload.setCancelable(false);
                        pdDownload.show();

                        list.clear();

                        String[] parts = downloadURL.split("_");

                        Log.d("Url Image ====", ":=" + parts[1]);

                        Download_Uri = Uri.parse(downloadURL);
                        Log.d("Url Image ====", ":=" + Download_Uri);

                        String img_pic_name = parts[1].contains(".jpg") ? parts[1] : parts[1] + ".jpg";


                        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                        request.setAllowedOverRoaming(false);
                        //request.setTitle("Download" + ".pdf");
                        request.setDescription("Downloading " + "Sample" + ".jpg");
                        request.setVisibleInDownloadsUi(true);
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/" + img_pic_name);


                        refid = downloadManager.enqueue(request);

                        Log.e("OUT", "" + refid);

                        list.add(refid);


                    }
                });

            }
        });
    }


    @JavascriptInterface
    public void ProfilePic(String param1) {

        p1 = param1;
        Log.v("ProfilePic : p1 = ", param1);
        if (p1 != null) {
            setEvent(p1);
        } else {
            setEvent("null_1");
        }
    }

    @JavascriptInterface
    public void registerUserOnServer(String emplContID) {
        AppPreference obj = new AppPreference();
        obj.saveEmplContID(getApplicationContext(), emplContID);
        new DeviceRegistrar().Register(getApplicationContext());
    }

    @JavascriptInterface
    public void deRegisterUserOnServer() {
        new DeviceRegistrar().DeRegister(getApplicationContext());
    }

    @JavascriptInterface
    public void deviceAccessLogOnServer(String emplContID) {
        AppPreference obj = new AppPreference();
        obj.saveEmplContID(getApplicationContext(), emplContID);
        new DeviceRegistrar().DeviceAccessed(getApplicationContext());
    }

    @JavascriptInterface
    public String uploadDocsToServer(String paramsupload) {

        paramValue = paramsupload;

        Log.d("Param JS Before Call", "@@@" + paramValue);


        // ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);

        if (askForPermission()) {


            //	showFileChooser();
            showPictureDialog();

        }


        // choosePhotoFromGallary();

        return fileNameWithoutExtn;

    }

    /**
     * Runtime Permission
     */
    private boolean askForPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            int hasCallPermission = ContextCompat.checkSelfPermission(ChannelPartnerActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasCallPermission != PackageManager.PERMISSION_GRANTED) {
                // Ask for permission
                // need to request permission
                if (ActivityCompat.shouldShowRequestPermissionRationale(ChannelPartnerActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // explain
                    showMessageOKCancel(
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ActivityCompat.requestPermissions(ChannelPartnerActivity.this,
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                            REQUEST_CODE_ASK_PERMISSIONS);
                                }
                            });
                    // if denied then working here
                } else {
                    // Request for permission
                    ActivityCompat.requestPermissions(ChannelPartnerActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_PERMISSIONS);
                }

                return false;
            } else {
                // permission granted and calling function working
                return true;
            }
        } else {
            return true;
        }
    }

    private void showMessageOKCancel(DialogInterface.OnClickListener okListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChannelPartnerActivity.this);
        final AlertDialog dialog = builder.setMessage("You need to grant access to Read External Storage")
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(
                        ContextCompat.getColor(ChannelPartnerActivity.this, android.R.color.holo_blue_light));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(
                        ContextCompat.getColor(ChannelPartnerActivity.this, android.R.color.holo_red_light));
            }
        });

        dialog.show();

    }

    private void showFileChooser() {
        Intent intent = new Intent();
        // intent.setType("application/pdf");
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_PDF_REQUEST);
    }


    @JavascriptInterface
    public void setImageReturnResponse(final String recievedResponse) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Log.d("Sending Response", recievedResponse);
                String responseString = "javascript:setImage('"
                        + recievedResponse + "');";

                mWebView.loadUrl(responseString);

            }
        });

    }

    @JavascriptInterface
    public void setDocReturnResponse(final String recievedResponse, final String valParam) {

        progressDialog.dismiss();

        Log.d("Parameter val ==", "" + valParam);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                Log.d("Response From Server=", recievedResponse);

                try {
                    if ("pan".equals(valParam)) {

                        String responsePanString = "javascript:setpan('"
                                + recievedResponse + "');";

                        Log.d("Pan Response", "##=" + responsePanString);

                        mWebView.loadUrl(responsePanString);

                    } else if ("rera".equals(valParam)) {

                        String responseReraString = "javascript:setrera('"
                                + recievedResponse + "');";
                        Log.d("Rera Response", "%%=" + responseReraString);
                        mWebView.loadUrl(responseReraString);
                    } else if ("gst".equals(valParam)) {

                        String responseGstString = "javascript:setgst('"
                                + recievedResponse + "');";
                        Log.d("Gst Response", "@@=" + responseGstString);
                        mWebView.loadUrl(responseGstString);

                    }
                    else if ("addrproof".equals(valParam)) {

                        String responseAddressProof = "javascript:setAddressProof('"
                                + recievedResponse + "');";
                        Log.d(" AddressProof Response", "@@=" + responseAddressProof);
                        mWebView.loadUrl(responseAddressProof);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //mWebView.loadUrl(responseString);
            }
        });

    }

    // Image Capture Click Function Call
    @JavascriptInterface
    public void setEvent(String p1) {

		/*// Determine Uri of camera image to save & Create Folder in Storage
		final File root = new File(Environment.getExternalStorageDirectory()
				+ File.separator + "MyDir" + File.separator);

		// Create Folder in Storage
		root.mkdirs();

		// final String fname = Utils.getUniqueImageFilename();
		// Image Name Set After Capture Image
		fname = "img_" + System.currentTimeMillis() + ".jpg";


		final File sdImageMainDirectory = new File(root, fname);

		//outputFileUri = Uri.fromFile(sdImageMainDirectory);      // old_fun() Commented By Ajay 27.7.2018

		// Added By Ajay 27.7.2018
		Uri outputFileUri = FileProvider.getUriForFile(ChannelPartnerActivity.this,
				BuildConfig.APPLICATION_ID + ".provider",
				sdImageMainDirectory);

       // Addeded by Ajay 27.7.2018
		//outputFileUri = FileProvider.getUriForFile(ChannelPartnerActivity.this, BuildConfig.APPLICATION_ID + ".provider",sdImageMainDirectory);

		// Camera.
		final List<Intent> cameraIntents = new ArrayList<Intent>();

		final Intent captureIntent = new Intent(
				MediaStore.ACTION_IMAGE_CAPTURE);

		final PackageManager packageManager = getPackageManager();
		final List<ResolveInfo> listCam = packageManager.queryIntentActivities(
				captureIntent, 0);

		for (ResolveInfo res : listCam) {
			final String packageName = res.activityInfo.packageName;
			final Intent intent = new Intent(captureIntent);

			intent.setComponent(new ComponentName(res.activityInfo.packageName,
					res.activityInfo.name));

			intent.setPackage(packageName);

		 // MediaStore.EXTRA_OUTPUT is passed as an extra to specify the image storage path
			intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

			cameraIntents.add(intent);
		}

		// Filesystem.
		final Intent galleryIntent = new Intent();
		galleryIntent.setType("image/*");
		galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
		galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);

			// Chooser of filesystem options.
		final Intent chooserIntent = Intent.createChooser(galleryIntent,
				"Select Image Source");

		// Add the camera options.
		//Intent.EXTRA_INITIAL_INTENTS is used to adds the multiple application intents at one place

		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
				cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

		startActivityForResult(chooserIntent, ACTIVITY_TAKE_PHOTO);
		Log.v(TAG, "Image Upload Option Selected");*/


        // New Fun Cap Image 21.8.2018

        //	showPictureDialog();

        //	takePhotoFromCamera();

        showPictureDialog();

    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(ChannelPartnerActivity.this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Capture Image",
                "Choose Image",
                "Choose Document"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //   boolean result = Utility.checkPermission(ChannelPartnerActivity.this);

                        switch (which) {

                            case 0:
                                //takePhotoFromCamera();
                                takePhotoFromCameraPan();
                                break;

                            case 1:
                                //choosePhotoFromGallary();

                                readExternalStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                                if (readExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                                    String requirePermission[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
                                    ActivityCompat.requestPermissions(ChannelPartnerActivity.this, requirePermission, REQUEST_PERMISSION_READ_EXTERNAL);
                                } else {

                                    // openPictureGallery();   cooment by ajay 4 oct 2018
                                    //showFileChooser();
                                    galleryIntent();

                                }


                                break;

                            case 2:
                                //choosePhotoFromGallary();

                                readExternalStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                                if (readExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                                    String requirePermission[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
                                    ActivityCompat.requestPermissions(ChannelPartnerActivity.this, requirePermission, REQUEST_PERMISSION_READ_EXTERNAL);
                                } else {
                                    //openDocumentGallery();
                                    OpenDocuments();
                                }


                                break;

                        }
                    }
                });
        pictureDialog.show();
    }

    public void OpenDocuments() {

        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("application/pdf");
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("application/pdf");
        Intent chooserIntent = Intent.createChooser(getIntent, "Select PDF");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, galleryIntent);
        galleryIntent.putExtra("return-data", true);
        //below permission is required to send uri through intent as its restricted above nougat to use freely
        chooserIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(chooserIntent, PICK_PDF_REQUEST);

        /*
        Intent getIntent = new Intent();
        getIntent.setType("application/pdf");
        getIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(getIntent, "Choose file"), PICK_PDF_REQUEST);
        */
    }


    /* Invoke android os system file browser to select images. */
    private void openPictureGallery() {
        // Create an intent.
        Intent openAlbumIntent = new Intent();

        // Only show images in the content chooser.
        // If you want to select all type data then openAlbumIntent.setType("*/*");
        // Must set type for the intent, otherwise there will throw android.content.ActivityNotFoundException: No Activity found to handle Intent { act=android.intent.action.GET_CONTENT }
        openAlbumIntent.setType("image/*");
        // Set action, this action will invoke android os browse content app.
        openAlbumIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Start the activity.
        // startActivityForResult(openAlbumIntent, REQUEST_CODE_BROWSE_PICTURE);
        startActivityForResult(openAlbumIntent, PICK_PDF_REQUEST);
    }

    private void openDocumentGallery() {
        // Create an intent.
        try {
            Intent openAlbumIntent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);

            // Only show images in the content chooser.
            // If you want to select all type data then openAlbumIntent.setType("*/*");
            // Must set type for the intent, otherwise there will throw android.content.ActivityNotFoundException: No Activity found to handle Intent { act=android.intent.action.GET_CONTENT }
            openAlbumIntent.setType("application/pdf");
            openAlbumIntent.setData(Uri.parse("package:" + "com.android.providers.downloads"));

            // Set action, this action will invoke android os browse content app.
            openAlbumIntent.setAction(Intent.ACTION_GET_CONTENT);

            // Start the activity.
            startActivityForResult(openAlbumIntent, PICK_PDF_REQUEST);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();

            //Open the generic Apps page:
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            intent.setType("application/pdf");
            //intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, PICK_PDF_REQUEST);
        }
/*
        Intent openAlbumIntent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);

        // Only show images in the content chooser.
        // If you want to select all type data then openAlbumIntent.setType("/*");
        // Must set type for the intent, otherwise there will throw android.content.ActivityNotFoundException: No Activity found to handle Intent { act=android.intent.action.GET_CONTENT }
        openAlbumIntent.setType("application/pdf");
        openAlbumIntent.setData(Uri.parse("package:" + "com.android.providers.downloads"));

        // Set action, this action will invoke android os browse content app.
        openAlbumIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Start the activity.
        startActivityForResult(openAlbumIntent, PICK_PDF_REQUEST);
        */
    }


    // Start 4th Oct 2018
    private void galleryIntent() {
        iscamaraFile = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            List<Intent> targets = new ArrayList<>();
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_PICK);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            List<ResolveInfo> candidates = getApplicationContext().getPackageManager().queryIntentActivities(intent, 0);

            for (ResolveInfo candidate : candidates) {
                String packageName = candidate.activityInfo.packageName;
                if (!packageName.equals("com.google.android.apps.photos") && !packageName.equals("com.google.android.apps.plus") && !packageName.equals("com.android.documentsui")) {
                    Intent iWantThis = new Intent();
                    iWantThis.setType("image/*");
                    iWantThis.setAction(Intent.ACTION_PICK);
                    iWantThis.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    iWantThis.setPackage(packageName);
                    targets.add(iWantThis);
                }
            }
            if (targets.size() > 0) {
                Intent chooser = Intent.createChooser(targets.remove(0), "Select Picture");
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, targets.toArray(new Parcelable[targets.size()]));
                startActivityForResult(chooser, SELECT_FILE);
            } else {
                Intent intent1 = new Intent(Intent.ACTION_PICK);
                intent1.setType("image/*");
                startActivityForResult(Intent.createChooser(intent1, "Select Picture"), SELECT_FILE);
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
        }
    }

    // End 4th Oct 2018


    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {


            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);

        } else {
            // we don't have it, request camera permission from system
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    TAKE_PHOTO);

           /* Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);*/
        }


    }

    private void takePhotoFromCameraPan() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {


            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String newPicFile = "Pixie";
            String outPath = "/sdcard/" + newPicFile;
            File outDir = new File(outPath);
            if (!outDir.exists()) {
                outDir.mkdir();
            }
            outPath = outPath + "/pan" + Calendar.getInstance()
                    .getTimeInMillis() + ".jpg";
            File outFile = new File(outPath);
            mCameraFileName = outFile.toString();

            //Uri outuri = Uri.fromFile(outFile);
            Uri outuri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", outFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outuri);


            startActivityForResult(intent, CAMERA_NEW);

        } else {
            // we don't have it, request camera permission from system
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CODE_CAMERA_PAN_PICTURE);

           /* Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);*/
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //System.out.println("Intent Data:OnActivity:"+"RESULT_OK"+data.getData());

		/*if (resultCode == RESULT_OK) {
			if (requestCode == ACTIVITY_TAKE_PHOTO) {
				final boolean isCamera;
				if (data == null) {
					isCamera = true;
				} else {
					final String action = data.getAction();
					if (action == null) {
						isCamera = false;
					} else {
						isCamera = action
								.equals(MediaStore.ACTION_IMAGE_CAPTURE); //To start the native camera
					}
				}

				Uri selectedImageUri;
				if (isCamera) {
					selectedImageUri = outputFileUri;
				} else {
					selectedImageUri = data == null ? null : data.getData();
				}

			//	Log.v(TAG, "Uri Created, Uri: " + selectedImageUri.toString());
				performCrop(selectedImageUri);
			}

			if (requestCode == RESULT_CROP) {

				Log.v(TAG, "Crop Activity");
				Bundle extras = data.getExtras();
				Bitmap selectedBitmap = null;
				int currentAPIVersion = Build.VERSION.SDK_INT;

                if(currentAPIVersion>= Build.VERSION_CODES.M){
                    try {
                    	selectedBitmap = Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());

					//	System.out.println("Intent Data:OnActivity:"+"RESULT_CROP"+data.getData());

                    }catch(Exception e){
                    	selectedBitmap = extras.getParcelable("data");
                        //showToast("Error saving image: "+e.getMessage());
                        e.printStackTrace();
                    }

                }else{
                	 selectedBitmap = extras.getParcelable("data");
                }


				// imageView1.setImageBitmap(selectedBitmap);

				File pictureFile = getOutputMediaFile(String
						.valueOf(selectedBitmap.getGenerationId()));

				try {
					FileOutputStream fos = new FileOutputStream(pictureFile);

					selectedBitmap
							.compress(Bitmap.CompressFormat.JPEG, 90, fos);   // quality  40

				} catch (FileNotFoundException e) {
					Log.d("ERROR", "File not found: " + e.getMessage());
				} catch (IOException e) {
					Log.d("ERROR", "Error accessing file: " + e.getMessage());
				}

				Log.v("Value of p1 = ", p1);
				System.out.println("Image Cropped File Path:- " + pictureFile.getAbsolutePath());
				ImageUploader(pictureFile.getAbsolutePath(), p1);

			}
		}*/

        // Added By Ajay 21.8.2018
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
		/*if (requestCode == GALLERY) {
			if (data != null) {
				Uri contentURI = data.getData();


				Log.d("Image URI", ":" + contentURI);

				try {
					*//*Bitmap bitmap = Images.Media.getBitmap(ChannelPartnerActivity.this.getContentResolver(), contentURI);

					Log.d("Image Bitmap", ":" + bitmap);

					String path = saveImage(bitmap);

					Log.d("Image Path Gallery", ":" + path);

					Toast.makeText(ChannelPartnerActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();*//*
				//	imageview.setImageBitmap(bitmap);

					//ImageUploader(path, p1);

					String filePath=FilePath.getPath(ChannelPartnerActivity.this,contentURI);


					DocUploader(path);

				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(ChannelPartnerActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
				}
			}

		}else*//* if (requestCode == PICK_PDF_REQUEST) {*/
        if (requestCode == REQUEST_CODE_BROWSE_PICTURE) {
            if (resultCode == RESULT_OK) {
                // Get return image uri. If select the image from camera the uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
                // If select the image from gallery the uri like content://media/external/images/media/1316970.
                Uri fileUri = data.getData();

                Log.d("TAG", "File URI OnActivity=" + fileUri);

                try {

                    String filePathOfFile = FilePathNew.getFilePath(ChannelPartnerActivity.this, fileUri);

                    File f = new File(filePathOfFile);

                    // Added By Ajay 24 Sep 2018
                    //	Uri photoURI = FileProvider.getUriForFile(this,getApplicationContext().getPackageName() + ".GenericFileProvider", f);

                    //	Log.d("URI Using File Prov=",":"+photoURI);

                    Log.d("TAG", "Path of File=" + f.getAbsolutePath());

                    Log.d("TAG", "File Name=" + f.getName());

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }


                //getUriRealPath(getApplicationContext(), fileUri);

                // Create content resolver.
                ContentResolver contentResolver = getContentResolver();

                try {
                    // Open the file input stream by the uri.
                    InputStream inputStream = contentResolver.openInputStream(fileUri);

                    // Get the bitmap.
                    Bitmap imgBitmap = BitmapFactory.decodeStream(inputStream);

                    String path = saveImage(imgBitmap);

                    ImageUploader(path, p1);


                    inputStream.close();

                } catch (FileNotFoundException ex) {
                    Log.e(TAG_BROWSE_PICTURE, ex.getMessage(), ex);
                } catch (IOException ex) {
                    Log.e(TAG_BROWSE_PICTURE, ex.getMessage(), ex);
                }
            }
        } else if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri contentURI = data.getData();

            Log.d("Onactivity IntentData:-", "###=" + contentURI);

            String filePathOfFile = null;
            try {
                filePathOfFile = FilePath.getPath(ChannelPartnerActivity.this, contentURI);
                //filePathOfFile = FilePathNew.getFilePath(ChannelPartnerActivity.this, contentURI);
                //Log.d("File Path In String:", ":=" + filePathOfFile);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d("File Path PDF", "::" + filePathOfFile);
            if (filePathOfFile != null) {
                File filePathBeforeUploadDoc = new File(filePathOfFile);

                Log.d("File Absuolute path", "==" + filePathBeforeUploadDoc.getAbsolutePath());


                Log.d("File Name", "::" + filePathBeforeUploadDoc.getName());

                String fileExtenstionMatchNew = getFileExtension(filePathBeforeUploadDoc);

                Log.d("File Extenstion==", "***" + fileExtenstionMatchNew);


                // Get length of file in bytes
                long fileSizeInBytes = filePathBeforeUploadDoc.length();
                Log.d(TAG, "File Size-Bytes=:" + fileSizeInBytes);
                if (fileSizeInBytes <= 0) {
                    Toast.makeText(act, "Unable to read file", Toast.LENGTH_SHORT).show();
                }

                //  Log.e("TAG","Image File Size :="+FileSizeCount.formatSize(fileSizeInBytes));

                // String fileSizeInMB=FileSizeCount.formatSize(fileSizeInBytes);

                long sizeInMb = fileSizeInBytes / (1024 * 1024);
                Log.d(TAG, "File Size-MB-New=:" + sizeInMb);

                int sizeValInt = (int) sizeInMb;

                Log.d("File Size Int:", "=" + sizeValInt);

			  /*  String array2[]= fileSizeInMB.split("M", 2);

			    String sizeFile=array2[0];

			    int sizeVal=Integer.parseInt(sizeFile);

			    Log.d(TAG,"Int Val Of Size=:"+sizeVal);

			    Log.d(TAG,"Split-0=:"+array2[0]);
			    Log.d(TAG,"Split-1=:"+array2[1]);
*/

                //  Log.e("TAG","File Size MB :="+fileSize);


                if (fileExtenstionMatchNew.equals(".pdf")) {
                    if (sizeValInt < 6) {

                        DocUploader(filePathBeforeUploadDoc.getAbsolutePath(), paramValue);

                    } else {

                        Toast.makeText(act, "Please Select File Less than 5 MB", Toast.LENGTH_LONG).show();
                    }

                    //DocUploader(f.getAbsolutePath(),paramValue);

                } else {

                    Toast.makeText(act, "Invalid File Format Please Select Image OR PDF File", Toast.LENGTH_SHORT).show();

                }
            } else {
                Toast.makeText(act, "Unable to read file", Toast.LENGTH_SHORT).show();
            }


            //DocUploader(f.getAbsolutePath(),paramValue);



            /*}*/

        } else if (requestCode == REQUEST_CODE_CAMERA_PAN_PICTURE) {

            if (resultCode == RESULT_OK) {
                // Get return image uri. If select the image from camera the uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
                // If select the image from gallery the uri like content://media/external/images/media/1316970.
                Uri fileUri = data.getData();

                Log.d("TAG", "File URI OnActivity=" + fileUri);

                try {

                    String filePathOfFile = FilePathNew.getFilePath(ChannelPartnerActivity.this, fileUri);

                    File f = new File(filePathOfFile);

                    // Added By Ajay 24 Sep 2018
                    //	Uri photoURI = FileProvider.getUriForFile(this,getApplicationContext().getPackageName() + ".GenericFileProvider", f);

                    //	Log.d("URI Using File Prov=",":"+photoURI);

                    Log.d("TAG", "Path of File=" + f.getAbsolutePath());

                    Log.d("TAG", "File Name=" + f.getName());

                    Log.d("File Name", "::" + f.getName());


                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }


                //getUriRealPath(getApplicationContext(), fileUri);

                // Create content resolver.
                ContentResolver contentResolver = getContentResolver();

                try {
                    // Open the file input stream by the uri.
                    InputStream inputStream = contentResolver.openInputStream(fileUri);

                    // Get the bitmap.
                    Bitmap imgBitmap = BitmapFactory.decodeStream(inputStream);

                    String path = saveImage(imgBitmap);

                    //	ImageUploader(path, p1);

                    DocUploader(path, paramValue);

                    inputStream.close();

                } catch (FileNotFoundException ex) {
                    Log.e(TAG_BROWSE_PICTURE, ex.getMessage(), ex);
                } catch (IOException ex) {
                    Log.e(TAG_BROWSE_PICTURE, ex.getMessage(), ex);
                }
            }

        } else if (requestCode == CAMERA_NEW) {                    // CAMERA

            Uri uri = null;
            if (data != null) {
                uri = data.getData();
            }
            if (uri == null && mCameraFileName != null) {
                uri = Uri.fromFile(new File(mCameraFileName));
            }
            File file = new File(mCameraFileName);
            if (!file.exists()) {
                file.mkdir();
            }
            DocUploader(mCameraFileName, paramValue);
            /* Jipson
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            //	imageview.setImageBitmap(thumbnail);

            String pathNew = saveImage(thumbnail);

            //ImageUploader(path, p1);

            DocUploader(pathNew, paramValue);


            Log.d("Image Path Camera", ":" + path);

             */
            //Toast.makeText(ChannelPartnerActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        } else if (requestCode == SELECT_FILE) {

               /* Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");*/

            final Uri selectedUri = data.getData();

            Log.d("URI - DATA - PICK FILE", ":" + selectedUri.toString());

            //  image.setImageBitmap(imageBitmap);

            File f = new File(selectedUri.toString());

            Log.d("File Absuolute path", "==" + f.getAbsolutePath());

            String filePathOfFile = null;
            try {
                filePathOfFile = FilePathNew.getFilePath(ChannelPartnerActivity.this, selectedUri);

                Log.d("File Path -String", ":" + filePathOfFile);

                //DocUploader(filePathOfFile,paramValue);


            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            File file_path_before_upload = new File(filePathOfFile);

            Log.d("File Absuolute path", "==" + file_path_before_upload.getAbsolutePath());


            Log.d("File Name", "::" + file_path_before_upload.getName());

            String fileExtenstionMatch = getFileExtension(file_path_before_upload);

            Log.d("File Extenstion==", "***" + fileExtenstionMatch);


            // Get length of file in bytes
            long fileSizeInBytes = file_path_before_upload.length();
            Log.d(TAG, "File Size-Bytes=:" + fileSizeInBytes);

            Log.e("TAG", "Image File Size :=" + FileSizeCount.formatSize(fileSizeInBytes));


            //String fileSizeInMB=FileSizeCount.formatSize(fileSizeInBytes);

            long sizeInMb = fileSizeInBytes / (1024 * 1024);
            Log.d(TAG, "File Size-MB-New=:" + sizeInMb);

            int sizeValInt = (int) sizeInMb;

            Log.d("File Size Int:", "=" + sizeValInt);


            if (fileExtenstionMatch.equals(".jpg") || fileExtenstionMatch.equals(".png") || fileExtenstionMatch.equals(".jpeg")) {
                if (sizeValInt < 6) {

                    DocUploader(file_path_before_upload.getAbsolutePath(), paramValue);

                } else {

                    Toast.makeText(act, "Please Select File Less than 5 MB", Toast.LENGTH_LONG).show();
                }

                //DocUploader(f.getAbsolutePath(),paramValue);

            } else {

                Toast.makeText(act, "Invalid File Format Please Select Image OR PDF File", Toast.LENGTH_SHORT).show();

            }

            //  image.setImageURI(selectedUri);
/*
                if (selectedUri != null) {
                    startCropActivity(selectedUri);
                } else {

                }*/
            // GlobalVar.errorLog(TAG, "REQUEST_CAMERA", "NO");
        }
    }

    public String uploadMultipart() {
        //getting name for the pdf
        //String name = editText.getText().toString().trim();
        //String name = "TEST";
        //getting the actual path of the pdf
        String path = FilePath.getPath(this, filePath);

        String fileNameWithExtn = path.substring(path.lastIndexOf('/') + 1, path.length());
        Log.d("FileNameWithExtn", "#=" + fileNameWithExtn);

        Log.d("%%%%%%%%%", "%%%%%%");

        fileNameWithoutExtn = fileNameWithExtn.substring(0, fileNameWithExtn.lastIndexOf('.'));
        Log.d("FileNameWithoutExtn", "@=" + fileNameWithoutExtn);

        Log.d("%%%%%%%%%", "%%%%%%");


        if (path == null) {

            Toast.makeText(this, "Please move your .pdf file to internal storage and retry", Toast.LENGTH_LONG).show();
        } else {
            //Uploading code
            try {
                String uploadId = UUID.randomUUID().toString();

                //Creating a multi part request
                new MultipartUploadRequest(this, uploadId, UPLOAD_URL)
                        .addFileToUpload(path, "pdf") //Adding file
                        .addParameter("name", fileNameWithoutExtn) //Adding text parameter to the request
                        .setNotificationConfig(new UploadNotificationConfig())
                        .setMaxRetries(2)
                        .startUpload(); //Starting the upload

            } catch (Exception exc) {
                Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        return fileNameWithoutExtn;
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        // For High Quality Image
        //int nh = (int) (myBitmap.getHeight() * (2048.0 / myBitmap.getWidth()));
        //Bitmap scaled = Bitmap.createScaledBitmap(myBitmap, 2048, nh, true);
        //scaled.compress(Bitmap.CompressFormat.PNG, 100, bytes);


        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();

            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            // Get length of file in bytes
            long fileSizeInBytes = f.length();
            Log.d(TAG, "File Size-Bytes Before Upload=:" + fileSizeInBytes);


            long sizeInKb = fileSizeInBytes / 1024;
            Log.d(TAG, "File Size-KB-Before Upload=:" + sizeInKb);

            long sizeInMb = fileSizeInBytes / (1024 * 1024);
            Log.d(TAG, "File Size-MB-New-Before Upload=:" + sizeInMb);

            int sizeValInt = (int) sizeInMb;
            Log.d("File Size Int:", "=" + sizeValInt);


            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private File getOutputMediaFile(String filename) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory() + "/Android/data/"
                        + getApplicationContext().getPackageName() + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name

        File mediaFile;
        String mImageName = fname;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + mImageName);

        return mediaFile;
    }

    @JavascriptInterface
    public void ImageUploader(final String filePath, final String p1) {

        new Thread(new Runnable() {
            public void run() {
                try {

                    Log.v(TAG, "Upload Activity " + filePath);
                    FileInputStream fstrm = null;
                    File file = new File(filePath);
                    fstrm = new FileInputStream(file);
                    ProfileImageUpload upload = new ProfileImageUpload(
                            Utils.getServerURL(getApplicationContext()) + "gplservice/CPProperties.svc/attachment/upload",
                            "front", file.getName(), p1,
                            getApplicationContext(), fname);


                    // http://netbizlabs.com/godrejchap_admin/uploadimage.aspx

                    // http://netbizlabs.com/godrejchap_admin/uploadimage.aspx
                    // new link

                    // http://192.168.0.15/test/godrej_cp/uploadimage.aspx

                    // http://netbizlabs.com/httpdocs/godrejsalesapp/uploadimage.aspx

                    String response = upload.sendNow(fstrm);

                    Log.d("Response recieved", response);

                    setImageReturnResponse(response);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @JavascriptInterface
    public String getURL() {
        return Utils.getServerURL(getApplicationContext());
    }

    @JavascriptInterface
    public String getDeviceLogID() {
        return Utils.getDeviceLogID(getApplicationContext());
    }


    @JavascriptInterface
    public void DocUploader(final String filePathDoc, final String paramSelectVal) {


        progressDialog = new ProgressDialog(ChannelPartnerActivity.this);
        progressDialog.setMessage("Pleas Wait........"); // Setting Message
        progressDialog.setTitle("Uploading File"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Log.d("In DocUpload Fun", "==" + paramSelectVal);

        new Thread(new Runnable() {
            public void run() {
                try {

                    Log.v(TAG, "Upload Activity Doc==" + filePathDoc);
                    //FileInputStream fstrm = null;

                    File filePathFrom = new File(filePathDoc);

                    Log.d("File pathhh", "==" + filePathFrom.getAbsolutePath());

                    Log.d("File Name", "::" + filePathFrom.getName());

                    String fileExtenstion = getFileExtension(filePathFrom);

                    Log.d("File Extenstion==", "***" + fileExtenstion);

                    Log.d("Random File Name", ":::=" + Calendar.getInstance()
                            .getTimeInMillis());


                    String fileSuffix = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                    Log.d("File Name Random", "%%%" + fileSuffix + fileExtenstion);

                    String filename_final = paramSelectVal + "_" + fileSuffix + fileExtenstion;

                    Log.d("Final File Name", ":=" + filename_final);

                    //FileInputStream fstrm = new FileInputStream(file);

                    // file name= file.getName() 3rd param
                    DocumentUpload upload = new DocumentUpload(
                            Utils.getServerURL(getApplicationContext()) + "gplservice/CPProperties.svc/attachment/upload",
                            "front", filename_final,
                            getApplicationContext());

                    // http://netbizlabs.com/godrejchap_admin/uploadimage.aspx

                    // http://netbizlabs.com/godrejchap_admin/uploadimage.aspx
                    // new link

                    // http://192.168.0.15/test/godrej_cp/uploadimage.aspx

                    // http://netbizlabs.com/httpdocs/godrejsalesapp/uploadimage.aspx


                    Log.d("File Read Before==", "###=" + filePathFrom.canRead());


                    if (filePathFrom.canRead()) {


                    } else {

                        filePathFrom.setReadable(true);

                        Log.d("File Read After==", "###=" + filePathFrom.canRead());

                        FileInputStream fstrmNew = null;
                        try {
                            fstrmNew = new FileInputStream(filePathFrom);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        String response = upload.sendNow(fstrmNew);
                        Log.d("Response Recieved Doc", response);
                    }

                    FileInputStream fstrmNew = null;
                    try {
                        fstrmNew = new FileInputStream(filePathFrom);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                    String response = upload.sendNow(fstrmNew);


                    Log.d("Response Recieved Doc", response);

                    //setImageReturnResponse(response);

                    setDocReturnResponse(filename_final, paramSelectVal);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf("."));
        else return "";
    }

    private void performCrop(Uri picUri) {
        try {
            // Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri

            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            anfe.printStackTrace();

            String errorMessage = "your device doesn't support the crop action! -ANF Excp";

            Log.d("device doesn't support", "=" + errorMessage);

            Toast toast = Toast
                    .makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();

        } catch (Exception e) {

            e.printStackTrace();

            String errorMessage = "your device doesn't support the crop action!";
            Log.d("device doesn't support", "=" + errorMessage);

            Toast toast = Toast
                    .makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(getContentResolver(), inImage,
                "Title", null);
        return Uri.parse(path);
    }

    @JavascriptInterface
    public void Reminder(String date, String title, String decription,
                         String place) {
        Log.d("Reminder	: ", date + "\n" + title + "\n" + decription + "\n"
                + place);

        String day = date.substring(0, 2);
        String month = date.substring(3, 5);
        String year = date.substring(6, 10);

        Log.d("Date	: ", year + "\n" + month + "\n" + day);

        CalendarEvent c = new CalendarEvent(getApplicationContext());
        c.setEvent(title, decription, place, Integer.valueOf(year),
                Integer.valueOf(month), Integer.valueOf(day));

    }

    @JavascriptInterface
    public void openYoutubeVideo(final String URL) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                System.out.println("Received y_code from JS : " + URL);

                Intent videoIntent = new Intent(ChannelPartnerActivity.this,
                        YoutubeActivity.class);
                videoIntent.putExtra("url", URL);
                startActivity(videoIntent);

            }
        });

    }


    /*********************************** GCM SECTION ******************************/
    // GCM Registration of device
    // This method will register device to GCM server

    /*
    public void storeGCM() {


        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);

        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                Config.DISPLAY_REGISTRATION_MESSAGE_ACTION));

        final String regId = GCMRegistrar.getRegistrationId(this);

        if (regId.equals("")) {
            GCMRegistrar.register(this, Config.GOOGLE_SENDER_ID);
        } else {
            // Device is already registered on GCM Server
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                gcmregid = regId;
                ClassRegister.register(getApplicationContext(), regId,
                        "Android");
            }
        }
    }
    */
/*
    public void storeFCM() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                Config.DISPLAY_REGISTRATION_MESSAGE_ACTION));

    }
    */
    public void dialog_appUpdate() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Godrej Partner Connect");
        builder.setIcon(R.drawable.icon2);
        builder.setMessage("An Upgraded version is now available on Playstore");
        builder.setCancelable(true);
        builder.setPositiveButton("Update",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // showToast("Package: "+getPackageName());
                        try {/*
                         * Jsoup .connect(
                         * "https://play.google.com/store/apps/details?id="
                         * + getPackageName() + "&hl=en") .userAgent(
                         * "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6"
                         * ) .timeout(10000) .execute();
                         */
                            dialog.cancel();
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            Uri data = Uri
                                    .parse("https://play.google.com/store/apps/details?id="
                                            + getPackageName() + "&hl=en");
                            intent.setData(data);
                            startActivity(intent);
                            //dialog.dismiss();


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });

        builder.show();

    }

    public void dialogDownloadFile() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Godrej Partner Connect");
        builder.setIcon(R.drawable.icon2);
        builder.setMessage("File Download Complete");
        builder.setCancelable(true);
        builder.setPositiveButton("View Downloaded File",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // showToast("Package: "+getPackageName());
                        try {

                            startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });

        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

        builder.show();

    }

    private boolean web_update() {
        try {
            String package_name = getPackageName();

            String curVersion = getApplicationContext().getPackageManager()
                    .getPackageInfo(package_name, 0).versionName;

            Log.d("Current Version", ":=" + curVersion);

            //String newVersion = curVersion;
            String newVersion = "";

            String url_print = "https://play.google.com/store/apps/details?id="
                    + package_name + "&hl=en";

            Log.d("URL-Print", ":=" + url_print);

            newVersion = Jsoup
                    .connect(
                            "https://play.google.com/store/apps/details?id="
                                    + package_name + "&hl=en")

                    .timeout(30000)
                    .userAgent(
                            "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com").get()
                    .select("div[itemprop=softwareVersion]").first().ownText();

            Log.d("NewVersion", ":=" + newVersion);

            return (value(curVersion) < value(newVersion)) ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private long value(String string) {
        string = string.trim();
        if (string.contains(".")) {
            final int index = string.lastIndexOf(".");
            return value(string.substring(0, index)) * 100 + value(string.substring(index + 1));
        } else {
            return Long.valueOf(string);
        }
    }

    @Override
    public void onGetResponse(boolean isUpdateAvailable) {

        //Log.d("Update Available","::"+isUpdateAvailable);

        Log.e("ResultAPPMAIN", String.valueOf(isUpdateAvailable));
        if (isUpdateAvailable) {
            showUpdateDialog();
        }

    }

    /**
     * Method to show update dialog
     */
    public void showUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChannelPartnerActivity.this);

        alertDialogBuilder.setTitle(ChannelPartnerActivity.this.getString(R.string.app_name));
        alertDialogBuilder.setMessage(ChannelPartnerActivity.this.getString(R.string.update_message));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.update_now, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChannelPartnerActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isForceUpdate) {
                    finish();
                }
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();
    }

    @JavascriptInterface
    public void DownloadInvoice(String fileUrl)
    {
        try
        {
            //String fileUrl = "http://cp.godrejproperties.com/data/empanelment/RERA_20201117155645.pdf";
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fileUrl));
            request.setDescription("Downloading");
            request.setTitle("Invoices");
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Invoices.zip");
            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
        }
        catch (Exception ex)
        {
            Log.d("Download Invoice", "");
        }
    }

}