package com.godrejproperties.customerapps;

public interface WSCallerVersionListener {
    public void onGetResponse(boolean isUpdateAvailable);
}