package com.godrejproperties.customerapps;

public interface Config {

	// Google project id
	static final String GOOGLE_SENDER_ID = "74240988336";

	// Broadcast reciever name to show gcm registration messages on screen
	static final String DISPLAY_REGISTRATION_MESSAGE_ACTION = "org.godrej.channelpartner.DISPLAY_REGISTRATION_MESSAGE";

	// Broadcast reciever name to show user messages on screen
	static final String DISPLAY_MESSAGE_ACTION = "org.godrej.channelpartner.DISPLAY_MESSAGE";

	// Parse server message with this name
	static final String EXTRA_MESSAGE = "message";


	//From balram
	public static final String TOPIC_GLOBAL = "global";

	// broadcast receiver intent filters
	public static final String REGISTRATION_COMPLETE = "registrationComplete";
	public static final String PUSH_NOTIFICATION = "pushNotification";

	// id to handle the notification in the notification tray
	public static final int NOTIFICATION_ID = 100;
	public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

	public static final String SHARED_PREF = "ah_firebase";

}
