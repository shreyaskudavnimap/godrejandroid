package com.godrejproperties.customerapps.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EmplContDeviceLogDto implements Serializable {
    @SerializedName("EmplContDeviceLogID")
    @Expose
    String emplContDeviceLogID;

    @SerializedName("EmplContID")
    @Expose
    String emplContID;

    @SerializedName("DeviceToken")
    @Expose
    String deviceToken;

    @SerializedName("MobilePlatform")
    @Expose
    String mobilePlatform;

    @SerializedName("Active")
    @Expose
    String active;

    public String getEmplContDeviceLogID(){
        return emplContDeviceLogID;
    }

    public String getEmplContID(){
        return emplContID;
    }

    public String getDeviceToken(){
        return deviceToken;
    }

    public String getMobilePlatform(){
        return mobilePlatform;
    }

    public String getActive(){
        return active;
    }

    public void setEmplContDeviceLogID(String emplContDeviceLogID){
        this.emplContDeviceLogID = emplContDeviceLogID;
    }

    public void setEmplContID(String emplContDeviceLogID){
        this.emplContID = emplContDeviceLogID;
    }

    public void setDeviceToken(String deviceToken){
        this.deviceToken = deviceToken;
    }

    public void setMobilePlatform(String mobilePlatform){
        this.mobilePlatform = mobilePlatform;
    }

    public void setActive(String active){
        this.active = active;
    }
}
