package com.godrejproperties.customerapps.model;

public class MsgDataModel {
    private String ChannelType;
    private String NotificationLogID;
    private String id;
    private String title;
    private String body;

    public String getChannelType() {
        return ChannelType;
    }

    public void setChannelType(String channelType) {
        ChannelType = channelType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotificationLogID() {
        return NotificationLogID;
    }

    public void setNotificationLogID(String notificationLogID) {
        NotificationLogID = notificationLogID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

}
