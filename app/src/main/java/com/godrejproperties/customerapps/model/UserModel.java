package com.godrejproperties.customerapps.model;

public class UserModel {
    long user_id;
    String supervisorEdp, name, email, edpNo, jobTitle, department, category, location, msg;
    int teamCount = 0;
    boolean isValid = false, isInDB = false;

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getSupervisorEdp() {
        return supervisorEdp;
    }

    public void setSupervisorEdp(String supervisorEdp) {
        this.supervisorEdp = supervisorEdp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEdpNo() {
        return edpNo;
    }

    public void setEdpNo(String edpNo) {
        this.edpNo = edpNo;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getTeamCount() {
        return teamCount;
    }

    public void setTeamCount(int teamCount) {
        this.teamCount = teamCount;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }

    public boolean isInDB() {
        return isInDB;
    }

    public void setIsInDB(boolean isInDB) {
        this.isInDB = isInDB;
    }

}
