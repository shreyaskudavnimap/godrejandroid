package com.godrejproperties.customerapps.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIOutput {

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("statusCode")
    @Expose
    int statusCode;

    @SerializedName("msg")
    @Expose
    String msg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
