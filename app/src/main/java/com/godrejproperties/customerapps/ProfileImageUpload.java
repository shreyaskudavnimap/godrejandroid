package com.godrejproperties.customerapps;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.util.Log;

public class ProfileImageUpload implements Runnable {
	URL connectURL;
	String responseString;
	String FileName;
	String type;
	String Latitude;
	String Longnitude;
	String SessionIDCount;
	byte[] dataToServer;
	FileInputStream fileInputStream = null;
	String s;
	Context context;
	String p1, fname;

	public ProfileImageUpload(String urlString, String Type, String Filename,
			String p1, Context context, String fname) {

		try {
			connectURL = new URL(urlString);
			this.FileName = Filename;
			type = Type;
			this.p1 = p1;
			this.context = context;
			this.fname = fname;

		} catch (Exception ex) {
			Log.i("HttpFileUpload", "URL Malformatted");
		}
	}

	public String sendNow(FileInputStream fStream) {

		fileInputStream = fStream;
		String iFileName = FileName;

		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		String Tag = "fSnd";
		try {
			Log.v(Tag, "Starting Http File Sending to URL" + FileName);

			HttpURLConnection conn = (HttpURLConnection) connectURL
					.openConnection();

			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);

			DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"filename\""
					+ lineEnd);
			dos.writeBytes(lineEnd);
			dos.writeBytes(FileName);
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + lineEnd);

			dos.writeBytes("Content-Disposition: form-data; name=\"type\""
					+ lineEnd);
			dos.writeBytes(lineEnd);
			dos.writeBytes(type);
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + lineEnd);

			Log.e("Value of p1 PImgUpad=", p1);
			dos.writeBytes("Content-Disposition: form-data; name=\"brokerid\""
					+ lineEnd);
			dos.writeBytes(lineEnd);
			dos.writeBytes(p1);
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + lineEnd);

			dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";FileName=\""
					+ iFileName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			Log.e(Tag, "Headers are written");

			// create a buffer of maximum size
			int bytesAvailable = fileInputStream.available();

			int maxBufferSize = 1024;
			int bufferSize = Math.min(bytesAvailable, maxBufferSize);
			byte[] buffer = new byte[bufferSize];

			// read file and write it into form...
			int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			fileInputStream.close();

			dos.flush();

			Log.e(Tag,
					"File Sent, Response: "
							+ String.valueOf(conn.getResponseCode()));

			InputStream is = conn.getInputStream();

			int ch;

			StringBuffer b = new StringBuffer();
			while ((ch = is.read()) != -1) {
				b.append((char) ch);
			}
			s = b.toString();
			
			Log.i("Response", s);
			dos.close();
		} catch (MalformedURLException ex) {
			Log.e(Tag, "URL error: " + ex.getMessage(), ex);
		}

		catch (IOException ioe) {
			Log.e(Tag, "IO error: " + ioe.getMessage(), ioe);
		}

		// System.out.println("Response111 " + s);
		return s;
	}

	public void run() {

	}

}
