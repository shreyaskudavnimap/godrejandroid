package com.godrejproperties.customerapps;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class NotificationDetailsActivity extends AppCompatActivity {


    TextView textViewNotificationDetails;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);
        Log.i(getString(R.string.tag_id),NotificationDetailsActivity.class.getName() + "=Act");

        textViewNotificationDetails =  (TextView)findViewById(R.id.textViewNotificationDetails);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        textViewNotificationDetails.setText(intent.getStringExtra("count"));
    }
}