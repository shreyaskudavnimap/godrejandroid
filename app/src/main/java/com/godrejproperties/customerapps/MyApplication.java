package com.godrejproperties.customerapps;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.godrejproperties.customerapps.R;
import com.godrejproperties.customerapps.service.AppPreference;
import com.godrejproperties.customerapps.service.DeviceRegistrar;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class MyApplication extends Application {
    private static GoogleAnalytics sAnalytics;  // shreyas
    private static Tracker sTracker;

    private static final String TAG = com.godrejproperties.customerapps.MyApplication.class.getSimpleName();

    MyAppsNotificationManager myAppsNotificationManager;
    AppPreference obj = new AppPreference();

    @Override
    public void onCreate() {
        super.onCreate();
        sAnalytics = GoogleAnalytics.getInstance(this);     // shreyas
        myAppsNotificationManager = MyAppsNotificationManager.getInstance(this);
        myAppsNotificationManager.registerNotificationChannelChannel(
                getString(R.string.FBCOfferID),
                getString(R.string.FBCOfferName),
                getString(R.string.FBCOfferDescription));
        myAppsNotificationManager.registerNotificationChannelChannel(
                getString(R.string.FBCEventID),
                getString(R.string.FBCEventName),
                getString(R.string.FBCEventDescription));
        myAppsNotificationManager.registerNotificationChannelChannel(
                getString(R.string.FBCProjectID),
                getString(R.string.FBCProjectName),
                getString(R.string.FBCProjectDescription));
        myAppsNotificationManager.registerNotificationChannelChannel(
                getString(R.string.FBCNoticationID),
                getString(R.string.FBCNoticationName),
                getString(R.string.FBCNoticationDescription));

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(!task.isSuccessful()) {
                    Log.i(getString(R.string.tag_id), "Task Failed");
                    obj.saveIsTokenValid(getApplicationContext(), "N" );
                    return;
                }
                Log.i(getString(R.string.tag_id), "The result: "+task.getResult().getToken());
                obj.saveIsTokenValid(getApplicationContext(), "Y" );
                obj.saveDeviceToken(getApplicationContext(), task.getResult().getToken());
                new DeviceRegistrar().Register(getApplicationContext());
            }
        });

    }


    public void triggerNotification(Class targetNotificationActivity, String channelId, String title, String text, String bigText, int priority, boolean autoCancel, int notificationId, int pendingIntentFlag){
        myAppsNotificationManager.triggerNotification(targetNotificationActivity,channelId,title,text, bigText, priority, autoCancel,notificationId, pendingIntentFlag);
    }

    public void triggerNotification(Class targetNotificationActivity, String channelId, String title, String text, String bigText, int priority, boolean autoCancel, int notificationId){
        myAppsNotificationManager.triggerNotification(targetNotificationActivity,channelId,title,text, bigText, priority, autoCancel,notificationId);
    }

    public void triggerNotificationWithBackStack(Class targetNotificationActivity, String channelId, String title, String text, String bigText, int priority, boolean autoCancel, int notificationId, int pendingIntentFlag){
        myAppsNotificationManager.triggerNotificationWithBackStack(targetNotificationActivity,channelId,title,text, bigText, priority, autoCancel,notificationId, pendingIntentFlag);
    }

    public void triggerNotificationWithBackStack(Class targetNotificationActivity, String channelId, String title, String text, String bigText, int priority, boolean autoCancel, int notificationId, int pendingIntentFlag, Intent intent){
        myAppsNotificationManager.triggerNotificationWithBackStack(targetNotificationActivity,channelId,title,text, bigText, priority, autoCancel,notificationId, pendingIntentFlag, intent);
    }

    public void updateNotification(Class targetNotificationActivity,String title,String text, String channelId, int notificationId, String bigpictureString, int pendingIntentflag){
        myAppsNotificationManager.updateWithPicture(targetNotificationActivity, title, text, channelId, notificationId, bigpictureString, pendingIntentflag);
    }

    public void cancelNotification(int notificaitonId){
        myAppsNotificationManager.cancelNotification(notificaitonId);
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }


}
