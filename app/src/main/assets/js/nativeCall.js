function uploadToServer(strText){
    window.target = '_blank';
    window.ChannelPartnerActivity.uploadDocsToServer(strText);
}


function RegisterUser(emplContID){
    window.ChannelPartnerActivity.registerUserOnServer(emplContID);
}

function DeRegisterUser(){
    console.log("DeRegisterUser");
    window.ChannelPartnerActivity.deRegisterUserOnServer();
}

function DeviceAccessed(emplContID){
    window.ChannelPartnerActivity.deviceAccessLogOnServer(emplContID);
}

function getServerURL(){
    return window.ChannelPartnerActivity.getURL();
}

function getDeviceLogID(){
    return window.ChannelPartnerActivity.getDeviceLogID();
}