/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.godrejproperties.customerapps;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.godrejproperties.customerapps";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 25;
  public static final String VERSION_NAME = "3.8";
}
