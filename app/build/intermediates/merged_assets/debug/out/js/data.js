﻿//var baseURL = "http://gplpartnerconnect.com/GodrejPartnerConnect/";
//Live


var baseURL = getServerURL();
var baseURLEOI = baseURL + "gplservice/";
var baseAPI = baseURL + "webapi/";
var liveURL = baseURL;

var updateURL = "https://godrej.my.salesforce.com/services/apexrest/RestBrokerUpdate";

var mstrPlanCount = null, flrPlanCount = null;

function ShowExploreList()
{
    var result = JSON.parse('{"status":"200","msg":"Successful","data":[{"proj_id":"51645","field_foyr_url":"https:\/\/customercare.godrejproperties.com\/gpl-api\/virtual_grid_mobile\/a1l6F000004QtUZQA0","field_is_available_for_booking":"1","proj_name":"Godrej Aqua","eoi_enabled":"0","proj_img":"https:\/\/d1jys7grhimvze.cloudfront.net\/backoffice\/data_content\/projects\/godrej_aqua_bangalore\/banners\/Godrej-Aqua-render-2.jpg","proj_logo_img":"https:\/\/d1jys7grhimvze.cloudfront.net\/backoffice\/data_content\/projects\/godrej_aqua_bangalore\/Aqua-Logo-450X71.png","typologies":"3 BHK,2 BHK","cities":"Hosahalli, Bangalore","possession":"Under Construction","url":"https:\/\/customercare.godrejproperties.comproperty-details\/godrej-aqua-bangalore\/?proj_id=51645","wishlist_status":"0","callus_india":"1800-258-2588","min_price":"5700000.00","callus_other_country":"+91-11-6657-5604","last_viewd":"2020-05-27 12:53:28","field_app_thumbnail_image":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejAqua_Blr_HomePage.jpg","field_property_display_order":"11"} , {"proj_id":"51645","field_foyr_url":"https:\/\/customercare.godrejproperties.com\/gpl-api\/virtual_grid_mobile\/a1l6F000004QtUZQA0","field_is_available_for_booking":"1","proj_name":"Godrej Aqua","eoi_enabled":"0","proj_img":"https:\/\/d1jys7grhimvze.cloudfront.net\/backoffice\/data_content\/projects\/godrej_aqua_bangalore\/banners\/Godrej-Aqua-render-2.jpg","proj_logo_img":"https:\/\/d1jys7grhimvze.cloudfront.net\/backoffice\/data_content\/projects\/godrej_aqua_bangalore\/Aqua-Logo-450X71.png","typologies":"3 BHK,2 BHK","cities":"Hosahalli, Bangalore","possession":"Under Construction","url":"https:\/\/customercare.godrejproperties.comproperty-details\/godrej-aqua-bangalore\/?proj_id=51645","wishlist_status":"0","callus_india":"1800-258-2588","min_price":"5700000.00","callus_other_country":"+91-11-6657-5604","last_viewd":"2020-05-27 12:53:28","field_app_thumbnail_image":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejAqua_Blr_HomePage.jpg","field_property_display_order":"11"}]}');
    var ul = $("#explore_div");
    //console.log(result);
    $.each(result.data, function (index, value) {
        ul.append('<div class="child"> <a href="test_details.html?proj_id=51645"> <div class="img-tile" style=\"background: url('+ value["proj_img"] +') no-repeat center center; background-size: auto 190px; border-radius: 5px;\"> </div> </a> <span class="child-title">'+ value["proj_name"] +", "+ value["cities"] +'</span> <br> <span class="child-price">Rs. ' + value["min_price"] + ' Lakh onwards</span> </div>');
    });
}

function ShowJustLaunch()
{
    var result = JSON.parse('{"status":"200","msg":"Successa","data":[{"proj_id":"765892","title":"Godrej Boulevard","field_city":"Pune","field_sub_location":"Manjri","field_property_details_typology":"1, 2 \u0026 3 BHK","field_app_thumbnail_image":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejBoulevard%2CPune_ImageGallery04.jpg","field_new_launched_image_url":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejBoulevard%2CPune_JustLaunched.jpg","field_price_onward":"5600000","starting_price":"5400000.00","wishlist_status":"0"}]}');
    var ul = $("#justlaunch_div");
    //console.log(result);
    $.each(result.data, function (index, value) {
        ul.append('<div class="child"> <div class="landscape-tile" style=\"background: url('+ value["field_app_thumbnail_image"] +') no-repeat center center; background-size: 100% auto; border-radius: 10px;\"> </div>  <span class="child-title">'+ value["title"] +", "+ value["field_city"] +'</span> <br> <span class="child-price">Rs. ' + value["field_price_onward"] + ' Lakh onwards</p> </div>');
    });
}

function ShowGreenLiving()
{
    var result = JSON.parse('{"status":"200","msg":"Successa","data":[{"proj_id":"404493","title":"Godrej Golf Links","field_city":"NCR","field_sub_location":"Noida","field_property_details_typology":"1,2,3 \u0026 4 BHK","field_app_thumbnail_image":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejGolf_NCR_homepage.jpg","field_new_launched_image_url":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejGolfLinks%2CGreaterNoida_JustLaunched.jpg","field_price_onward":"11500000","starting_price":"11500000.00","wishlist_status":"0"}, {"proj_id":"404493","title":"Godrej Golf Links","field_city":"NCR","field_sub_location":"Noida","field_property_details_typology":"1,2,3 \u0026 4 BHK","field_app_thumbnail_image":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejGolf_NCR_homepage.jpg","field_new_launched_image_url":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejGolfLinks%2CGreaterNoida_JustLaunched.jpg","field_price_onward":"11500000","starting_price":"11500000.00","wishlist_status":"0"}]}');
    var ul = $("#greenliving_div");
    //console.log(result);
    $.each(result.data, function (index, value) {
        ul.append('<div class="child"> <div class="img-tile" style=\"background: url('+ value["field_app_thumbnail_image"] +') no-repeat center center; background-size: auto 190px; border-radius: 5px;\"> </div> <span class="child-title">'+ value["title"] +", "+ value["field_city"] +'</span> <br> <span class="child-price">Rs. ' + value["field_price_onward"] + ' Lakh onwards</span> </div>');
    });
}

function ShowAwardWinning()
{
    var result = JSON.parse('{"status":"200","msg":"Successa","data":[{"proj_id":"404493","title":"Godrej Golf Links","field_city":"NCR","field_sub_location":"Noida","field_property_details_typology":"1,2,3 \u0026 4 BHK","field_app_thumbnail_image":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejGolf_NCR_homepage.jpg","field_new_launched_image_url":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/GodrejGolfLinks%2CGreaterNoida_JustLaunched.jpg","field_price_onward":"11500000","starting_price":"11500000.00","wishlist_status":"0"}]}');
    var ul = $("#awardwinning_div");
    //console.log(result);
    $.each(result.data, function (index, value) {
        ul.append('<div class="child"> <div class="img-tile" style=\"background: url('+ value["field_app_thumbnail_image"] +') no-repeat center center; background-size: auto 190px; border-radius: 5px;\"> </div> <span class="child-title">'+ value["title"] +", "+ value["field_city"] +'</span> <br> <span class="child-price">Rs. ' + value["field_price_onward"] + ' Lakh onwards</p> </div>');
    });
}

function ShowPropertiesDetails()
{
    var result = JSON.parse('{"status":"200","msg":"Successa","data":[{"proj_id":"51645","title":"Godrej Aqua","field_city":"Bangalore","field_sub_location":"Hosahalli","field_project_details_offer_text":"3 Lakh off till 2nd April","proj_vid":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-03\/GodrejAquaBangaloreFullProjectWalkthroughAppPage_05.mp4","field_project_details_video_thum":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-04\/Aqua.mp4","field_project_details_descriptio":"At Godrej Aqua, in the heart of North Bangalore, ......","field_project_details_excerpt":"At Godrej Aqua, in the heart of North Bangalore, you\u2019ll come across state-of-the-art water ","starting_price":"5700000.00","wishlist_status":"0","possesion_dt":"2024","field_property_details_typology":"2, 3 BHK","eoi_enabled":"0","field_is_available_for_booking":"1","lat":"13.146188","lng":"77.637624","callus_india":"1800-258-2588","callus_other_country":"+91-11-6657-5604","field_foyr_url":"https:\/\/customercare.godrejproperties.com\/gpl-api\/virtual_grid_mobile\/a1l6F000004QtUZQA0","show_floor_plan":"0","show_unit_plan":"1"}]}');
    //console.log(result.data[0]["field_project_details_excerpt"]);
    $("#test_description").append(result.data[0]["field_project_details_excerpt"]);

    var imagelist = JSON.parse('{"0":{"title":"Gallery Image for Godrej Aqua, Bengaluru_51645","details":[{"field_caption":"Welcome to thoughtfully designed water-secure residences at Godrej Aqua","field_gallery_image_url":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-03\/GodrejAqua%2CBlr_ImageGallery_1.jpg","field_gallery_thumb_image_url":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-03\/GodrejAqua%2CBlr_ImageGallery_1.jpg","field_gallery_display_order":"1"},{"field_caption":"Test Video","field_gallery_image_url":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-03\/GodrejAquaBangaloreFullProjectWalkthroughAppPage_05.mp4","field_gallery_thumb_image_url":"https:\/\/customercare.godrejproperties.com\/sites\/default\/files\/2020-03\/GodrejAquaBangaloreFullProjectWalkthroughAppPage_05.mp4","field_gallery_display_order":"11"}]},"status":"200","msg":"Successful"}');
    console.log(imagelist[0].details);
    $.each(imagelist[0].details, function (index, value) {
        console.log(value["field_gallery_thumb_image_url"]);
    });
}

function DefaultAjaxError(xhr, ajaxOptions, thrownError){
    $('.ui-loader').hide();
    console.log(xhr);
    console.log(ajaxOptions);
    console.log(thrownError);
    alert("Error: "+ xhr.status + " " + thrownError + "\nPlease try again later ");
}

//Deve
/*
var baseURL = "http://10.2.90.47:82/";
var baseURLEOI = "http://10.2.90.47:82/";
var liveURL = "http://10.2.90.47:82/"
*/

// dev URL - http://103.239.139.89/godrejadmin/
// new dev url - http://103.239.139.89/godrejadmin/gplservice/
var sfdc_baseURL = "http://onlinebooking.netbizlabs.com/gpl_sfdc/";

function getdevice(deviceid, platform) {
    //alert(deviceid);
    localStorage.setItem("deviceid", deviceid);
    localStorage.setItem("platform", platform);
}

//for live support
function openlink(link) {
    window.target = '_blank';
    window.ChannelPartnerActivity.openURLinDialog(link);
}

function toggleGst()
{
    $('#toggleGst').toggle();
}
// for set image
function setImage(imageUrl) {
    //alert(imageUrl);
    $("#img_pic").attr("src", '');
    //alert(imageUrl);
    $("#img_pic").attr("src", imageUrl);
}

//make call on live support page
function makeCall(rel) {
    window.target = '_blank';
    window.ChannelPartnerActivity.makePhoneCall(rel);
}

//change profile pic
function ChangePic() {
    //alert(localStorage.getItem("brokerid"));
    $('#a_remove').show();

    window.target = '_blank';
    window.ChannelPartnerActivity.ProfilePic(localStorage.getItem("brokerid"));
}
//To change user image in myaccount section
function changeimage() {
    window.ChannelPartnerActivity.selectImage();
}

function no_net() {

    $('.net_no').show();
};

$(document).on('click', '.net_no h1 a', function () {
    $('.net_no,.ui-loader-default').hide();
});


function SetReminder(date, title, decription, place, eventid) {
    $('ui-loader').show();
    if ($("#chk_attend").prop("checked") == false) {
        showMsg('Please check yes, i am attending');
        return false;
    }
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/UpdateReminder/' + localStorage.getItem("brokerid") + "/" + eventid,
        success: function (response) {
            $.each(response.InsertReminderResult, function (index, value) {
                if (value.status == "success") {
                    $('ui-loader').hide();
                    $('.setcheckbox' + eventid).attr('checked', 'checked');
                    $('.setcheckbox' + eventid).attr('disabled', 'disabled');
                    $('#setanchore' + eventid).removeClass('a_reminder');
                }
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });

    window.target = '_blank';
    window.ChannelPartnerActivity.Reminder(date, title, decription, place);
}
//for android
function ytonline(ytnmber) {
    openVideo(ytnmber);
}

function openVideo(link) {
    window.target = '_blank';
    window.ChannelPartnerActivity.openYoutubeVideo(link);
}
//for inbox
function getTextDoc(projectid) {
    var div = $("#text");
    var divse = '';
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc/DisplayTextDoc/" + projectid,
        success: function (response) {
            $.each(response.DisplayTextDocResult, function (index, value) {
                //alert(value.status);
                if (value.status == "success") {
                    if (value.type == "Text") {
                        if ($(document).width() >= 767) {
                            var big_text = 75;
                        }
                        else {
                            var big_text = 30;
                        }
                        divse += '<h1><img class="img" src="images/inbox_gray2.png"><span class="title1_1"><span id"desc1">' + value.title + '</span></span><span class="date1"><strong>Date:</strong> ' + value.date + '</span><br><span class="desc1"><strong>Description: </strong><span id"desc1">' + value.text.substring(0, big_text) + '...</span><a href="my-inbox-inner.html?id=' + value.id + '">Read More</a></span><br></h1>';
                    }
                    else {
                        //divse += '<h1><img class="img doc_img" src="images/Document_Gray.png"><a  customlink="' + value.image + '" class="view1 ui-link">View document</a></h1>';
                        divse += '<h1><img class="img doc_img" src="images/Document_Gray.png"><span class="title1_1"><span id"desc1">' + value.title + '</span></span><span class="date1"><strong>Date:</strong> ' + value.date + '</span><br><a rel="' + value.image + '" class="view1 ui-link" href="javascript:void(0);">View document</a></h1>';
                    }
                }
                else {
                    divse += '<span style="text-align: center; text-transform: none; display: block; color: #333333;">No Text Documents</span>';
                }
            });
            //alert(divse);
            div.append(divse);

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

$.mobile.document.on("click", "#text .view1", function (evt) {
    var onee = $(this).attr('rel');
    openlink(onee);
   // window.ChannelPartnerActivity.openURLImageinDialog(onee);
    evt.preventDefault();
});

$.mobile.document.on("click", "#cpkitdoc .view1", function (evt) {
    var onee = $(this).attr('rel');
    openlink(onee);
   // window.ChannelPartnerActivity.openURLImageinDialog(onee);
    evt.preventDefault();
});

$.mobile.document.on("click", "#campaigndoc .view1", function (evt) {
    var onee = $(this).attr('rel');
    openlink(onee);
    //window.ChannelPartnerActivity.openURLImageinDialog(onee);
    evt.preventDefault();
});

$.mobile.document.on("click", "#div_policy .view1", function (evt) {
    var onee = $(this).attr('rel');
    if(onee.substr(onee.length - 4) == ".pdf")
    {
        openlink(onee);
    }
  // window.ChannelPartnerActivity.openURLImageinDialog(onee);
    evt.preventDefault();
});


$.mobile.document.on("click", ".view_plan_link", function (evt) {
    //alert($(this).attr("data-med"));
    var onee = $(this).attr("data-med");
    //"http://gplpartnerconnect.com/GodrejPartnerConnect/images/Gallery/" + value.Image;
    //openlink(onee);
     window.ChannelPartnerActivity.openURLImageinDialog(onee);


    evt.preventDefault();
});

function GetDocInner(id) {
    var div = $("#text");
    var divse = '';
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc/DisplayDocById/" + id,
        success: function (response) {
            $.each(response.GetDocByIdResult, function (index, value) {
                if (value.status == "success") {
                    divse += '<h1><img class="img" src="images/inbox_gray2.png"><span class="title1_1"><span id="title">' + value.title + '</span></span><span class="date1"><strong>Date:</strong> 20/20/2012</span><br><span class="desc1"><strong>Description: </strong><span id="desc">' + value.text + '</span></span><br><h1>';
                }
            });
            div.append(divse);
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

function sanitizeInput(value)
{
    var lt = /</g,
        gt = />/g,
        ap = /'/g,
        ic = /"/g;
    return value.toString().replace(lt, "&lt;").replace(gt, "&gt;").replace(ap, "&#39;").replace(ic, "&#34;");
}


//To login
function Login() {
    var username = sanitizeInput($('#username').val());
    var password = sanitizeInput($('#password').val());
    var validEmail = new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    if (!validEmail.test(username)) {
        showMsg('Invalid email format.');
        return false;
    }

    var validPass = new RegExp(/^[a-zA-Z0-9~@#$^*()_+=[\]{}|\\,.?: -]*$/);
    if (!validPass.test(password)) {
        showMsg('Password contains invalid characters.');
        return false;
    }

    if (username == null || username == "" || username.lenght > 50) {
        showMsg('Please enter username.');
        return false;
    }
    if (password == null || password == "" || password.lenght > 50) {
        showMsg('Please enter password.');
        return false;
    }

    //var addstring = username + "/" + password;
    var logindata = {username:username, password:password };
    //data:{Login : btoa(unescape(encodeURIComponent(JSON.stringify(logindata)))) },
    $('.ui-loader').show();
    $.ajax({
        //ServiceCallID: 1,
        type: "POST",
        //url: baseURLEOI + 'RestServiceImpl.svc/CheckLogin/' + addstring,
        url: baseAPI + "GetEmplLoginDetails.ashx",
        //contentType: "application/json; charset=utf-8",
        //data:{Login : JSON.stringify(logindata) },
        data:{Login : btoa(unescape(encodeURIComponent(JSON.stringify(logindata)))) },
        dataType: "json",
        success: function (response) {
            $('.ui-loader').hide();
            console.log(response);
            if (response.statusCode == 200) {
                var responseData = response.data;
                RegisterUser(responseData.EmplContID);
                localStorage.setItem("panno", responseData.PanNo);
                localStorage.setItem("mobileno", responseData.Mobile);
                localStorage.setItem("email", responseData.Email);
                localStorage.setItem("isOTPGenerated", responseData.isSentOTP);
                localStorage.setItem("showOTPDialog", 1);
                $('a.logo').attr('href', 'new_launch.html');
                localStorage.setItem("emplcontid", responseData.EmplContID);
                console.log("USERID",responseData.EmplContID);
                localStorage.setItem("brokerid", responseData.BrokerID);
                localStorage.setItem("brokeraccountid", responseData.BrokerAccountID);
                localStorage.setItem("emplcontdevicelogid", responseData.EmplContDeviceLogID);
                localStorage.setItem("cp_name", responseData.Name);
                $('.ui-loader').hide();
                console.log("OTP Status at Login==>",responseData.isSentOTP);
                $.mobile.pageContainer.pagecontainer("change", "new_launch.html", { transition: "slide" });
                $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: baseAPI + "BrokerDataSync.ashx?userid="+ responseData.EmplContID,
                        async: true,
                        success:  function (response) {}
                });
                ChannelPartnerActivity.SendGA("Launch Screen - " + username); // shreyas
            }
            else {
                $('#password').val('');
                showMsg('Username/Password mismatch.');
                return false;
            }

            /*
            $.each(response.GetLoginDetailsResult, function (index, value) {
                if (value.status == "success") {
                    var device = value.userid + "/" + localStorage.getItem("deviceid") + "/" + localStorage.getItem("platform");
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: baseURLEOI + "RestServiceImpl.svc" + "/UpdateDevice/" + device,
                        async: false,
                        success: function (response) {
                            $.each(response.GetDeviceResult, function (index, value) {
                                if (value.status == "success") {
                                }
                            });

                        },
                        failure: function (response) {
                            alert('Fail');
                        }
                    });

                    $.ajax({
                     type: "GET",
                     contentType: "application/json; charset=utf-8",
                     url: baseURLEOI + "EOIService.svc" + "/login/" + addstring,
                     async: false,
                     success: function (response) {
                      if (response.statusCode == 200) {
                      var responseData = response.data
                                         localStorage.setItem("panno", responseData.panno);
                                         localStorage.setItem("mobileno", responseData.mobileno);
                                         localStorage.setItem("email", responseData.email);
                                         localStorage.setItem("isOTPGenerated", responseData.isSentOTP);
                                         localStorage.setItem("showOTPDialog", 1);
                                         console.log("OTP Status at Login==>",responseData.isSentOTP);
                     }
                     },
                     failure: function (response) {
                     }
                  });

                    $.mobile.pageContainer.pagecontainer("change", "new_launch.html", { transition: "slide" });
                    $('a.logo').attr('href', 'new_launch.html');
                    localStorage.setItem("userid", value.userid);
                    console.log("USERID",value.userid);
                    localStorage.setItem("brokerid", value.brokerid);
                    localStorage.setItem("brokeraccountid", value.brokeraccountid);
                    localStorage.setItem("cp_name", value.name);
                    $('.ui-loader').hide();
                }
                else {
                    $('#password').val('');
                    showMsg('Username/Password mismatch.');
                    return false;
                }
            });
            */
        },
        error: DefaultAjaxError
    });
}

function checksession() {
    if (localStorage.getItem("emplcontid") != null) {

    }
    else {
        $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "slide" });
    }

}
function checksession_index() {
    if (localStorage.getItem("emplcontid") != null) {
        $.mobile.pageContainer.pagecontainer("change", "new_launch.html", { transition: "slide" });
        $('a.logo').attr('href', 'new_launch.html');
    }
    else {
        $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "slide" });
        $('a.logo').attr('href', 'javascript:void(0);');
    }

}

//logout
function logout() {

    var logoutData = {EmplContID:localStorage.getItem("emplcontid"), EmplContDeviceLogID:localStorage.getItem("emplcontdevicelogid")};
    DeRegisterUser();
    sessionStorage.clear();
    localStorage.clear();

    if (localStorage.getItem("emplcontid") == null) {
        $.mobile.pageContainer.pagecontainer("change", "index.html", { transition: "slide" });
        $('a.logo').attr('href', 'javascript:void(0);');
        setTimeout(function () {
            //$("#index .text").center();
            setTimeout(function () {
                $('.ui-loader').hide();
            }, 1000);
        }, 1000);
    }



}

// To Get event calender of user
function loadEventCalender(userid) {
    //var div='<ul>';
    var div = '';
    var temp1 = '';
    var temp2 = 0;

    var events = [];
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/GetEventsReminder/" + userid,
        async: false,
        success: function (response) {
            $.each(response.FetchEventsReminderResult, function (index, value) {
                if (value.status == "success") {
                    item = {};
                    item["title"] = value.title;
                    item["start"] = value.date;
                    item["type"] = value.type;
                    events.push(item);
                }
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
    console.log(events);
    //console.log(events);
    $('#calendar').fullCalendar({
        // defaultDate: '2015-02-12',
        //editable: true,
        //eventLimit: true, // allow "more" link when too many events
        events: events,

        eventRender: function (event, element, bool) {
            var dataToFind = moment(event.start).format('YYYY-MM-DD');

            var div = '<ul>';

            if ($("td[data-date='" + dataToFind + "']").hasClass("selectdate fc-title event")) {
                if (event.type == "Event") {
                    $("td[data-date='" + dataToFind + "']").removeClass('event');
                    $("td[data-date='" + dataToFind + "']").addClass('event');
                }
                if (event.type == "Appointment") {
                    $("td[data-date='" + dataToFind + "']").removeClass('event');
                    $("td[data-date='" + dataToFind + "']").removeClass('appt');
                    $("td[data-date='" + dataToFind + "']").addClass('both');
                }
            }
            else if ($("td[data-date='" + dataToFind + "']").hasClass("selectdate fc-title appt")) {
                if (event.type == "Appointment") {
                    $("td[data-date='" + dataToFind + "']").removeClass('appt');
                    $("td[data-date='" + dataToFind + "']").addClass('appt');
                }
                if (event.type == "Event") {
                    $("td[data-date='" + dataToFind + "']").removeClass('event');
                    $("td[data-date='" + dataToFind + "']").removeClass('appt');
                    $("td[data-date='" + dataToFind + "']").addClass('both');
                }
            }
            else {
                if (event.type == "Event") {
                    $("td[data-date='" + dataToFind + "']").addClass('selectdate fc-title event');
                }
                if (event.type == "Appointment") {
                    $("td[data-date='" + dataToFind + "']").addClass('selectdate fc-title appt');
                }
            }

            var el = $("td[data-date='" + dataToFind + "']");
            var res = '';
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: baseURLEOI + "RestServiceImpl.svc" + "/GetEventsReminderDtae/" + userid + "/" + el.attr("data-date"),
                // async: false,
                success: function (response) {
                    var div = "<ul>";
                    $.each(response.FetchEventsReminderDateResult, function (index, value) {
                        if (value.status == "success") {
                            if (value.type == "Event") {
                                div += '<li class="event">' + value.title + '</li>';
                            }
                            else {
                                div += '<li class="appt">' + value.title + '</li>';
                            }
                        }
                        res = div + "</ul>";
                    });
                    Tipped.create(el, res, {
                    });
                },
                failure: function (response) {
                    alert('Fail');
                }
            });
            //console.log(res);
            //$("td[data-date='" + dataToFind + "']").html($("td[data-date='" + dataToFind + "']").html() + event.title);
            // el.qtip({
            //     content: event.title
            // });
            //$('.ui-loader').toggle(bool);
            //            if(event.type=="Event" || event.type=="Appointment")
            //                $("td[data-date='" + dataToFind + "']").attr('title',event.title);
            //            else
            //            {
            //                var div='<ul>';
            //                $.each( events, function( i, value ) {
            //                    div+='<li class="event">'+ value.title +'</li>';
            //                });
            //                var calculate = div +'</ul>';
            //                //alert(calculate);
            //                $("td[data-date='" + dataToFind + "']").attr('title',calculate);
            //            }
            //            Tipped.create(el,res, {
            ////              skin: 'white',
            ////              hook: 'topleft',
            ////              hideOn: false,
            ////              closeButton: true,
            //              onShow: function(content, element) {
            //
            //              }
            //             });

        },
        //    loading: function(bool,event, element) {
        //     alert(event);
        //         var dataToFind = moment(event.start).format('YYYY-MM-DD');
        //         $("td[data-date='" + dataToFind + "']").addClass('selectdate fc-title');
        //        $('.ui-loader').toggle(bool);
        //        Tipped.create(el);
        //    },
        eventAfterRender: function (event, element, cell) {

        }
        //{
        //    title: 'All Day Event',
        //    start: '2015-02-01'
        //},
        //{
        //    title: 'Long Event',
        //    start: '2015-02-07',
        //    end: '2015-02-10'
        //},
        // eventClick: function (event) {
        // opens events in a popup window
        //     alert(event.title);
        //window.open(event.url, 'gcalevent', 'width=700,height=600');
        //    return false;
        //  },



    });

    $("#loading").hide();


    $('.fc-event-container').closest('tbody').hide();

    $('.fc-prev-button span').click(function () {
        $('.fc-event-container').closest('tbody').hide();
    });

    $('.fc-next-button span').click(function () {
        $('.fc-event-container').closest('tbody').hide();
    });
    /*$('#calendar').fullCalendar({
        // defaultDate: '2015-02-12',
        //editable: true,
        //eventLimit: true, // allow "more" link when too many events
        events: events,

        eventRender: function (event, element,bool) {
            var dataToFind = moment(event.start).format('YYYY-MM-DD');



            if($("td[data-date='" + dataToFind + "']").hasClass("selectdate fc-title event"))
            {
                if(event.type=="Event"){
                    $("td[data-date='" + dataToFind + "']").removeClass('event');
                    $("td[data-date='" + dataToFind + "']").addClass('event');
                    div+='<li class="event">'+ event.title +'</li>';
                }
                else(event.type=="Appointment")
                {
                    $("td[data-date='" + dataToFind + "']").removeClass('event');
                    $("td[data-date='" + dataToFind + "']").removeClass('appt');
                    $("td[data-date='" + dataToFind + "']").addClass('both');
                    div += '<li class="both">' + event.title + '</li>';
                    div += '</ul>';
                    temp2 = 1;
                    temp3 = 0;
                }
            }
            else if($("td[data-date='" + dataToFind + "']").hasClass("selectdate fc-title appt"))
            {
                if(event.type=="Appointment"){
                    $("td[data-date='" + dataToFind + "']").removeClass('appt');
                    $("td[data-date='" + dataToFind + "']").addClass('appt');
                    div+='<li class="appt">'+ event.title +'</li>';
                 }
                else (event.type=="Event")
                {
                    $("td[data-date='" + dataToFind + "']").removeClass('event');
                    $("td[data-date='" + dataToFind + "']").removeClass('appt');
                    $("td[data-date='" + dataToFind + "']").addClass('both');
                    div += '<li class="both">' + event.title + '</li>';
                    div += '</ul>';
                    temp2 = 1;
                    temp3 = 0;
                }
            }
            else{
                if(event.type=="Event")
                {
                    div = '';
                    div += '<ul>';
                    $("td[data-date='" + dataToFind + "']").addClass('selectdate fc-title event');
                    div += '<li class="event">' + event.title + '</li>';
                    //temp2 = 0;
                    temp3 = 1;
                }
                else (event.type=="Appointment")
                {
                    div = '';
                    alert('im here');
                    div += '<ul>';
                    $("td[data-date='" + dataToFind + "']").addClass('selectdate fc-title appt');
                    div += '<li class="appt">' + event.title + '</li>';
                    //temp2 = 0;
                    temp3 = 1;
                }
            }
            alert(temp2 +'-'+temp3);
            if (temp2 > 0 && temp3 > 0) {
                alert('become greater 0');
                temp1 = '';

            }

            var calculate = temp1 + div;
            alert(temp1);
            alert(calculate);

            $("td[data-date='" + dataToFind + "']").attr('title', calculate);
            temp1 = calculate;
            var el = $("td[data-date='" + dataToFind + "']");
            //$("td[data-date='" + dataToFind + "']").html($("td[data-date='" + dataToFind + "']").html() + event.title);
            // el.qtip({
            //     content: event.title
            // });
            //$('.ui-loader').toggle(bool);

//            $.each( events, function( i, value ) {
//                if(event.type=="Event")
//                    div+='<li class="event">'+ event.title +'</li>';
//                    //$("td[data-date='" + dataToFind + "']").attr('title',event.title);
//                else
//                    div+='<li class="appt">'+ event.title +'</li>';
//                     //$("td[data-date='" + dataToFind + "']").attr('title',event.title);
//            });
//            var calculate = div +'</ul>';
//            alert(calculate);
//            $("td[data-date='" + dataToFind + "']").attr('title',calculate);
            Tipped.create(el);
        },
        //    loading: function(bool,event, element) {
        //     alert(event);
        //         var dataToFind = moment(event.start).format('YYYY-MM-DD');
        //         $("td[data-date='" + dataToFind + "']").addClass('selectdate fc-title');
        //        $('.ui-loader').toggle(bool);
        //        Tipped.create(el);
        //    },
        eventAfterRender: function (event, element, cell) {

        }
        //{
        //    title: 'All Day Event',
        //    start: '2015-02-01'
        //},
        //{
        //    title: 'Long Event',
        //    start: '2015-02-07',
        //    end: '2015-02-10'
        //},
        // eventClick: function (event) {
        // opens events in a popup window
        //     alert(event.title);
        //window.open(event.url, 'gcalevent', 'width=700,height=600');
        //    return false;
        //  },



    });

    $("#loading").hide();


    $('.fc-event-container').closest('tbody').hide();

    $('.fc-prev-button span').click(function () {
        $('.fc-event-container').closest('tbody').hide();
    });

    $('.fc-next-button span').click(function () {
        $('.fc-event-container').closest('tbody').hide();
    });*/

}

// To Get User Information
function GetUserDetails(userid) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/FetchUserDetails/" + userid,
        async: false,
        success: function (response) {
            $.each(response.GetUserDetailsResult, function (index, value) {
                if (value.status == "success") {
                    $('#name').html(value.name);
                    $('#email').html(value.email);
                    $('#mobile').html(value.mobileno);
                    $('#comp').html(value.company);
                    $("#img_pic").attr("src", '');
                    //alert(value.imagename);
                    if (value.imagename == "NULL" || value.imagename == '') {
                        $("#a_remove").css("display", "none");
                        $("#img_pic").attr("src", "images/edit_dummy.jpg");
                    }
                    else {
                        $("#a_remove").css("display", "block  ");
                        $("#img_pic").attr("src", value.image);
                    }
                }
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To Get User Information
function GetUsesPic() {
    var img = $("#img_project");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/FetchUserPic/" + userid,
        async: false,
        success: function (response) {
            $.each(response.GetUserPicResult, function (index, value) {
                if (value.status == "success") {
                    img.attr("src", value.image);
                }
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

var bookData = {};
//To Check broker exist

function checkdetails() {
debugger
    bookData = {};
    //alert("checkdetails");
    var panno = $('#panno').val().toUpperCase();
    console.log("panno = " + panno);
    $('.ui-loader').show();
    if (panno == null || panno == "" || panno.lenght > 1) {
        showMsg('Please enter PAN No.');
        return false;
    }
    if(panno.length != 10){
        showMsg('Please enter valid PAN No.');
        return false;
    }
    if (!panno.match(/[A-Z]{5}[0-9]{4}[A-Z]{1}/)) {
        showMsg('Please enter valid PAN No.');
        return false;
    }

    //console.log("ajax call");
//    var foo = {panno: btoa(unescape(encodeURIComponent(JSON.stringify(panno)))) } // delete
//    console.log(foo);
    $.ajax({
        ServiceCallID: 1,
        type: "POST",
        method :"POST",
        //url: baseURLEOI + 'RestServiceImpl.svc/GetEmplDetails/' + panno,
        url: baseAPI + "GetEmplDetailsPanNo.ashx",
        data: {panno: btoa(unescape(encodeURIComponent(JSON.stringify(panno)))) },
        //contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            bookData = response;
            console.log(bookData);
            if (bookData.EmplID == "0") {
                $('#hdnpan').val(panno);
                objReralist = new Array();;
                objGSTlist = new Array();;
                $.mobile.pageContainer.pagecontainer("change", "request_for_empanelment_new.html", { transition: "none" });
                $('.ui-loader').hide();
            }
            else{
                if(bookData.status == "A"){
                    showMsg('Partner account is already created with this PAN number.Please Login!!', function (event, ui) {
                        $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                    });
                    return false;
                }
                else if(bookData.status == "R"){
                    showMsg('Your empanelment request has been rejected. Please refer comments and resubmit', function (event, ui) {
                        $.mobile.pageContainer.pagecontainer("change", "request_for_empanelment_new.html", { transition: "none" });
                    });
                    return false;
                }
                else if(bookData.status == "P"){
                    showMsg('Empanelment request in progress. Although you edit your empanelment request and resubmit', function (event, ui) {
                        $.mobile.pageContainer.pagecontainer("change", "request_for_empanelment_new.html", { transition: "none" });
                    });
                    return false;
                }
                else{
                    showMsg('Empanelment request in progress. Please try after some time!!', function (event, ui) {
                        $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                    });
                    return false;
                }
            }
        },
        error: DefaultAjaxError
    });
}

function fillForm(){
    if(bookData.EmplID != "0"){
        $('#comp').val(bookData.Company_Name);
        $('#entity_type').val(bookData.entity_type);
        $('#fname').val(bookData.FirstName);
        $('#lname').val(bookData.LastName);
        $('#mobno').val(bookData.Mobile);
        $('#emailid').val(bookData.Email);
        $('#panno2').val(bookData.Pan_No);
        $('#hdn_pan').val(bookData.pancerti);
        $('#upload_pan').val(bookData.pancerti);


        $('#billing_street').val(bookData.Billing_Street);
        $('#billing_zip').val(bookData.Billing_Zip);
        $('#billing_country option:contains(' + bookData.Billing_Country + ')').attr('selected', 'selected');
        fillBillingstate($('#billing_country').val());
        $('#billing_state option:contains(' + bookData.Billing_State + ')').attr('selected', 'selected');
        $('#txtbilling_state').val(bookData.Billing_State);
        fillBillingcity($('#billing_state').val());
        $('#billing_city option:contains(' + bookData.Billing_City + ')').attr('selected', 'selected');
        $('#txtbilling_city').val(bookData.Billing_City);

        $('#communication_street').val(bookData.Communication_Street);
        $('#communication_zip').val(bookData.Communication_Zip);
        $('#communication_country option:contains(' + bookData.Communication_Country + ')').attr('selected', 'selected');
        fillCommunicationstate($('#communication_country').val());
        $('#communication_state option:contains(' + bookData.Communication_State + ')').attr('selected', 'selected');
        $('#txtcommunication_state').val(bookData.Communication_State);
        fillCommunicationcity($('#communication_state').val());
        $('#communication_city option:contains(' + bookData.Communication_City + ')').attr('selected', 'selected');
        $('#txtcommunication_city').val(bookData.Communication_City);

        $('#registered_street').val(bookData.Registered_Street);
        $('#registered_zip').val(bookData.Registered_Zip);
        $('#registered_country option:contains(' + bookData.Registered_Country + ')').attr('selected', 'selected');
        fillRegisteredstate($('#registered_country').val());
        $('#registered_state option:contains(' + bookData.Registered_State + ')').attr('selected', 'selected');
        $('#txtregistered_state').val(bookData.Registered_State);
        fillRegisteredcity($('#registered_state').val());
        $('#registered_city option:contains(' + bookData.Registered_City + ')').attr('selected', 'selected');
        $('#txtregistered_city').val(bookData.Registered_City);


        if(bookData.Registered_Street == bookData.Communication_Street
                && bookData.Registered_Country == bookData.Communication_Country
                && bookData.Registered_State == bookData.Communication_State
                && bookData.Registered_City == bookData.Communication_City
                && bookData.Registered_Zip == bookData.Communication_Zip)
        {
            $('#same_address').attr('checked', 'checked');
            $("#registered_street").prop("disabled", 'disabled');
            $("#registered_country").prop("disabled", true);
            $("#registered_state").prop("disabled", true);
            $("#txtregistered_state").prop("disabled", 'disabled');
            $("#registered_city").prop("disabled", true);
            $("#txtregistered_city").prop("disabled", 'disabled');
            $("#registered_zip").prop("disabled", 'disabled');
        }

        $("#registered_zip")
        $("#tblReraList th.Comment").show();
        $("#tblGstList th.Comment").show();
        objGSTlist = [];
        objReralist = [];
        $.each(bookData.GstDoc, function(i){
            this.i = i;
            objGSTlist.push(this);
            AddTrGST(this);
        });

        $.each(bookData.ReraDoc, function(i){
            this.i = i;
            objReralist.push(this);
            AddTrRera(this);
        });
    /*


        //var region = $('#ddlRegion :selected').text();
        var enterpriseid = "";
        var brokerid = localStorage.getItem("brokerid");
        //alert("broker"+brokerid);

        var Reradoclist = objReralist;
        var Gstdoclist = objGSTlist;


    */
    }
}

function alphanumeric_check(e) {

    var charpos = e.search("[^A-Za-z\. ]");
    if (charpos >= 0 || e.charAt(0) == ' ' || e.length < 2) {
        return 'NO';
    }
    else
        return 'YES';
}

function alphanumeric_checkname(e) {

    var charpos = e.search("[^A-Za-z\. ]");
    if (charpos >= 0 || e.charAt(0) == ' ' || e.length < 1) {
        return 'NO';
    }
    else
        return 'YES';
}

//$("#emailid").blur(function () {
/*$(document).("blur","#emailid",function(){
    alert("email");

});*/

//To insert details of Empanelment form

function GetEmplDetails(panno, CallBack){
    $.ajax({
        ServiceCallID: 1,
        type: "POST",
        method :"POST",
        //url: baseURLEOI + 'RestServiceImpl.svc/GetEmplDetails/' + panno,
        url: baseAPI + "GetEmplDetailsPanNo.ashx",
        data: {panno: btoa(unescape(encodeURIComponent(JSON.stringify(panno))))},
        //contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var flag = true;
            if (response.EmplID != "0") {
                if(response.status == "A"){
                    flag = false;
                    showMsg('Partner account is already created with this PAN number.Please Login!!', function (event, ui) {
                        $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                    });
                }
            }
            console.log("flag " + flag)
            if(flag){
                CallBack();
            }

        },
        error: DefaultAjaxError
    });
}
function InsertEmpl() {
    $('.ui-loader').show();
    var email = $('#emailid').val().toLowerCase();
    var panno = $('#panno2').val();

    $('.ui-loader').show();
    $.ajax({
        type: "POST",
        //url: baseURL + "CPService.svc" + "/GetEmpl/" + email,
        url: baseAPI + "GetEmplDetailsEmail.ashx",
        data: {email: email, EmplID : bookData.EmplID},
        async: false,
        //contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var flag = true;
            if (response.EmplID != "0") {
                $('.ui-loader').hide();
                flag = false;
                $('#email').val('');
                showMsg('Email id already exist');
            }
            if(flag){
                GetEmplDetails(panno, SaveEmpl);
            }

        },
        error: DefaultAjaxError

    });
}
function SaveEmpl(){
/*
    var fullname = fname + ' ' + lname;
        bookData = {
            "Company_Name": company, "entity_type": entitytype, "FirstName": fname, "LastName": lname, "Mobile": mobile, "Email": email, "communication_address": commaddress, "registered_address": regaddress, "Pan_No": panno, "pancerti": pancerti, "BrokerID": "NULL", "ReraDoc": Reradoclist, "GstDoc": Gstdoclist
            , "Billing_City": billing_city, "Billing_Zip": billing_zip, "Billing_Street": billing_street, "Billing_State": billing_state, "Billing_Country": billing_country
            , "Registered_City": registered_city, "Registered_Zip": registered_zip, "Registered_Street": registered_street, "Registered_State": registered_state, "Registered_Country": registered_country
            , "Communication_City": communication_city, "Communication_Zip": communication_zip, "Communication_Street": communication_street, "Communication_State": communication_state, "Communication_Country": communication_country
        };
        */

     if (!$("#confirm_gst").is(":checked"))     // shreyas
     {
        if (objGSTlist.length == 0) {
            showMsg('Please enter GST details.');
            return false;
        }
     }

    bookData.communication_address = $('#communication_address').val();
    bookData.registered_address = $("#registered_address").val();
    bookData.Pan_No = $('#panno2').val();
    bookData.pancerti = $('#hdn_pan').val();


    //var region = $('#ddlRegion :selected').text();
    var enterpriseid = "";
    var brokerid = localStorage.getItem("brokerid");
    //alert("broker"+brokerid);
    bookData.ReraDoc = objReralist;
    bookData.GstDoc = objGSTlist;



    bookData.Company_Name = $('#comp').val();
    if (bookData.Company_Name == null || bookData.Company_Name == "") {
        showMsg('Please enter company name.');
        return false;
    }
    if (bookData.Company_Name.length > 100) {
        showMsg('Company name should be maximum 100 alphabets.');
        return false;
    }


    bookData.entity_type = $('#entity_type :selected').text();
    if (bookData.entity_type  == 'Type of entity') {
        showMsg('Please select type of entity.');
        return false;
    }

    bookData.FirstName = $('#fname').val();
    if (bookData.FirstName == null || bookData.FirstName == "" || bookData.FirstName.lenght > 100 || bookData.FirstName.lenght < 1) {
        showMsg('Please enter first name.');
        return false;
    }
    //var chkname = alphanumeric_checkname(bookData.FirstName);

    if (alphanumeric_checkname(bookData.FirstName) == 'NO') {
        showMsg('Please enter valid first name.');
        return false;
    }

    bookData.LastName = $('#lname').val();
    if (bookData.LastName == null || bookData.LastName == "" || bookData.LastName.lenght > 100 || bookData.LastName.lenght < 3) {
        showMsg('Please enter last name.');
        return false;
    }

    if (alphanumeric_checkname(bookData.LastName) == 'NO') {
        showMsg('Please enter valid last name.');
        return false;
    }

    bookData.Mobile = $('#mobno').val();

    if (bookData.Mobile == null || bookData.Mobile == "") {
        showMsg('Please enter mobile number.');
        return false;
    }

    if (bookData.Mobile.length < 10 || bookData.Mobile.length > 10) {
        showMsg.html('Mobile number Should be 10 digits.');
        return false;
    }

    var numPattern = /\d+(,\d{1,3})?/;

    if (!numPattern.test(bookData.Mobile)) {
        showMsg('Please enter numeric value.');
        return false;
    }

    bookData.Email = $('#emailid').val().toLowerCase();
    if (bookData.Email == null || bookData.Email == "" || bookData.Email.lenght > 100) {
        showMsg('Please enter email ID.');
        return false;
    }
    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!filter.test(bookData.Email)) {
        showMsg('Please enter valid email id.');
        return false;
    }

    //Billing Data
    bookData.Billing_Street = $("#billing_street").val();
    bookData.Billing_Country = $("#billing_country").find(":selected").text();
    if(bookData.Billing_Country.toUpperCase()=="INDIA")
    {
        bookData.Billing_State = $("#billing_state").find(":selected").text();
        bookData.Billing_City = $("#billing_city").find(":selected").text();
    }
    else
    {
        bookData.Billing_State = $("#txtbilling_state").val();
        bookData.Billing_City = $("#txtbilling_city").val();
    }


    if (bookData.Billing_Street == null || bookData.Billing_Street == "") {
        showMsg('Please enter billing street.');
        return false;
    }
    if (bookData.Billing_Country == "SELECT COUNTRY") {
        showMsg('Please select billing country.');
        return false;
    }
    if(bookData.Billing_Country.toUpperCase()=="INDIA"){
        if (bookData.Billing_State == "SELECT STATE") {
            showMsg('Please select billing state.');
            return false;
        }
        if (bookData.Billing_City == "SELECT CITY") {
            showMsg('Please select billing city.');
            return false;
        }
    }
    else
    {
        if(bookData.Billing_State == null || bookData.Billing_State == "") {
            showMsg('Please enter billing state.');
            return false;
        }
        if(bookData.Billing_City == null || bookData.Billing_City == "") {
            showMsg('Please enter billing city.');
            return false;
        }
    }

    bookData.Billing_Zip = $("#billing_zip").val();
    if (bookData.Billing_Zip == null || bookData.Billing_Zip == "") {
        showMsg('Please enter billing zip code.');
        return false;
    }

    //Communication data
    bookData.Communication_Street = $("#communication_street").val();
    bookData.Communication_Country = $("#communication_country").find(":selected").text();
    if(bookData.Communication_Country.toUpperCase()=="INDIA")
    {
        bookData.Communication_State = $("#communication_state").find(":selected").text();
        bookData.Communication_City = $("#communication_city").find(":selected").text();
    }
    else
    {
        bookData.Communication_State = $("#txtcommunication_state").val();
        bookData.Communication_City = $("#txtcommunication_city").val();
    }
    if (bookData.Communication_Street == null || bookData.Communication_Street == "") {
        showMsg('Please enter communication street.');
        return false;
    }
    if (bookData.Communication_Country == "SELECT COUNTRY") {
        showMsg('Please select communication country.');
        return false;
    }
    if(bookData.Communication_Country.toUpperCase()=="INDIA"){
        if (bookData.Communication_State == "SELECT STATE") {
            showMsg('Please select communication state.');
            return false;
        }
        if (bookData.Communication_City == "SELECT CITY") {
            showMsg('Please select communication city.');
            return false;
        }
    }
    else
    {
        if(bookData.Communication_State == null || bookData.Communication_State == "") {
            showMsg('Please enter communication state.');
            return false;
        }
        if(bookData.Communication_City == null || bookData.Communication_City == "") {
            showMsg('Please enter communication city.');
            return false;
        }
    }
    bookData.Communication_Zip = $("#communication_zip").val();
    if (bookData.Communication_Zip == null || bookData.Communication_Zip == "") {
        showMsg('Please enter communication zip code.');
        return false;
    }

    //Registered Data
    bookData.Registered_Street = $("#registered_street").val();
    bookData.Registered_Country = $("#registered_country").find(":selected").text();
    if(bookData.Registered_Country.toUpperCase()=="INDIA")
    {
        bookData.Registered_State = $("#registered_state").find(":selected").text();
        bookData.Registered_City = $("#registered_city").find(":selected").text();
    }
    else
    {
        bookData.Registered_State = $("#txtregistered_state").val();
        bookData.Registered_City = $("#txtregistered_city").val();
    }


    if (bookData.Registered_Street == null || bookData.Registered_Street == "") {
        showMsg('Please enter registered street.');
        return false;
    }
    if (bookData.Registered_Country == "SELECT COUNTRY") {
        showMsg('Please select registered country.');
        return false;
    }
    if(bookData.Registered_Country.toUpperCase()=="INDIA"){
        if (bookData.Registered_State == "SELECT STATE") {
            showMsg('Please select registered state.');
            return false;
        }
        if (bookData.Registered_City == "SELECT CITY") {
            showMsg('Please select registered city.');
            return false;
        }
    }
    else
    {
        if(bookData.Registered_State == null || bookData.Registered_State == "") {
            showMsg('Please enter registered state.');
            return false;
        }
        if(bookData.Registered_City == null || bookData.Registered_City == "") {
            showMsg('Please select registered city.');
            return false;
        }

    }

    bookData.Registered_Zip = $("#registered_zip").val();
    if (bookData.Registered_Zip == null || bookData.Registered_Zip == "") {
        showMsg('Please enter registered zip code.');
        return false;
    }


    if (bookData.Pan_No == null || bookData.Pan_No == "" || bookData.Pan_No.lenght > 50) {
        showMsg('Please enter PAN No.');
        return false;
    }

    if (!bookData.Pan_No.match(/[A-Z]{5}[0-9]{4}[A-Z]{1}/)) {
        showMsg('Please enter valid PAN No.');
        return false;
    }

    if (bookData.pancerti == null || bookData.pancerti == "") {
        showMsg('Please upload pan certificate.');
        return false;
    }

    if (bookData.ReraDoc.length == 0) {
        showMsg('Please enter RERA details.');
        return false;
    }
    console.log("User Enpanelment data ==>",bookData);

    if ($("#chkAgree").prop("checked") == false) {
        showMsg('Please accept terms and conditions.');
        return false;
    }
    // Change 2th November 2020
    if (!bookData.Billing_Zip.match(/^\d+$/))
    {
        showMsg('Please enter valid zipcode. Numbers only');
        return false;
    }
    if (!bookData.Communication_Zip.match(/^\d+$/))
    {
        showMsg('Please enter valid zipcode. Numbers only');
        return false;
    }
    if (!bookData.Registered_Zip.match(/^\d+$/))
    {
        showMsg('Please enter valid zipcode. Numbers only');
        return false;
    }

               // url: baseURLEOI + 'CPProperties.svc/InsertEmpl',

    // var addstring = company + "/" + entitytype + "/" + fullname + "/" + mobile + "/" + email + "/" + commaddress + "/" + regaddress + "/" + panno + "/" + pancerti + "/" + brokerid;
    //var addstring = fullname + "/" + mobile + "/" + email + "/" + panno + "/" + taxno + "/" + company + "/" + region + "/" + brokerid + "/" + pancerti + "/" + doclist;

    if (brokerid == "" || brokerid == null || brokerid == "null") {
        console.log("Inside Insert Enpanelment IF condition");
        $('.ui-loader').show();
        $.ajax({
            type: "POST",
            url: baseAPI + 'InsertEmpanelment.ashx',
            data: {"empl":  JSON.stringify(bookData)},
            //contentType: "application/json; charset=utf-8",
            //dataType: "json",
            processData: true,
            async: false,
            success: function (resp) {
                response = JSON.parse(resp);
                console.log(response);
                $('.ui-loader').hide();
                if(response.status == "success"){
                    //$.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                    $('#error_content').empty().html('You have registered successfully. You will receive your user id and password shortly by email.');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2({
                            callbackClose: function () {
                                $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                                $('.ui-loader').hide();
                            }
                        });
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 500);
                }
                else{
                    $('#error_content').empty().html(response.msg);
                }

            },
            error: DefaultAjaxError

        });
    }
    else {
        console.log("Inside Insert Enpanelment ELSE condition");
        $('.ui-loader').show();
        $.ajax({
            type: "POST",
            url: baseURLEOI + 'CPProperties.svc/InsertEmpl',
            data: JSON.stringify(bookData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: true,
            async: false,
            success: function (data, status, jqXHR) {
                $('.ui-loader').hide();
                //$.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                $('#error_content').empty().html('Thank you for submitting the form. Upon approval, the admin will share login details via email.');
                setTimeout(function () {
                    $('#enquiry_inline').simpledialog2({
                        callbackClose: function () {
                            $.mobile.pageContainer.pagecontainer("change", "login.html", { transition: "none" });
                            $('.ui-loader').hide();
                        }
                    });
                    $('.ui-simpledialog-screen').css('height', $(document).height());
                }, 500);
            },
            error: DefaultAjaxError

        });
    }
}


function randString() {

    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

// To display project list
function DisplayGetProjects(projecttype, projectstatus) {
    var addstring = projecttype + "/" + projectstatus;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/DisplayGetProjects/' + addstring,
        async: false,
        success: function (response) {
            var ul = $("#ul_proj");
            $.each(response.GetProjectsResult, function (index, value) {
                if (value.status == "success") {
                    ul.append('<li><a href="project_details.html?projectid=' + value.projectid + '" data-transition="slide">' + value.title + '<br><span>' + value.cityname + '</span></a></li>');
                }
                else
                    ul.append('<li style="text-align:center;">No properties found</li>');
            });
        },
        error: DefaultAjaxError
    });
}

// To add lead
function AddLead() {

    //alert($('#ddlapartment').val());
    var firstname = $('#txtfirstname').val();
    var lastname = $('#txtlastname').val();
    var email = $('#txtemail').val();
    var country = $('#ddlCountry :selected').text();
    var state = $('#txtState').val();
    var city = $('#txtCity').val();
    var mobile = $('#txtmobile').val();
    var project = $('#ddlproject :selected').text();
    var projectid = $('#ddlproject :selected').val();
    var apartment = $('#ddlapartment').val();
    var description = $('#txtcomment').val();
    var userid = localStorage.getItem("emplcontid");
    var brokerid = localStorage.getItem("brokerid");
    var brokeraccountid = '';// localStorage.getItem("brokeraccountid");
    var brokercontactid = '';
    var cp_name = localStorage.getItem("cp_name");
    var sfdcname = localStorage.getItem("sfdcname");
    var budget = $('#ddlBudget :selected').val();

    var strfirst = alphanumeric_checkname(firstname);

    if (firstname == null || firstname == "" || firstname.length > 50 || firstname.length < 1) {
        $('#error_content').empty().html('Please enter first name.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (strfirst == 'NO') {
        $('#error_content').empty().html('Please enter valid first name.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    var strlast = alphanumeric_check(lastname);

    if (lastname == null || lastname == "" || lastname.lenght > 50) {
        $('#error_content').empty().html('Please enter last name.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (strlast == 'NO') {
        $('#error_content').empty().html('Please enter valid last name.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (email == null || email == "" || email.lenght > 40) {
        $('#error_content').empty().html('Please enter email id.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!filter.test(email)) {
        $('#error_content').empty().html('Please enter valid email id.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (country == "SELECT COUNTRY *") {
        $('#error_content').empty().html('Please select country.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }


    if (state == null || state == "" || state == '') {
        state = 'NA';
    }
    else {
        var strstate = alphanumeric_check(state);

        if (strstate == 'NO') {
            $('#error_content').empty().html('Please enter valid state.');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2();
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }
    }


    if (city == null || city == "") {
        city = 'NA';
    }

    else {
        var strcity = alphanumeric_check(city);

        if (strcity == 'NO') {
            $('#error_content').empty().html('Please enter valid city.');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2();
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }

    }

    if (mobile == null || mobile == "") {
        $('#error_content').empty().html('Please enter mobile number.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (country == "India") {
        if (mobile.charAt(0) == "0") {
            $('#error_content').empty().html('Please enter valid mobile number');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2();
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }
        if (mobile.length < 10 || mobile.length > 12) {
            $('#error_content').empty().html('Mobile number should be of minimum 10 digits and maximun 12 digits.');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2();
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }
    }
    //    else
    //    {
    //        if (mobile.length < 6 || mobile.length > 20) {
    //            $('#error_content').empty().html('Mobile number should be of minimum 6 digits and maximun 20 digits.');
    //                        setTimeout(function () {
    //            $('#enquiry_inline').simpledialog2();
    //            $('.ui-simpledialog-screen').css('height', $(document).height());
    //            },500);
    //            return false;
    //        }
    //    }

    //    if (mobile.length < 10 || mobile.length > 10) {
    //        $('#error_content').empty().html('Mobile number Should be 10 digits.');
    //        setTimeout(function () {
    //            $('#enquiry_inline').simpledialog2();
    //            $('.ui-simpledialog-screen').css('height', $(document).height());
    //        }, 500);
    //        return false;
    //    }

    var numPattern = /\d+(,\d{1,3})?/;

    if (!numPattern.test(mobile)) {
        $('#error_content').empty().html('Please enter numeric value.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (project == 'Select Project' || project == ' SELECT PROJECT ') {
        $('#error_content').empty().html('Please select project');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }


    /*if (apartment == null || apartment == "" || apartment == 'Types of Apartments *' || apartment == 'TYPES OF APARTMENTS *' || apartment == "0") {
        $('#error_content').empty().html('Please select Types of Apartments.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }*/

    if (apartment == 'Types of Apartments *') {
        apartment = '';
    }

    if (description == null || description == "" || description.length > 500) {
        $('#error_content').empty().html('Please enter comments.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }
    var remark = "";
    if (budget == "0")
        remark = description + "; " + apartment;
    else
        remark = description + "; " + apartment + "; " + budget;
    var addstring = firstname + "/" + lastname + "/" + email + "/" + country + "/" + mobile + "/" + projectid + "/" + project + "/" + description + "/" + state + "/" + city + "/" + apartment + "/" + userid + "/" + brokerid + "/" + remark + "/" + sfdcname;
    var currentdate = new Date();
    var datetime = (currentdate.getMonth() + 1) + "/" + currentdate.getDate() + "/" + currentdate.getFullYear();

    //alert(remark);
    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        url: baseURLEOI + 'RestServiceImpl.svc/AddLead2/' + addstring,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            $('.ui-loader').show();
            $.each(response.InsertLead2Result, function (index, value) {
                if (value.status == "success") {
                    //alert('Lead created successfully.');
                    //  console.log('In side  add lead1  : Brokeraccount ID ' + brokeraccountid + '  BrokerID' + brokerid);
                    //  alert(value.BrokerAccountID);
                    brokeraccountid = value.BrokerAccountID;
                    brokercontactid = value.BrokerContactID;

                    // alert(brokeraccountid + ' and con : ' + brokercontactid);
                    var addcodeval = '';
                    var projectcodephp = '';
                    if (projectid == "24") {
                        addcodeval = '48226';
                        projectcodephp = "a2X6F000000JF1XUAW";
                    }
                    else if (projectid == "22") {
                        addcodeval = '48212';
                        projectcodephp = "a2X6F000000JF1JUAW";
                    }
                    else if (projectid == "23") {
                        addcodeval = '48218';
                        projectcodephp = "a2X6F000000JF1PUAW";
                    }
                    else if (projectid == "40") {
                        addcodeval = '48233';
                        projectcodephp = "a2X6F000000JF1eUAG";
                    }
                    else if (projectid == "41") {
                        addcodeval = '48216';
                        projectcodephp = "a2X6F000000JF1NUAW";
                    }
                    else if (projectid == "42") {
                        addcodeval = '48237';
                        projectcodephp = "a2X6F000000JF1iUAG";
                    }
                    else if (projectid == "43") {
                        addcodeval = '48210';
                        projectcodephp = "a2X6F000000JF1HUAW";
                    }

                    else if (projectid == "45") {
                        addcodeval = '48230';
                        projectcodephp = "a2X6F000000JF1bUAG";
                    }
                    //alert(baseURL);
                    var phpurl = 'title=' + 'NA' + '&firstName=' + firstname + '&lastname=' + lastname + '&city=' + city + '&state=' + state + '&Country=' + country + '&phoneNo=' + mobile + '&email=' + email + '&prjcode=' + projectcodephp + '&comment=' + description + '&visitDate=' + 'NA' + '&websiteName=' + 'NA' + '&addCode=' + addcodeval + '&type=' + apartment + '&Src=' + 'Broker' + '&CountryCode=' + 'NA' + '&rating=' + 'Hot' + '&trnsId=' + 'NA' + '&trnsStatus=' + 'NA' + '&BrokerAccID=' + brokeraccountid + '&BrokerConID=' + brokercontactid + '&Projectid=' + projectid;
                    $.ajax({
                        ServiceCallID: 1,
                        type: "POST",
                        url: sfdc_baseURL + 'lead_submit.php',
                        data: phpurl,
                        dataType: "json",
                        success: function (response) {
                            alert('Hi reply: ' + response.status);
                        }
                    });

                    var URL = "";
                    //if (projectid == "44") {
                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej Platinum ,1035,Kolkata&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
                    //}
                    //else if (projectid == "20") {
                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej Summit JV, Gurgaon&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
                    //}
                    //else if (projectid == "34") {
                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej Pine, Thane&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
                    //}
                    //else if (projectid == "53") {
                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej City, Panvel, Mumbai&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
                    //}
                    //else if (projectid == "35") {
                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej Edenwoods, Thane&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
                    //}
                    //else if (projectid == "51") {
                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej United, Whitefield,Bengaluru&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
                    //}
                    //else {



                    //URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=' + sfdcname + '&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '&00N9000000Az9hU=' + remark + '';
                    //// }
                    //console.log(URL);
                    ////alert(URL);
                    //$.ajax({
                    //    ServiceCallID: 1,
                    //    type: "POST",
                    //    url: URL,
                    //    success: function (response) {
                    //    }
                    //});

                    $('#error_content').empty().html('Lead created successfully. Please check Lead Status into My Account.');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2();
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 1000);



                    $('#txtfirstname').val('');
                    $('#txtlastname').val('');
                    $('#txtemail').val('');
                    $('#ddlCountry').val(0);
                    $('#txtState').val('');
                    $('#txtCity').val('');
                    $('#txtmobile').val('');
                    //$('#ddlapartment').val(0);
                    $('#txtcomment').val('');
                    $('#ddlBudget').val(0);

                    fillprojectdetails();
                    fillapartment(localStorage.getItem(0));
                    $('.ui-loader').hide();
                }
            });
        },

        failure: function (response) {
            alert('Fail');
        }
    });
}
//Start Coment by Umesh Pawar 14/04/2017
// To add lead
//function AddLead() {

//    //alert($('#ddlapartment').val());
//    var firstname = $('#txtfirstname').val();
//    var lastname = $('#txtlastname').val();
//    var email = $('#txtemail').val();
//    var country = $('#ddlCountry :selected').text();
//    var state = $('#txtState').val();
//    var city = $('#txtCity').val();
//    var mobile = $('#txtmobile').val();
//    var project = $('#ddlproject :selected').text();
//    var projectid = $('#ddlproject :selected').val();
//    var apartment = $('#ddlapartment').val();
//    var description = $('#txtcomment').val();
//    var userid = localStorage.getItem("emplcontid");
//    var brokerid = localStorage.getItem("brokerid");
//    var brokeraccountid = localStorage.getItem("brokeraccountid");
//     var cp_name = localStorage.getItem("cp_name");
//    var sfdcname = localStorage.getItem("sfdcname");
//    var budget = $('#ddlBudget :selected').val();

//    var strfirst = alphanumeric_checkname(firstname);

//    if (firstname == null || firstname == "" || firstname.length > 50 || firstname.length < 1) {
//        $('#error_content').empty().html('Please enter first name.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    if (strfirst == 'NO') {
//        $('#error_content').empty().html('Please enter valid first name.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    var strlast = alphanumeric_check(lastname);

//    if (lastname == null || lastname == "" || lastname.lenght > 50) {
//        $('#error_content').empty().html('Please enter last name.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    if (strlast == 'NO') {
//        $('#error_content').empty().html('Please enter valid last name.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    if (email == null || email == "" || email.lenght > 40) {
//        $('#error_content').empty().html('Please enter email id.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

//    if (!filter.test(email)) {
//        $('#error_content').empty().html('Please enter valid email id.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    if (country == "SELECT COUNTRY *") {
//        $('#error_content').empty().html('Please select country.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }


//    if (state == null || state == "" || state == '') {
//        state = 'NA';

//    }
//    else {
//        var strstate = alphanumeric_check(state);

//        if (strstate == 'NO') {
//            $('#error_content').empty().html('Please enter valid state.');
//            setTimeout(function () {
//                $('#enquiry_inline').simpledialog2();
//                $('.ui-simpledialog-screen').css('height', $(document).height());
//            }, 500);
//            return false;
//        }
//    }


//    if (city == null || city == "") {
//        city = 'NA';
//    }

//    else {
//        var strcity = alphanumeric_check(city);

//        if (strcity == 'NO') {
//            $('#error_content').empty().html('Please enter valid city.');
//            setTimeout(function () {
//                $('#enquiry_inline').simpledialog2();
//                $('.ui-simpledialog-screen').css('height', $(document).height());
//            }, 500);
//            return false;
//        }

//    }

//    if (mobile == null || mobile == "") {
//        $('#error_content').empty().html('Please enter mobile number.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    if (country == "India") {
//        if (mobile.charAt(0) == "0") {
//            $('#error_content').empty().html('Please enter valid mobile number');
//            setTimeout(function () {
//                $('#enquiry_inline').simpledialog2();
//                $('.ui-simpledialog-screen').css('height', $(document).height());
//            }, 500);
//            return false;
//        }
//        if (mobile.length < 10 || mobile.length > 12) {
//            $('#error_content').empty().html('Mobile number should be of minimum 10 digits and maximun 12 digits.');
//            setTimeout(function () {
//                $('#enquiry_inline').simpledialog2();
//                $('.ui-simpledialog-screen').css('height', $(document).height());
//            }, 500);
//            return false;
//        }
//    }
//    //    else
//    //    {
//    //        if (mobile.length < 6 || mobile.length > 20) {
//    //            $('#error_content').empty().html('Mobile number should be of minimum 6 digits and maximun 20 digits.');
//    //                        setTimeout(function () {
//    //            $('#enquiry_inline').simpledialog2();
//    //            $('.ui-simpledialog-screen').css('height', $(document).height());
//    //            },500);
//    //            return false;
//    //        }
//    //    }

//    //    if (mobile.length < 10 || mobile.length > 10) {
//    //        $('#error_content').empty().html('Mobile number Should be 10 digits.');
//    //        setTimeout(function () {
//    //            $('#enquiry_inline').simpledialog2();
//    //            $('.ui-simpledialog-screen').css('height', $(document).height());
//    //        }, 500);
//    //        return false;
//    //    }

//    var numPattern = /\d+(,\d{1,3})?/;

//    if (!numPattern.test(mobile)) {
//        $('#error_content').empty().html('Please enter numeric value.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    if (project == 'Select Project' || project == ' SELECT PROJECT ') {
//        $('#error_content').empty().html('Please select project');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }


//    if (apartment == null || apartment == "" || apartment == 'Types of Apartments *' || apartment == 'TYPES OF APARTMENTS *' || apartment == "0") {
//        $('#error_content').empty().html('Please select Types of Apartments.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }

//    if (description == null || description == "" || description.length > 500) {
//        $('#error_content').empty().html('Please enter comments.');
//        setTimeout(function () {
//            $('#enquiry_inline').simpledialog2();
//            $('.ui-simpledialog-screen').css('height', $(document).height());
//        }, 500);
//        return false;
//    }
//    var remark = "";
//    if (budget == "0")
//        remark = description + "; " + apartment;
//    else
//        remark = description + "; " + apartment + "; " + budget;
//    var addstring = firstname + "/" + lastname + "/" + email + "/" + country + "/" + mobile + "/" + projectid + "/" + project + "/" + description + "/" + state + "/" + city + "/" + apartment + "/" + userid + "/" + brokerid + "/" + remark + "/" + sfdcname;
//    var currentdate = new Date();
//    var datetime = (currentdate.getMonth() + 1) + "/" + currentdate.getDate() + "/" + currentdate.getFullYear();

//    //alert(remark);
//    $.ajax({
//        ServiceCallID: 1,
//        type: "GET",
//        url: baseURL + 'RestServiceImpl.svc/AddLead1/' + addstring,
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        async: false,
//        success: function (response) {
//            $('.ui-loader').show();
//            $.each(response.InsertLead1Result, function (index, value) {
//                if (value.status == "success") {
//                    //alert('Lead created successfully.');
//                    console.log('In side  add lead1  : Brokeraccount ID ' + brokeraccountid + '  BrokerID' + brokerid);
//                    var addcodeval = '';
//                    var projectcodephp = '';
//                    if (projectid == "24")
//                    {
//                        addcodeval='48226';
//                        projectcodephp = "a2X6F000000JF1XUAW";
//                    }
//                    else if (projectid == "22") {
//                        addcodeval='48212';
//                        projectcodephp = "a2X6F000000JF1JUAW";
//                    }
//                    else if (projectid == "23") {
//                        addcodeval = '48218';
//                        projectcodephp = "a2X6F000000JF1PUAW";
//                    }
//                    else if (projectid == "40") {
//                        addcodeval = '48233';
//                        projectcodephp = "a2X6F000000JF1eUAG";
//                    }
//                    else if (projectid == "41") {
//                        addcodeval = '48216';
//                        projectcodephp = "a2X6F000000JF1NUAW";
//                    }
//                    else if (projectid == "42") {
//                        addcodeval='48237';
//                        projectcodephp = "a2X6F000000JF1iUAG";
//                    }
//                    else if (projectid == "43") {
//                        addcodeval = '48210';
//                        projectcodephp = "a2X6F000000JF1HUAW";
//                    }

//                    else if (projectid == "45") {
//                        addcodeval = '48230';
//                        projectcodephp = "a2X6F000000JF1bUAG";
//                    }

//                    var phpurl = 'title=' + 'NA' + '&firstName=' + firstname + '&lastname=' + lastname + '&city=' + city + '&state=' + state + '&Country=' + country + '&phoneNo=' + mobile + '&email=' + email + '&prjcode=' + projectcodephp + '&comment=' + description + '&visitDate=' + 'NA' + '&websiteName=' + 'NA' + '&addCode=' + addcodeval + '&type=' + apartment + '&Src=' + 'Broker' + '&CountryCode=' + 'NA' + '&rating=' + 'Hot' + '&trnsId=' + 'NA' + '&trnsStatus=' + 'NA' + '&BrokerAccID=' + brokeraccountid + '&BrokerConID=' + brokerid + '&Projectid=' + projectid;
//                    $.ajax({
//                        ServiceCallID: 1,
//                        type: "POST",
//                        url: baseURL + 'lead_submit.php',
//                        data: phpurl,
//                        dataType: "json",
//                        success: function (response) {
//                           // alert('Hi reply: ' + response.status);
//                        }
//                    });

//                    var URL = "";
//                    //if (projectid == "44") {
//                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej Platinum ,1035,Kolkata&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
//                    //}
//                    //else if (projectid == "20") {
//                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej Summit JV, Gurgaon&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
//                    //}
//                    //else if (projectid == "34") {
//                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej Pine, Thane&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
//                    //}
//                    //else if (projectid == "53") {
//                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej City, Panvel, Mumbai&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
//                    //}
//                    //else if (projectid == "35") {
//                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej Edenwoods, Thane&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
//                    //}
//                    //else if (projectid == "51") {
//                    //    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=Godrej United, Whitefield,Bengaluru&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '';
//                    //}
//                    //else {
//                    URL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&oid=00D900000000kOQ&URL=&lead_source=Channel%20partner&00N90000000I4Zx=CP%20App&lead_origin=CP%20App&00N90000000HYW3=Open&00N90000000HYVj=' + datetime + '&first_name=' + firstname + '&last_name=' + lastname + '&00N90000000k2nZ=' + country + '&00N90000000kb3h=&city=' + city + '&00N90000000kb3i=' + mobile + '&00N900000012Up9=' + sfdcname + '&email=' + email + '&recordType=&00N90000000HYVr=&rating=Warm&Campaign_ID=7019000000161v2&00N90000000kWuc=&00N9000000EFbzF=' + brokerid + '&00N9000000Az9hU=' + remark + '';
//                    // }
//                    console.log(URL);
//                    //alert(URL);
//                    $.ajax({
//                        ServiceCallID: 1,
//                        type: "POST",
//                        url: URL,
//                        success: function (response) {
//                        }
//                    });

//                    $('#error_content').empty().html('Lead created successfully. Please check Lead Status into My Account.');
//                    setTimeout(function () {
//                        $('#enquiry_inline').simpledialog2();
//                        $('.ui-simpledialog-screen').css('height', $(document).height());
//                    }, 1000);



//                    $('#txtfirstname').val('');
//                    $('#txtlastname').val('');
//                    $('#txtemail').val('');
//                    $('#ddlCountry').val(0);
//                    $('#txtState').val('');
//                    $('#txtCity').val('');
//                    $('#txtmobile').val('');
//                    //$('#ddlapartment').val(0);
//                    $('#txtcomment').val('');
//                    $('#ddlBudget').val(0);

//                    fillprojectdetails();
//                    fillapartment(localStorage.getItem(0));
//                    $('.ui-loader').hide();
//                }
//            });
//        },

//        failure: function (response) {
//            alert('Fail');
//        }
//    });
//}
//End Coment by Umesh Pawar 14/04/2017
// To fill dropdown for project
function fillproject() {
    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/DisplayAllProjects',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';

            if (localStorage.getItem("projectid") == null) {
                optionsValues += '<option value="SELECT PROJECT"> SELECT PROJECT* </option>';
            }
            else {
                $('#ddlproject').attr('disabled', true);
            }
            //alert(localStorage.getItem("projectid"));
            var sel_project = '';

            $.each(response.GetAllProjectsResult, function (index, value) {

                if (value.projectid == localStorage.getItem("projectid")) {
                    sel_project = "selected";
                }

                if (value.status == "success") {
                    optionsValues += '<option value="' + value.projectid + '" ' + sel_project + '>' + value.title + ', ' + value.cityname + '</option>';
                }
                sel_project = "";

                optionsValues += '</select>';
                var options = $('#ddlproject');
                options.html(optionsValues);

            });

        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

// To fill dropdown for project for only send enquery
function fillprojectdetails() {
    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/DisplayAllProjects',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';


            optionsValues += '<option value="SELECT PROJECT"> SELECT PROJECT* </option>';

            $.each(response.GetAllProjectsResult, function (index, value) {

                if (value.status == "success") {
                    optionsValues += '<option value="' + value.projectid + '">' + value.title + ', ' + value.cityname + '</option>';

                }

                optionsValues += '</select>';
                var options = $('#ddlproject');
                options.html(optionsValues);

            });

        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

// To fill dropdown for Apartment
function fillapartment(projectid) {

    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/DisplayProjectApartment/' + projectid,
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="TYPES OF APARTMENTS *">  TYPES OF APARTMENTS *</option>';
            $.each(response.GetProjectApartmentResult, function (index, value) {
                if (value.status == "success") {
                    optionsValues += '<option value="' + value.apartment + '">' + value.apartment + '</option>';
                    if (value.sfdcname != "")
                        localStorage.setItem("sfdcname", value.sfdcname);
                    else
                        localStorage.setItem("sfdcname", "");
                }
                else {
                    if (projectid == null) {
                        optionsValues = '';
                        optionsValues = '<option value="0">  TYPES OF APARTMENTS *</option>';
                    }
                    else {
                        optionsValues = '';
                        optionsValues = '<option value="0"> Apartment not found! </option>';
                    }
                    localStorage.setItem("sfdcname", "");
                }

                optionsValues += '</select>';
                var options = $('#ddlapartment');
                options.html(optionsValues);

            });

        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

// To Get Cities
function GetCity() {
console.log("Inside EOI link API call for City");
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/GetCities',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="0">  CHOOSE BY CITY </option>';

            $.each(response.FetchCitiesResult, function (index, value) {
                if (value.status == "success") {

                    optionsValues += '<option value="' + value.cityid + '">' + value.cityname + '</option>';
                }
            });
            var options = $('#ddlCity');
            options.html(optionsValues);
            $('.ui-loader').hide();
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

// To Get Cities for appointment page
function GetCityAppt() {
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/GetCities',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="0">  MEETING PLACE </option>';

            $.each(response.FetchCitiesResult, function (index, value) {
                if (value.status == "success") {

                    optionsValues += '<option value="' + value.cityid + '">' + value.cityname + '</option>';
                }
            });
            var options = $('#ddlCity');
            options.html(optionsValues);
            $('.ui-loader').hide();
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

//to get project main image

function GetImage(projectid) {
    $('.ui-loader').show();

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/GetProjImage/' + projectid,
        success: function (response) {
            var img = $("#img_project");
            $.each(response.FetchProjImageResult, function (index, value) {
                if (value.status == "success") {
                    if (value.image != "" && value.image != null) {
                        img.attr("src", value.image);
                    }
                }
                else {
                    img.attr("src", "images/coming_soon.jpg");
                }
            });


        },
        failure: function (response) {
            alert(response.d);

        }
    });
}


// to Get ProjectMenu information
function DisplayProjectMenu(projectid) {
    $('.ui-loader').show();
    var div = '';

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/DisplayProjectMenu/' + projectid,
        success: function (response) {
            var link = '';
            var click = '';
            div = $("#ul_list");
            var img = $("#img_project");
            var divse = '<div data-role="navbar"><ul>';
            $.each(response.GetProjectMenuResult, function (index, value) {
                if (value.status == "success") {
                    if (value.image != "" && value.image != null) {
                        img.attr("src", value.image);
                    }
                    if (value.menutitle == "Features") {
                        link = 'project_features.html?menuid=' + value.menuid + '&projectid=' + value.projectid + '';
                    }
                    if (value.menutitle == "Plans") {
                        link = 'floor_plan.html?menuid=' + value.menuid + '&projectid=' + value.projectid + '';
                    }
                    if (value.menutitle == "Gallery") {
                        link = 'image_gallery.html?projectid=' + value.projectid + '';
                        click = "";
                    }
                    if (value.menutitle == "Construction Status") {
                        link = 'construction_status.html?projectid=' + value.projectid + '';
                        click = "";
                    }
                    if (value.menutitle == "Contact Us") {
                        link = 'project_contactus.html?projectid=' + value.projectid + '';
                        click = "";
                    }
                    $("#span_title_1").html(value.projecttitle + ' | <span id="span_title_2">' + value.cityname + '</span>');
                    divse += '<li><a href="' + link + '" class="ui-btn-active">' + value.menutitle + '</a></li>';
                }
                else {
                    divse += '<li class=""><a class="ui-btn-active no_img ui-link ui-btn" href="#"></a></li>';

                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: baseURLEOI + "RestServiceImpl.svc" + "/GetProjectName/" + projectid,
                        processData: false,
                        async: false,
                        success: function (response) {
                            $.each(response.FetchProjectNameResult, function (index, value) {
                                if (value.status == "success") {
                                    $("#span_title_1").html(value.projecttitle + ' | <span id="span_title_2">' + value.cityname + '</span>');
                                }
                            });
                        },
                        failure: function (response) {
                            alert('Fail');
                        }
                    });
                }
            });
            div.append(divse + "</ul></div>").trigger('create');
            img.one("load", function () {

            }).each(function () {
                setTimeout(function () {
                    // var nav_height = $('#projects_details .inner_pages_container.project_inner_wrap #ul_list .ui-navbar').height();
                    // var img_height = window.innerHeight - $('#projects_details .inner_pages_container').height();
                    // var other_height = $('.title').height() + $('.header').height() + $('#projects_details .appointment').height();
                    // $('#projects_details .inner_pages_container.project_inner_wrap #ul_list li a').css('height',((img_height + nav_height) - other_height)/3);
                    // console.log('45');]
                    //var calc_size = $('#projects_details .ui-panel-wrapper').height() - $('#projects_details .ui-content').height() - $('.ui-header').height();
                    var wnd_h = window.innerHeight;
                    var cont_h = $('#projects_details .ui-content').height();
                    if (wnd_h == cont_h) {

                    }
                    else {
                        var calc_size = $('#projects_details').height() - $('#projects_details .ui-content').height();
                        //var curr_height = $('#projects_details .ui-navbar .ui-link.ui-btn').innerHeight();
                        // $('#projects_details .ui-navbar .ui-link.ui-btn').css('padding-bottom', calc_size * 2);
                    }
                    $('.ui-loader').hide();
                }, 2010);
            });
            //$('.ui-loader').hide();
        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

// To Get All active Launches
function GetNewLaunch() {

    /*var interval = setInterval(function () {
        $.mobile.loading('show');
        clearInterval(interval);
    }, 1); */

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'CPProperties.svc/getbannerjson',
        processData: false,
        success: function (response) {
            var innertext = "";
            console.log(response)
            $.each(response.gethomeBannersResult, function (index, value) {
                if (value.status == "success") {
                    innertext += '<div class="item"><a href="project_details.html?projectid=' + value.relatedLink + '"><img class="lazyOwl" src="' + value.imagename + '" alt="' + value.title + '">';
                    innertext += '<h2>NEW LAUNCH</h2><div class="launch_place"><h3>' + value.title + ' - ' + value.cityname + '</h3><br><h4>EXPLORE NOW</h4><span class="arrow_a"><img src="images/arrow_white2.png"></span></div></a></div>';
                }
            });
            $(".owl-carousel").html(innertext);
            $("#new-owl-demo").owlCarousel({
                navigation: false,
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
                autoPlay: 3000,
                lazyLoad: true

            });
            setTimeout(function () {
                var a = $('.three_box').outerHeight();
                var b = $('.header').outerHeight();
                var e = $(".launch_place").height();
                var c = a + b;
                $("#new-owl-demo-wrapper").css('height', 'auto');
                $(".item").css("height", window.innerHeight - c + 1);
                $('.launch_new_container .item a > img').css('height', window.innerHeight - c - e);

                var d = window.innerHeight - c;

                var f = d - e;
                //$(".launch_place").css("top", f - 20);
                $('.ui-loader').hide();
                setTimeout(function () {
                    $('.three_box').show();
                    // $('#pre_launch_content_inline').simpledialog2({
                    //     callbackOpen: function () {
                    //         $('body .ui-simpledialog-screen-modal').addClass('full_opacity');
                    //         // var me = this;
                    //         // $('.ui-loader').hide();
                    //         // setTimeout(function () { me.close(); }, 2000);
                    //     }
                    // });
                    // //$('#pre_launch_content_inline').simpledialog2();
                    // $('.ui-simpledialog-screen').css('height', $(document).height());


                    // $('.preregister').click(function () {
                    //   //  alert('call external');
                    //     window.target = '_blank';

                    //     window.ChannelPartnerActivity.openURL("http://gplpartnerconnect.com/godrejpartnerconnect/register.aspx");
                    //     $('.preclose').trigger('click');
                    // });
                }, 100);

            }, 100);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

// To Get All active Launches for my account
function GetNewLaunchInfo() {

    var interval = setInterval(function () {
        $.mobile.loading('show');
        clearInterval(interval);
    }, 1);
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/getbannerjson',
        processData: false,
        success: function (response) {
            var innertext = "";
            $.each(response.gethomeBannersResult, function (index, value) {
                if (value.status == "success") {
                    innertext += ' <div class="item"><img src="' + value.imagename + '" alt="' + value.title + '">';
                    innertext += '<div class="my_account_ban_text"><h3>' + value.title + ' - ' + value.cityname + '</h3><h4>Here’s TO INFINITE OPPORTUNITIES</h4></div></div>';
                }
            });
            //$(".owl-carousel").html(innertext);
            // $("#my-account-owl-demo").owlCarousel({
            //     navigation: false,
            //     slideSpeed: 300,
            //     paginationSpeed: 400,
            //     singleItem: true
            // });
            $("#my-account-owl-demo-wrapper").css('height', 'auto');
            //setTimeout(function () {
            $('.ui-loader').hide();
            //}, 100);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

$(document).on('click', '.phone_chat ul li span a,.phone_chat ul li > a', function () {
    $(this).each(function () {
        var rel = $(this).attr('rel');

        makeCall(rel);

    });
});




//Project listing by city
function FillByCity(id) {
    localStorage.setItem("selectCityFilterId",  id);                        // Shreyas 6th July 2020
    if (id == 0) {
        DisplayGetProjects(localStorage.getItem("ptype"), $('#span_type').html());
    }
    else {

        var addstring = id + "/" + localStorage.getItem("ptype") + "/" + $('#span_type').html();

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: baseURLEOI + "RestServiceImpl.svc" + '/FetchProperty_ByCityId/' + addstring,
            async: false,
            success: function (response) {
                $("#ul_proj").empty();
                var ul = $("#ul_proj");
                ul.append(" ");
                var classname = "";
                var flag = "";
                $.each(response.GetProperty_ByCityIdResult, function (index, value) {

                    if (value.status == "success") {
                        if (value.title != 'null' && value.cityname != 'null' && value.projectid != 'null')

                            ul.append('<li id=""><a href="project_details.html?projectid=' + value.projectid + '" data-transition="slide">' + value.title + '<br><span>' + value.cityname + '</span></a></li>');
                        //   ul.append('<li id=""><a href="walk_in_summary.html?projectid=' + value.projectid + '" data-transition="slide">' + value.title + '<br><span>' + value.cityname + '</span></a></li>');
                        // <li id="li_' + value.projectid + '" class="' + classname + '"><a onclick="addfavourite(' + value.projectid + ', ' + localStorage.getItem("emplcontid") + ',' + flag + ');" href="javascript:void(0);">Star</a><a data-transition="slide" href="projectdetails.html?projectid=' + value.projectid + '">' + value.title + '<br><span>' + value.cityname + '</span></a></li>');
                    }
                    else
                        ul.append('<li style="text-align:center;">Properties not found!</li>');
                });
            },
            failure: function (response) {
                alert('Fail');
            }
        });
    }
}


// To Get ProjectsInnerDetails accoding to menuid and projectid
function GetInnerDetail(txtmenuid, txtprojectid) {
    //$('.ui-loader').show();

    var addstring = txtmenuid + "/" + txtprojectid;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/FetchInnerDetails/" + addstring,
        //async: false,
        success: function (response) {
        console.log(response);
        mstrPlanCount = response;         // Shreyas 30.07.2020 Coming_soon
            var img = $("#img_main");
            var div = $('#img_main_wrapper_overview');
            $.each(response.GetProjectsInnerDetailsResult, function (index, value) {
                if (value.status == "success") {
                    $('.ui-loader').hide();
                    if (value.menutitle == "Plans") {
                        $("#div_desc").html(value.projecttitle + "<span>" + value.cityname + "</span>");
                    }
                    else {
                        if (value.description != "" && value.description != null)
                            $("#div_desc").html(value.description);


                    }
                }
                if ($('#div_desc p').length <= 1) {
                    $('#read_more').hide();
                }
                $(document).on('click', '#read_more', function () {
                    $('#div_desc p').css('display', 'block');
                    $(this).hide();
                    $('.read_less').show();
                });
                $(document).on('click', '.read_less', function () {
                    $('#div_desc p').css('display', 'none');
                    $('#div_desc p:first-child').css('display', 'block');
                    $(this).hide();
                    $('#read_more').show();
                });
                // var showChar = 100;
                //             var ellipsestext = "...";
                //             var moretext = "more";
                //             var lesstext = "less";
                //             $('.more').each(function() {
                //                 var content = $(this).html();

                //                 if(content.length > showChar) {

                //                     var c = content.substr(0, showChar);
                //                     var h = content.substr(showChar-1, content.length - showChar);

                //                     var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                //                     $(this).html(html);
                //                 }
                //                 });
                //             $(".morelink").click(function(){
                //                 if($(this).hasClass("less")) {
                //                     $(this).removeClass("less");
                //                     $(this).html(moretext);
                //                 } else {
                //                     $(this).addClass("less");
                //                     $(this).html(lesstext);
                //                 }
                //                 $(this).parent().prev().toggle();
                //                 $(this).prev().toggle();
                //                 return false;
                //             });
                // $('#div_desc').readmore({
                //              speed: 75,
                //              lessLink: '<a href="#" class="read_less"> Read Less</a>',
                //              collapsedHeight: 77,
                //            });

            });


        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To get Projects features/Highlights
function GetProjectFetures(projectid) {
    //$('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/GetProjectFetures/" + projectid,
        processData: false,
        async: false,
        success: function (response) {
            var tbl = $("#tblhigh");
            console.log("GetProjectFetures", response.FetchProjectFeturesResult);
            $.each(response.FetchProjectFeturesResult, function (index, value) {
                if (value.status == "success") {
                    if (value.icon == "")
                        tbl.append('<div><span>' + value.description + '</span></div>');
                    else
                        tbl.append('<div><img src=' + value.icon + '><span>' + value.description + '</span></div>');

                    $("#tblhigh").one("load", function () {

                    }).each(function () {
                        /*setTimeout(function () {
                            $('.overview > div').each(function () {
                                var hhh = $(this).innerHeight();
                                $(this).find('img').css('margin-bottom', hhh / 2);
                            });
                        }, 1000);*/
                    });
                }
                setTimeout(function () {
                    //$('.ui-loader').hide();
                }, 1000);
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To get Project FloorPlans
function GetProjectFloorPlans(projectid) {
    //$('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/GetProjectFloorPlans/" + projectid,
        async: false,
        success: function (response) {
            //$('.ui-loader').show();
            console.log(response);
            flrPlanCount = response;
            var div = $("#divinner");
            $.each(response.FetchProjectFloorPlansResult, function (index, value) {
                if (value.status == "success") {
                    if (value.image == null || value.image == "" || value.image == '' || value.image == 'https://cp.godrejproperties.com/images/FloorPlans/') {
                        div.append("<div class='heading_div' >" + value.title + "<a class='view_plan_link' style='text-decoration:none;'>&nbsp;<img src='images/star.png' style='display:none;'></a></div>");
                    }
                    else {
                        div.append("<div class='heading_div only_one' >" + value.title + "<a class='view_plan_link restro_deal_ul2__img--main' data-author='' data-med-size='1024x1000' data-role='none' data-med='" + value.image + "' data-size='1600x1067' herf='" + value.image + "'>View Plan<img src='images/star.png' style='display:none;'></a></div>");
                    }

                    var addstring = projectid + "/" + value.id;
                    var divse = '<div class="container_div">';
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: baseURLEOI + "RestServiceImpl.svc" + "/DisplayProjectinnerFloorPlans/" + addstring,
                        async: false,
                        success: function (response) {
                            $.each(response.FetchProjectinnerFloorPlansResult, function (index, value) {
                                if (value.status == "success") {
                                    divse += "<div class='' >" + value.title + "<a class='view_plan_link restro_deal_ul2__img--main' data-author='' data-med-size='1024x683' data-role='none' data-med='" + value.image + "' data-size='1600x1067' herf='" + value.image + "'>View Plan<img src='images/star.png' style='display:none;'></a></div>";
                                }
                            });

                            div.append(divse + "</div>");
                            $('.ui-loader').hide();
                        },
                        failure: function (response) {
                        }
                    });
                    $('#img_main_wrapper').css('display', 'inline-block').css('min-height', '100%');

                    //$('.heading_div').each(function (e) {

                    // $('.heading_div').click(function (e) {
                    //      $('.container_div').stop().slideUp();
                    //      $('.heading_div').stop().removeClass('active');

                    //      if ($(this).next().is(":visible")) {
                    //          $(this).next().stop().slideUp();
                    //          $(this).stop().removeClass('active');
                    //      }
                    //      else {
                    //          $(this).next().stop().slideDown();
                    //          $(this).stop().addClass('active');
                    //      }

                    //      e.preventDefault();
                    //  });
                    //});

                }
            });
            // $('.heading_div').each(function (e) {

            //     $(this).click(function (e) {
            //         $('.container_div').stop().slideUp();
            //         $('.heading_div').stop().removeClass('active');

            //         if ($(this).next().is(":visible")) {
            //             $(this).next().stop().slideUp();
            //             $(this).stop().removeClass('active');
            //         }
            //         else {
            //             $(this).next().stop().slideDown();
            //             $(this).stop().addClass('active');
            //         }

            //         //e.preventDefault();
            //     });
            // });
            $(document).on('click', '.heading_div', function (e) {
                $(this).each(function (e) {
                    $('.container_div').stop().slideUp();
                    $('.heading_div').stop().removeClass('active');

                    if ($(this).next().is(":visible")) {
                        $(this).next().stop().slideUp();
                        $(this).stop().removeClass('active');
                    }
                    else {
                        $(this).next().stop().slideDown();
                        $(this).stop().addClass('active');
                    }

                    e.preventDefault();
                });
                //e.preventDefault();
            });

            // (function () {
            //     $('.pswp__img').css('display', 'none !important');
            //     var initPhotoSwipeFromDOM = function (gallerySelector) {

            //         var parseThumbnailElements = function (el) {
            //             var thumbElements = el.childNodes,
            //                 numNodes = thumbElements.length,
            //                 items = [],
            //                 el,
            //                 childElements,
            //                 thumbnailEl,
            //                 size,
            //                 item;

            //             for (var i = 0; i < numNodes; i++) {
            //                 el = thumbElements[i];

            //                 // include only element nodes
            //                 if (el.nodeType !== 1) {
            //                     continue;
            //                 }

            //                 childElements = el.children;

            //                 size = el.getAttribute('data-size').split('x');

            //                 // create slide object
            //                 item = {
            //                     src: el.getAttribute('href'),
            //                     w: parseInt(size[0], 10),
            //                     h: parseInt(size[1], 10),
            //                     author: el.getAttribute('data-author')
            //                 };

            //                 item.el = el; // save link to element for getThumbBoundsFn

            //                 if (childElements.length > 0) {
            //                     item.msrc = childElements[0].getAttribute('src'); // thumbnail url
            //                     if (childElements.length > 1) {
            //                         item.title = childElements[1].innerHTML; // caption (contents of figure)
            //                     }
            //                 }


            //                 var mediumSrc = el.getAttribute('data-med');
            //                 if (mediumSrc) {
            //                     size = el.getAttribute('data-med-size').split('x');
            //                     // "medium-sized" image
            //                     item.m = {
            //                         src: mediumSrc,
            //                         w: parseInt(size[0], 10),
            //                         h: parseInt(size[1], 10)
            //                     };
            //                 }
            //                 // original image
            //                 item.o = {
            //                     src: item.src,
            //                     w: item.w,
            //                     h: item.h
            //                 };

            //                 items.push(item);
            //             }

            //             return items;
            //         };

            //         // find nearest parent element
            //         var closest = function closest(el, fn) {
            //             return el && (fn(el) ? el : closest(el.parentNode, fn));
            //         };

            //         var onThumbnailsClick = function (e) {
            //             e = e || window.event;
            //             e.preventDefault ? e.preventDefault() : e.returnValue = false;

            //             var eTarget = e.target || e.srcElement;

            //             var clickedListItem = closest(eTarget, function (el) {
            //                 return el.tagName === 'A';
            //             });

            //             if (!clickedListItem) {
            //                 return;
            //             }

            //             var clickedGallery = clickedListItem.parentNode;

            //             var childNodes = clickedListItem.parentNode.childNodes,
            //                 numChildNodes = childNodes.length,
            //                 nodeIndex = 0,
            //                 index;

            //             for (var i = 0; i < numChildNodes; i++) {
            //                 if (childNodes[i].nodeType !== 1) {
            //                     continue;
            //                 }

            //                 if (childNodes[i] === clickedListItem) {
            //                     index = nodeIndex;
            //                     break;
            //                 }
            //                 nodeIndex++;
            //             }

            //             if (index >= 0) {
            //                 openPhotoSwipe(index, clickedGallery);
            //             }
            //             return false;
            //         };

            //         var photoswipeParseHash = function () {
            //             var hash = window.location.hash.substring(1),
            //             params = {};

            //             if (hash.length < 5) { // pid=1
            //                 return params;
            //             }

            //             var vars = hash.split('&');
            //             for (var i = 0; i < vars.length; i++) {
            //                 if (!vars[i]) {
            //                     continue;
            //                 }
            //                 var pair = vars[i].split('=');
            //                 if (pair.length < 2) {
            //                     continue;
            //                 }
            //                 params[pair[0]] = pair[1];
            //             }

            //             if (params.gid) {
            //                 params.gid = parseInt(params.gid, 10);
            //             }

            //             if (!params.hasOwnProperty('pid')) {
            //                 return params;
            //             }
            //             params.pid = parseInt(params.pid, 10);
            //             return params;
            //         };

            //         var openPhotoSwipe = function (index, galleryElement, disableAnimation) {

            //             var pswpElement = document.querySelectorAll('.pswp')[0],
            //                 gallery,
            //                 options,
            //                 items;

            //             items = parseThumbnailElements(galleryElement);

            //             // define options (if needed)
            //             options = {
            //                 index: index,

            //                 galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            //                 getThumbBoundsFn: function (index) {
            //                     // See Options->getThumbBoundsFn section of docs for more info
            //                     var thumbnail = items[index].el.children[0],
            //                         pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
            //                         rect = thumbnail.getBoundingClientRect();

            //                     return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
            //                 },

            //                 addCaptionHTMLFn: function (item, captionEl, isFake) {
            //                     if (!item.title) {
            //                         captionEl.children[0].innerText = '';
            //                         return false;
            //                     }
            //                     captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
            //                     return true;
            //                 },
            //                 closeOnScroll: false,
            //                 closeOnVerticalDrag: false

            //             };

            //             var radios = document.getElementsByName('gallery-style');
            //             for (var i = 0, length = radios.length; i < length; i++) {
            //                 if (radios[i].checked) {
            //                     if (radios[i].id == 'radio-all-controls') {

            //                     } else if (radios[i].id == 'radio-minimal-black') {
            //                         options.mainClass = 'pswp--minimal--dark';
            //                         options.barsSize = { top: 0, bottom: 0 };
            //                         options.captionEl = false;
            //                         options.fullscreenEl = false;
            //                         options.shareEl = false;
            //                         options.bgOpacity = 0.85;
            //                         options.tapToClose = true;
            //                         options.tapToToggleControls = false;
            //                     }
            //                     break;
            //                 }
            //             }

            //             if (disableAnimation) {
            //                 options.showAnimationDuration = 0;
            //             }

            //             // Pass data to PhotoSwipe and initialize it
            //             gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

            //             // see: http://photoswipe.com/documentation/responsive-images.html
            //             var realViewportWidth,
            //                 useLargeImages = false,
            //                 firstResize = true,
            //                 imageSrcWillChange;

            //             gallery.listen('beforeResize', function () {

            //                 var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
            //                 dpiRatio = Math.min(dpiRatio, 2.5);
            //                 realViewportWidth = gallery.viewportSize.x * dpiRatio;


            //                 if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
            //                     if (!useLargeImages) {
            //                         useLargeImages = true;
            //                         imageSrcWillChange = true;
            //                     }

            //                 } else {
            //                     if (useLargeImages) {
            //                         useLargeImages = false;
            //                         imageSrcWillChange = true;
            //                     }
            //                 }

            //                 if (imageSrcWillChange && !firstResize) {
            //                     gallery.invalidateCurrItems();
            //                 }

            //                 if (firstResize) {
            //                     firstResize = false;
            //                 }

            //                 imageSrcWillChange = false;

            //             });

            //             gallery.listen('gettingData', function (index, item) {
            //                 if (useLargeImages) {
            //                     item.src = item.o.src;
            //                     item.w = item.o.w;
            //                     item.h = item.o.h;
            //                 } else {
            //                     item.src = item.m.src;
            //                     item.w = item.m.w;
            //                     item.h = item.m.h;
            //                 }
            //             });

            //             gallery.init();
            //             // $('.gallery_wrap').show();
            //             // $('.gallery_wrap').css('top',$('.floorplan').offset().top);

            //             //  var ua = navigator.userAgent;
            //             // var event1 = (ua.match(/iPad/i)) ? 'touchstart' : 'mouseup';
            //             // $('.close_btn_lightbox').bind(event1, function() {
            //             //      $('.gallery_wrap').hide().css('top','auto');
            //             //  });
            //             // $('.imagegallery a').bind(event1, function() {
            //             //      $('.gallery_wrap').show().css('top',$('.gallery_solo').offset().top);
            //             //  });
            //         };

            //         // select all gallery elements
            //         var galleryElements = document.querySelectorAll(gallerySelector);
            //         for (var i = 0, l = galleryElements.length; i < l; i++) {
            //             galleryElements[i].setAttribute('data-pswp-uid', i + 1);
            //             galleryElements[i].onclick = onThumbnailsClick;
            //         }

            //         // Parse URL and open gallery if it contains #&pid=3&gid=1
            //         var hashData = photoswipeParseHash();
            //         if (hashData.pid > 0 && hashData.gid > 0) {
            //             openPhotoSwipe(hashData.pid - 1, galleryElements[hashData.gid - 1], true);
            //         }
            //     };

            //     initPhotoSwipeFromDOM('.floorplan a');

            // })();


        },
        failure: function (response) {
            alert('Fail');
        }
    });

    if(mstrPlanCount == null && flrPlanCount.FetchProjectFloorPlansResult.length == 1 &&  flrPlanCount.FetchProjectFloorPlansResult[0]["status"] == "fail")
    {
        $('.ui-loader').hide();
        $(".img_wrapper").append('<div class="img_wrapper"><img id="img_project" src="images/coming_soon.jpg" width="100%" height="auto"></div>');
    }
    if(mstrPlanCount != null && mstrPlanCount.GetProjectsInnerDetailsResult.length == 1 && flrPlanCount.FetchProjectFloorPlansResult.length == 1 &&
       mstrPlanCount.GetProjectsInnerDetailsResult[0]["status"] == "fail" && flrPlanCount.FetchProjectFloorPlansResult[0]["status"] == "fail")
    {
        $('.ui-loader').hide();
        $(".img_wrapper").append('<div class="img_wrapper"><img id="img_project" src="images/coming_soon.jpg"  width="100%" height="auto"></div>');
    }
}

//get project images
function GetProjectImages(projectid) {
    var interval = setInterval(function () {
        $.mobile.loading('show');
        clearInterval(interval);
    }, 1);
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/GetGallery/" + projectid,
        async: false,
        success: function (response) {
        console.log(response);
            // Shreyas 24.07.2020 Coming_soon_issue
            if(response.FetchGalleryResult.length == 1 && response.FetchGalleryResult[0]["status"] == "fail")
            {
                $(".img_wrapper").append('<div class="img_wrapper"><img id="img_project" src="images/coming_soon.jpg"></div>');
            }
            else
            {
                $.each(response.FetchGalleryResult, function (index, value) {
                    if (value.status == "success") {
                        $("#owl_image_gallery").append("<div><img data-src='" + value.Image + "' rel='" + value.Image + "' class='lazyOwl'></div>");
                    }
                });
            }
            var owl = $("#owl_image_gallery");

            owl.owlCarousel({
                items: 4, //10 items above 1000px browser width
                itemsDesktop: [1000, 4], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 4], // betweem 900px and 601px
                itemsTablet: [600, 2], //2 items between 600 and 0
                itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
                pagination: false,
                lazyLoad: true,
                navigation: true,
                afterInit: function (elem) {
                    //var that = this
                    //that.owlControls.prependTo(elem)
                    $(document).on('click', '#image_gallery .owl-carousel .owl-item img', function () {
                        $(this).each(function () {
                            $(this).parent().addClass('active_a');
                            var src_name = $(this).attr('rel');
                            $('#img_gallery_big').attr('src', src_name);
                            $('#img_gallery_big').parent().attr('data-med', src_name);
                            // $('#img_gallery_big').parent().attr('href', src_name);
                        });
                    });
                }
            });
            $('#image_gallery .owl-carousel .owl-item:first-child img').click();
            // (function () {
            //     $('.pswp__img').css('display', 'none !important');
            //     var initPhotoSwipeFromDOM = function (gallerySelector) {

            //         var parseThumbnailElements = function (el) {
            //             var thumbElements = el.childNodes,
            //                 numNodes = thumbElements.length,
            //                 items = [],
            //                 el,
            //                 childElements,
            //                 thumbnailEl,
            //                 size,
            //                 item;

            //             for (var i = 0; i < numNodes; i++) {
            //                 el = thumbElements[i];

            //                 // include only element nodes
            //                 if (el.nodeType !== 1) {
            //                     continue;
            //                 }

            //                 childElements = el.children;

            //                 size = el.getAttribute('data-size').split('x');

            //                 // create slide object
            //                 item = {
            //                     src: el.getAttribute('href'),
            //                     w: parseInt(size[0], 10),
            //                     h: parseInt(size[1], 10),
            //                     author: el.getAttribute('data-author')
            //                 };

            //                 item.el = el; // save link to element for getThumbBoundsFn

            //                 if (childElements.length > 0) {
            //                     item.msrc = childElements[0].getAttribute('src'); // thumbnail url
            //                     if (childElements.length > 1) {
            //                         item.title = childElements[1].innerHTML; // caption (contents of figure)
            //                     }
            //                 }


            //                 var mediumSrc = el.getAttribute('data-med');
            //                 if (mediumSrc) {
            //                     size = el.getAttribute('data-med-size').split('x');
            //                     // "medium-sized" image
            //                     item.m = {
            //                         src: mediumSrc,
            //                         w: parseInt(size[0], 10),
            //                         h: parseInt(size[1], 10)
            //                     };
            //                 }
            //                 // original image
            //                 item.o = {
            //                     src: item.src,
            //                     w: item.w,
            //                     h: item.h
            //                 };

            //                 items.push(item);
            //             }

            //             return items;
            //         };

            //         // find nearest parent element
            //         var closest = function closest(el, fn) {
            //             return el && (fn(el) ? el : closest(el.parentNode, fn));
            //         };

            //         var onThumbnailsClick = function (e) {
            //             e = e || window.event;
            //             e.preventDefault ? e.preventDefault() : e.returnValue = false;

            //             var eTarget = e.target || e.srcElement;

            //             var clickedListItem = closest(eTarget, function (el) {
            //                 return el.tagName === 'A';
            //             });

            //             if (!clickedListItem) {
            //                 return;
            //             }

            //             var clickedGallery = clickedListItem.parentNode;

            //             var childNodes = clickedListItem.parentNode.childNodes,
            //                 numChildNodes = childNodes.length,
            //                 nodeIndex = 0,
            //                 index;

            //             for (var i = 0; i < numChildNodes; i++) {
            //                 if (childNodes[i].nodeType !== 1) {
            //                     continue;
            //                 }

            //                 if (childNodes[i] === clickedListItem) {
            //                     index = nodeIndex;
            //                     break;
            //                 }
            //                 nodeIndex++;
            //             }

            //             if (index >= 0) {
            //                 openPhotoSwipe(index, clickedGallery);
            //             }
            //             return false;
            //         };

            //         var photoswipeParseHash = function () {
            //             var hash = window.location.hash.substring(1),
            //             params = {};

            //             if (hash.length < 5) { // pid=1
            //                 return params;
            //             }

            //             var vars = hash.split('&');
            //             for (var i = 0; i < vars.length; i++) {
            //                 if (!vars[i]) {
            //                     continue;
            //                 }
            //                 var pair = vars[i].split('=');
            //                 if (pair.length < 2) {
            //                     continue;
            //                 }
            //                 params[pair[0]] = pair[1];
            //             }

            //             if (params.gid) {
            //                 params.gid = parseInt(params.gid, 10);
            //             }

            //             if (!params.hasOwnProperty('pid')) {
            //                 return params;
            //             }
            //             params.pid = parseInt(params.pid, 10);
            //             return params;
            //         };

            //         var openPhotoSwipe = function (index, galleryElement, disableAnimation) {

            //             var pswpElement = document.querySelectorAll('.pswp')[0],
            //                 gallery,
            //                 options,
            //                 items;

            //             items = parseThumbnailElements(galleryElement);

            //             // define options (if needed)
            //             options = {
            //                 index: index,

            //                 galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            //                 getThumbBoundsFn: function (index) {
            //                     // See Options->getThumbBoundsFn section of docs for more info
            //                     var thumbnail = items[index].el.children[0],
            //                         pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
            //                         rect = thumbnail.getBoundingClientRect();

            //                     return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
            //                 },

            //                 addCaptionHTMLFn: function (item, captionEl, isFake) {
            //                     if (!item.title) {
            //                         captionEl.children[0].innerText = '';
            //                         return false;
            //                     }
            //                     captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
            //                     return true;
            //                 },
            //                 closeOnScroll: false,
            //                 closeOnVerticalDrag: false

            //             };

            //             var radios = document.getElementsByName('gallery-style');
            //             for (var i = 0, length = radios.length; i < length; i++) {
            //                 if (radios[i].checked) {
            //                     if (radios[i].id == 'radio-all-controls') {

            //                     } else if (radios[i].id == 'radio-minimal-black') {
            //                         options.mainClass = 'pswp--minimal--dark';
            //                         options.barsSize = { top: 0, bottom: 0 };
            //                         options.captionEl = false;
            //                         options.fullscreenEl = false;
            //                         options.shareEl = false;
            //                         options.bgOpacity = 0.85;
            //                         options.tapToClose = true;
            //                         options.tapToToggleControls = false;
            //                     }
            //                     break;
            //                 }
            //             }

            //             if (disableAnimation) {
            //                 options.showAnimationDuration = 0;
            //             }

            //             // Pass data to PhotoSwipe and initialize it
            //             gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

            //             // see: http://photoswipe.com/documentation/responsive-images.html
            //             var realViewportWidth,
            //                 useLargeImages = false,
            //                 firstResize = true,
            //                 imageSrcWillChange;

            //             gallery.listen('beforeResize', function () {

            //                 var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
            //                 dpiRatio = Math.min(dpiRatio, 2.5);
            //                 realViewportWidth = gallery.viewportSize.x * dpiRatio;


            //                 if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
            //                     if (!useLargeImages) {
            //                         useLargeImages = true;
            //                         imageSrcWillChange = true;
            //                     }

            //                 } else {
            //                     if (useLargeImages) {
            //                         useLargeImages = false;
            //                         imageSrcWillChange = true;
            //                     }
            //                 }

            //                 if (imageSrcWillChange && !firstResize) {
            //                     gallery.invalidateCurrItems();
            //                 }

            //                 if (firstResize) {
            //                     firstResize = false;
            //                 }

            //                 imageSrcWillChange = false;

            //             });

            //             gallery.listen('gettingData', function (index, item) {
            //                 if (useLargeImages) {
            //                     item.src = item.o.src;
            //                     item.w = item.o.w;
            //                     item.h = item.o.h;
            //                 } else {
            //                     item.src = item.m.src;
            //                     item.w = item.m.w;
            //                     item.h = item.m.h;
            //                 }
            //             });

            //             gallery.init();
            //             // $('.gallery_wrap').show();
            //             // $('.gallery_wrap').css('top',$('.floorplan').offset().top);

            //             //  var ua = navigator.userAgent;
            //             // var event1 = (ua.match(/iPad/i)) ? 'touchstart' : 'mouseup';
            //             // $('.close_btn_lightbox').bind(event1, function() {
            //             //      $('.gallery_wrap').hide().css('top','auto');
            //             //  });
            //             // $('.imagegallery a').bind(event1, function() {
            //             //      $('.gallery_wrap').show().css('top',$('.gallery_solo').offset().top);
            //             //  });
            //         };

            //         // select all gallery elements
            //         var galleryElements = document.querySelectorAll(gallerySelector);
            //         for (var i = 0, l = galleryElements.length; i < l; i++) {
            //             galleryElements[i].setAttribute('data-pswp-uid', i + 1);
            //             galleryElements[i].onclick = onThumbnailsClick;
            //         }

            //         // Parse URL and open gallery if it contains #&pid=3&gid=1
            //         var hashData = photoswipeParseHash();
            //         if (hashData.pid > 0 && hashData.gid > 0) {
            //             openPhotoSwipe(hashData.pid - 1, galleryElements[hashData.gid - 1], true);
            //         }
            //     };

            //     initPhotoSwipeFromDOM('#img_gallery_big');

            // })();
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

//Book an appointment form by ved on 25-06-2015

function BookanAppointment() {
    var name = $('#username').val();
    var email = $('#emailid').val();
    var mobile = $('#mobile').val();
    var company = $('#company').val();
    var city = $('#ddlCity :selected').val();
    var projectid = $('#ddlproject :selected').val();
    var date = $('#datepicker').val();
    var comment = $('#comment').val();
    var brokerid = localStorage.getItem("brokerid");


    if (name == null || name == "" || name.length > 100) {
        $('#error_content').empty().html('Please enter name.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2({
                callbackClose: function () {
                    var me = this;
                    $('.ui-loader').hide();
                    setTimeout(function () { me.close(); }, 2000);
                }
            });
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }
    var chkname = alphanumeric_check(name);

    if (chkname == 'NO') {
        $('#error_content').empty().html('Please enter valid name.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2({
                callbackClose: function () {
                    var me = this;
                    $('.ui-loader').hide();
                    setTimeout(function () { me.close(); }, 2000);
                }
            });
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }


    if (email == null || email == "" || email.lenght > 100) {
        $('#error_content').empty().html('Please enter email ID.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2({
                callbackClose: function () {
                    var me = this;
                    $('.ui-loader').hide();
                    setTimeout(function () { me.close(); }, 2000);
                }
            });
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }
    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!filter.test(email)) {
        //alert('Please provide a valid email address');
        $('#error_content').empty().html('Please enter valid email id.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (mobile == null || mobile == "") {
        //alert("Please Enter Mobile Number.");
        $('#error_content').empty().html('Please enter mobile number.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (mobile.length < 10 || mobile.length > 10) {
        $('#error_content').empty().html('Mobile number should be 10 digits.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    var numPattern = /\d+(,\d{1,3})?/;

    if (!numPattern.test(mobile)) {
        //alert('Please Enter Numeric Value');
        $('#error_content').empty().html('Please enter numeric value.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (company == null || company == "" || company.length > 100) {
        $('#error_content').empty().html('Please enter valid company name.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2({
                callbackClose: function () {
                    var me = this;
                    $('.ui-loader').hide();
                    setTimeout(function () { me.close(); }, 2000);
                }
            });
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (city == '0') {
        //alert("Please Select the Project Title.");
        $('#error_content').empty().html('Please select meeting place');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (projectid == ' SELECT PROJECT* ') {
        //alert("Please Select the Project Title.");
        $('#error_content').empty().html('Please select project');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (date == null || date == "") {
        $('#error_content').empty().html('Please enter appointment date');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }
    if (comment == null || comment == "" || comment == "COMMENTS") {
        $('#error_content').empty().html('Please enter comments');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }
    if (comment.lenght > 500) {
        $('#error_content').empty().html('Please enter comments');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    //var date1 = date.split('/');
    //var apptdate = date1[1] + "-" + date1[0] + "-" + date1[2];

    var addstring = name + "/" + mobile + "/" + email + "/" + company + "/" + city + "/" + projectid + "/" + comment + "/" + brokerid + "/" + date;
    //var addstring = name + "/" + email + "/" + mobile + "/" + company + "/" + city + "/" + projectid + "/" + comment + "/" + brokerid + "/" + date;

    $('.ui-loader').show();
    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        url: baseURLEOI + 'RestServiceImpl.svc/InsertAppointment/' + addstring,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $.each(response.InsertApptResult, function (index, value) {
                if (value.status == "success") {

                    //$('#mobile').val(0);
                    //$('#company').val('');
                    $('#ddlCity').val(0);
                    //$('#ddlproject').val(0);
                    $('#datepicker').val('');
                    $('#comment').val('');

                    if (localStorage.getItem("projectid") == null) {
                        fillproject();
                    }
                    else {
                        fillprojectdetails();
                    }
                    $('#error_content').empty().html('Thank you for showing interest. Please check Site Visit Request into My Account.');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2({
                            callbackClose: function () {
                                var me = this;
                                $('.ui-loader').hide();
                                setTimeout(function () { me.close(); }, 2000);
                            }
                        });
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 1000);
                    //$('.ui-loader').hide();
                    return false;

                }
                else {
                    $('#error_content').empty().html('Oops!! Something went wrong.');
                    //setTimeout(function () {
                    $('#enquiry_inline').simpledialog2({
                        callbackClose: function () {
                            var me = this;
                            $('.ui-loader').hide();
                            setTimeout(function () { me.close(); }, 2000);
                        }
                    });
                    $('.ui-simpledialog-screen').css('height', $(document).height());
                    //}, 1000);
                    //$('.ui-loader').hide();
                    return false;
                }
            });
        },

        failure: function (response) {
            alert('Fail');
        }
    });
}

// to get offers and schemes
function GetOffers() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc/DisplayOffers",
        async: false,
        success: function (response) {
            var ul = $("#ul_offers");
            $.each(response.GetOffersResult, function (index, value) {
                if (value.status == "success") {
                    ul.append('<li><a href="offer_details.html?offerid=' + value.offerid + '"><span><img src="' + value.image + '"></span><div class="text">' + value.title + '</div></a></li>')
                }
                else
                    ul.append('<li style="text-align: center; padding: 8px 0px; font-size:18px;">Coming soon</li>');
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// to get offers and schemes inners
function GetOfferInner(offerid) {
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc/DisplayOfferdetails/" + offerid,
        async: false,
        success: function (response) {
            var img = $("#offer_detail_img");
            $.each(response.GetOffersdetailsResult, function (index, value) {
                if (value.status == "success") {
                    $("#title").html(value.title);
                    $("#text_inner").html(value.description);
                    if (value.image != "" && value.image != null) {
                        img.attr("src", value.image);
                        img.parent().attr('data-med', value.image);
                        img.parent().attr('href', value.image);
                    }

                }
            });
            // if($('.offer_detail_wrapper #text p').length <= 1){
            //   $('.offer_detail_wrapper #read_more').hide();
            // }
            // $(document).on('click','#read_more',function(){
            //   $('.offer_detail_wrapper #text p').css('display','block');
            //   $('.offer_detail_wrapper #text table').css('display','table');
            //   $(this).hide();
            //   $('.offer_detail_wrapper .read_less').show();
            // });
            // $(document).on('click','.offer_detail_wrapper .read_less',function(){
            //   $('.offer_detail_wrapper #text p,.offer_detail_wrapper #text table').css('display','none');
            //   $('.offer_detail_wrapper #text p:first-child').css('display','block');
            //   $(this).hide();
            //   $('.offer_detail_wrapper #read_more').show();
            // });

            //
            (function () {
                $('.pswp__img').css('display', 'none !important');
                var initPhotoSwipeFromDOM = function (gallerySelector) {

                    var parseThumbnailElements = function (el) {
                        var thumbElements = el.childNodes,
                            numNodes = thumbElements.length,
                            items = [],
                            el,
                            childElements,
                            thumbnailEl,
                            size,
                            item;

                        for (var i = 0; i < numNodes; i++) {
                            el = thumbElements[i];

                            // include only element nodes
                            if (el.nodeType !== 1) {
                                continue;
                            }

                            childElements = el.children;

                            size = el.getAttribute('data-size').split('x');

                            // create slide object
                            item = {
                                src: el.getAttribute('href'),
                                w: parseInt(size[0], 10),
                                h: parseInt(size[1], 10),
                                author: el.getAttribute('data-author')
                            };

                            item.el = el; // save link to element for getThumbBoundsFn

                            if (childElements.length > 0) {
                                item.msrc = childElements[0].getAttribute('src'); // thumbnail url
                                if (childElements.length > 1) {
                                    item.title = childElements[1].innerHTML; // caption (contents of figure)
                                }
                            }


                            var mediumSrc = el.getAttribute('data-med');
                            if (mediumSrc) {
                                size = el.getAttribute('data-med-size').split('x');
                                // "medium-sized" image
                                item.m = {
                                    src: mediumSrc,
                                    w: parseInt(size[0], 10),
                                    h: parseInt(size[1], 10)
                                };
                            }
                            // original image
                            item.o = {
                                src: item.src,
                                w: item.w,
                                h: item.h
                            };

                            items.push(item);
                        }

                        return items;
                    };

                    // find nearest parent element
                    var closest = function closest(el, fn) {
                        return el && (fn(el) ? el : closest(el.parentNode, fn));
                    };

                    var onThumbnailsClick = function (e) {
                        e = e || window.event;
                        e.preventDefault ? e.preventDefault() : e.returnValue = false;

                        var eTarget = e.target || e.srcElement;

                        var clickedListItem = closest(eTarget, function (el) {
                            return el.tagName === 'A';
                        });

                        if (!clickedListItem) {
                            return;
                        }

                        var clickedGallery = clickedListItem.parentNode;

                        var childNodes = clickedListItem.parentNode.childNodes,
                            numChildNodes = childNodes.length,
                            nodeIndex = 0,
                            index;

                        for (var i = 0; i < numChildNodes; i++) {
                            if (childNodes[i].nodeType !== 1) {
                                continue;
                            }

                            if (childNodes[i] === clickedListItem) {
                                index = nodeIndex;
                                break;
                            }
                            nodeIndex++;
                        }

                        if (index >= 0) {
                            openPhotoSwipe(index, clickedGallery);
                        }
                        return false;
                    };

                    var photoswipeParseHash = function () {
                        var hash = window.location.hash.substring(1),
                        params = {};

                        if (hash.length < 5) { // pid=1
                            return params;
                        }

                        var vars = hash.split('&');
                        for (var i = 0; i < vars.length; i++) {
                            if (!vars[i]) {
                                continue;
                            }
                            var pair = vars[i].split('=');
                            if (pair.length < 2) {
                                continue;
                            }
                            params[pair[0]] = pair[1];
                        }

                        if (params.gid) {
                            params.gid = parseInt(params.gid, 10);
                        }

                        if (!params.hasOwnProperty('pid')) {
                            return params;
                        }
                        params.pid = parseInt(params.pid, 10);
                        return params;
                    };

                    var openPhotoSwipe = function (index, galleryElement, disableAnimation) {

                        var pswpElement = document.querySelectorAll('.pswp')[0],
                            gallery,
                            options,
                            items;

                        items = parseThumbnailElements(galleryElement);

                        // define options (if needed)
                        options = {
                            index: index,

                            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                            getThumbBoundsFn: function (index) {
                                // See Options->getThumbBoundsFn section of docs for more info
                                var thumbnail = items[index].el.children[0],
                                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                    rect = thumbnail.getBoundingClientRect();

                                return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
                            },

                            addCaptionHTMLFn: function (item, captionEl, isFake) {
                                if (!item.title) {
                                    captionEl.children[0].innerText = '';
                                    return false;
                                }
                                captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
                                return true;
                            },
                            closeOnScroll: false,
                            closeOnVerticalDrag: false

                        };

                        var radios = document.getElementsByName('gallery-style');
                        for (var i = 0, length = radios.length; i < length; i++) {
                            if (radios[i].checked) {
                                if (radios[i].id == 'radio-all-controls') {

                                } else if (radios[i].id == 'radio-minimal-black') {
                                    options.mainClass = 'pswp--minimal--dark';
                                    options.barsSize = { top: 0, bottom: 0 };
                                    options.captionEl = false;
                                    options.fullscreenEl = false;
                                    options.shareEl = false;
                                    options.bgOpacity = 0.85;
                                    options.tapToClose = true;
                                    options.tapToToggleControls = false;
                                }
                                break;
                            }
                        }

                        if (disableAnimation) {
                            options.showAnimationDuration = 0;
                        }

                        // Pass data to PhotoSwipe and initialize it
                        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

                        // see: http://photoswipe.com/documentation/responsive-images.html
                        var realViewportWidth,
                            useLargeImages = false,
                            firstResize = true,
                            imageSrcWillChange;

                        gallery.listen('beforeResize', function () {

                            var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
                            dpiRatio = Math.min(dpiRatio, 2.5);
                            realViewportWidth = gallery.viewportSize.x * dpiRatio;


                            if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
                                if (!useLargeImages) {
                                    useLargeImages = true;
                                    imageSrcWillChange = true;
                                }

                            } else {
                                if (useLargeImages) {
                                    useLargeImages = false;
                                    imageSrcWillChange = true;
                                }
                            }

                            if (imageSrcWillChange && !firstResize) {
                                gallery.invalidateCurrItems();
                            }

                            if (firstResize) {
                                firstResize = false;
                            }

                            imageSrcWillChange = false;

                        });

                        gallery.listen('gettingData', function (index, item) {
                            if (useLargeImages) {
                                item.src = item.o.src;
                                item.w = item.o.w;
                                item.h = item.o.h;
                            } else {
                                item.src = item.m.src;
                                item.w = item.m.w;
                                item.h = item.m.h;
                            }
                        });

                        gallery.init();
                        // $('.gallery_wrap').show();
                        // $('.gallery_wrap').css('top',$('.floorplan').offset().top);

                        //  var ua = navigator.userAgent;
                        // var event1 = (ua.match(/iPad/i)) ? 'touchstart' : 'mouseup';
                        // $('.close_btn_lightbox').bind(event1, function() {
                        //      $('.gallery_wrap').hide().css('top','auto');
                        //  });
                        // $('.imagegallery a').bind(event1, function() {
                        //      $('.gallery_wrap').show().css('top',$('.gallery_solo').offset().top);
                        //  });
                    };

                    // select all gallery elements
                    var galleryElements = document.querySelectorAll(gallerySelector);
                    for (var i = 0, l = galleryElements.length; i < l; i++) {
                        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
                        galleryElements[i].onclick = onThumbnailsClick;
                    }

                    // Parse URL and open gallery if it contains #&pid=3&gid=1
                    var hashData = photoswipeParseHash();
                    if (hashData.pid > 0 && hashData.gid > 0) {
                        openPhotoSwipe(hashData.pid - 1, galleryElements[hashData.gid - 1], true);
                    }
                };

                initPhotoSwipeFromDOM('#offer_detail_img');

            })();

            setTimeout(function () {
                $('.ui-loader').hide();
            }, 100);

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To display project list
function DisplayProperty() {
    $('.ui-loader').hide();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc/DisplayProperty",
        async: false,
        success: function (response) {
            $("#ul_list").empty();
            var ul = $("#ul_list");
            $.each(response.GetPropertyResult, function (index, value) {
                if (value.status == "success") {
                    //ul.append('<li><a href="property_details.html?propertyid=' + value.propertyid + '"><span><img src="' + value.image + '"></span><div class="text"><h1>' + value.title + '</h1>' + value.description + '</div></a></li>');
                    ul.append('<li><a href="property_details.html?propertyid=' + value.propertyid + '"><span><img src="' + value.image + '"></span><div class="text"><h1>' + value.title + '</h1></div></a></li>');

                    $('#ul_list img').one("load", function () {

                    }).each(function () {
                        $('.ui-loader').hide();
                    });
                }
                else
                    ul.append('<li style="text-align:center; padding:10px 0 0;">No updates available</li>');
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

//Property listing by city
function PropertyByCity(id) {

    if (id == 0) {
        DisplayProperty();
    }
    else {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: baseURLEOI + "RestServiceImpl.svc/DisplayPropertyByCity/" + id,
            async: false,
            success: function (response) {
                $("#ul_list").empty();
                var ul = $("#ul_list");
                ul.append(" ");
                var classname = "";
                var flag = "";
                $.each(response.GetPropertyByCityResult, function (index, value) {
                    if (value.status == "success") {
                        ul.append('<li><a href="property_details.html?propertyid=' + value.propertyid + '"><span><img src="' + value.image + '"></span><div class="text"><h1>' + value.title + '</h1></div></a></li>');
                    }
                    else
                        ul.append('<li style="text-align:center;">No properties found</li>');
                });
            },
            failure: function (response) {
                alert('Fail');
            }
        });
    }
}

// to get property details page
function GetPropertDetails(propertyid) {
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc/DisplayPropertyDetails/" + propertyid,
        async: false,
        success: function (response) {
            var img = $("#property_img");
            $.each(response.GetPropertyDetailsResult, function (index, value) {
                if (value.status == "success") {
                    $("#text").html(value.description);
                    $("#name").html(value.title);
                    if (value.image != "" && value.image != null) {
                        img.attr("src", value.image);
                        img.parent().attr('data-med', value.image);
                        img.parent().attr('href', value.image);
                    }
                }
            });

            (function () {
                $('.pswp__img').css('display', 'none !important');
                var initPhotoSwipeFromDOM = function (gallerySelector) {

                    var parseThumbnailElements = function (el) {
                        var thumbElements = el.childNodes,
                            numNodes = thumbElements.length,
                            items = [],
                            el,
                            childElements,
                            thumbnailEl,
                            size,
                            item;

                        for (var i = 0; i < numNodes; i++) {
                            el = thumbElements[i];

                            // include only element nodes
                            if (el.nodeType !== 1) {
                                continue;
                            }

                            childElements = el.children;

                            size = el.getAttribute('data-size').split('x');

                            // create slide object
                            item = {
                                src: el.getAttribute('href'),
                                w: parseInt(size[0], 10),
                                h: parseInt(size[1], 10),
                                author: el.getAttribute('data-author')
                            };

                            item.el = el; // save link to element for getThumbBoundsFn

                            if (childElements.length > 0) {
                                item.msrc = childElements[0].getAttribute('src'); // thumbnail url
                                if (childElements.length > 1) {
                                    item.title = childElements[1].innerHTML; // caption (contents of figure)
                                }
                            }


                            var mediumSrc = el.getAttribute('data-med');
                            if (mediumSrc) {
                                size = el.getAttribute('data-med-size').split('x');
                                // "medium-sized" image
                                item.m = {
                                    src: mediumSrc,
                                    w: parseInt(size[0], 10),
                                    h: parseInt(size[1], 10)
                                };
                            }
                            // original image
                            item.o = {
                                src: item.src,
                                w: item.w,
                                h: item.h
                            };

                            items.push(item);
                        }

                        return items;
                    };

                    // find nearest parent element
                    var closest = function closest(el, fn) {
                        return el && (fn(el) ? el : closest(el.parentNode, fn));
                    };

                    var onThumbnailsClick = function (e) {
                        e = e || window.event;
                        e.preventDefault ? e.preventDefault() : e.returnValue = false;

                        var eTarget = e.target || e.srcElement;

                        var clickedListItem = closest(eTarget, function (el) {
                            return el.tagName === 'A';
                        });

                        if (!clickedListItem) {
                            return;
                        }

                        var clickedGallery = clickedListItem.parentNode;

                        var childNodes = clickedListItem.parentNode.childNodes,
                            numChildNodes = childNodes.length,
                            nodeIndex = 0,
                            index;

                        for (var i = 0; i < numChildNodes; i++) {
                            if (childNodes[i].nodeType !== 1) {
                                continue;
                            }

                            if (childNodes[i] === clickedListItem) {
                                index = nodeIndex;
                                break;
                            }
                            nodeIndex++;
                        }

                        if (index >= 0) {
                            openPhotoSwipe(index, clickedGallery);
                        }
                        return false;
                    };

                    var photoswipeParseHash = function () {
                        var hash = window.location.hash.substring(1),
                        params = {};

                        if (hash.length < 5) { // pid=1
                            return params;
                        }

                        var vars = hash.split('&');
                        for (var i = 0; i < vars.length; i++) {
                            if (!vars[i]) {
                                continue;
                            }
                            var pair = vars[i].split('=');
                            if (pair.length < 2) {
                                continue;
                            }
                            params[pair[0]] = pair[1];
                        }

                        if (params.gid) {
                            params.gid = parseInt(params.gid, 10);
                        }

                        if (!params.hasOwnProperty('pid')) {
                            return params;
                        }
                        params.pid = parseInt(params.pid, 10);
                        return params;
                    };

                    var openPhotoSwipe = function (index, galleryElement, disableAnimation) {

                        var pswpElement = document.querySelectorAll('.pswp')[0],
                            gallery,
                            options,
                            items;

                        items = parseThumbnailElements(galleryElement);

                        // define options (if needed)
                        options = {
                            index: index,

                            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                            getThumbBoundsFn: function (index) {
                                // See Options->getThumbBoundsFn section of docs for more info
                                var thumbnail = items[index].el.children[0],
                                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                    rect = thumbnail.getBoundingClientRect();

                                return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
                            },

                            addCaptionHTMLFn: function (item, captionEl, isFake) {
                                if (!item.title) {
                                    captionEl.children[0].innerText = '';
                                    return false;
                                }
                                captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
                                return true;
                            },
                            closeOnScroll: false,
                            closeOnVerticalDrag: false

                        };

                        var radios = document.getElementsByName('gallery-style');
                        for (var i = 0, length = radios.length; i < length; i++) {
                            if (radios[i].checked) {
                                if (radios[i].id == 'radio-all-controls') {

                                } else if (radios[i].id == 'radio-minimal-black') {
                                    options.mainClass = 'pswp--minimal--dark';
                                    options.barsSize = { top: 0, bottom: 0 };
                                    options.captionEl = false;
                                    options.fullscreenEl = false;
                                    options.shareEl = false;
                                    options.bgOpacity = 0.85;
                                    options.tapToClose = true;
                                    options.tapToToggleControls = false;
                                }
                                break;
                            }
                        }

                        if (disableAnimation) {
                            options.showAnimationDuration = 0;
                        }

                        // Pass data to PhotoSwipe and initialize it
                        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

                        // see: http://photoswipe.com/documentation/responsive-images.html
                        var realViewportWidth,
                            useLargeImages = false,
                            firstResize = true,
                            imageSrcWillChange;

                        gallery.listen('beforeResize', function () {

                            var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
                            dpiRatio = Math.min(dpiRatio, 2.5);
                            realViewportWidth = gallery.viewportSize.x * dpiRatio;


                            if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
                                if (!useLargeImages) {
                                    useLargeImages = true;
                                    imageSrcWillChange = true;
                                }

                            } else {
                                if (useLargeImages) {
                                    useLargeImages = false;
                                    imageSrcWillChange = true;
                                }
                            }

                            if (imageSrcWillChange && !firstResize) {
                                gallery.invalidateCurrItems();
                            }

                            if (firstResize) {
                                firstResize = false;
                            }

                            imageSrcWillChange = false;

                        });

                        gallery.listen('gettingData', function (index, item) {
                            if (useLargeImages) {
                                item.src = item.o.src;
                                item.w = item.o.w;
                                item.h = item.o.h;
                            } else {
                                item.src = item.m.src;
                                item.w = item.m.w;
                                item.h = item.m.h;
                            }
                        });

                        gallery.init();
                        // $('.gallery_wrap').show();
                        // $('.gallery_wrap').css('top',$('.floorplan').offset().top);

                        //  var ua = navigator.userAgent;
                        // var event1 = (ua.match(/iPad/i)) ? 'touchstart' : 'mouseup';
                        // $('.close_btn_lightbox').bind(event1, function() {
                        //      $('.gallery_wrap').hide().css('top','auto');
                        //  });
                        // $('.imagegallery a').bind(event1, function() {
                        //      $('.gallery_wrap').show().css('top',$('.gallery_solo').offset().top);
                        //  });
                    };

                    // select all gallery elements
                    var galleryElements = document.querySelectorAll(gallerySelector);
                    for (var i = 0, l = galleryElements.length; i < l; i++) {
                        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
                        galleryElements[i].onclick = onThumbnailsClick;
                    }

                    // Parse URL and open gallery if it contains #&pid=3&gid=1
                    var hashData = photoswipeParseHash();
                    if (hashData.pid > 0 && hashData.gid > 0) {
                        openPhotoSwipe(hashData.pid - 1, galleryElements[hashData.gid - 1], true);
                    }
                };

                initPhotoSwipeFromDOM('#property_img');

            })();

            setTimeout(function () {
                $('.ui-loader').hide();
            }, 100);

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// to get FAQ
function GetFaq() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/GetFaq/' + "Faq",
        async: false,
        success: function (response) {
            var div = $("#div_faq");
            $.each(response.FetchFaqResult, function (index, value) {
                if (value.status == "success") {
                    div.prepend('<div class="faq_row"><span class="qstn">' + value.question + '</span><span class="answ">' + value.answer + '</span></div>');
                    //<li><a href="offer_details.html?offerid=' + value.offerid + '"><span><img src="' + value.image + '"></span><div class="text">' + value.description + '</div></a></li>');
                }
                //else
                //    div.prepend('<div class="faq_row" style="text-align: center; padding: 8px 0px;">Coming soon</div>');
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// to get Expert query
function GetExpertQuery() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/GetFaq/' + "Expert",
        async: false,
        success: function (response) {
            var div = $("#div_ask");
            var i = 0;
            $.each(response.FetchFaqResult, function (index, value) {
                if (value.status == "success") {
                    i += 1;
                    div.append('<div class="ask_row"><span class="qstn"> Q' + i + ' ' + value.question + '</span><span class="answ" style="display:none;">' + value.answer + '</span></div>');
                }
                //else
                //    div.append('<div class="ask_row" style="text-align: center; padding: 8px 0px;">Coming soon</div>');
            });

            $('.ask_row .qstn').click(function (e) {
                $(this).each(function (e) {
                    $('.ask_row .answ').stop().slideUp();
                    $('.ask_row .qstn').stop().parent().removeClass('active');

                    if ($(this).next().is(":visible")) {
                        $(this).next().stop().slideUp();
                        $(this).stop().parent().removeClass('active');
                    }
                    else {
                        $(this).next().stop().slideDown();
                        $(this).stop().parent().addClass('active');
                    }
                });
            });
            setTimeout(function () {
                $('.ask_wrapper .ask_row:nth-of-type(2n) .qstn').trigger('click');
            }, 1000);
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// to get appointment approved/rejected
function GetAppt() {
    $('.ui-loader').show();
    var div = $("#ulappt");
    div.html('');
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/DisplayApptDetails/' + localStorage.getItem("brokerid") + "/0",
        async: false,
        success: function (response) {
            var divse = "";
            var ul = $("#ulappt");
            $.each(response.GetApptDetailsResult, function (index, value) {
                $('.ui-loader').hide();
                if (value.status == "success") {
                    if (value.approve == "W") {
                        //divse += '<li><a data-transition="slide"><span>' + value.project + ',' + value.appointmentDate + ',' + value.city + ',' + value.comments + '</span></a><div class="second"><input type="checkbox" name="1"></div><div class="first select"><input type="checkbox" name="2" checked="checked"></div></li>';
                        divse += '<li><a data-transition="slide"><span><strong>Project Name: </strong> ' + value.project + '<br><strong>Appointment Date: </strong>' + value.appointmentDate + '<br><strong>Selected Location: </strong>' + value.city + '</span></a><div class="text"><strong>Purpose: </strong> ' + value.comments + '</div><a class="await" href="#">Awaiting approval</a></li>';
                    }
                    else if (value.approve == "A") {
                        //divse += '<li><a data-transition="slide"><span>' + value.project + ',' + value.appointmentDate + ',' + value.city + ',' + value.comments + '</span></a><div class="second"><input type="checkbox" name="1"></div><div class="first select"><input type="checkbox" name="2" checked="checked"></div></li>';
                        divse += '<li><a data-transition="slide"><span><strong>Project Name: </strong> ' + value.project + '<br><strong>Appointment Date: </strong>' + value.appointmentDate + '<br><strong>Selected Location: </strong>' + value.city + '</span></a><div class="text"><strong>Purpose: </strong> ' + value.comments + '</div><a class="await approved" href="#">Approved</a></li>';
                    }
                    else {
                        //divse += '<li><a data-transition="slide"><span>' + value.project + ',' + value.appointmentDate + ',' + value.city + ',' + value.comments + '</span></a><div class="second select"><input type="checkbox" name="1"></div><div class="first"><input type="checkbox" name="2" checked="checked"></div></li>';
                        divse += '<li><a data-transition="slide"><span><strong>Project Name: </strong> ' + value.project + '<br><strong>Appointment Date: </strong>' + value.appointmentDate + '<br><strong>Selected Location: </strong>' + value.city + '</span></a><div class="text"><strong>Purpose: </strong> ' + value.comments + '</div><a class="await red" href="#">Rejected</a></li>';
                    }
                }
                else {
                    divse += '<li style="text-align:center;">No Request found</li>';
                }
            });
            ul.append(divse);
            $('.ui-content ul.list.list2 li > div.text').each(function () {
                //console.log($(this).text().length);
                var this_leng = $(this).text().length;
                setTimeout(function () {
                    if (this_leng > 300) {
                        $('#ulappt .text').readmore({
                            speed: 75,
                            lessLink: '<a href="#" class="read_less"> Read Less</a>',
                            collapsedHeight: 44,
                        });
                    }
                }, 500);
            });
            /*if($(document).width() <= 767){
                 $('#ulappt .text').readmore({
                     speed: 75,
                     lessLink: '<a href="#" class="read_less"> Read Less</a>',
                     collapsedHeight: 46,
                   });
                // console.log('<767');
             }else{
                $('#ulappt .text').readmore({
                     speed: 75,
                     lessLink: '<a href="#" class="read_less"> Read Less</a>',
                     collapsedHeight: 46,
                   });
                 //console.log('<768');
             }*/


        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To Get User Information
function GetRequest(month) {
    $('.ui-loader').show();

    var brokerid = localStorage.getItem("brokerid");
    var addstring = brokerid + "/" + month;
    var div = $("#ulappt");
    div.html('');
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/DisplayApptDetails/' + addstring,
        async: false,
        success: function (response) {
            var divse = "";
            var ul = $("#ulappt");
            $.each(response.GetApptDetailsResult, function (index, value) {
                $('.ui-loader').hide();
                if (value.status == "success") {
                    //alert(value.approve);
                    if (value.approve == "W") {
                        //divse += '<li><a data-transition="slide"><span>' + value.project + ',' + value.appointmentDate + ',' + value.city + ',' + value.comments + '</span></a><div class="second"><input type="checkbox" name="1"></div><div class="first select"><input type="checkbox" name="2" checked="checked"></div></li>';
                        divse += '<li><a data-transition="slide"><span><strong>Project Name: </strong> ' + value.project + '<br><strong>Appointment Date: </strong>' + value.appointmentDate + '<br><strong>Selected Location: </strong>' + value.city + '</span></a><div class="text"><strong>Purpose: </strong> ' + value.comments + '</div><a class="await" href="#">Awaiting approval</a></li>';
                    }
                    else if (value.approve == "A") {
                        //divse += '<li><a data-transition="slide"><span>' + value.project + ',' + value.appointmentDate + ',' + value.city + ',' + value.comments + '</span></a><div class="second"><input type="checkbox" name="1"></div><div class="first select"><input type="checkbox" name="2" checked="checked"></div></li>';
                        divse += '<li><a data-transition="slide"><span><strong>Project Name: </strong> ' + value.project + '<br><strong>Appointment Date: </strong>' + value.appointmentDate + '<br><strong>Selected Location: </strong>' + value.city + '</span></a><div class="text"><strong>Purpose: </strong> ' + value.comments + '</div><a class="await approved" href="#">Approved</a></li>';
                    }
                    else {
                        //divse += '<li><a data-transition="slide"><span>' + value.project + ',' + value.appointmentDate + ',' + value.city + ',' + value.comments + '</span></a><div class="second select"><input type="checkbox" name="1"></div><div class="first"><input type="checkbox" name="2" checked="checked"></div></li>';
                        divse += '<li><a data-transition="slide"><span><strong>Project Name: </strong> ' + value.project + '<br><strong>Appointment Date: </strong>' + value.appointmentDate + '<br><strong>Selected Location: </strong>' + value.city + '</span></a><div class="text"><strong>Purpose: </strong> ' + value.comments + '</div><a class="await red" href="#">Rejected</a></li>';
                    }
                }
                else {
                    div.append('<li style="text-align:center;">No Request found</li>');
                }
            });
            ul.append(divse);
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

//To insert faq
function insertfaq(type) {
    //alert(type);
    var question = $('#ques').val();

    if (question == null || question == "" || question.lenght > 200) {
        $('#error_content').empty().html('Please enter Question.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }
    var addstring = question + "/" + localStorage.getItem("brokerid") + "/" + type;

    $('.ui-loader').show();
    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        url: baseURLEOI + 'RestServiceImpl.svc/InsertFaq/' + addstring,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $.each(response.EnterFaqResult, function (index, value) {
                if (value.status == "success") {
                    $('#ques').val('');
                    $('.ui-loader').hide();
                    $('#error_content').empty().html('Thank you for enquiring. We will get back to you soon.');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2();
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 500);
                }
            });
        },

        failure: function (response) {
            $('.ui-loader').hide();
            alert('Fail');
        }
    });
}


// To get Events
function GetEvents() {
    //$('.ui-loader').show();
    var broker = localStorage.getItem("brokerid");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/GetEvents",
        async: false,
        success: function (response) {
            var div = $("#div_event");
            var check = "";
            var readonly = "";
            var reclass = "";
            $.each(response.FetchEventsResult, function (index, value) {
                if (value.status == "success") {
                    div.append("<h1>" + value.title + "<span id='date'>date : " + value.date + "</span><span id='city'>" + value.place + "</span></h1>");
                    var divse = '<div class="data_div" style="display:none;">';
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: baseURLEOI + "RestServiceImpl.svc" + "/GetEventsDetails/" + value.id,
                        async: false,
                        datatype: "json",
                        success: function (response) {
                            $.each(response.FetchEventsDetailsResult, function (index, value) {
                                if (value.status == "success") {
                                    var add = broker + "/" + value.id;
                                    $.ajax({
                                        type: "GET",
                                        url: baseURLEOI + 'RestServiceImpl.svc/GetAttend/' + add,
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        async: false,
                                        success: function (response) {
                                            $.each(response.AttendResult, function (index, value) {
                                                if (value.status == "success") {
                                                    check = 'checked=checked';
                                                    readonly = 'disabled=disabled';
                                                    reclass = '';
                                                    //alert(check);
                                                }
                                                else {
                                                    check = '';
                                                    readonly = '';
                                                    reclass = 'a_reminder';
                                                }
                                            });
                                        },

                                        failure: function (response) {
                                            alert('Fail');
                                        }
                                    });
                                    divse += '<span id="offer_detail_img_wrapper"><img src=' + value.image + ' id="offer_detail_img"></span><span id="text">' + value.description + '</span><span class="checkbox1"><input id="chk_attend" class="setcheckbox' + value.id + '" type="checkbox" ' + check + ' ' + readonly + ' data-role="none" >yes, i am attending</span><a id="setanchore' + value.id + '" class="' + reclass + '" customdate="' + value.date + '" customtitle="' + value.title + '" customdesc="' + value.description + '" customplace="' + value.place + '" customeventid="' + value.id + '"><span class="set_reminder">set reminder</span></a>';
                                    div.append(divse + "</div>");
                                }

                            });
                            $(document).on('click', '.event_detail_wrapper > h1', function (e) {
                                //('.event_detail_wrapper h1').click(function (e) {
                                $(this).each(function (e) {
                                    $('.data_div').stop().slideUp();
                                    $('.event_detail_wrapper > h1').stop().removeClass('active');

                                    if ($(this).next().is(":visible")) {
                                        $(this).next().stop().slideUp();
                                        $(this).stop().removeClass('active');
                                    }
                                    else {
                                        $(this).next().stop().slideDown();
                                        $(this).stop().addClass('active');
                                    }

                                    e.preventDefault();
                                });
                            });
                        },
                        failure: function (response) {
                        }
                    });
                }
                else {
                    div.append("<h1>No leads found</h1>");
                }
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}
$(document).on("click", ".a_reminder", function () {
    //alert("hi");
    SetReminder($(this).attr('customdate'), $(this).attr('customtitle'), $(this).attr('customdesc'), $(this).attr('customplace'), $(this).attr('customeventid'));
});

//function OnChangeCheckbox(checkbox, id) {
//    if (checkbox.checked) {
//        $.ajax({
//            type: "GET",
//            contentType: "application/json; charset=utf-8",
//            url: baseURL + "RestServiceImpl.svc" + '/UpdateEvents/' + id + "/Yes",
//            success: function (response) {
//                var div = $("#divinner");
//                $.each(response.UpdateEventsDetailsResult, function (index, value) {
//                    if (value.status == "success") {
//                    $(this).prop('disabled', 'disabled');
//                    }
//                });
//            },
//            failure: function (response) {
//                alert('Fail');
//            }
//        });
//    }
//    else {
//        $.ajax({
//            type: "GET",
//            contentType: "application/json; charset=utf-8",
//            url: baseURL + "RestServiceImpl.svc" + '/UpdateEvents/' + id + "/No",
//            success: function (response) {
//                var div = $("#divinner");
//                $.each(response.UpdateEventsDetailsResult, function (index, value) {
//                    if (value.status == "success") {
//                    }
//                });
//            },
//            failure: function (response) {
//                alert('Fail');
//            }
//        });
//    }
//}



// to get news
function GetNews() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/GetNews',
        success: function (response) {
            var div = $("#divinner");
            $.each(response.FetchNewsResult, function (index, value) {
                if (value.status == "success") {
                    div.prepend('<a href="newsfeed_details.html?newsid=' + value.id + '"><div class="each_news"><div class="news_thumb"><img src=' + value.image + ' alt="thumb_image"></div><div class="news_data"><h3>' + value.title + '</h3><p>' + value.description + '><br><br><em>' + value.date + '</em></p></div></div></a>');
                }
                else
                    div.append('<div class="each_news" style="text-align: center; padding: 8px 0px;">Coming soon</div>');
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}


// to get news as per id
function GetNewsByID(newsid) {
    var div = $("#divnews");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/GetNewsDetails/' + newsid,
        success: function (response) {
            $.each(response.FetchNewsDetailsResult, function (index, value) {
                if (value.status == "success") {
                    div.append('<div class="detail_top"><h1>' + value.title + '<span id="date">' + value.date + '</span></h1><span id="text">' + value.description + '</span></div>');

                }
                else {
                    div.append('<div class="each_news" style="text-align: center; padding: 8px 0px;">Coming soon</div>');
                }
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });

    var divse = "";
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/GetVideoByNews/" + newsid,
        success: function (response) {
            $.each(response.FetchVideoByNewsResult, function (index, value) {
                if (value.status == "success") {
                    var strurl = "http://img.youtube.com/vi/" + value.video + "/hqdefault.jpg";
                    var strvurl = "http://www.youtube.com/embed/" + value.video;
                    divse += '<div class="item"><a href="javascript:void(0);" class="fancybox-media" data-transition="none" rel="' + strvurl + '"><img src="' + strurl + '" alt="logo_image"></a></div>';
                }
            });

            $('#news_owl_wrapper').append('<div id="news_owl" class="owl-carousel">' + divse + '</div>');
            $("#news_owl").owlCarousel({
                items: 3, //10 items above 1000px browser width
                itemsDesktop: [1000, 4], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 4], // betweem 900px and 601px
                itemsTablet: [600, 3], //2 items between 600 and 0
                itemsMobile: true, // itemsMobile disabled - inherit from itemsTablet option
                pagination: true,
                lazyLoad: true,
                navigation: false,
                itemsMargin: 10,
                afterInit: function (elem) {
                    //var that = this
                    //that.owlControls.prependTo(elem)
                }
            });
            $(document).on("click", ".fancybox-media", function (e) {
                $(this).each(function (e) {
                    //alert();
                    var strvurl = $(this).attr('rel');
                    var video_id = strvurl.split('embed/')[1];
                    var ampersandPosition = video_id.indexOf('&');
                    if (ampersandPosition != -1) {
                        video_id = video_id.substring(0, ampersandPosition);
                    }
                    //alert(video_id);
                    ytonline(video_id);
                    // ytonline(strvurl);

                    e.preventDefault();
                });
                // });
                // $.fancybox({
                //     openEffect  : 'none',
                //     closeEffect : 'none',
                //     padding:0,
                //     // href: this.href,
                //     // type: "iframe",
                //     // iframe : {
                //     //   preload: false
                //     // },
                //     helpers : {
                //         media : {}
                //     }
                // });
                //return false
            });
        },
        failure: function (response) {
        }
    });
}


// to get top performers
function GetPerformers() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/GetPerformer',
        success: function (response) {
            var div = $("#div_top_span");
            $.each(response.GetPerformerDetailsResult, function (index, value) {
                if (value.status == "success") {
                    div.prepend('<h1><img src="images/dummy_img_top.jpg" class="img"><div>' + value.name + '<span class="sub_name">' + value.org_Name + '</span></div></h1>');
                }
                else
                    div.prepend('<h1 style="text-align: center; padding: 8px 0px;">Coming soon</h1>');
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// to get commision as per broker
function GetCommision() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/GetCommision/' + localStorage.getItem("brokerid"),
        success: function (response) {
            var div = $("#divcomm");
            $.each(response.GetCommisionDetailsResult, function (index, value) {
                if (value.status == "success") {
                    $("#div_comm").show();
                    div.append('<h1><div>' + value.project + '</div><div>your commission<span class="rupee">Rs. ' + value.brokerage + '</span><span class="date">date : ' + value.date + '</span></div></h1>');
                }
                else {
                    $("#div_comm").hide();
                    div.append('<h1 style="text-align: center; padding: 8px 0px;border-bottom:none;">No commission available</h1>');
                }

            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/GetTotalCommision/' + localStorage.getItem("brokerid"),
        success: function (response) {
            $.each(response.GetTotalCommisionDetailsResult, function (index, value) {
                if (value.status == "success") {
                    $('#amount').html(value.sum);
                    //alert($('#amount').html());
                    var commision = parseFloat(value.sum.replace(/,/g, ""));
                    //alert(value.sum);
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: baseURLEOI + "RestServiceImpl.svc" + '/GetPurchaseSum/' + localStorage.getItem("brokerid"),
                        success: function (response) {
                            $.each(response.GetPurchaseTotalResult, function (index, value) {
                                if (value.status == "success") {
                                    var purchase = parseFloat(value.amount.replace(/,/g, ""));
                                    //alert(purchase);
                                    //var total = (commision - purchase).toLocaleString()
                                    var sum = commaSeparateNumber((commision - purchase).toFixed(2));
                                    //var total =(commision-purchase)
                                    //var total2 = total.toLocaleString();
                                    //console.log(total);
                                    //$('#bal_comm').html(total.toFixed(2));
                                    console.log(sum);
                                    if (sum == 'NaN')
                                        sum = '0';
                                    $('#bal_comm').html(sum);

                                    //alert(total);
                                }
                            });
                        },
                        failure: function (response) {
                            alert('Fail');
                        }
                    });
                }
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

// to get purchase as per broker
function GetPurchase() {
    var div = $("#div_pay");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/GetPurchaseSum/' + localStorage.getItem("brokerid"),
        success: function (response) {
            $.each(response.GetPurchaseTotalResult, function (index, value) {
                if (value.status == "success") {
                    //$('#rs').html(value.amount);
                    if (value.amount != "") {
                        $('#div_pay').append('<div class="heading" id="div_payment">Total amount paid as on date<br><span>Rs.</span><span id="rs">' + value.amount + '</span></div>');
                    }

                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: baseURLEOI + "RestServiceImpl.svc" + '/GetPurchase/' + localStorage.getItem("brokerid"),
                        success: function (response) {
                            $.each(response.GetPurchaseDetailsResult, function (index, value) {
                                if (value.status == "success") {
                                    //div.prepend('<h1><div>P.O.No.</div><div>'+ value.po_no+'</div></h1><h1><div>Creation Date</div><div>'+ value.date +'</div></h1><h1><div>Amount</div><div>'+ value.amount +'</div>');
                                    div.append(' <h1><div>PO NO.<span class="no1">' + value.po_no + '</span></div><div>Amount<span class="rupee">Rs. ' + value.amount + '</span><span class="date">Creation date : ' + value.date + '</span></div></h1>');

                                    //<h1><div>' + value.project + '</div><div>your commission<span class="rupee">Rs. ' + value.brokerage + '</span><span class="date">date : ' + value.date + '</span></div></h1>');
                                }
                                else
                                    div.append('<h1 style="text-align: center; padding: 8px 0px;border-bottom:none;">No payment status available</h1>');
                            });
                        },
                        failure: function (response) {
                            alert('Fail');
                        }
                    });
                }
                else {
                    //$('div_payment').html("");
                }
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

//set property images
function SetImages() {
    var projecttype = localStorage.getItem("ptype");
    if (projecttype == "Residential") {
        $("#img_current").attr("src", "images/resi_current.jpg");
        $("#img_comp").attr("src", "images/resi_complete.jpg");
        $("#img_upcoming").attr("src", "images/resi_azure.jpg");
        $('#link .sub_menu a#a_res small').css('color', '#000');
        $('#link .sub_menu a#a_com small').css('color', '');
    }
    else {
        $("#img_current").attr("src", "images/comm_current.jpg");
        $("#img_comp").attr("src", "images/comm_complete.jpg");
        $("#img_upcoming").attr("src", "images/comm_upcoming.jpg");
        $('#link .sub_menu a#a_com small').css('color', '#000');
        $('#link .sub_menu a#a_res small').css('color', '');
    }
}


// to get lead status
function GetLeadStatus() {
    //alert(localStorage.getItem("brokerid"));
    $('.ui-loader').show();
    var selecthtml = '';
    $.ajax({
        ServiceCallID: 1,
        type: "POST",
        url: sfdc_baseURL + 'getlead_api.php',
        data: 'broker_id=' + localStorage.getItem("brokerid"),
        //data: 'broker_id=a06O000000IejZ2',
        dataType: "json",
        success: function (response) {
            //alert(response.status);
            if (response.status != "No Record") {
                $.each(response, function (i, item) {
                    //var str=
                    //alert(response[i].status.indexOf("Open"));
                    if (response[i].status == "Assigned") {
                        var status = 'Assigned';
                        var styling = '<span class="bar_status" style="display:none;"></span><br><img src="images/statusicon1.png">';
                    }
                        //else if (response[i].status.indexOf("Open") > -1) {
                        //    var status = 'Open';
                        //    var styling = '<span class="bar_status"></span>';
                        //}
                    else if (response[i].status == "Requested to Site Visit") {
                        var status = 'Requested to Site Visit';
                        var styling = '<span class="bar_status" style="display:none;"></span><br><img src="images/statusicon2.png"></span>';
                    }
                    else if (response[i].status == "Site Visit") {
                        var status = 'Site Visit';
                        var styling = '<span class="bar_status style="display:none;"></span><br><img src="images/statusicon3.png""></span>';
                    }
                    else if (response[i].status == "Closed") {
                        var status = 'Closed';
                        var styling = '<span class="bar_status" style="display:none;"></span><br><img src="images/statusicon4.png"></span>';
                    }
                    else if (response[i].status == "Contacted") {
                        var status = 'Contacted';
                        var styling = '<span class="bar_status" style="display:none;"></span><br><img src="images/statusicon5.png"></span>';
                    }
                    else if (response[i].status == "Qualified") {
                        var status = 'Converted';
                        var styling = '<span class="bar_status" style="display:none;"></span><br><img src="images/green_check_round.png"></span>';
                    }
                    else {
                        var status = 'In Process';
                        var styling = '<span class="bar_status"></span>';
                    }

                    //selecthtml += '<h1><div>' + response[i].project_name + '<span class="no1">Name : ' + response[i].name + '</span></div><div>' + status + styling + '</div></h1>';
                    $('#div_lead').append('<h1><div>' + response[i].project_name + '<span class="no1">Name : ' + response[i].name + '</span></div><div>' + status + styling + '</div></h1>');
                });
                // $('#div_lead').one("load", function () {

                // }).each(function () {
                //     setTimeout(function () {
                //         //$('.ui-loader').hide();
                //     }, 110);
                // });

            }
            else {
                $('#div_lead').append('<h1 style="text-align: center; padding: 8px 0px;">No leads found</h1>');
            }
            //            $('#div_lead').html(selecthtml);
            setTimeout(function () {
                $('.ui-loader').hide();
            }, 110);

        }

    });

}

// To Get User Information
function GetUserInfo() {
    $('.ui-loader').show();
    $('#txtemail :input').prop('disabled', true);
    var brokerid = localStorage.getItem("brokerid");
    GetCountry();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/FetchUsers/" + brokerid,
        async: false,
        success: function (response) {
            $.each(response.GetUserInfoResult, function (index, value) {
                if (value.status == "success") {
                    $('#txtname').val(value.name);
                    $('#txtemail').val(value.email);
                }
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}


function BindCode() {
    var countryCode = document.getElementById('ddlCountry').value;
    document.getElementById('txtCountryCode').value = countryCode;
}

//Bind Country

function GetCountry() {
    setSelect('#ddlCountry', './countryList.xml', 'Country');

    function setSelect(selectid, xmlpath, xmlnode) {
        var loadingtext = '-- Loading --';
        var loadinghtml = '<option value="">' + loadingtext + '</option>';
        var randomno = Math.ceil(Math.random() * 999);
        $(selectid).html(loadinghtml);
        $.ajax({
            url: xmlpath,
            success: function (xml) {
                parseSelectXML(xml, selectid, xmlnode)
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    function parseSelectXML(xml, selectid, xmlnode) {
        var firstoption = 'SELECT COUNTRY';
        var firsthtml = '<option value="">' + firstoption + '</option>';
        var selecthtml = '';

        $(xml).find(xmlnode).each(function () {
            var selectvalue = $(this).attr('code');//$(this).find('code').value;
            var selecttext = $(this).attr('name')// $(this).find('name').attr('value');
            selecthtml += '<option value="' + selectvalue + '">' + selecttext + '</option>';
        });

        $(selectid).html(firsthtml + selecthtml);
    }

}

// To Get Response Message
function Message() {
    var brokerid = localStorage.getItem("brokerid");
    var country = "null";
    var msgtype = "TEXT";
    var message = document.getElementById('txtmessage').value;
    if (message == null || message == "") {

        $('#error_content').empty().html('Please enter message.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2({
                callbackClose: function () {
                    var me = this;
                    $('.ui-loader').hide();
                    setTimeout(function () { me.close(); }, 2000);
                }
            });
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    var addstring = brokerid + "/" + msgtype + "/" + message + "/" + country;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/AddMessages/' + addstring,
        processData: false,
        success: function (response) {
            $.each(response.InsertMessagesResult, function (index, value) {
                if (value.status == "success") {
                    $('#txtmessage').val('');
                    $('#error_content').empty().html('Thank you for enquiring. We will get back to you soon.');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2({
                            callbackClose: function () {
                                var me = this;
                                $('.ui-loader').hide();
                                setTimeout(function () { me.close(); }, 2000);
                            }
                        });
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 800);
                }
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To insert Mobile number for enquery
function MobileMessage() {
    var brokerid = localStorage.getItem("brokerid");

    var country = $('#ddlCountry option:selected').text();
    if (country == 0) {
        $('#error_content').empty().html('Please select country.');
        $('#enquiry_inline').simpledialog2({
            callbackClose: function () {
                var me = this;
                $('.ui-loader').hide();
                setTimeout(function () { me.close(); }, 2000);
            }
        });
        $('.ui-simpledialog-screen').css('height', $(document).height());
        return false;
    }

    var msgtype = "Mobile";
    var mobile = document.getElementById('txtmobile').value;
    if (mobile == null || mobile == "") {
        $('#error_content').empty().html('Please enter mobile mumber.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2({
                callbackClose: function () {
                    var me = this;
                    $('.ui-loader').hide();
                    setTimeout(function () { me.close(); }, 2000);
                }
            });
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    var numPattern = /\d+(,\d{1,3})?/;

    if (!numPattern.test($('#txtmobile').val())) {

        $('#error_content').empty().html('Please enter valid mobile number.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2({
                callbackClose: function () {
                    var me = this;
                    $('.ui-loader').hide();
                    setTimeout(function () { me.close(); }, 2000);
                }
            });
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }

    if (country == "India") {
        if (mobile.charAt(0) == "0") {
            $('#error_content').empty().html('Please enter valid mobile number');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2();
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }
        if (mobile.length < 10 || mobile.length > 12) {
            $('#error_content').empty().html('Mobile number should be of minimum 10 digits and maximun 12 digits.');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2();
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }
    }
    //    else
    //    {
    //    if (mobile.length < 6 || mobile.length > 20) {
    //        $('#error_content').empty().html('Mobile number should be of minimum 6 digits and maximun 20 digits.');
    //                    setTimeout(function () {
    //        $('#enquiry_inline').simpledialog2();
    //        $('.ui-simpledialog-screen').css('height', $(document).height());
    //        },500);
    //        return false;
    //    }
    //    }

    //    if (mobile.length < 10 || mobile.length > 10) {
    //        $('#error_content').empty().html('Mobile number should be minimum 10 digits.');
    //        setTimeout(function () {
    //            $('#enquiry_inline').simpledialog2({
    //                callbackClose: function () {
    //                    var me = this;
    //                    $('.ui-loader').hide();
    //                    setTimeout(function () { me.close(); }, 2000);
    //                }
    //            });
    //            $('.ui-simpledialog-screen').css('height', $(document).height());
    //        }, 500);
    //        return false;
    //    }
    var code = document.getElementById('txtCountryCode').value;
    var mobileno = code + '-' + mobile;
    var addstring = brokerid + "/" + msgtype + "/" + mobileno + "/" + country;


    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + '/AddMessages/' + addstring,
        processData: false,
        success: function (response) {

            $.each(response.InsertMessagesResult, function (index, value) {
                if (value.status == "success") {
                    $('#txtmobile').val(' ');
                    $('#txtmobile').attr("value", '');
                    GetCountry();
                    //$('#ddlCountry :selected').text('SELECT COUNTRY');
                    //SELECT COUNTRY
                    $('#error_content').empty().html('Thank you for enquiring. We will get back to you soon.');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2({
                            callbackClose: function () {
                                var me = this;
                                $('.ui-loader').hide();
                                setTimeout(function () { me.close(); }, 2000);
                            }
                        });
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 500);
                }
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}
// //for ios
// function ytonline(ytnmber) {
//     var filter = 'http://www.youtube.com/embed/'+ ytnmber+'?wmode=opaque&autoplay=1'
//     alert(filter);
//     openVideo_ios(filter);
// }
// function openVideo_ios(filter, successCallback, failureCallback){
//     //alert(ytnmber)
//     cordova.exec(successCallback, failureCallback, "MyPlugin", "VideoPlay", [filter]);
// }

// To Get User Information
function getDetails() {
    $('.ui-loader').show();
    $('#username').prop('readonly', true);
    $('#emailid').prop('readonly', true);
    $('#mobile').prop('readonly', true);
    $('#company').prop('readonly', true);
    var brokerid = localStorage.getItem("brokerid");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/FetchUsers/" + brokerid,
        async: false,
        success: function (response) {
            $.each(response.GetUserInfoResult, function (index, value) {
                if (value.status == "success") {
                    $('#username').val(value.name);
                    $('#emailid').val(value.email);
                    $('#mobile').val(value.mobileno);
                    $('#company').val(value.company);
                }
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To Get User Information
function GetEventsCal(month) {
    $('.ui-loader').show();

    var brokerid = localStorage.getItem("brokerid");
    var addstring = brokerid + "/" + month;
    var div = $("#div_top_span");
    div.html('');
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/GetEventsReminder/" + addstring,
        async: false,
        success: function (response) {
            $.each(response.FetchEventsReminderResult, function (index, value) {
                $('.ui-loader').hide();
                if (value.status == "success") {
                    div.append('<h1><div>' + value.title + '<span class="sub_name">' + value.place + ' on ' + value.date + '</span></div></h1>');
                }
                else
                    div.append('<h1 style="text-align: center; padding: 8px 0px; font-size:16px;">No reminders</h1>');
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}



// To Get change password
function ChangePassword() {
    var brokerid = localStorage.getItem("brokerid");
    var userid = localStorage.getItem("emplcontid");
    var oldpwd = $('#oldpwd').val();
    var newpwd = $('#newpwd').val();
    var confirm = $('#confirmpwd').val();
    var brokerid = localStorage.getItem("brokerid");

    if (oldpwd == null || oldpwd == "") {
        showMsg('Please enter old password.');
        return false;
    }

    if (newpwd == null || newpwd == "") {
        showMsg('Please enter new password.');
        return false;
    }

    if (newpwd.length < 6 || newpwd.length > 15) {
        showMsg('Password length should be minimum 6 digits and maximum 15 digits.');
        /*$('#error_content').empty().html('Password length should be minimum 6 digits and maximum 15 digits.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);*/
        return false;
    }
    if (newpwd != confirm) {
        showMsg('New password and confirm password field does not match');
        return false;
        $('#confirmpwd').val('')
    }

    //get old password

    $('.ui-loader').show();

    $.ajax({
        type: "GET",
        //contentType: "application/json; charset=utf-8",
        //url: baseURL + "CPService.svc" + "/GetPassword/" + userid,
        url: baseAPI + "ChangePassword.ashx",
        data: {"userid": userid, "old_password": oldpwd, "new_password": newpwd},
        dataType: "json",
        //async: false,
        success: function (response) {
            $('#oldpwd').val('');
            $('#newpwd').val('');
            $('#confirmpwd').val('');
            if(response.statusCode == 400){
                showMsg(response.msg);
                return false;
            }
            else{
                showMsg(response.msg);
                return false;
            }
            /*
            $.each(response.GetPasswordResult, function (index, value) {
                $('.ui-loader').hide();
                if (value.status == "success") {
                    if (value.Password != oldpwd) {
                        $('#oldpwd').val('');
                        $('#error_content').empty().html('Please enter correct password');
                        setTimeout(function () {
                            $('#enquiry_inline').simpledialog2();
                            $('.ui-simpledialog-screen').css('height', $(document).height());
                        }, 500);
                        return false;
                        $('#oldpwd').val('')
                    }
                    else {
                        //update new password
                        $.ajax({
                            type: "GET",
                            contentType: "application/json; charset=utf-8",
                            url: baseURL + "CPService.svc" + "/ChangePassword/" + userid + "/" + newpwd,
                            async: false,
                            success: function (response) {
                                $.each(response.ChangePasswordResult, function (index, value) {
                                    $('.ui-loader').hide();
                                    if (value.status == "success") {
                                        if (value.Password != oldpwd) {
                                            $('#error_content').empty().html('Password successfully changed.');
                                            setTimeout(function () {
                                                $('#enquiry_inline').simpledialog2();
                                                $('.ui-simpledialog-screen').css('height', $(document).height());
                                            }, 500);
                                            return false;

                                            $('#oldpwd').val('');
                                            $('#newpwd').val('');
                                            $('#confirmpwd').val('');
                                        }
                                    }
                                });

                            },
                            failure: function (response) {
                                alert('Fail');
                            }
                        });
                    }
                }
            });
            */

        },
        error: DefaultAjaxError
    });
}

// To ForgotPassword
function ForgotPassword() {

    var email = $('#email').val();
    var brokerid = localStorage.getItem("brokerid");

    if (email == null || email == "" || email.lenght > 100) {
        $('#error_content').empty().html('Please enter email ID.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2({
                callbackClose: function () {
                    var me = this;
                    $('.ui-loader').hide();
                    setTimeout(function () { me.close(); }, 2000);
                }
            });
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }
    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!filter.test(email)) {
        $('#error_content').empty().html('Please enter valid email id.');
        setTimeout(function () {
            $('#enquiry_inline').simpledialog2();
            $('.ui-simpledialog-screen').css('height', $(document).height());
        }, 500);
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        //contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "RestServiceImpl.svc" + "/GetEmail/" + email,
        url: baseAPI + "ForgotPassword.ashx",
        dataType: "json",
        data: {"email": email},
        //async: false,
        success: function (response) {
            $('.ui-loader').hide();
            $('#email').val('');
            if(response.statusCode == 400){
                showMsg(response.msg);
                return false;
            }
            else{
                showMsg(response.msg);
                return false;
            }
            /*
            $.each(response.FetchEmailResult, function (index, value) {
                $('.ui-loader').hide();
                if (value.status != "success") {
                    $('#email').val('');
                    $('#error_content').empty().html('Email id does not exist');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2();
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 500);
                    return false;
                    $('#email').val('');
                }
                else {
                    var brokerid = localStorage.getItem("brokerid");
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: baseURL + "CPService.svc" + "/ForgotPassword/" + email,
                        async: false,
                        success: function (response) {
                            $.each(response.ForgotPasswordResult, function (index, value) {
                                $('.ui-loader').hide();
                                if (value.status == "success") {
                                    $('#email').val('');
                                    $('#error_content').empty().html('Your request has been accepted. You will receive a new password shortly.');
                                    setTimeout(function () {
                                        $('#enquiry_inline').simpledialog2();
                                        $('.ui-simpledialog-screen').css('height', $(document).height());
                                    }, 500);
                                    return false;
                                }
                            });

                        },
                        failure: function (response) {
                            alert('Fail');
                        }
                    });
                }
            });
            */
        },
        error: DefaultAjaxError
    });
}

function RemovePic() {
    var brokerid = localStorage.getItem("brokerid");

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "RestServiceImpl.svc" + "/ChangePic/" + brokerid,
        async: false,
        success: function (response) {
            $.each(response.RemovePicResult, function (index, value) {
                $('.ui-loader').hide();
                if (value.status == "success") {
                    $("#img_pic").attr("src", '');
                    $("#img_pic").attr("src", "images/edit_dummy.jpg");
                    $("#a_remove").css("display", "none");
                }
            });

        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To get Projects features/ContactUs
function GetProjectContactUs(projectid) {

    //$('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc" + "/GetContactUs/" + projectid,
        processData: false,
        async: false,
        success: function (response) {
            var div = $("#div_desc");
            console.log(response);
            if(response.GetContactUsjsonResult.length == 1 && response.GetContactUsjsonResult[0]["projectid"] == "null")
            {
                $(".img_wrapper").append('<div class="img_wrapper"><img id="img_project" src="images/coming_soon.jpg" width="100%" height="auto""></div>');
            }
            else
            {
                $.each(response.GetContactUsjsonResult, function (index, value) {
                    //if (value.status == "success") {
                    div.html(value.description);
                    //}
                    setTimeout(function () {
                        //$('.ui-loader').hide();
                    }, 1000);
                });
            }
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}


//get construction updates
function GetProjectConstructionImages(projectid) {
    var interval = setInterval(function () {
        $.mobile.loading('show');
        clearInterval(interval);
    }, 1);
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc/GetContructionStatus/" + projectid + "/0" + "/0",
        async: false,
        success: function (response) {
            // Shreyas 24.07.2020 Coming_soon_issue
            if(response.GetContructionStatusjsonResult.length == 1 && response.GetContructionStatusjsonResult[0]["image"] == "null")
            {
                $(".img_wrapper").append('<div class="img_wrapper"><img id="img_project" src="images/coming_soon.jpg"></div>');
            }
            else
            {
                $.each(response.GetContructionStatusjsonResult, function (index, value) {
                    //if (value.image == null) {
                        $("#owl_image_gallery1").append("<div><img data-src='" + value.image + "' rel='" + value.image + "' class='lazyOwl'></div>");
                    //}
                });
            }
            var owl = $("#owl_image_gallery1");

            owl.owlCarousel({
                items: 4, //10 items above 1000px browser width
                itemsDesktop: [1000, 4], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 4], // betweem 900px and 601px
                itemsTablet: [600, 2], //2 items between 600 and 0
                itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
                pagination: false,
                lazyLoad: true,
                navigation: true,
                afterInit: function (elem) {
                    //var that = this
                    //that.owlControls.prependTo(elem)
                    $(document).on('click', '#construction_image .owl-carousel .owl-item img', function () {
                        $(this).each(function () {
                            $(this).parent().addClass('active_a');
                            var src_name = $(this).attr('rel');
                            $('#img_gallery_big').attr('src', src_name);
                            $('#img_gallery_big').parent().attr('data-med', src_name);
                            // $('#img_gallery_big').parent().attr('href', src_name);
                        });
                    });
                }
            });
            $('#construction_image .owl-carousel .owl-item:first-child img').click();
            // (function () {
            //     $('.pswp__img').css('display', 'none !important');
            //     var initPhotoSwipeFromDOM = function (gallerySelector) {

            //         var parseThumbnailElements = function (el) {
            //             var thumbElements = el.childNodes,
            //                 numNodes = thumbElements.length,
            //                 items = [],
            //                 el,
            //                 childElements,
            //                 thumbnailEl,
            //                 size,
            //                 item;

            //             for (var i = 0; i < numNodes; i++) {
            //                 el = thumbElements[i];

            //                 // include only element nodes
            //                 if (el.nodeType !== 1) {
            //                     continue;
            //                 }

            //                 childElements = el.children;

            //                 size = el.getAttribute('data-size').split('x');

            //                 // create slide object
            //                 item = {
            //                     src: el.getAttribute('href'),
            //                     w: parseInt(size[0], 10),
            //                     h: parseInt(size[1], 10),
            //                     author: el.getAttribute('data-author')
            //                 };

            //                 item.el = el; // save link to element for getThumbBoundsFn

            //                 if (childElements.length > 0) {
            //                     item.msrc = childElements[0].getAttribute('src'); // thumbnail url
            //                     if (childElements.length > 1) {
            //                         item.title = childElements[1].innerHTML; // caption (contents of figure)
            //                     }
            //                 }


            //                 var mediumSrc = el.getAttribute('data-med');
            //                 if (mediumSrc) {
            //                     size = el.getAttribute('data-med-size').split('x');
            //                     // "medium-sized" image
            //                     item.m = {
            //                         src: mediumSrc,
            //                         w: parseInt(size[0], 10),
            //                         h: parseInt(size[1], 10)
            //                     };
            //                 }
            //                 // original image
            //                 item.o = {
            //                     src: item.src,
            //                     w: item.w,
            //                     h: item.h
            //                 };

            //                 items.push(item);
            //             }

            //             return items;
            //         };

            //         // find nearest parent element
            //         var closest = function closest(el, fn) {
            //             return el && (fn(el) ? el : closest(el.parentNode, fn));
            //         };

            //         var onThumbnailsClick = function (e) {
            //             e = e || window.event;
            //             e.preventDefault ? e.preventDefault() : e.returnValue = false;

            //             var eTarget = e.target || e.srcElement;

            //             var clickedListItem = closest(eTarget, function (el) {
            //                 return el.tagName === 'A';
            //             });

            //             if (!clickedListItem) {
            //                 return;
            //             }

            //             var clickedGallery = clickedListItem.parentNode;

            //             var childNodes = clickedListItem.parentNode.childNodes,
            //                 numChildNodes = childNodes.length,
            //                 nodeIndex = 0,
            //                 index;

            //             for (var i = 0; i < numChildNodes; i++) {
            //                 if (childNodes[i].nodeType !== 1) {
            //                     continue;
            //                 }

            //                 if (childNodes[i] === clickedListItem) {
            //                     index = nodeIndex;
            //                     break;
            //                 }
            //                 nodeIndex++;
            //             }

            //             if (index >= 0) {
            //                 openPhotoSwipe(index, clickedGallery);
            //             }
            //             return false;
            //         };

            //         var photoswipeParseHash = function () {
            //             var hash = window.location.hash.substring(1),
            //             params = {};

            //             if (hash.length < 5) { // pid=1
            //                 return params;
            //             }

            //             var vars = hash.split('&');
            //             for (var i = 0; i < vars.length; i++) {
            //                 if (!vars[i]) {
            //                     continue;
            //                 }
            //                 var pair = vars[i].split('=');
            //                 if (pair.length < 2) {
            //                     continue;
            //                 }
            //                 params[pair[0]] = pair[1];
            //             }

            //             if (params.gid) {
            //                 params.gid = parseInt(params.gid, 10);
            //             }

            //             if (!params.hasOwnProperty('pid')) {
            //                 return params;
            //             }
            //             params.pid = parseInt(params.pid, 10);
            //             return params;
            //         };

            //         var openPhotoSwipe = function (index, galleryElement, disableAnimation) {

            //             var pswpElement = document.querySelectorAll('.pswp')[0],
            //                 gallery,
            //                 options,
            //                 items;

            //             items = parseThumbnailElements(galleryElement);

            //             // define options (if needed)
            //             options = {
            //                 index: index,

            //                 galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            //                 getThumbBoundsFn: function (index) {
            //                     // See Options->getThumbBoundsFn section of docs for more info
            //                     var thumbnail = items[index].el.children[0],
            //                         pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
            //                         rect = thumbnail.getBoundingClientRect();

            //                     return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
            //                 },

            //                 addCaptionHTMLFn: function (item, captionEl, isFake) {
            //                     if (!item.title) {
            //                         captionEl.children[0].innerText = '';
            //                         return false;
            //                     }
            //                     captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
            //                     return true;
            //                 },
            //                 closeOnScroll: false,
            //                 closeOnVerticalDrag: false

            //             };

            //             var radios = document.getElementsByName('gallery-style');
            //             for (var i = 0, length = radios.length; i < length; i++) {
            //                 if (radios[i].checked) {
            //                     if (radios[i].id == 'radio-all-controls') {

            //                     } else if (radios[i].id == 'radio-minimal-black') {
            //                         options.mainClass = 'pswp--minimal--dark';
            //                         options.barsSize = { top: 0, bottom: 0 };
            //                         options.captionEl = false;
            //                         options.fullscreenEl = false;
            //                         options.shareEl = false;
            //                         options.bgOpacity = 0.85;
            //                         options.tapToClose = true;
            //                         options.tapToToggleControls = false;
            //                     }
            //                     break;
            //                 }
            //             }

            //             if (disableAnimation) {
            //                 options.showAnimationDuration = 0;
            //             }

            //             // Pass data to PhotoSwipe and initialize it
            //             gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

            //             // see: http://photoswipe.com/documentation/responsive-images.html
            //             var realViewportWidth,
            //                 useLargeImages = false,
            //                 firstResize = true,
            //                 imageSrcWillChange;

            //             gallery.listen('beforeResize', function () {

            //                 var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
            //                 dpiRatio = Math.min(dpiRatio, 2.5);
            //                 realViewportWidth = gallery.viewportSize.x * dpiRatio;


            //                 if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
            //                     if (!useLargeImages) {
            //                         useLargeImages = true;
            //                         imageSrcWillChange = true;
            //                     }

            //                 } else {
            //                     if (useLargeImages) {
            //                         useLargeImages = false;
            //                         imageSrcWillChange = true;
            //                     }
            //                 }

            //                 if (imageSrcWillChange && !firstResize) {
            //                     gallery.invalidateCurrItems();
            //                 }

            //                 if (firstResize) {
            //                     firstResize = false;
            //                 }

            //                 imageSrcWillChange = false;

            //             });

            //             gallery.listen('gettingData', function (index, item) {
            //                 if (useLargeImages) {
            //                     item.src = item.o.src;
            //                     item.w = item.o.w;
            //                     item.h = item.o.h;
            //                 } else {
            //                     item.src = item.m.src;
            //                     item.w = item.m.w;
            //                     item.h = item.m.h;
            //                 }
            //             });

            //             gallery.init();
            //             // $('.gallery_wrap').show();
            //             // $('.gallery_wrap').css('top',$('.floorplan').offset().top);

            //             //  var ua = navigator.userAgent;
            //             // var event1 = (ua.match(/iPad/i)) ? 'touchstart' : 'mouseup';
            //             // $('.close_btn_lightbox').bind(event1, function() {
            //             //      $('.gallery_wrap').hide().css('top','auto');
            //             //  });
            //             // $('.imagegallery a').bind(event1, function() {
            //             //      $('.gallery_wrap').show().css('top',$('.gallery_solo').offset().top);
            //             //  });
            //         };

            //         // select all gallery elements
            //         var galleryElements = document.querySelectorAll(gallerySelector);
            //         for (var i = 0, l = galleryElements.length; i < l; i++) {
            //             galleryElements[i].setAttribute('data-pswp-uid', i + 1);
            //             galleryElements[i].onclick = onThumbnailsClick;
            //         }

            //         // Parse URL and open gallery if it contains #&pid=3&gid=1
            //         var hashData = photoswipeParseHash();
            //         if (hashData.pid > 0 && hashData.gid > 0) {
            //             openPhotoSwipe(hashData.pid - 1, galleryElements[hashData.gid - 1], true);
            //         }
            //     };

            //     initPhotoSwipeFromDOM('#img_gallery_big');

            // })();
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To fill dropdown for year for construction updates
function fillyear() {
    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'CPService.svc/GetContructionYear',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="0">  CHOOSE BY YEAR</option>';
            $.each(response.GetContructionYearjsonResult, function (index, value) {
                if (value.year != "") {
                    optionsValues += '<option value="' + value.year + '">' + value.year + '</option>';
                }
                else {
                    optionsValues = '';
                    optionsValues = '<option value="0"> Year not found! </option>';
                }

                optionsValues += '</select>';
                var options = $('#ddlYear');
                options.html(optionsValues);

            });

        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

//get construction updates on year change
function fillstatusyear(year) {
    var interval = setInterval(function () {
        $.mobile.loading('show');
        clearInterval(interval);
    }, 1);
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc" + "/GetContructionStatus/" + localStorage.getItem("projectid") + "/" + year + "/0",
        async: false,
        success: function (response) {
            $("#owl_image_gallery1").html("");
            $.each(response.GetContructionStatusjsonResult, function (index, value) {
                //if (value.status == "success") {
                $("#owl_image_gallery1").append("<div><img data-src='" + value.image + "' rel='" + value.image + "' class='lazyOwl'></div>");
                // }
            });
            var owl = $("#owl_image_gallery1");

            owl.owlCarousel({
                items: 4, //10 items above 1000px browser width
                itemsDesktop: [1000, 4], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 4], // betweem 900px and 601px
                itemsTablet: [600, 2], //2 items between 600 and 0
                itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
                pagination: false,
                lazyLoad: true,
                navigation: true,
                afterInit: function (elem) {
                    //var that = this
                    //that.owlControls.prependTo(elem)
                    $(document).on('click', '#construction_image .owl-carousel .owl-item img', function () {
                        $(this).each(function () {
                            $(this).parent().addClass('active_a');
                            var src_name = $(this).attr('rel');
                            $('#img_gallery_big').attr('src', src_name);
                            $('#img_gallery_big').parent().attr('data-med', src_name);
                            $('#img_gallery_big').parent().attr('href', src_name);
                        });
                    });
                }
            });
            $('#construction_image .owl-carousel .owl-item:first-child img').click();
            (function () {
                $('.pswp__img').css('display', 'none !important');
                var initPhotoSwipeFromDOM = function (gallerySelector) {

                    var parseThumbnailElements = function (el) {
                        var thumbElements = el.childNodes,
                            numNodes = thumbElements.length,
                            items = [],
                            el,
                            childElements,
                            thumbnailEl,
                            size,
                            item;

                        for (var i = 0; i < numNodes; i++) {
                            el = thumbElements[i];

                            // include only element nodes
                            if (el.nodeType !== 1) {
                                continue;
                            }

                            childElements = el.children;

                            size = el.getAttribute('data-size').split('x');

                            // create slide object
                            item = {
                                src: el.getAttribute('href'),
                                w: parseInt(size[0], 10),
                                h: parseInt(size[1], 10),
                                author: el.getAttribute('data-author')
                            };

                            item.el = el; // save link to element for getThumbBoundsFn

                            if (childElements.length > 0) {
                                item.msrc = childElements[0].getAttribute('src'); // thumbnail url
                                if (childElements.length > 1) {
                                    item.title = childElements[1].innerHTML; // caption (contents of figure)
                                }
                            }


                            var mediumSrc = el.getAttribute('data-med');
                            if (mediumSrc) {
                                size = el.getAttribute('data-med-size').split('x');
                                // "medium-sized" image
                                item.m = {
                                    src: mediumSrc,
                                    w: parseInt(size[0], 10),
                                    h: parseInt(size[1], 10)
                                };
                            }
                            // original image
                            item.o = {
                                src: item.src,
                                w: item.w,
                                h: item.h
                            };

                            items.push(item);
                        }

                        return items;
                    };

                    // find nearest parent element
                    var closest = function closest(el, fn) {
                        return el && (fn(el) ? el : closest(el.parentNode, fn));
                    };

                    var onThumbnailsClick = function (e) {
                        e = e || window.event;
                        e.preventDefault ? e.preventDefault() : e.returnValue = false;

                        var eTarget = e.target || e.srcElement;

                        var clickedListItem = closest(eTarget, function (el) {
                            return el.tagName === 'A';
                        });

                        if (!clickedListItem) {
                            return;
                        }

                        var clickedGallery = clickedListItem.parentNode;

                        var childNodes = clickedListItem.parentNode.childNodes,
                            numChildNodes = childNodes.length,
                            nodeIndex = 0,
                            index;

                        for (var i = 0; i < numChildNodes; i++) {
                            if (childNodes[i].nodeType !== 1) {
                                continue;
                            }

                            if (childNodes[i] === clickedListItem) {
                                index = nodeIndex;
                                break;
                            }
                            nodeIndex++;
                        }

                        if (index >= 0) {
                            openPhotoSwipe(index, clickedGallery);
                        }
                        return false;
                    };

                    var photoswipeParseHash = function () {
                        var hash = window.location.hash.substring(1),
                        params = {};

                        if (hash.length < 5) { // pid=1
                            return params;
                        }

                        var vars = hash.split('&');
                        for (var i = 0; i < vars.length; i++) {
                            if (!vars[i]) {
                                continue;
                            }
                            var pair = vars[i].split('=');
                            if (pair.length < 2) {
                                continue;
                            }
                            params[pair[0]] = pair[1];
                        }

                        if (params.gid) {
                            params.gid = parseInt(params.gid, 10);
                        }

                        if (!params.hasOwnProperty('pid')) {
                            return params;
                        }
                        params.pid = parseInt(params.pid, 10);
                        return params;
                    };

                    var openPhotoSwipe = function (index, galleryElement, disableAnimation) {

                        var pswpElement = document.querySelectorAll('.pswp')[0],
                            gallery,
                            options,
                            items;

                        items = parseThumbnailElements(galleryElement);

                        // define options (if needed)
                        options = {
                            index: index,

                            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                            getThumbBoundsFn: function (index) {
                                // See Options->getThumbBoundsFn section of docs for more info
                                var thumbnail = items[index].el.children[0],
                                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                    rect = thumbnail.getBoundingClientRect();

                                return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
                            },

                            addCaptionHTMLFn: function (item, captionEl, isFake) {
                                if (!item.title) {
                                    captionEl.children[0].innerText = '';
                                    return false;
                                }
                                captionEl.children[0].innerHTML = item.title + '<br/><small>Photo: ' + item.author + '</small>';
                                return true;
                            },
                            closeOnScroll: false,
                            closeOnVerticalDrag: false

                        };

                        var radios = document.getElementsByName('gallery-style');
                        for (var i = 0, length = radios.length; i < length; i++) {
                            if (radios[i].checked) {
                                if (radios[i].id == 'radio-all-controls') {

                                } else if (radios[i].id == 'radio-minimal-black') {
                                    options.mainClass = 'pswp--minimal--dark';
                                    options.barsSize = { top: 0, bottom: 0 };
                                    options.captionEl = false;
                                    options.fullscreenEl = false;
                                    options.shareEl = false;
                                    options.bgOpacity = 0.85;
                                    options.tapToClose = true;
                                    options.tapToToggleControls = false;
                                }
                                break;
                            }
                        }

                        if (disableAnimation) {
                            options.showAnimationDuration = 0;
                        }

                        // Pass data to PhotoSwipe and initialize it
                        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

                        // see: http://photoswipe.com/documentation/responsive-images.html
                        var realViewportWidth,
                            useLargeImages = false,
                            firstResize = true,
                            imageSrcWillChange;

                        gallery.listen('beforeResize', function () {

                            var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
                            dpiRatio = Math.min(dpiRatio, 2.5);
                            realViewportWidth = gallery.viewportSize.x * dpiRatio;


                            if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
                                if (!useLargeImages) {
                                    useLargeImages = true;
                                    imageSrcWillChange = true;
                                }

                            } else {
                                if (useLargeImages) {
                                    useLargeImages = false;
                                    imageSrcWillChange = true;
                                }
                            }

                            if (imageSrcWillChange && !firstResize) {
                                gallery.invalidateCurrItems();
                            }

                            if (firstResize) {
                                firstResize = false;
                            }

                            imageSrcWillChange = false;

                        });

                        gallery.listen('gettingData', function (index, item) {
                            if (useLargeImages) {
                                item.src = item.o.src;
                                item.w = item.o.w;
                                item.h = item.o.h;
                            } else {
                                item.src = item.m.src;
                                item.w = item.m.w;
                                item.h = item.m.h;
                            }
                        });

                        gallery.init();
                        // $('.gallery_wrap').show();
                        // $('.gallery_wrap').css('top',$('.floorplan').offset().top);

                        //  var ua = navigator.userAgent;
                        // var event1 = (ua.match(/iPad/i)) ? 'touchstart' : 'mouseup';
                        // $('.close_btn_lightbox').bind(event1, function() {
                        //      $('.gallery_wrap').hide().css('top','auto');
                        //  });
                        // $('.imagegallery a').bind(event1, function() {
                        //      $('.gallery_wrap').show().css('top',$('.gallery_solo').offset().top);
                        //  });
                    };

                    // select all gallery elements
                    var galleryElements = document.querySelectorAll(gallerySelector);
                    for (var i = 0, l = galleryElements.length; i < l; i++) {
                        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
                        galleryElements[i].onclick = onThumbnailsClick;
                    }

                    // Parse URL and open gallery if it contains #&pid=3&gid=1
                    var hashData = photoswipeParseHash();
                    if (hashData.pid > 0 && hashData.gid > 0) {
                        openPhotoSwipe(hashData.pid - 1, galleryElements[hashData.gid - 1], true);
                    }
                };

                initPhotoSwipeFromDOM('#img_gallery_big');

            })();
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}


function AddList() {

    var state = $("#state").val();
    var rerano = $("#frera").val();
    var rcerti = $("#hdn_rera").val();
    var gstcerti = $("#hdn_gst").val();

    var no = 0;
    if ($('#tblList tbody tr').length = 0) {
        no = 1;
    }
    else {
        no = 1 + parseInt($('#tblList tbody tr').length);
    }

    var markup = "<tr><td>" + state + "</td><td>" + rerano + "</td><td>" + rcerti + "</td><td>" + gstcerti + "</td></tr>";
    $("#tblList").append(markup);


    $('#State').val("SELECT STATE");
    $("#frera").val("");
    $("#upload_rera_cert").val("");
    $("#upload_gst_cert").val("");

    //var stulist = new Array();

    ////get all of the values except the header
    //$("#tableItem tr:not(:first)").each(function () {
    //    var tds = $(this).find("td");
    //    //you could use the Find method to find the texbox or the dropdownlist and get the value.
    //    var SStudent = { ItemID: $(tds[0]).html(), SrNo: $(tds[1]).html(), Rate: $(tds[3]).html(), Quantity: $(tds[4]).html(), Amount: $(tds[5]).html() }
    //    stulist.push(SStudent);
    //});



}


// To fill dropdown for project for only send enquery
function fillState() {
    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'CPService.svc/GetState',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';


            optionsValues += '<option value="SELECT STATE">SELECT STATE*</option>';

            $.each(response.GetStatejsonResult, function (index, value) {

                //if (value.status == "success") {
                optionsValues += '<option value="' + value.State + '">' + value.State + '</option>';

                //}

                optionsValues += '</select>';
                var reraoptions = $('#rera_state');
                reraoptions.html(optionsValues);


                var gstoptions = $('#gst_state');
                gstoptions.html(optionsValues);

            });

        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

function fillCommState() {
    var optionsValues = '<select>';
    optionsValues += '<option value="SELECT STATE">SELECT STATE</option>';
    optionsValues += '</select>';
    var reraoptions = $('#comm_state');
    reraoptions.html(optionsValues);

    optionsValues = '<select>';
    optionsValues += '<option value="SELECT STATE">SELECT CITY</option>';
    optionsValues += '</select>';
    var reraoptions = $('#comm_city');
    reraoptions.html(optionsValues);
}

function fillRegState() {
    var optionsValues = '<select>';
    optionsValues += '<option value="SELECT STATE">SELECT STATE</option>';
    optionsValues += '</select>';
    var reraoptions = $('#reg_state');
    reraoptions.html(optionsValues);

    optionsValues = '<select>';
    optionsValues += '<option value="SELECT STATE">SELECT CITY</option>';
    optionsValues += '</select>';
    var reraoptions = $('#reg_city');
    reraoptions.html(optionsValues);
}

function addpan() {
    // alert();
    //window.target = '_blank';
    //var pancerti=window.ChannelPartnerActivity.openURLinDialog(link);
    //var pancerti = window.ChannelPartnerActivity.uploadDocsToServer("pan");
    //console.log("Add PAN ==>",pancerti);
    // alert(pancerti);
    // $("#hdn_pan").val(pancerti);
    uploadToServer("pan");
}

function setpan(pancerti) {
    //pancerti = pancerti.replace("http://gplpartnerconnect.com/GodrejPartnerConnect/images/EmplDocs/", "");
    console.log("SET PAN ==>",pancerti);
    // alert(pancerti);
    $("#hdn_pan").val(pancerti);
    $("#upload_pan").val(pancerti);
    console.log("hdn_pan ==>",pancerti);
    console.log("upload_pan ==>",pancerti);
}
function addrera() {
    // setrera("DOC_20180912130929.docx");
    //window.target = '_blank';
    //var reracerti = window.ChannelPartnerActivity.uploadDocsToServer("rera");
    // alert(reracerti);
    // $("#hdn_rera").val(reracerti);
    // alert($("#upload_rera_cert").val());
    // $("#hdn_rera").val($("#upload_rera_cert").val());
     uploadToServer("rera");

}
function setrera(reracerti) {
    //alert(reracerti)
    //reracerti = reracerti.replace("http://gplpartnerconnect.com/GodrejPartnerConnect/images/EmplDocs/", "");
    //alert(reracerti);
    $("#hdn_rera").val(reracerti);
    $("#upload_rera_cert").val(reracerti);
}
function addgst() {
    window.target = '_blank';
    var gstcerti = window.ChannelPartnerActivity.uploadDocsToServer("gst");
    // alert(gstcerti);
    // $("#hdn_gst").val(gstcerti);
    //alert($("#upload_gst_cert").val());
    //$("#hdn_gst").val($("#upload_gst_cert").val());
}

function setgst(gstcerti) {
    //gstcerti = gstcerti.replace("http://gplpartnerconnect.com/GodrejPartnerConnect/images/EmplDocs/", "");
    //alert(gstcerti);
    $("#hdn_gst").val(gstcerti);
    $("#upload_gst_cert").val(gstcerti);
}
var objReralist = new Array();
var objGSTlist = new Array();
function SaveRera() {
    $('.ui-loader').show();
 //   var state = $("#rera_state").val();
    var objRera = {};

    objRera.i = parseInt($("#rerai").val());
    objRera.id = $("#reraID").val();
    objRera.state = $("#rera_state").find(":selected").text();
    objRera.rerano = $("#rerano").val();
    objRera.reracerti = $("#hdn_rera").val();

    if (objRera.state == "SELECT STATE*") {
        showMsg('Please select state.');
        return false;
    }
    if (objRera.rerano == "") {
        showMsg('Please enter RERA No.');
        return false;
    }
    // if (ValidateRera(rerano)) {
    // }
    // else {
    //     $('#error_content').empty().html('Please enter valid Rera no.');
    //     setTimeout(function () {
    //         $('#enquiry_inline').simpledialog2({
    //             callbackClose: function () {
    //                 var me = this;
    //                 $('.ui-loader').hide();
    //                 //setTimeout(function () { me.close(); }, 2000);
    //             }
    //         });
    //         $('.ui-simpledialog-screen').css('height', $(document).height());
    //     }, 500);
    //     return false;
    // }
    if (objRera.reracerti == "") {
        showMsg('Please select RERA certificate.')
        return false;
    }

    var objReraStateCheck = jQuery.grep(objReralist, function(value) {
        if(value.status == "D")
            return false;
        else
            return value.state == objRera.state;
    });
    var objReraNoCheck = jQuery.grep(objReralist, function(value) {
        if(value.status == "D")
            return false;
        else
            return value.rerano == objRera.rerano;
    });
    objRera.comment = "";


    if(isNaN(objRera.i)){
        if(objReraStateCheck.length > 0){
            showMsg('State aleady selected')
            return false;
        }
        if(objReraNoCheck.length > 0){
            showMsg('Rera no. aleady selected')
            return false;
        }
        if(objReralist.length > 0){
            objRera.i = objReralist[objReralist.length - 1].i + 1;
        }
        else{
            objRera.i = 0;
        }
        objReralist.push(objRera);
    }
    else{
        if(objReraStateCheck.length > 0){
            if(objReraStateCheck[0].i != objRera.i){
                showMsg('State aleady selected')
                return false;
            }
        }
        if(objReraNoCheck.length > 0){
            if(objReraNoCheck[0].i != objRera.i){
                showMsg('Rera no. aleady selected')
                return false;
            }
        }
        for(var i = 0; i < objReralist.length; i++){
            if(objReralist[i].i == objRera.i){
                objReralist[i] = objRera;
                break;
            }
        }
        $("#tblReraList tr[data-i="+ objRera.i+"]").remove();
    }

    console.log(objReralist);
    AddTrRera(objRera);

    ClearReraFrom();
    $('.rera_popup_wrap').hide();
    $('.ui-loader').hide();
}

function ClearReraFrom(){
    $('#rera_state').val("SELECT STATE");
    $("#rerano").val("");
    $("#hdn_rera").val("");
    $("#reraID").val("");
    $("#rerai").val("");
    $("#upload_rera_cert").val("");
}

function AddTrRera(objRera){
    var markup = "<tr data-i='"+ objRera.i + "'>";
    markup += "<td>" + objRera.state + "</td>";
    markup += "<td><a rel=" + objRera.reracerti + " class='viewdoc' href='javascript:void(0);'>" + objRera.rerano + "</a></td>";
    markup += "<td>" + objRera.reracerti + "</td>";
    if(bookData.EmplID != "0"){
        if(objRera.comment != "")
            markup += "<td><a href='javascript:showMsg(\"" + objRera.comment + "\");'>click_here</a></td>";
        else
            markup += "<td></td>";
    }
    if(objRera.status != "Approve"){
        markup += "<td><a href='javascript:void(0)' class='editRera' title='Delete'>Edit</a></td>";
        markup += "<td><a href='javascript:void(0)' id='deleteRera' title='Delete'><i class='fa fa-trash' aria-hidden='true'></a></td>";
    }
    else{
        markup += "<td></td>";
        markup += "<td></td>";
    }
    markup += "</tr>";
    $("#tblReraList").append(markup);

}

function SaveGst() {
    $('.ui-loader').show();
    var objGST = {};

    objGST.i = parseInt($("#gsti").val());
    objGST.id = $("#gstID").val();
    objGST.state = $("#gst_state").find(":selected").text();
    objGST.gstno = $("#gst_no").val();
    objGST.gstcerti = $("#hdn_gst").val();

    /*var state = $("#gst_state").find(":selected").text();
    var gstno = $("#gst_no").val();
    var gcerti = $("#hdn_gst").val();*/

    if (objGST.state == "SELECT STATE*") {
        showMsg('Please select state.');
        return false;
    }
    if (objGST.gstno == "") {
        showMsg('Please enter GST No.');
        return false;
    }


    if (!ValidateGSTIN(objGST.gstno)) {
        showMsg('Please enter valid gstno.');
        return false;
    }
    if (objGST.gstcerti == "") {
        showMsg('Please select GST certificate.');
        return false;
    }

    var objGSTStateCheck = jQuery.grep(objGSTlist, function(value) {
        if(value.status == "D")
            return false;
        else
            return value.state == objGST.state;
    });
    var objGSTNoCheck = jQuery.grep(objGSTlist, function(value) {
        if(value.status == "D")
            return false;
        else
            return value.gstno == objGST.gstno;
    });
    objGST.comment = "";

    var markup = "";
    if(isNaN(objGST.i)){
        if(objGSTStateCheck.length > 0){
            showMsg('State aleady selected')
            return false;
        }
        if(objGSTNoCheck.length > 0){
            showMsg('GST No. aleady selected')
            return false;
        }
        if(objGSTlist.length > 0){
            objGST.i = objGSTlist[objGSTlist.length - 1].i + 1;
        }
        else{
            objGST.i = 0;
        }
        objGSTlist.push(objGST);
    }
    else{
        if(objGSTStateCheck.length > 0){
            if(objGSTStateCheck[0].i != objGST.i){
                showMsg('State aleady selected')
                return false;
            }
        }
        if(objGSTNoCheck.length > 0){
            if(objGSTNoCheck[0].i != objGST.i){
                showMsg('GST No. aleady selected')
                return false;
            }
        }
        for(var i = 0; i < objGSTlist.length; i++){
            if(objGSTlist[i].i == objGST.i){
                objGSTlist[i] = objGST;
                break;
            }
        }
        $("#tblGstList tr[data-i="+ objGST.i+"]").remove();
    }

    AddTrGST(objGST);
    console.log(objGSTlist)

    ClearGSTFrom();
    $('.gst_popup_wrap').hide();
    $('.ui-loader').hide();
}

function ClearGSTFrom(){
    $('#gst_state').val("SELECT STATE");
    $("#gst_no").val("");
    $("#hdn_gst").val("");
    $("#gsti").val("");
    $("#gstID").val("");
    $("#upload_gst_cert").val("");
}

function AddTrGST(objGST){
    console.log(objGST);
    var markup = "<tr data-i='"+ objGST.i + "'>";
    markup += "<td>" + objGST.state + "</td>";
    markup += "<td><a rel=" + objGST.gstcerti + " class='viewdoc' href='javascript:void(0);'>" + objGST.gstno + "</a></td>";
    markup += "<td>" + objGST.gstcerti + "</td>";
    if(bookData.EmplID != "0"){
        if(objGST.comment != "")
            markup += "<td><a href='javascript:showMsg(\"" + objGST.comment + "\");'>click_here</a></td>";
        else
            markup += "<td></td>";
    }

    if(objGST.status != "Approve"){
        markup += "<td><a href='javascript:void(0)' class='editGst' title='Delete'>Edit</a></td>"
        markup += "<td><a href='javascript:void(0)' id='deleteGst' title='Delete'><i class='fa fa-trash' aria-hidden='true'></a></td>";
    }
    else{
        markup += "<td></td>";
        markup += "<td></td>";
    }
    markup += "</tr>";

    //var markup = "<tr><td>" + state + "</td><td>" + gstno + "</td><td>" + gcerti + "</td><td><a rel=" + gcerti + " class='viewdoc' href='javascript:void(0);'>View </a></td><td><a href='javascript:void(0)' id='deleteGst' title='Delete'><i class='fa fa-trash' aria-hidden='true'></i></a></td></tr>";
    $("#tblGstList").append(markup);
}


$(document).on('click', '#tblGstList .editGst', function () {
    var $tr = $(this).closest('tr')
    var i = $tr.data("i");
    var objGST = jQuery.grep(objGSTlist, function(value) {
        return value.i == i;
    });


    $("#gsti").val(objGST[0].i);
    $("#gst_no").val(objGST[0].gstno);
    $("#gst_state").val(objGST[0].state);
    $("#hdn_gst").val(objGST[0].gstcerti);
    $("#gstID").val(objGST[0].id);
    $('.gst_popup_wrap').show();
});

$(document).on('click', '#tblGstList #deleteGst', function () {
    var $tr = $(this).closest('tr')
    var removeID = $tr.data("i");
    objGSTlist = jQuery.grep(objGSTlist, function(value) {
        if(value.i == removeID){
            if(value.id == ""){
                return false;
            }
            else{
                value.status = "D";
                return true;
            }
        }
        else{
            return true;
        }
    });
    $tr.remove();
});

$(document).on('click', '#tblReraList .editRera', function () {
    console.log(objReralist);
    var $tr = $(this).closest('tr')
    var i = $tr.data("i");
    var objRera = jQuery.grep(objReralist, function(value) {
        return value.i == i;
    });
    $("#rerai").val(objRera[0].i);
    $("#rerano").val(objRera[0].rerano);
    $("#rera_state").val(objRera[0].state);
    $("#hdn_rera").val(objRera[0].reracerti);
    $("#reraID").val(objRera[0].id);
    $('.rera_popup_wrap').show();
});

$(document).on('click', '#tblReraList #deleteRera', function () {
    var $tr = $(this).closest('tr')
    var removeID = $tr.data("i");
    objReralist = jQuery.grep(objReralist, function(value) {
        if(value.i == removeID){
            if(value.id == ""){
                return false;
            }
            else{
                value.status = "D";
                return true;
            }
        }
        else{
            return true;
        }
    });
    $tr.remove();
});

$.mobile.document.on("click", ".viewdoc", function (evt) {
    var onee = "https://cp.godrejproperties.com/data/empanelment/" + $(this).attr('rel');
    openlink(onee);
    evt.preventDefault();
});
function OpenRera() {
    $('.rera_popup_wrap').show();
}
function OpenGst() {
    $('.gst_popup_wrap').show();
}
function CloseRera() {
    ClearReraFrom();
    $('.rera_popup_wrap').hide();
}
function CloseGst() {
    ClearGSTFrom();
    $('.gst_popup_wrap').hide();
}
function OpenTerms() {
    $('.terms_popup_wrap').show();
}
function CloseTerms() {
    $('.terms_popup_wrap').hide();
}

function fillregaddr() {

    if ($("#same_address").is(":checked")) {
        $("#registered_street").val($("#communication_street").val());
        $('#registered_country').val($('#communication_country').val());
        if($('#communication_country option:selected').text().toUpperCase()=="INDIA")
        {
            fillRegisteredstate($('#registered_country').val());
            $('#registered_state').val($('#communication_state').val());
            fillRegisteredcity($('#registered_state').val());
            $('#registered_city').val($('#communication_city').val());


            $('#txtregistered_state').hide();
            $('#txtregistered_city').hide();
            $('#registered_state').show();
            $('#registered_city').show();
        }
        else
        {
            $('#txtregistered_state').val($('#txtcommunication_state').val());
            $('#txtregistered_city').val($('#txtcommunication_city').val());

            $('#txtregistered_state').show();
            $('#txtregistered_city').show();
            $('#registered_state').hide();
            $('#registered_city').hide();
        }
        // fillRegisteredstate($('#registered_country').val());
        // $('#registered_state').val($('#communication_state').val());
        // fillRegisteredcity($('#registered_state').val());
        // $('#registered_city').val($('#communication_city').val());
        $("#registered_zip").val($("#communication_zip").val());
        // $("#registered_address").val($("#communication_address").val());
        // $("#registered_address").attr('disabled', 'disabled');
        $("#registered_street").prop("disabled", 'disabled');
        $("#registered_country").prop("disabled", true);
        $("#registered_state").prop("disabled", true);
        $("#txtregistered_state").prop("disabled", 'disabled');
        $("#registered_city").prop("disabled", true);
        $("#txtregistered_city").prop("disabled", 'disabled');
        $("#registered_zip").prop("disabled", 'disabled');
    }
    else {

        $("#registered_street").removeAttr("disabled");
        $("#registered_country").removeAttr("disabled");
        $("#registered_state").removeAttr("disabled");
        $("#txtregistered_state").removeAttr("disabled");
        $("#registered_city").removeAttr("disabled");
        $("#txtregistered_city").removeAttr("disabled");
        $("#registered_zip").removeAttr("disabled");
        //$("#registered_street").val("");
        //  var CoptionsValues = '<option value="SELECT COUNTRY"> SELECT COUNTRY </option>';
        // $('#registered_country').html(CoptionsValues);

         $('#txtregistered_state').hide();
            $('#txtregistered_city').hide();
            $('#registered_state').show();
            $('#registered_city').show();

         var optionsValues = '<option value="SELECT STATE"> SELECT STATE* </option>';
        $('#registered_state').html(optionsValues);

         var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#registered_city').html(cityoptionsValues);

        //$("#registered_zip").val("");
        // $("#registered_address").val("");
        // $("#registered_address").removeAttr('disabled');

    }
}

function ValidateGSTIN(GSTIN) {
    var filter = new RegExp(/\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/);
    if (filter.test(GSTIN)) {
        return true;
    }
    else {
        return false;
    }

}

function ValidateRera(Rera) {
    var filter = /^(P)[0-9]{11}$/;
    if (filter.test(Rera)) {
        return true;
    }
    else {
        return false;
    }

}

// To fill dropdown for billing country for only send enquery
function fillcountry() {
        var optionsValues = '<option value="SELECT STATE"> SELECT STATE* </option>';
        $('#billing_state').html(optionsValues);
        $('#communication_state').html(optionsValues);
        $('#registered_state').html(optionsValues);

        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#billing_city').html(cityoptionsValues);
        $('#communication_city').html(cityoptionsValues);
        $('#Registered_city').html(cityoptionsValues);
    debugger
    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetCountry', // baseURLEOI delete
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="SELECT COUNTRY"> SELECT COUNTRY </option>';
           optionsValues += '<option value="1">India</option>';

            $.each(response.GetCountryjsonResult, function (index, value) {
                   if(value.CountryName.toUpperCase() != "INDIA"){
                        optionsValues += '<option value="' + value.CountryId + '">' + value.CountryName + '</option>';
                   }

            });
            optionsValues += '</select>';

            var reraoptions = $('#billing_country');
            reraoptions.html(optionsValues);

            var commuoptions = $('#communication_country');
            commuoptions.html(optionsValues);

            var registeredoptions = $('#registered_country');
            registeredoptions.html(optionsValues);
        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

function fillCommcountry() {
    var optionsValues = '<option value="SELECT STATE"> SELECT STATE </option>';
    $('#comm_state').html(optionsValues);

    var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
    $('#comm_city').html(cityoptionsValues);

    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetCountry', // baseURLEOI delete
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="SELECT COUNTRY"> SELECT COUNTRY </option>';
            optionsValues += '<option value="1">India</option>';

            $.each(response.GetCountryjsonResult, function (index, value) {
                   if(value.CountryName.toUpperCase() != "INDIA"){
                        optionsValues += '<option value="' + value.CountryId + '">' + value.CountryName + '</option>';
                   }
            });
            optionsValues += '</select>';

            var countrydropdown = $('#new_CommCountry');
            countrydropdown.html(optionsValues);
        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

function fillRegcountry() {
    var optionsValues = '<option value="SELECT STATE"> SELECT STATE </option>';
    $('#reg_state').html(optionsValues);

//    var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
//    $('#comm_city').html(cityoptionsValues);

    $.ajax({
        ServiceCallID: 1,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetCountry', // baseURLEOI delete
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="SELECT COUNTRY"> SELECT COUNTRY </option>';
            optionsValues += '<option value="1">India</option>';

            $.each(response.GetCountryjsonResult, function (index, value) {
                   if(value.CountryName.toUpperCase() != "INDIA"){
                        optionsValues += '<option value="' + value.CountryId + '">' + value.CountryName + '</option>';
                   }
            });
            optionsValues += '</select>';

            var countrydropdown = $('#new_RegCountry');
            countrydropdown.html(optionsValues);
        },
        failure: function (response) {
            alert(response.d);

        }
    });
}

// To fill dropdown for billing country for only send enquery
function fillBillingstate(Country) {
    $('#billing_state').html("");
    $('#billing_city').html("");
    if (Country == "SELECT COUNTRY") {
        var optionsValues = '<option value="SELECT STATE"> SELECT STATE* </option>';
        $('#billing_state').html(optionsValues);

        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#billing_city').html(cityoptionsValues);
    }
    else {
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetStateData/' + Country,
            processData: false,
            async: false,
            success: function (response) {
                var optionsValues = ''
                //'<select>';
                optionsValues += '<option value="SELECT STATE"> SELECT STATE* </option>';

                $.each(response.GetStateDatajsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.StateId + '">' + value.StateName + '</option>';
                    //optionsValues += '</select>';
                });

                var reraoptions = $('#billing_state');
                reraoptions.html(optionsValues);

                var cityoptionsValues = '<option value="SELECT CITY">SELECT CITY </option>';
                $('#billing_city').html(cityoptionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }

    if($('#billing_country option:selected').text().toUpperCase()=="INDIA")
    {
    	$('#txtbilling_state').hide();
    	$('#txtbilling_city').hide();
    	$('#billing_state').show();
    	$('#billing_city').show();
    }
    else
    {
    	$('#txtbilling_state').show();
    	$('#txtbilling_city').show();
    	$('#billing_state').hide();
    	$('#billing_city').hide();
    }
}

function fillCommstate(Country) {
    debugger
    $('#comm_state').html("");
    $('#comm_city').html("");
    if (Country == "SELECT COUNTRY") {
        var optionsValues = '<option value="SELECT STATE"> SELECT STATE* </option>';
        $('#comm_state').html(optionsValues);

        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#comm_city').html(cityoptionsValues);
    }
    else {
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetStateData/' + Country,
            processData: false,
            async: false,
            success: function (response) {
                var optionsValues = ''
                //'<select>';
                optionsValues += '<option value="SELECT STATE"> SELECT STATE* </option>';

                $.each(response.GetStateDatajsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.StateId + '">' + value.StateName + '</option>';
                    //optionsValues += '</select>';
                });

                var reraoptions = $('#comm_state');
                reraoptions.html(optionsValues);

                var cityoptionsValues = '<option value="SELECT CITY">SELECT CITY </option>';
                $('#comm_city').html(cityoptionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }

    if($('#new_CommCountry option:selected').text().toUpperCase()=="INDIA")
    {
    	$('#txtcomm_state').hide();
    	$('#txtcomm_city').hide();
    	$('#comm_state').show();
    	$('#comm_city').show();
    }
    else
    {
    	$('#txtcomm_state').show();
    	$('#txtcomm_city').show();
    	$('#comm_state').hide();
    	$('#comm_city').hide();
    }
}

function fillRegstate(Country) {
    debugger
    $('#comm_state').html("");
    $('#comm_city').html("");
    if (Country == "SELECT COUNTRY") {
        var optionsValues = '<option value="SELECT STATE"> SELECT STATE* </option>';
        $('#comm_state').html(optionsValues);

        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#comm_city').html(cityoptionsValues);
    }
    else {
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetStateData/' + Country,
            processData: false,
            async: false,
            success: function (response) {
                var optionsValues = ''
                //'<select>';
                optionsValues += '<option value="SELECT STATE"> SELECT STATE* </option>';

                $.each(response.GetStateDatajsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.StateId + '">' + value.StateName + '</option>';
                    //optionsValues += '</select>';
                });

                var reraoptions = $('#reg_state');
                reraoptions.html(optionsValues);

                var cityoptionsValues = '<option value="SELECT CITY">SELECT CITY </option>';
                $('#reg_city').html(cityoptionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }

    if($('#new_RegCountry option:selected').text().toUpperCase()=="INDIA")
    {
    	$('#txtreg_state').hide();
    	$('#txtreg_city').hide();
    	$('#reg_state').show();
    	$('#reg_city').show();
    }
    else
    {
    	$('#txtreg_state').show();
    	$('#txtreg_city').show();
    	$('#reg_state').hide();
    	$('#reg_city').hide();
    }
}

// To fill dropdown for billing country for only send enquery
function fillCommunicationstate(Country) {
    $('#communication_state').html("");
    $('#communication_city').html("");
    if (Country == "SELECT COUNTRY") {
        var optionsValues = '<option value="SELECT STATE"> SELECT STATE* </option>';
        $('#communication_state').html(optionsValues);

        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#communication_city').html(cityoptionsValues);
    }
    else {
    	if ($("#same_address").is(":checked")) {
    		$('#registered_country').val($('#communication_country').val());
    	}
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetStateData/' + Country,
            processData: false,
            async: false,
            success: function (response) {
                var optionsValues = '<select>';
                optionsValues += '<option value="SELECT STATE"> SELECT STATE* </option>';

                $.each(response.GetStateDatajsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.StateId + '">' + value.StateName + '</option>';
                    optionsValues += '</select>';
                });

                var reraoptions = $('#communication_state');
                reraoptions.html(optionsValues);

                var cityoptionsValues = '<option value="SELECT CITY">SELECT CITY </option>';
                $('#communication_city').html(cityoptionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }


    if($('#communication_country option:selected').text().toUpperCase()=="INDIA")
    {
    	$('#txtcommunication_state').hide();
    	$('#txtcommunication_city').hide();
    	$('#communication_state').show();
    	$('#communication_city').show();
    }
    else
    {
    	$('#txtcommunication_state').show();
    	$('#txtcommunication_city').show();
    	$('#communication_state').hide();
    	$('#communication_city').hide();
    }
}

// To fill dropdown for billing country for only send enquery
function fillRegisteredstate(Country) {
    $('#registered_state').html("");
    $('#registered_city').html("");
    if (Country == "SELECT COUNTRY") {
        var optionsValues = '<option value="SELECT STATE"> SELECT STATE* </option>';
        $('#registered_state').html(optionsValues);

        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#registered_city').html(cityoptionsValues);
    }
    else {
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetStateData/' + Country,
            processData: false,
            async: false,
            success: function (response) {
                var optionsValues = '<select>';
                optionsValues += '<option value="SELECT STATE"> SELECT STATE* </option>';

                $.each(response.GetStateDatajsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.StateId + '">' + value.StateName + '</option>';
                    optionsValues += '</select>';
                });

                var reraoptions = $('#registered_state');
                reraoptions.html(optionsValues);

                var cityoptionsValues = '<option value="SELECT CITY">SELECT CITY</option>';
                $('#registered_city').html(cityoptionsValues);

            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }


    if($('#registered_country option:selected').text().toUpperCase()=="INDIA")
    {
    	$('#txtregistered_state').hide();
    	$('#txtregistered_city').hide();
    	$('#registered_state').show();
    	$('#registered_city').show();
    }
    else
    {
    	$('#txtregistered_state').show();
    	$('#txtregistered_city').show();
    	$('#registered_state').hide();
    	$('#registered_city').hide();
    }
}




// To fill dropdown for billing country for only send enquery
function fillBillingcity(State) {
    $('#billing_city').html("");
    if (State == "SELECT STATE") {
        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#billing_city').html(cityoptionsValues);
    }
    else {
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetCity/' + State,
            processData: false,
            async: false,
            success: function (response) {

                var optionsValues = ''
                //'<select>';
                optionsValues += '<option value="SELECT CITY"> SELECT CITY </option>';

                $.each(response.GetCityjsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.CityId + '">' + value.CityName + '</option>';
                    //optionsValues += '</select>';
                });

                var reraoptions = $('#billing_city');
                reraoptions.html(optionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }
}

function fillCommcity(State) {
    $('#comm_city').html("");
    if (State == "SELECT STATE") {
        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#comm_city').html(cityoptionsValues);
    }
    else {
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetCity/' + State,
            processData: false,
            async: false,
            success: function (response) {

                var optionsValues = ''
                //'<select>';
                optionsValues += '<option value="SELECT CITY"> SELECT CITY </option>';

                $.each(response.GetCityjsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.CityId + '">' + value.CityName + '</option>';
                    //optionsValues += '</select>';
                });

                var reraoptions = $('#comm_city');
                reraoptions.html(optionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }
}

function fillRegcity(State) {
    $('#reg_city').html("");
    if (State == "SELECT STATE") {
        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#reg_city').html(cityoptionsValues);
    }
    else {
        debugger
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetCity/' + State,
            processData: false,
            async: false,
            success: function (response) {

                var optionsValues = ''
                //'<select>';
                optionsValues += '<option value="SELECT CITY"> SELECT CITY </option>';

                $.each(response.GetCityjsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.CityId + '">' + value.CityName + '</option>';
                    //optionsValues += '</select>';
                });

                var reraoptions = $('#reg_city');
                reraoptions.html(optionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }
}

// To fill dropdown for billing country for only send enquery
function fillCommunicationcity(State) {
    $('#communication_city').html("");
    if (State == "SELECT STATE") {
        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#communication_city').html(cityoptionsValues);
    }
    else {
    	if ($("#same_address").is(":checked")) {
    		$('#registered_state').val($('#communication_state').val());
        }
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetCity/' + State,
            processData: false,
            async: false,
            success: function (response) {
                var optionsValues = '<select>';
                optionsValues += '<option value="SELECT CITY">SELECT CITY </option>';

                $.each(response.GetCityjsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.CityId + '">' + value.CityName + '</option>';
                    optionsValues += '</select>';
                });

                var reraoptions = $('#communication_city');
                reraoptions.html(optionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }
}

// To fill dropdown for billing country for only send enquery
function fillRegisteredcity(State) {
    $('#registered_city').html("");
    if (State == "SELECT STATE") {

        var cityoptionsValues = '<option value="SELECT CITY"> SELECT CITY </option>';
        $('#registered_city').html(cityoptionsValues);
    }
    else {
        $.ajax({
            ServiceCallID: 1,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://cp.godrejproperties.com/gplservice/' + 'CPService.svc/GetCity/' + State,
            processData: false,
            async: false,
            success: function (response) {
                var optionsValues = '<select>';
                optionsValues += '<option value="SELECT CITY">SELECT CITY </option>';

                $.each(response.GetCityjsonResult, function (index, value) {
                    optionsValues += '<option value="' + value.CityId + '">' + value.CityName + '</option>';
                    optionsValues += '</select>';
                });

                var reraoptions = $('#registered_city');
                reraoptions.html(optionsValues);
            },
            failure: function (response) {
                alert(response.d);

            }
        });
    }
}

function changeCommumnicationCity(City){
     if ($("#same_address").is(":checked")) {
        $('#registered_city').val($('#communication_city').val());
    }
}

function DisplayNotification(LogID){
debugger
    var hasData = false;
    $.each(notificationData, function(index,value){
        if(value.NotificationID == LogID){      //if(value.NotificationID == LogID){    // NOTIFY Shreyas
            hasData = true;
        }
    });
    if(hasData){
        ShowNotificationDetails(LogID);
    }
    else{
        GetAllNotification(function(){
            console.log("DisplayNotification else LogID = " + LogID);
            ShowNotificationDetails(LogID);
        });
    }
}

function ShowNotificationDetails(LogID){
    $.each(notificationData, function(index,value){
    console.log(value);
        if(value.NotificationID == LogID){       //if(value.NotificationLogID == LogID){    // NOTIFY Shreyas
            $("#notification_title").html(value.NotificationTitle);
            $("#notification_body").html(value.NotificationBody);
            $("#notification_image").html("<img src='" + baseURL + value.NotificationImage + "' width='100%' height='auto' />");
        }
    });
}

var notificationData = []

function GetAllNotification(CallBack){
    var DeviceLogID = getDeviceLogID();
    $.ajax({
        type:"GET",
        content: "application/json; charset=utf-8",                                                 // NOTIFY Shreyas
        url: baseAPI + "GetNotificationLog.ashx?devicelogid=" + localStorage.getItem("emplcontid"), //DeviceLogID, //localStorage.getItem("emplcontid")
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response)

            notificationData = response;
            CallBack();

        }

    });
}

function fillNotifcation(){
    debugger
    var ul = $("#ul_notif");
    ul.html("");
    if(notificationData.length > 0){
        $.each(notificationData, function(index,value){     //NotificationLogID
            ul.append('<li><a href="notification_details.html?id=' + value.NotificationID + '" data-transition="slide">' + value.NotificationTitle + '<br><span>' + value.SubTitle + '</span></a></li>');
        });
    }
}

function DisplayAllNotification(){
    console.log("DisplayAllNotification");
    GetAllNotification(fillNotifcation);
    //$.ajax({});
}

// To display project list
function DisplayAllProjects(cityid) {
    $("#ul_inboxproj").html("");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc" + '/DisplayAllProjects/'+cityid,
        async: false,
        success: function (response) {
            var ul = $("#ul_inboxproj");
            $.each(response.DisplayAllProjectsResult, function (index, value) {
                if (value.Status == "Success") {
                    ul.append('<li><a href="my-inbox.html?projectid=' + value.projectid + '" data-transition="slide">' + value.title + '<br><span>' + value.cityname + '</span></a></li>');
                }
                else
                    ul.append('<li style="text-align:center;">No properties found</li>');
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

//////////////////////////////////////////// New APIs Implementation ////////////////////////////////////////////////////////////////

function GetEOICity() {
    alert("userid " + localStorage.getItem("emplcontid"));
    $('.ui-loader').show();
    $("#ddlProject").html('<option value="SELECT PROJECT"> SELECT PROJECT* </option>');
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'RestServiceImpl.svc/GetCities',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="CHOOSE CITY">  CHOOSE CITY* </option>';
            $.each(response.FetchCitiesResult, function (index, value) {
                if (value.status == "success") {
                    optionsValues += '<option value="' + value.cityid + '">' + value.cityname + '</option>';
                }
            });
            var options = $('#ddlCity');
            options.html(optionsValues);
            $('.ui-loader').hide();
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

// To display EOI project list
function DisplayEOIProjects(cityid) {
    $("#ddlProject").html("");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc" + '/DisplayAllProjects/'+cityid,
        async: false,
        success: function (response) {
                        var optionsValues = ''
                        optionsValues += '<option value="SELECT PROJECT"> SELECT PROJECT* </option>';
                        $.each(response.DisplayAllProjectsResult, function (index, value) {
                            optionsValues += '<option value="' + value.projectid + '">' + value.title + '</option>';
                        });
                        var reraoptions = $('#ddlProject');
                        reraoptions.html(optionsValues);
                    },
                    failure: function (response) {
                       alert(response.d);
                    }
    });
}

// Submit EOI link
function ShareEOI(){
    console.log("Inside submit EOI link");
    var brokerid = localStorage.getItem("brokerid");
    var eoiCity = $("#ddlCity").val();
    var eoiProject = $("#ddlProject").val();
    var customerFirstName = $('#customerFirstName').val();
    var customerLastName = $('#customerLastName').val();
    var mobile = $('#mobileNumber').val();
    var email = $('#emailID').val();

  if (eoiCity == "CHOOSE CITY") {

                    $('#error_content').empty().html('Please select city.');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2({
                            callbackClose: function () {
                                var me = this;
                                $('.ui-loader').hide();
                                setTimeout(function () { me.close(); }, 2000);
                            }
                        });
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 500);
                    return false;
                }

            if (eoiProject == "SELECT PROJECT") {

                            $('#error_content').empty().html('Please select project.');
                            setTimeout(function () {
                             $('#enquiry_inline').simpledialog2({
                                    callbackClose: function () {
                                    var me = this;
                                    $('.ui-loader').hide();
                                    setTimeout(function () { me.close(); }, 2000);
                                }
                            });
                            $('.ui-simpledialog-screen').css('height', $(document).height());
                         }, 500);
                        return false;
                         }

    if (customerFirstName == null || customerFirstName == "" || customerFirstName.lenght > 100 || customerFirstName.lenght < 1) {
        console.log("Inside customerFirstName");
            $('#error_content').empty().html('Please enter customer first name.');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2({
                    callbackClose: function () {
                        var me = this;
                        $('.ui-loader').hide();
                    }
                });
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }

        var chkname = alphanumeric_checkname(customerFirstName);

        if (chkname == 'NO') {
            $('#error_content').empty().html('Please enter valid customer first name.');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2({
                    callbackClose: function () {
                        var me = this;
                        $('.ui-loader').hide();
                    }
                });
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }

        chkname = alphanumeric_checkname(customerLastName);

        if (chkname == 'NO') {
            $('#error_content').empty().html('Please enter valid customer last name.');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2({
                    callbackClose: function () {
                        var me = this;
                        $('.ui-loader').hide();
                    }
                });
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }


        if (customerLastName == null || customerLastName == "" || customerLastName.lenght > 100 || customerLastName.lenght < 1) {
            console.log("Inside customerLastName");
            $('#error_content').empty().html('Please enter customer last name.');
            setTimeout(function () {
                $('#enquiry_inline').simpledialog2({
                    callbackClose: function () {
                        var me = this;
                        $('.ui-loader').hide();
                    }
                });
                $('.ui-simpledialog-screen').css('height', $(document).height());
            }, 500);
            return false;
        }

         if (mobile == null || mobile == "") {
                $('#error_content').empty().html('Please enter mobile number.');
                setTimeout(function () {
                    $('#enquiry_inline').simpledialog2({
                        callbackClose: function () {
                            var me = this;
                            $('.ui-loader').hide();
                        }
                    });
                    $('.ui-simpledialog-screen').css('height', $(document).height());
                }, 500);
                return false;
            }

            if (mobile.length < 10 || mobile.length > 10) {
                $('#error_content').empty().html('Mobile number Should be 10 digits.');
                setTimeout(function () {
                    $('#enquiry_inline').simpledialog2({
                        callbackClose: function () {
                            var me = this;
                            $('.ui-loader').hide();
                        }
                    });
                    $('.ui-simpledialog-screen').css('height', $(document).height());
                }, 500);
                return false;
            }

            var numPattern = /\d+(,\d{1,3})?/;

            if (!numPattern.test(mobile)) {
                $('#error_content').empty().html('Please enter numeric value.');
                setTimeout(function () {
                    $('#enquiry_inline').simpledialog2({
                        callbackClose: function () {
                            var me = this;
                            $('.ui-loader').hide();
                        }
                    });
                    $('.ui-simpledialog-screen').css('height', $(document).height());
                }, 500);
                return false;
            }

            if (email == null || email == "" || email.lenght > 100) {
                  console.log("Inside email");
                $('#error_content').empty().html('Please enter email ID.');
                setTimeout(function () {
                    $('#enquiry_inline').simpledialog2({
                        callbackClose: function () {
                            var me = this;
                            $('.ui-loader').hide();
                        }
                    });
                    $('.ui-simpledialog-screen').css('height', $(document).height());
                }, 500);
                return false;
            }
            var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (!filter.test(email)) {
                console.log("Inside email",email);
                $('#error_content').empty().html('Please enter valid email id.');
                setTimeout(function () {
                    $('#enquiry_inline').simpledialog2({
                        callbackClose: function () {
                            var me = this;
                            $('.ui-loader').hide();
                        }
                    });
                    $('.ui-simpledialog-screen').css('height', $(document).height());
                }, 500);
                return false;
            }

var eoiData = {
               "brokerId":brokerid, "cityId": eoiCity, "projectId":eoiProject, "customerFirstName" : customerFirstName, "customerLastName" : customerLastName, "mobileNo" : mobile, "emailId" :email
}
                                 $('.ui-loader').show();
                                 $.ajax({
                                     type: "POST",
                                     url: baseURLEOI + 'EOIService.svc/saveEOIShareData',
                                     data: JSON.stringify(eoiData),
                                     contentType: "application/json; charset=utf-8",
                                     dataType: "json",
                                     processData: true,
                                     async: false,
                                     success: function (response) {
                                     $('.ui-loader').hide();
                                     if(response.status == "Success"){
                                     $('#error_content').empty().html('Thank you! EOI form has been sent successfully.');
                                         setTimeout(function () {
                                             $('#enquiry_inline').simpledialog2({
                                                 callbackClose: function () {
                                                     $('.ui-loader').hide();
                                                 }
                                             });
                                             $('.ui-simpledialog-screen').css('height', $(document).height());
                                         }, 500);
                                         }else if(response.status == "Fail"){
                                          $('.ui-loader').hide();
                                           alert('Something went wrong, Please try again later.');
                                         }
                                     },
                                     failure: function (response) {
                                         $('.ui-loader').hide();
                                         alert('Fail');
                                     }
                                 });
}

function SearchEOIStatus(){
    var brokerid = localStorage.getItem("brokerid");
    var eoiCity = $("#ddlCity").val();
    var eoiProject = $("#ddlProject").val();

  if (eoiCity == "CHOOSE CITY") {
                    $('#error_content').empty().html('Please select city.');
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2({
                            callbackClose: function () {
                                var me = this;
                                setTimeout(function () { me.close(); }, 2000);
                            }
                        });
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 500);
                    return false;
                }

  if (eoiProject == "SELECT PROJECT") {

                            $('#error_content').empty().html('Please select project.');
                            setTimeout(function () {
                             $('#enquiry_inline').simpledialog2({
                                    callbackClose: function () {
                                    var me = this;
                                    setTimeout(function () { me.close(); }, 2000);
                                }
                            });
                            $('.ui-simpledialog-screen').css('height', $(document).height());
                         }, 500);
                        return false;
                         }

                                var eoiSearchData = brokerid + "/" + eoiCity + "/" + eoiProject;
                                $('.ui-loader').show();
                                $.ajax({
                                        type: "GET",
                                        contentType: "application/json; charset=utf-8",
                                        url: baseURLEOI + 'EOIService.svc/getEOIStatus/'+ eoiSearchData,
                                        async: false,
                                        success: function (response) {
                                                if (response.status == "Success") {
                                                     setTimeout(function () {
                                                          $('.ui-loader').hide();
                                                          }, 500);

                                                if(response.data.length>0){
                                                document.getElementById("noDataFound").style.display = "none"; // to hide
                                                $(".tblEOIStatusRow").remove();
                                                  var EOIStatusTable = document.getElementById("EOIStatus");
                                                  $.each(response.data, function (index, value) {
                                                        var tableRow = EOIStatusTable.insertRow(index+1);
                                                        $(tableRow).css({"border-bottom": "1px solid grey", "text-align": "center"});
                                                         tableRow.className="tblEOIStatusRow";
                                                         var mobile = tableRow.insertCell(0);
                                                         var dateSharing = tableRow.insertCell(1);
                                                         var status = tableRow.insertCell(2);
                                                         mobile.innerHTML = value.mobileNo;
                                                         dateSharing.innerHTML = value.dateOfSharing;
                                                         var checkedStatus = (value.eoiVisited)?"<span style='color:green'>CHECKED</span>":"<span style='color:red'>NOT-CHECKED</span>";
                                                         var submittedStatus = (value.eoiSubmited)?"<span style='color:green'>SUBMITTED</span>":"<span style='color:red'>NOT-SUBMITTED</span>";
                                                         status.innerHTML = checkedStatus + " | " + submittedStatus;
                                                     });
                                            }else{
                                            $(".tblEOIStatusRow").remove();
                                            document.getElementById("noDataFound").style.display = "block"; // to show
                                            $('.ui-loader').hide();
                                            }
                                            }
                                             else{
                                             $('.ui-loader').hide();
                                                  }
                                    },failure: function (response) {
                                         $('.ui-loader').hide();
                                        alert('Fail');
                                       }
          });
}

var WalkInFiscalYearData = [];
// To display quarter and financial year of walk in status.
function GetWalkInFiscalYear() {
    var userid = localStorage.getItem("emplcontid");

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + "GetEnquiryFiscalYear.ashx?userid="+ userid,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            var optionsValues = ''
            //optionsValues += '<option value="SELECT QUARTER & FINANCIAL YEAR"> SELECT QUARTER & FINANCIAL YEAR* </option>';
            optionsValues += '<option value="">SELECT FINANCIAL YEAR*</option>';
            if (response.status == "Success") {
                WalkInFiscalYearData = response.data;
                $.each(WalkInFiscalYearData, function (index, value) {
                    optionsValues += '<option value="' + value.FiscalYearID + '">' + value.FiscalYearName + '</option>';
                });
            }
            var walkInFiscalOptions = $('#ddlWalkInFiscal');
            walkInFiscalOptions.html(optionsValues);
            $("#ddlWalkInQuarters").html('<option value="">SELECT QUARTER* </option>')
       },
       error: DefaultAjaxError
    });
}

function fillWalkInQuarters(fiscalyearID, WalkInQuarter){
    if(fiscalyearID == ""){
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a financial year.');
        return false;
    }
    var optionsValues = ''
    optionsValues += '<option value="">SELECT QUARTER*</option>';
    $.each(WalkInFiscalYearData, function (index, value) {
        if(value.FiscalYearID == fiscalyearID){
            $.each(value.Quarters, function (index_Q, value_Q) {
                if(typeof WalkInQuarter === 'undefined')
                    optionsValues += '<option value="' + value_Q.QuarterName + '">' + value_Q.QuarterName + '</option>';
                else if (WalkInQuarter == value_Q.QuarterName)
                    optionsValues += '<option value="' + value_Q.QuarterName + '"  selected="selected">' + value_Q.QuarterName + '</option>';
                else
                    optionsValues += '<option value="' + value_Q.QuarterName + '">' + value_Q.QuarterName + '</option>';
            });
        }
    });
    var walkInQuartersOptions = $('#ddlWalkInQuarters');
    walkInQuartersOptions.html(optionsValues);
}

function walkInExcel(){
    var userid = localStorage.getItem("emplcontid");
    var fiscalYearID  = $('#ddlWalkInFiscal').val();
    if(fiscalYearID == ""){
        showMsg('Please select a financial year.');
        return false;
    }
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "MailEnquiryExcel.ashx?userid="+ userid + "&fid=" + fiscalYearID,
        success: function (response) {
            console.log(response);
            showMsg('Email has been sent on your registered email ID');
        }
    });
    return false;
}

function bookingExcel(){
    var userid = localStorage.getItem("emplcontid");
    var fiscalYearID  = $('#ddlBookingFiscal').val();
    if(fiscalYearID == ""){
        showMsg('Please select a financial year.');
        return false;
    }
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "MailBookingExcel.ashx?userid="+ userid + "&fid=" + fiscalYearID,
        success: function (response) {
            console.log(response);
            showMsg('Email has been sent on your registered email ID');
        }
    });
    return false;
}

function bookingCancelledExcel(){
    var userid = localStorage.getItem("emplcontid");
    var fiscalYearID  = $('#ddlBookingFiscal').val();
    if(fiscalYearID == ""){
        showMsg('Please select a financial year.');
        return false;
    }
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "MailBookingCancelExcel.ashx?userid="+ userid + "&fid=" + fiscalYearID,
        success: function (response) {
            console.log(response);
            showMsg('Email has been sent on your registered email ID');
        }
    });
    return false;
}

// To display quarter and financial year of booking status.
var BookingFiscalYearData = [];
function GetBookingFiscalYear() {
    //var panno = localStorage.getItem("panno");
    var userid = localStorage.getItem("emplcontid");

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "GetBookingFiscalYear.ashx?userid="+ userid,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            var optionsValues = ''
            optionsValues += '<option value="">SELECT FINANCIAL YEAR*</option>';
            if (response.status == "Success") {
                BookingFiscalYearData = response.data;
                $.each(BookingFiscalYearData, function (index, value) {
                    optionsValues += '<option value="' + value.FiscalYearID + '">' + value.FiscalYearName + '</option>';
                });
            }
            var BookingFiscalOptions = $('#ddlBookingFiscal');
            BookingFiscalOptions.html(optionsValues);
            $("#ddlBookingQuarters").html('<option value="">SELECT QUARTER* </option>');
       },
       error: DefaultAjaxError
    });
}

//var BookingCancelFiscalYearData = [];
function GetBookingCancelFiscalYear() {
    //var panno = localStorage.getItem("panno");
    var userid = localStorage.getItem("emplcontid");

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "GetBookingFiscalYear.ashx?userid="+ userid,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            var optionsValues = ''
            optionsValues += '<option value="">SELECT FINANCIAL YEAR*</option>';
            if (response.status == "Success") {
                BookingFiscalYearData = response.data;
                $.each(BookingFiscalYearData, function (index, value) {
                    optionsValues += '<option value="' + value.FiscalYearID + '">' + value.FiscalYearName + '</option>';
                    return false;
                });
            }
            var BookingFiscalOptions = $('#ddlBookingFiscal');
            BookingFiscalOptions.html(optionsValues);
            $("#ddlBookingQuarters").html('<option value="">SELECT QUARTER* </option>');
       },
       error: DefaultAjaxError
    });
}

var InvoiceFiscalYearData = [];
function GetInvoiceFiscalYear() {
    debugger
    //var panno = localStorage.getItem("panno");
    var userid = localStorage.getItem("emplcontid");

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + "GetInvoiceFiscalYear.ashx?userid="+ userid,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            var optionsValues = ''
            optionsValues += '<option value="">SELECT FINANCIAL YEAR*</option>';
            if (response.status == "Success") {
                InvoiceFiscalYearData = response.data;
                $.each(InvoiceFiscalYearData, function (index, value) {
                    optionsValues += '<option value="' + value.FiscalYearID + '">' + value.FiscalYearName + '</option>';
                });
            }
            var InvoiceFiscalOptions = $('#ddlInvoiceFiscal');
            InvoiceFiscalOptions.html(optionsValues);
            $("#ddlInvoiceQuarters").html('<option value="">SELECT QUARTER* </option>');
       },
       error: DefaultAjaxError
    });
}

var InvoiceDownloadFiscalYearData = [];
function GetInvoiceDownloadFiscalYear() {
    debugger
    //var panno = localStorage.getItem("panno");
    var userid = localStorage.getItem("emplcontid");

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "GetInvoiceFiscalYear.ashx?userid="+ userid,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            var optionsValues = ''
            optionsValues += '<option value="">SELECT FINANCIAL YEAR*</option>';
            if (response.status == "Success") {
                InvoiceDownloadFiscalYearData = response.data;
                $.each(InvoiceDownloadFiscalYearData, function (index, value) {
                    optionsValues += '<option value="' + value.FiscalYearID + '">' + value.FiscalYearName + '</option>';
                });
            }
            var InvoiceDownloadFiscalOptions = $('#ddlInvoiceDownloadFiscal');
            InvoiceDownloadFiscalOptions.html(optionsValues);
            $("#ddlInvoiceDownloadQuarters").html('<option value="">SELECT QUARTER* </option>');
            $("#ddlInvoiceDownloadRegion").html('<option value="">SELECT REGION* </option>');
            $("#ddlInvoiceDownloadProject").html('<option value="">SELECT PROJECT* </option>');
       },
       error: DefaultAjaxError
    });
}

function fillBookingQuarters(fiscalyearID, BookingQuarter){
    if(fiscalyearID == ""){
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a financial year.');
        return false;
    }
    var optionsValues = ''
    optionsValues += '<option value="">SELECT QUARTER*</option>';
    $.each(BookingFiscalYearData, function (index, value) {
        if(value.FiscalYearID == fiscalyearID){
            $.each(value.Quarters, function (index_Q, value_Q) {
                if(typeof BookingQuarter === 'undefined')
                    optionsValues += '<option value="' + value_Q.QuarterName + '">' + value_Q.QuarterName + '</option>';
                else if (BookingQuarter == value_Q.QuarterName)
                    optionsValues += '<option value="' + value_Q.QuarterName + '"  selected="selected">' + value_Q.QuarterName + '</option>';
                else
                    optionsValues += '<option value="' + value_Q.QuarterName + '">' + value_Q.QuarterName + '</option>';
            });
        }
    });
    var BookingFiscalOptions = $('#ddlBookingQuarters');
    BookingFiscalOptions.html(optionsValues);
}

function fillInvoiceQuarters(fiscalyearID, BookingQuarter){
    debugger
    if(fiscalyearID == ""){
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a financial year.');
        return false;
    }
    var optionsValues = ''
    optionsValues += '<option value="">SELECT QUARTER*</option>';
    $.each(InvoiceFiscalYearData, function (index, value) {
        if(value.FiscalYearID == fiscalyearID){
            $.each(value.Quarters, function (index_Q, value_Q) {
                if(typeof BookingQuarter === 'undefined')
                    optionsValues += '<option value="' + value_Q.QuarterName + '">' + value_Q.QuarterName + '</option>';
                else if (BookingQuarter == value_Q.QuarterName)
                    optionsValues += '<option value="' + value_Q.QuarterName + '"  selected="selected">' + value_Q.QuarterName + '</option>';
                else
                    optionsValues += '<option value="' + value_Q.QuarterName + '">' + value_Q.QuarterName + '</option>';
            });
        }
    });
    optionsValues += '<option value="ALL">ALL QUARTER</option>';
    var BookingFiscalOptions = $('#ddlInvoiceQuarters');
    BookingFiscalOptions.html(optionsValues);
}

function fillInvoiceDownloadQuarters(fiscalyearID, BookingQuarter){
    if(fiscalyearID == ""){
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a financial year.');
        return false;
    }
    var optionsValues = ''
    optionsValues += '<option value="">SELECT QUARTER*</option>';
    $.each(InvoiceDownloadFiscalYearData, function (index, value) {
        if(value.FiscalYearID == fiscalyearID){
            $.each(value.Quarters, function (index_Q, value_Q) {
                if(typeof BookingQuarter === 'undefined')
                    optionsValues += '<option value="' + value_Q.QuarterName + '">' + value_Q.QuarterName + '</option>';
                else if (BookingQuarter == value_Q.QuarterName)
                    optionsValues += '<option value="' + value_Q.QuarterName + '"  selected="selected">' + value_Q.QuarterName + '</option>';
                else
                    optionsValues += '<option value="' + value_Q.QuarterName + '">' + value_Q.QuarterName + '</option>';
            });
        }
    });
    var BookingFiscalOptions = $('#ddlInvoiceDownloadQuarters');
    BookingFiscalOptions.html(optionsValues);
}

function fillInvoiceDownloadRegion(){

}

function GetWalkInFiscalYearSD(){

    var userid = localStorage.getItem("emplcontid");
    var WalkInFiscalYearID = localStorage.getItem("WalkInFiscalYearID");
    var WalkInQuarter = localStorage.getItem("WalkInQuarter");
    var WalkInProjectId = localStorage.getItem("WalkInProjectId");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "GetEnquiryFiscalYear.ashx?userid="+ userid,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            var optionsValues = ''
            optionsValues += '<option value="">SELECT FINANCIAL YEAR*</option>';
            if (response.status == "Success") {
                WalkInFiscalYearData = response.data;
                $.each(WalkInFiscalYearData, function (index, value) {
                    if(WalkInFiscalYearID == value.FiscalYearID)
                        optionsValues += '<option value="' + value.FiscalYearID + '" selected="selected">' + value.FiscalYearName + '</option>';
                    else
                        optionsValues += '<option value="' + value.FiscalYearID + '">' + value.FiscalYearName + '</option>';
                });
            }
            var WalkInFiscalOptions = $('#ddlWalkInFiscal');
            WalkInFiscalOptions.html(optionsValues);
            fillWalkInQuarters(WalkInFiscalYearID, WalkInQuarter);
            displayWalkInSummaryStatus(WalkInQuarter, WalkInProjectId);
        },
        error: DefaultAjaxError
    });

}

function GetBookingFiscalYearSD() {
    //var panno = localStorage.getItem("panno");
    var userid = localStorage.getItem("emplcontid");
    var BookingFiscalYearID = localStorage.getItem("BookingFiscalYearID");
    var BookingQuarter = localStorage.getItem("BookingQuarter");
    var BookingProjectId = localStorage.getItem("BookingProjectId");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "GetBookingFiscalYear.ashx?userid="+ userid,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            var optionsValues = ''
            optionsValues += '<option value="">SELECT FINANCIAL YEAR*</option>';
            if (response.status == "Success") {
                BookingFiscalYearData = response.data;
                $.each(BookingFiscalYearData, function (index, value) {
                    if(BookingFiscalYearID == value.FiscalYearID)
                        optionsValues += '<option value="' + value.FiscalYearID + '" selected="selected">' + value.FiscalYearName + '</option>';
                    else
                        optionsValues += '<option value="' + value.FiscalYearID + '">' + value.FiscalYearName + '</option>';
                });
            }
            var BookingFiscalOptions = $('#ddlBookingFiscal');
            BookingFiscalOptions.html(optionsValues);
            fillBookingQuarters(BookingFiscalYearID, BookingQuarter);
            /*
            optionsValues += '<option value="SELECT QUARTER & FINANCIAL YEAR"> SELECT QUARTER & FINANCIAL YEAR* </option>';
            if (response.status == "Success") {
                $.each(response.data, function (index, value) {
                    if(BookingQuarter == value.QuarterName)
                        optionsValues += '<option value="' + value.QuarterName + '" selected="selected">' + value.QuarterName + '</option>';
                    else
                        optionsValues += '<option value="' + value.QuarterName + '">' + value.QuarterName + '</option>';
                });
            }
            var reraoptions = $('#ddlBookingQuarters');
            reraoptions.html(optionsValues);

            */
            displayBookingSummaryStatus(BookingQuarter, BookingProjectId);
       },
       error: DefaultAjaxError
    });
}

function GetBookingCancelFiscalYearSD() {
    //var panno = localStorage.getItem("panno");
    var userid = localStorage.getItem("emplcontid");
    var BookingFiscalYearID = localStorage.getItem("BookingFiscalYearID");
    var BookingQuarter = localStorage.getItem("BookingQuarter");
    var BookingProjectId = localStorage.getItem("BookingProjectId");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + "CPProperties.svc/getQuarters/"+ panno + "/" + 1,
        url: baseAPI + "GetBookingFiscalYear.ashx?userid="+ userid,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            var optionsValues = ''
            optionsValues += '<option value="">SELECT FINANCIAL YEAR*</option>';
            if (response.status == "Success") {
                BookingFiscalYearData = response.data;
                $.each(BookingFiscalYearData, function (index, value) {
                    if(BookingFiscalYearID == value.FiscalYearID)
                        optionsValues += '<option value="' + value.FiscalYearID + '" selected="selected">' + value.FiscalYearName + '</option>';
                    else
                        optionsValues += '<option value="' + value.FiscalYearID + '">' + value.FiscalYearName + '</option>';
                });
            }
            var BookingFiscalOptions = $('#ddlBookingFiscal');
            BookingFiscalOptions.html(optionsValues);
            fillBookingQuarters(BookingFiscalYearID, BookingQuarter);
            /*
            optionsValues += '<option value="SELECT QUARTER & FINANCIAL YEAR"> SELECT QUARTER & FINANCIAL YEAR* </option>';
            if (response.status == "Success") {
                $.each(response.data, function (index, value) {
                    if(BookingQuarter == value.QuarterName)
                        optionsValues += '<option value="' + value.QuarterName + '" selected="selected">' + value.QuarterName + '</option>';
                    else
                        optionsValues += '<option value="' + value.QuarterName + '">' + value.QuarterName + '</option>';
                });
            }
            var reraoptions = $('#ddlBookingQuarters');
            reraoptions.html(optionsValues);

            */
            displayBookingCancelSummaryStatus(BookingQuarter, BookingProjectId);
       },
       error: DefaultAjaxError
    });
}

var trBookingDisplay = ""
function displayBookingSummary(quarter){
    var userid = localStorage.getItem("emplcontid");
    var fiscalYearID  = $('#ddlBookingFiscal').val();
    if (quarter == "") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a quarter');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + 'GetBookingSummary.ashx',
        data:{"userid" : userid, "quarter": quarter},
        dataType: "json",
        async: false,
        success: function (response) {
            console.log(response);
            //alert("Called");
            if(response.length > 0){
                $("#noDataFound").hide();
                $(".tblBookingSummaryRow").remove();
                $(".tblBookingTotalRow").remove();
                $BookingSummaryTable = $("#BookingSummary");
                var totalBooking = 0; totalNetBV = 0;
                $.each(response, function (index, value) {
                    totalBooking = totalBooking + parseInt(value.BookingCount);
                    totalNetBV = totalNetBV + parseInt(value.NetBV);
                    $tr = $("<tr></tr>");
                    $tr.addClass("tblBookingSummaryRow");
                    $tr.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                    $tr.append("<td>"+value.Location+"</td>");
                    $tr.append("<td style='text-align:center;'>"+value.BookingCount+"</td>");
                    $tr.append("<td style='text-align:center;'>"+(parseFloat(value.NetBV)/10000000).toFixed(2)+"</td>");
                    $tr.click(function(){
                        $(".tblBSRP").hide();
                        if(trBookingDisplay == ".BS"+value.Location){
                            trBookingDisplay = "";
                            $(".BS"+value.Location).hide();
                        }
                        else{
                            $(".BS"+value.Location).show();
                            trBookingDisplay = ".BS"+value.Location;
                        }
                    });
                    $BookingSummaryTable.append($tr);
                    $.each(value.detail, function (i, v) {
                        $trp = $("<tr></tr>");
                        $trp.addClass("tblBookingSummaryRow");
                        $trp.addClass("tblBSRP");
                        $trp.addClass("BS"+value.Location);
                        $trp.css({"border-bottom": "1px solid grey"});
                        $trp.append("<td><a href='#' onclick=\"RedirectBookingStatus('"+fiscalYearID+"','" + quarter + "', '"+ v.SFDCProjectID +"')\">"+v.ProjectName+"</a></td>");
                        $trp.append("<td style='text-align:center;'>"+v.BookingCount+"</td>");
                        $trp.append("<td style='text-align:center;'>"+(parseFloat(v.NetBV)/10000000).toFixed(2)+"</td>");
                        $trp.hide();
                        $BookingSummaryTable.append($trp);
                    });
                });
                $trt = $("<tr  class='tblBookingTotalRow'></tr>");
                $trt.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                $trt.append("<td>Total</td>");
                $trt.append("<td style='text-align:center;'>"+totalBooking+"</td>");
                $trt.append("<td style='text-align:center;'>"+(parseFloat(totalNetBV)/10000000).toFixed(2)+"</td>");
                $BookingSummaryTable.append($trt);

            }
            else{
                $('.ui-loader').hide();
                $(".tblBookingSummaryRow").remove();
                $("#noDataFound").show();
            }
            $('.ui-loader').hide();
        },
        error: DefaultAjaxError
    });
}

var trBookingCancelDisplay = ""
function displayBookingCancelSummary(quarter){
    var userid = localStorage.getItem("emplcontid");
    var fiscalYearID  = $('#ddlBookingFiscal').val();
    if (quarter == "") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a quarter');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + 'GetBookingCancelSummary.ashx',
        data:{"userid" : userid, "quarter": quarter},
        dataType: "json",
        async: false,
        success: function (response) {
            console.log(response);
            //alert("Called");
            if(response.length > 0){
                $("#noDataFound").hide();
                $(".tblBookingSummaryRow").remove();
                $(".tblBookingTotalRow").remove();
                $BookingSummaryTable = $("#BookingSummary");
                var totalBooking = 0; totalNetBV = 0;
                $.each(response, function (index, value) {
                    totalBooking = totalBooking + parseInt(value.BookingCount);
                    totalNetBV = totalNetBV + parseInt(value.NetBV);
                    $tr = $("<tr></tr>");
                    $tr.addClass("tblBookingSummaryRow");
                    $tr.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                    $tr.append("<td>"+value.Location+"</td>");
                    $tr.append("<td style='text-align:center;'>"+value.BookingCount+"</td>");
                    $tr.append("<td style='text-align:center;'>"+(parseFloat(value.NetBV)/10000000).toFixed(2)+"</td>");
                    $tr.click(function(){
                        $(".tblBSRP").hide();
                        if(trBookingCancelDisplay == ".BS"+value.Location){
                            trBookingCancelDisplay = "";
                            $(".BS"+value.Location).hide();
                        }
                        else{
                            $(".BS"+value.Location).show();
                            trBookingCancelDisplay = ".BS"+value.Location;
                        }
                    });
                    $BookingSummaryTable.append($tr);
                    $.each(value.detail, function (i, v) {
                        $trp = $("<tr></tr>");
                        $trp.addClass("tblBookingSummaryRow");
                        $trp.addClass("tblBSRP");
                        $trp.addClass("BS"+value.Location);
                        $trp.css({"border-bottom": "1px solid grey"});
                        $trp.append("<td><a href='#' onclick=\"RedirectBookingCancelStatus('"+fiscalYearID+"','" + quarter + "', '"+ v.SFDCProjectID +"')\">"+v.ProjectName+"</a></td>");
                        $trp.append("<td style='text-align:center;'>"+v.BookingCount+"</td>");
                        $trp.append("<td style='text-align:center;'>"+(parseFloat(v.NetBV)/10000000).toFixed(2)+"</td>");
                        $trp.hide();
                        $BookingSummaryTable.append($trp);
                    });
                });
                $trt = $("<tr  class='tblBookingTotalRow'></tr>");
                $trt.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                $trt.append("<td>Total</td>");
                $trt.append("<td style='text-align:center;'>"+totalBooking+"</td>");
                $trt.append("<td style='text-align:center;'>"+(parseFloat(totalNetBV)/10000000).toFixed(2)+"</td>");
                $BookingSummaryTable.append($trt);

            }
            else{
                $('.ui-loader').hide();
                $(".tblBookingSummaryRow").remove();
                $("#noDataFound").show();
            }
            $('.ui-loader').hide();
        },
        error: DefaultAjaxError
    });
}

var trWalkInDisplay = "";
function displayWalkInSummary(quarter){
    var userid = localStorage.getItem("emplcontid");
    var fiscalYearID  = $('#ddlWalkInFiscal').val();
    if (quarter == "") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a quarter & financial year.');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + 'GetEnquirySummary.ashx',
        data:{"userid" : userid, "quarter": quarter},
        dataType: "json",
        async: false,
        success: function (response) {
            console.log(response);
            //alert("Called");
            if(response.length > 0){
                $("#noDataFound").hide();
                $(".tblWalkInSummaryRow").remove();
                $(".tblWalkInTotalRow").remove();
                var totalEC = 0;
                $WalkInSummaryTable = $("#WalkInSummary");
                $.each(response, function (index, value) {
                    totalEC = totalEC + parseInt(value.EnquiryCount);
                    $tr = $("<tr></tr>");
                    $tr.addClass("tblWalkInSummaryRow");
                    $tr.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                    $tr.append("<td>"+value.Location+"</td>");
                    $tr.append("<td style='text-align:center;'>"+value.EnquiryCount+"</td>");
                    $tr.click(function(){
                        $(".tblBSRP").hide();
                        if(trWalkInDisplay == ".BS"+value.Location){
                            trWalkInDisplay = "";
                            $(".BS"+value.Location).hide();
                        }
                        else{
                            $(".BS"+value.Location).show();
                            trWalkInDisplay = ".BS"+value.Location;
                        }
                    });
                    $WalkInSummaryTable.append($tr);
                    $.each(value.detail, function (i, v) {
                        $trp = $("<tr></tr>");
                        $trp.addClass("tblWalkInSummaryRow");
                        $trp.addClass("tblBSRP");
                        $trp.addClass("BS"+value.Location);
                        $trp.css({"border-bottom": "1px solid grey"});
                        $trp.append("<td><a href='#' onclick=\"RedirectWalkINStatus('" + fiscalYearID+ "', '" + quarter + "', '"+ v.SFDCProjectID +"')\">"+v.ProjectName+"</a></td>");
                       // $trp.append("<td>"+v.ProjectName+"</td>");
                        $trp.append("<td style='text-align:center;'>"+v.EnquiryCount+"</td>");
                        $trp.hide();
                        $WalkInSummaryTable.append($trp);
                    });
                });
                $trt = $("<tr  class='tblWalkInTotalRow'></tr>");
                $trt.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                $trt.append("<td>Total</td>");
                $trt.append("<td style='text-align:center;'>"+totalEC+"</td>");
                $WalkInSummaryTable.append($trt);
            }
            else{
                $('.ui-loader').hide();
                $(".tblWalkInSummaryRow").remove();
                $("#noDataFound").show();
            }
            $('.ui-loader').hide();
        },
        error: DefaultAjaxError
    });
}

var trInvoiceDisplay = ""
function displayInvoiceSummary(quarter){
    var userid = localStorage.getItem("emplcontid");
    var fiscalYearID  = $('#ddlBookingFiscal').val();
    if (quarter == "") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a quarter');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + '.ashx',
        data:{"userid" : userid, "quarter": quarter},
        dataType: "json",
        async: false,
        success: function (response) {
            console.log(response);
            //alert("Called");
            if(response.length > 0){
                $("#noDataFound").hide();
                $(".tblBookingSummaryRow").remove();
                $(".tblBookingTotalRow").remove();
                $InvoiceSummaryTable = $("#InvoiceSummary");
                var totalBooking = 0; totalNetBV = 0;
                debugger
                $.each(response, function (index, value) {
                    totalBooking = totalBooking + parseInt(value.BookingCount);
                    totalNetBV = totalNetBV + parseInt(value.NetBV);
                    $tr = $("<tr></tr>");
                    $tr.addClass("tblBookingSummaryRow");
                    $tr.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                    $tr.append("<td>"+value.Location+"</td>");                                          // TD Value 1
                    $tr.append("<td style='text-align:center;'></td>");                               // TD Value 2
                    $tr.append("<td style='text-align:center;'></td>");                                 // TD Value 3
                    $tr.append("<td style='text-align:center;'></td>");
                    $tr.click(function(){
                        $(".tblBSRP").hide();
                        if(trInvoiceDisplay == ".BS"+value.Location){
                            trInvoiceDisplay = "";
                            $(".BS"+value.Location).hide();
                        }
                        else{
                            $(".BS"+value.Location).show();
                            trInvoiceDisplay = ".BS"+value.Location;
                        }
                    });
                    $InvoiceSummaryTable.append($tr);
                    $.each(value.detail, function (i, v) {                          // Child rows logic
                        $trp = $("<tr></tr>");
                        $trp.addClass("tblBookingSummaryRow");
                        $trp.addClass("tblBSRP");
                        $trp.addClass("BS"+value.Location);
                        $trp.css({"border-bottom": "1px solid grey"});
                        $trp.append("<td><a href='#' onclick=\"RedirectInvoice()\">"+v.ProjectName+"</a></td>");
                        $trp.append("<td style='text-align:center;'></td>");
                        $trp.append("<td style='text-align:center;'></td>");
                        $trp.append("<td style='text-align:center;'></td>");
                        $trp.hide();
                        $InvoiceSummaryTable.append($trp);
                    });
                });
                $trt = $("<tr  class='tblBookingTotalRow'></tr>");
                $trt.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                $trt.append("<td>Total</td>");
                $trt.append("<td style='text-align:center;'></td>");
                $trt.append("<td style='text-align:center;'></td>");
                $trt.append("<td style='text-align:center;'></td>");
                $InvoiceSummaryTable.append($trt);

            }
            else{
                $('.ui-loader').hide();
                $(".tblBookingSummaryRow").remove();
                $("#noDataFound").show();
            }
            $('.ui-loader').hide();
        },
        error: DefaultAjaxError
    });
}           // Pending


function displayInvoiceDownloadRegion(quarter){
    var userid = localStorage.getItem("emplcontid");
    var fiscalYearID  = $('#ddlInvoiceDownloadFiscal').val();

    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + 'GetInvoiceRegionProject.ashx',
        data:{"flag" : "region", "userid" : userid, "quarter": quarter},
        dataType: "json",
        async: false,
        success: function (response) {
            console.log(response);
            //alert("Called");
            if(response.data.length > 0)
            {
                var optionsValues = ''
                optionsValues += '<option value="">SELECT REGION*</option>';
                $.each(response.data, function (__index, region) {
                    $.each(region, function (index, value) {
                    debugger
                        optionsValues += '<option value="' + value + '">' + value + '</option>';
                    });
                });
                var options = $('#ddlInvoiceDownloadRegion');
                options.html(optionsValues);
            }
            else{
                $('.ui-loader').hide();
                $("#noDataFound").show();
            }
            $('.ui-loader').hide();
        },
        error: DefaultAjaxError
    });
}

var trInvoiceDownloadDisplay = ""
function displayInvoiceDownload(region){
    var userid = localStorage.getItem("emplcontid");
    var quarter  = $('#ddlInvoiceDownloadQuarters').val();

    if (quarter == "") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        showMsg('Please select a quarter');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetInvoiceRegionProject.ashx',
        data:{"flag" : "project", "userid" : userid, "quarter": quarter, "region": region},
        dataType: "json",
        async: false,
        success: function (response) {
            console.log(response);
            //alert("Called");
            if(response.data.length > 0){
                $("#noDataFound").hide();
                $(".tblBookingSummaryRow").remove();
                $(".tblBookingTotalRow").remove();
                $InvoiceDownloadSummaryTable = $("#InvoiceDownload");
                var totalBooking = 0; totalNetBV = 0;
                debugger
                $.each(response.data, function (index, value) {
                    $tr = $("<tr></tr>");
                    $tr.addClass("tblBookingSummaryRow");
                    $tr.css({"border-bottom": "2px solid grey","font-weight":"bold"});
                    $tr.append("<td>"+value.Project+"</td>");                                                   // TD Value 1
                    $tr.append("<td style='text-align:center;'><a href='#' onclick='GetFilesDownloadInvoice(\""+value.Project+"\")'>Download (ZIP)</a></td>");                               // TD Value 2
                    $InvoiceDownloadSummaryTable.append($tr);
                });
            }
            else{
                $('.ui-loader').hide();
                $(".tblBookingSummaryRow").remove();
                $("#noDataFound").show();
            }
            $('.ui-loader').hide();
        },
        error: DefaultAjaxError
    });
}

function GetFilesDownloadInvoice(project)
{
    var userid = localStorage.getItem("emplcontid");
    $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: baseAPI + 'GetInvoiceRegionProject.ashx',
            data:{"flag" : "attachments", "userid" : userid, "project": project},
            dataType: "json",
            async: false,
            success: function (response) {
                console.log(response);
                if(response.data != "no_files")
                {
                    window.ChannelPartnerActivity.DownloadInvoice(response.data);
                }
            },
            error: DefaultAjaxError
        });
}

//function displayInvoiceDownload(project) {
//    var fiscalyear  = $('#ddlInvoiceDownloadFiscal').val();
//    var quarter  = $('#ddlInvoiceDownloadQuarters').val();
//    var region  = $('#ddlInvoiceDownloadRegion').val();
//    if(fiscalyear != null && quarter != null && region != null && project != null) {
//        show("#inv-download-btn");
//    }
//
//}

function RedirectInvoice(){
    //localStorage.setItem("BookingFiscalYearID", fiscalYearID);
    //localStorage.setItem("BookingQuarter", quarter);
    //localStorage.setItem("BookingProjectId", projectId);
    $.mobile.pageContainer.pagecontainer("change", "invoice_details.html", { transition: "none" });
}

function RedirectBookingStatus(fiscalYearID, quarter, projectId){
    localStorage.setItem("BookingFiscalYearID", fiscalYearID);
    localStorage.setItem("BookingQuarter", quarter);
    localStorage.setItem("BookingProjectId", projectId);
    $.mobile.pageContainer.pagecontainer("change", "booking_summary_status.html", { transition: "none" });
}

function RedirectBookingCancelStatus(fiscalYearID, quarter, projectId){
    localStorage.setItem("BookingFiscalYearID", fiscalYearID);
    localStorage.setItem("BookingQuarter", quarter);
    localStorage.setItem("BookingProjectId", projectId);
    $.mobile.pageContainer.pagecontainer("change", "booking_cancel_summary_status.html", { transition: "none" });
}

function RedirectWalkINStatus(fiscalYearID, quarter, projectId){
    localStorage.setItem("WalkInFiscalYearID", fiscalYearID);
    localStorage.setItem("WalkInQuarter", quarter);
    localStorage.setItem("WalkInProjectId", projectId);
    $.mobile.pageContainer.pagecontainer("change", "walk_in_summary_status.html", { transition: "none" });
}



// Shreyas Customs
function show(id){
    $(id).show();
}
function hide(id){
    $(id).find('input:text').val('');
    $(id).hide();
}
function getValue(id){
    return $(id).val();
}
function setValue(id, value){
    $(id).val(value);
}
function invalidate(id, error) {
    var value = $(id).val();
    if(!value) {
        if(error) {
            showMsg(error);
            return true;
        }
    }
}
//

function hideIds() {
for(var i=0; i<arguments.length; i++){
        hide(arguments[i]);
    }
}

function openUploadDialog(id) {
    uploadToServer(id);
}

function AddGstRow(objGST){
    console.log(objGST);
    var markup = "<tr data-i='"+ objGST.i + "'>";
    markup += "<td>" + objGST.state + "</td>";
    markup += "<td><a rel=" + objGST.gstcerti + " class='viewdoc' href='javascript:void(0);'>" + objGST.gstno + "</a></td>";
    markup += "<td>" + objGST.gstcerti + "</td>";
    if(objGST.EmplID == "0"){
        markup += "<td><a href='javascript:void(0)' class='editGst' title='Delete'>Edit</a></td>"
        markup += "<td><a href='javascript:void(0)' id='deleteGst' title='Delete'><i class='fa fa-trash' aria-hidden='true'></a></td>";
    }
    else{
        markup += "<td></td>";
        markup += "<td></td>";
    }
    markup += "</tr>";
    $("#tblGstList").append(markup);
}

function AddReraRow(objRera){
    var markup = "<tr data-i='"+ objRera.i + "'>";
    markup += "<td>" + objRera.state + "</td>";
    markup += "<td><a rel=" + objRera.reracerti + " class='viewdoc' href='javascript:void(0);'>" + objRera.rerano + "</a></td>";
    markup += "<td>" + objRera.reracerti + "</td>";
    if(objRera.EmplID == "0"){
        markup += "<td><a href='javascript:void(0)' class='editRera' title='Delete'>Edit</a></td>";
        markup += "<td><a href='javascript:void(0)' id='deleteRera' title='Delete'><i class='fa fa-trash' aria-hidden='true'></a></td>";
    }
    else{
        markup += "<td></td>";
        markup += "<td></td>";
    }
    markup += "</tr>";
    $("#tblReraList").append(markup);

}

function fillDocDetails(doctype) {

    $.ajax({
            type: "GET",
            url: baseAPI + 'GetProfileData.ashx?module=get_reragst&doctype=' + doctype + '&userid=' + localStorage.getItem("emplcontid"),
            processData: true,
            async: false,
            dataType: "json",
            success: function (resp) {
                var docs = resp.data;
                if(doctype == "GET_RERA") {
                    $.each(docs, function (index, row) {
                        AddReraRow(row);
                    });
                }
                if(doctype == "GET_GST") {
                    $.each(docs, function (index, row) {
                        AddGstRow(row);
                    });
                }
            },
            error: DefaultAjaxError
        });
}

function submitReqRera() {
    console.log(objReralist);
    var json = {
                userid: localStorage.getItem("emplcontid"),
                list : objReralist
//                list: [{
//                     reracerti:"rera_20200907120105.jpg",
//                     rerano:"22",
//                     state:"Andra Pradesh",
//                     insertflag : true;
//                  }]
               };

    $.ajax({
        type: "POST",
        url: baseAPI + 'GetProfileData.ashx?module=rera_submit',
        data: {"data":  JSON.stringify(json)},
        processData: true,
        async: false,
        dataType: "json",
        success: function (resp) {
        //debugger
            if(resp.data == "Added") {
                hide("#reqChg_Rera");
                showMsg("Request has been submitted.");
            }
            if(resp.data == "no_insertflag") {
                showMsg("No data to update.");
            }
        },
        error: DefaultAjaxError
    });
}

function submitReqGst() {
    console.log(objReralist);
    var json = {
                userid: localStorage.getItem("emplcontid"),
                list : objGSTlist
//                list: [{
//                     gstcerti:"gst_20200907120105.jpg",
//                     gstno:"22",
//                     state:"Andra Pradesh",
//                     insertflag : true
//                  }]
               };

    $.ajax({
        type: "POST",
        url: baseAPI + 'GetProfileData.ashx?module=gst_submit',
        data: {"data":  JSON.stringify(json)},
        processData: true,
        async: false,
        dataType: "json",
        success: function (resp) {
        //debugger
            if(resp.data == "Added") {
                hide("#reqChg_Gst");
                showMsg("Request has been submitted.");
            }
            if(resp.data == "no_insertflag") {
                showMsg("No data to update.");
            }
        },
        error: DefaultAjaxError
    });
}

function saveReqGst() {
    $('.ui-loader').show();
    var objGST = {};

    objGST.i = parseInt($("#gsti").val());
    objGST.id = $("#gstID").val();
    objGST.state = $("#gst_state").find(":selected").text();
    objGST.gstno = $("#gst_no").val();
    objGST.gstcerti = $("#hdn_gst").val();
    objGST.insertflag = true;

    if (objGST.state == "SELECT STATE*") {
        showMsg('Please select state.');
        return false;
    }
    if (objGST.gstno == "") {
        showMsg('Please enter GST No.');
        return false;
    }

    if (!ValidateGSTIN(objGST.gstno)) {
        showMsg('Please enter valid gstno.');
        return false;
    }
    if (objGST.gstcerti == "") {
        showMsg('Please select GST certificate.');
        return false;
    }

    var objGSTStateCheck = jQuery.grep(objGSTlist, function(value) {
        if(value.status == "D")
            return false;
        else
            return value.state == objGST.state;
    });
    var objGSTNoCheck = jQuery.grep(objGSTlist, function(value) {
        if(value.status == "D")
            return false;
        else
            return value.gstno == objGST.gstno;
    });
    objGST.comment = "";

    var markup = "";
    if(isNaN(objGST.i)){
        if(objGSTStateCheck.length > 0){
            showMsg('State aleady selected')
            return false;
        }
        if(objGSTNoCheck.length > 0){
            showMsg('GST No. aleady selected')
            return false;
        }
        if(objGSTlist.length > 0){
            objGST.i = objGSTlist[objGSTlist.length - 1].i + 1;
        }
        else{
            objGST.i = 0;
        }
        objGSTlist.push(objGST);
    }
    else{
        if(objGSTStateCheck.length > 0){
            if(objGSTStateCheck[0].i != objGST.i){
                showMsg('State aleady selected')
                return false;
            }
        }
        if(objGSTNoCheck.length > 0){
            if(objGSTNoCheck[0].i != objGST.i){
                showMsg('GST No. aleady selected')
                return false;
            }
        }
        for(var i = 0; i < objGSTlist.length; i++){
            if(objGSTlist[i].i == objGST.i){
                objGSTlist[i] = objGST;
                break;
            }
        }
        $("#tblGstList tr[data-i="+ objGST.i+"]").remove();
    }

    AddTrGST(objGST);
    console.log(objGSTlist)

    ClearGSTFrom();
    $('.gst_popup_wrap').hide();
    $('.ui-loader').hide();
}

function saveReqRera() {
    debugger
    var objRera = {};
    objRera.i = parseInt($("#rerai").val());
    objRera.id = $("#reraID").val();
    objRera.state = $("#rera_state").find(":selected").text();
    objRera.rerano = $("#rerano").val();
    objRera.reracerti = $("#hdn_rera").val();
    objRera.insertflag = true;

    if (objRera.state == "SELECT STATE*") {
        showMsg('Please select state.');
        return false;
    }
    if (objRera.rerano == "") {
        showMsg('Please enter RERA No.');
        return false;
    }

    if (objRera.reracerti == "") {
        showMsg('Please select RERA certificate.')
        return false;
    }

    var objReraStateCheck = jQuery.grep(objReralist, function(value) {
        if(value.status == "D")
            return false;
        else
            return value.state == objRera.state;
    });
    var objReraNoCheck = jQuery.grep(objReralist, function(value) {
        if(value.status == "D")
            return false;
        else
            return value.rerano == objRera.rerano;
    });
    objRera.comment = "";


    if(isNaN(objRera.i)){
        if(objReraStateCheck.length > 0){
            showMsg('State aleady selected')
            return false;
        }
        if(objReraNoCheck.length > 0){
            showMsg('Rera no. aleady selected')
            return false;
        }
        if(objReralist.length > 0){
            objRera.i = objReralist[objReralist.length - 1].i + 1;
        }
        else{
            objRera.i = 0;
        }
        objReralist.push(objRera);
    }
    else{
        if(objReraStateCheck.length > 0){
            if(objReraStateCheck[0].i != objRera.i){
                showMsg('State aleady selected')
                return false;
            }
        }
        if(objReraNoCheck.length > 0){
            if(objReraNoCheck[0].i != objRera.i){
                showMsg('Rera no. aleady selected')
                return false;
            }
        }
        for(var i = 0; i < objReralist.length; i++){
            if(objReralist[i].i == objRera.i){
                objReralist[i] = objRera;
                break;
            }
        }
        $("#tblReraList tr[data-i="+ objRera.i+"]").remove();
    }

    console.log(objReralist);
    AddTrRera(objRera);
    ClearReraFrom();
}

function submitLoginGst() {
    var input_user = getValue("#txtVerifyUser_Gst");
    var input_pass = getValue("#txtVerifyPass_Gst");
    if(invalidate("#txtVerifyUser_Gst", "Please enter username")) {
        return false
    }
    if(invalidate("#txtVerifyPass_Gst", "Please enter password")) {
        return false
    }

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetProfileData.ashx',
        data: 'userid=' + localStorage.getItem("emplcontid") + '&module=reragst_login' +
        '&input_user=' + input_user + '&input_pass=' + input_pass,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            if(response.data == "False") {
                showMsg("Invalid Login Details.");
            }
            if(response.data == "True") {
                hide("#txtVerifyUser_Gst");
                hide("#txtVerifyPass_Gst");
                hide("#loginsubmit_gst");
                show("#gst_block");
            }
        },
        error: DefaultAjaxError
    });
}

function submitLoginRera() {
    var input_user = getValue("#txtVerifyUser_Rera");
    var input_pass = getValue("#txtVerifyPass_Rera");
    if(invalidate("#txtVerifyUser_Rera", "Please enter username")) {
        return false
    }
    if(invalidate("#txtVerifyPass_Rera", "Please enter password")) {
        return false
    }

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetProfileData.ashx',
        data: 'userid=' + localStorage.getItem("emplcontid") + '&module=reragst_login' +
        '&input_user=' + input_user + '&input_pass=' + input_pass,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            if(response.data == "False") {
                showMsg("Invalid Login Details.");
            }
            if(response.data == "True") {
                hide("#txtVerifyUser_Rera");
                hide("#txtVerifyPass_Rera");
                hide("#loginsubmit_rera");
                show("#rera_block");
            }
        },
        error: DefaultAjaxError
    });
}

function submitReqRegAddress() {
    debugger
    var oldRegCountry = getValue("#old_RegCountry");
    var newRegCountry = $("#new_RegCountry").find(":selected").text();

    var oldRegState = getValue("#old_RegState");
    var oldRegCity = getValue("#old_RegCity");

    if($('#new_RegCountry option:selected').text().toUpperCase()=="INDIA")
    {
       var newRegCity = $("#reg_city").find(":selected").text();
       var newRegState = $("#reg_state").find(":selected").text();
    }
    else
    {
       var newRegCity = getValue("#txtreg_city");
       var newRegState = getValue("#txtreg_state");
    }

    var oldRegAddress = getValue("#old_RegAddress");
    var newRegAddress = getValue("#new_RegAddress");
    var oldRegZipCode = getValue("#old_RegZipCode");
    var newRegZipCode = getValue("#new_RegZipCode");

    var addrProof = getValue("#hdn_AddrProof");
    if(invalidate("#new_RegAddress", "Please enter new Registered Address")) {
        return false
    }

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetProfileData.ashx',
        data: 'userid=' + localStorage.getItem("emplcontid") + '&module=reg_addr'
        + '&oldRegAddress=' + oldRegAddress + '&newRegAddress=' + newRegAddress
        + '&oldRegCountry=' + oldRegCountry + '&newRegCountry=' + newRegCountry
        + '&oldRegState=' + oldRegState + '&newRegState=' + newRegState
        + '&oldRegCity=' + oldRegCity + '&newRegCity=' + newRegCity
        + '&oldRegZipCode=' + oldRegZipCode + '&newRegZipCode=' + newRegZipCode
        + '&addrProof='+ addrProof,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.data == "Updated") {
                hide("#reqChg_RegAddress");
                showMsg("Request sent for approval.");
            }
        },
        error: DefaultAjaxError
    });
}

function setAddressProof(filename) {
    debugger
    setValue("#upload_AddrProof", filename);
    setValue("#hdn_AddrProof", filename);
}

function GetUserProfileDetails() {
    var result;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetProfileData.ashx',
        data: 'userid=' + localStorage.getItem("emplcontid") + '&module=get_all',
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            result = response.data[0];
        },
        error: DefaultAjaxError
    });
    return result;
}

function GetAccessToken() {
    var result;
    debugger
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetProfileData.ashx',
        data: 'module=getToken',
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            result = response.data;
        },
        error: DefaultAjaxError
    });
    return result;
}

function UpdateSFDC(body)
{
    debugger
    var result;
    var token = GetAccessToken();
    console.log("api", updateURL)
    console.log("body", body)
    console.log("token", token)
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: updateURL,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        },
        data: body,
        async: false,
        success: function (response) {
            console.log(response);
            result = response[0];
        },
        error: DefaultAjaxError
    });

    return result;
}

function submitContactNo() {
    var newContact = getValue("#new_ContactNumber");
    var oldContact = getValue("#old_ContactNumber");
    var txtVerifyOtp = getValue("#txtVerifyOtp");
    var input_user = getValue("#txtVerifyUser_Contact");
    var input_pass = getValue("#txtVerifyPass_Contact");
    if(invalidate("#txtVerifyOtp", "Please enter OTP")) {
        return false
    }
    if(invalidate("#txtVerifyUser_Contact", "Please enter username")) {
        return false
    }
    if(invalidate("#txtVerifyPass_Contact", "Please enter password")) {
        return false
    }

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetProfileData.ashx',
        data: 'userid=' + localStorage.getItem("emplcontid") + '&module=submit_otp&txtVerifyOtp=' + txtVerifyOtp +
        '&new_contact=' + newContact + '&old_contact=' + oldContact +
        '&input_user='+ input_user +'&input_pass='+ input_pass,
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.data == "Updated") {
                hide("#reqChg_ContactNo")

                // SFDC UPDATE
                var Empldetails = GetUserProfileDetails();
                var _empid = "EMPL" + Empldetails.EmplID;
                console.log("Empldetails : ", Empldetails);
                var body =
                {
                    "ei" : {
                    "brokerid" : Empldetails.BrokerID,
                    "Name" :  Empldetails.Company_Name ,
                    "Pannumber" :  Empldetails.Pan_No ,
                    "GSTIN" : "",
                    "GSTINState" : "",
                    "BrokerType" : "",
                    "BrokerCategory" :  "RCP" ,
                    "CompanyCategory" : "",
                    "MobileNo" :  Empldetails.Mobile ,
                    "Phone" :  Empldetails.Mobile ,
                    "BrokerEntityType" :  Empldetails.entity_type ,
                    "RegionName" : "",
                    "EmailId" :  Empldetails.Email ,
                    "Empanelment" :  _empid ,
                    "BillingCity" :  Empldetails.Billing_City ,
                    "BillingZipPostal" :  Empldetails.Billing_Zip ,
                    "BillingStreet" :  Empldetails.Billing_Street ,
                    "BillingStateProvince" :  Empldetails.Billing_State ,
                    "BillingCountry" :  Empldetails.Billing_Country ,
                    "RegisteredCity" :  Empldetails.Registered_City ,
                    "RegisteredZipPostal" :  Empldetails.Registered_Zip ,
                    "RegisteredStreet" :  Empldetails.Registered_Street ,
                    "RegisteredStateProvince" :  Empldetails.Registered_State ,
                    "RegisteredCountry" :  Empldetails.Registered_Country ,
                    "CommunicationCity" :  Empldetails.Communication_City ,
                    "CommunicationZipPostal" :  Empldetails.Communication_Zip ,
                    "CommunicationStreet" :  Empldetails.Communication_Street ,
                    "CommunicationStateProvince" :  Empldetails.Communication_State ,
                    "CommunicationCountry" :  Empldetails.Communication_Country
                    }
                };
                var msg = UpdateSFDC(JSON.stringify(body));
                if(msg[0] = "Successfully Update")
                {
                    showMsg("New contact updated succesfully.");
                }
                else
                {
                    showMsg("Error while updating SFDC. Please contact the support team.");
                }
            }
            if(response.data == "INVALID_PIN") {
                showMsg("Invalid OTP.");
            }
            if(response.data == "INVALID_LOGIN") {
                showMsg("Invalid Login Details.");
            }
        },
        error: DefaultAjaxError
    });
}


function submitReqCommAddress(){
    debugger
    var oldCommAddress = getValue("#old_CommAddress");
    var newCommAddress = getValue("#new_CommAddress");

    var oldCommCity = getValue("#old_CommCity");
    var oldCommState = getValue("#old_CommState");

    if($('#new_CommCountry option:selected').text().toUpperCase()=="INDIA")
    {
        var newCommCity = $("#comm_city").find(":selected").text();
        var newCommState = $("#comm_state").find(":selected").text();
    }
    else
    {
        var newCommCity = getValue("#txtcomm_city");
        var newCommState = getValue("#txtcomm_state");
    }

    var oldCommZipPostal = getValue("#old_CommZipPostal");
    var newCommZipPostal = getValue("#new_CommZipPostal");

    var input_user = getValue("#txtVerifyUser_CommAddr");
    var input_pass = getValue("#txtVerifyPass_CommAddr");
    if(invalidate("#new_CommAddress", "Please enter new address")) {
        return false
    }
    if(invalidate("#comm_city", "Please enter new city")) {
        return false
    }
    if(invalidate("#comm_state", "Please enter new state")) {
        return false
    }
    if(invalidate("#new_CommZipPostal", "Please enter new zipcode")) {
        return false
    }

    if(invalidate("#txtVerifyUser_CommAddr", "Please enter username")) {
        return false
    }
    if(invalidate("#txtVerifyPass_CommAddr", "Please enter password")) {
        return false
    }
    $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: baseAPI + 'GetProfileData.ashx',
            data: 'userid=' + localStorage.getItem("emplcontid") + '&module=comm_addr'
            + '&newCommAddress=' + newCommAddress + '&oldCommAddress='+ oldCommAddress
            + '&newCommCity='+ newCommCity + '&oldCommCity='+ oldCommCity
            + '&newCommState='+ newCommState + '&oldCommState='+ oldCommState
            + '&newCommZipPostal='+ newCommZipPostal + '&oldCommZipPostal='+ oldCommZipPostal
            + '&input_user=' + input_user +'&input_pass=' + input_pass,
            async: false,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.data == "Updated") {
                    hide("#reqChg_CommAddress")

                    // SFDC UPDATE
                    var Empldetails = GetUserProfileDetails();
                    var _empid = "EMPL" + Empldetails.EmplID;
                    console.log("Empldetails : ", Empldetails);
                    var body =
                    {
                        "ei" : {
                        "brokerid" : Empldetails.BrokerID,
                        "Name" :  Empldetails.Company_Name ,
                        "Pannumber" :  Empldetails.Pan_No ,
                        "GSTIN" : "",
                        "GSTINState" : "",
                        "BrokerType" : "",
                        "BrokerCategory" :  "RCP" ,
                        "CompanyCategory" : "",
                        "MobileNo" :  Empldetails.Mobile ,
                        "Phone" :  Empldetails.Mobile ,
                        "BrokerEntityType" :  Empldetails.entity_type ,
                        "RegionName" : "",
                        "EmailId" :  Empldetails.Email ,
                        "Empanelment" :  _empid ,
                        "BillingCity" :  Empldetails.Billing_City ,
                        "BillingZipPostal" :  Empldetails.Billing_Zip ,
                        "BillingStreet" :  Empldetails.Billing_Street ,
                        "BillingStateProvince" :  Empldetails.Billing_State ,
                        "BillingCountry" :  Empldetails.Billing_Country ,
                        "RegisteredCity" :  Empldetails.Registered_City ,
                        "RegisteredZipPostal" :  Empldetails.Registered_Zip ,
                        "RegisteredStreet" :  Empldetails.Registered_Street ,
                        "RegisteredStateProvince" :  Empldetails.Registered_State ,
                        "RegisteredCountry" :  Empldetails.Registered_Country ,
                        "CommunicationCity" :  Empldetails.Communication_City ,
                        "CommunicationZipPostal" :  Empldetails.Communication_Zip ,
                        "CommunicationStreet" :  Empldetails.Communication_Street ,
                        "CommunicationStateProvince" :  Empldetails.Communication_State ,
                        "CommunicationCountry" :  Empldetails.Communication_Country
                        }
                    };
                    var msg = UpdateSFDC(JSON.stringify(body));
                    if(msg[0] = "Successfully Update")
                    {
                        showMsg("Address updated succesfully.");
                    }
                    else
                    {
                        showMsg("Error while updating SFDC. Please contact the support team.");
                    }
                }
                if(response.data == "INVALID_LOGIN") {
                    showMsg("Invalid Login Details.");
                }
            },
            error: DefaultAjaxError
    });
}

function submitReqEmail() {
    var newEmail = getValue("#new_Email");
    var oldEmail = getValue("#old_Email");
    var input_user = getValue("#txtVerifyUser_Email");
    var input_pass = getValue("#txtVerifyPass_Email");
    if(invalidate("#new_Email", "Please enter new email")) {
        return false
    }
    if(invalidate("#txtVerifyUser_Email", "Please enter username")) {
        return false
    }
    if(invalidate("#txtVerifyPass_Email", "Please enter password")) {
        return false
    }
    $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: baseAPI + 'GetProfileData.ashx',
            data: 'userid=' + localStorage.getItem("emplcontid") + '&module=email&newEmail=' +
            newEmail + '&oldEmail='+ oldEmail + '&input_user=' + input_user +'&input_pass=' + input_pass,
            async: false,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.data == "Updated") {
                debugger
                    hide("#reqChg_Email");

                    // SFDC UPDATE
                    var Empldetails = GetUserProfileDetails();
                    console.log("Empldetails : ", Empldetails);
                    var _empid = "EMPL" + Empldetails.EmplID;
                    var body =
                    {
                        "ei" : {
                        "brokerid" : Empldetails.BrokerID,
                        "Name" :  Empldetails.Company_Name ,
                        "Pannumber" :  Empldetails.Pan_No ,
                        "GSTIN" : "",
                        "GSTINState" : "",
                        "BrokerType" : "",
                        "BrokerCategory" :  "RCP" ,
                        "CompanyCategory" : "",
                        "MobileNo" :  Empldetails.Mobile ,
                        "Phone" :  Empldetails.Mobile ,
                        "BrokerEntityType" :  Empldetails.entity_type ,
                        "RegionName" : "",
                        "EmailId" :  Empldetails.Email ,
                        "Empanelment" :  _empid ,
                        "BillingCity" :  Empldetails.Billing_City ,
                        "BillingZipPostal" :  Empldetails.Billing_Zip ,
                        "BillingStreet" :  Empldetails.Billing_Street ,
                        "BillingStateProvince" :  Empldetails.Billing_State ,
                        "BillingCountry" :  Empldetails.Billing_Country ,
                        "RegisteredCity" :  Empldetails.Registered_City ,
                        "RegisteredZipPostal" :  Empldetails.Registered_Zip ,
                        "RegisteredStreet" :  Empldetails.Registered_Street ,
                        "RegisteredStateProvince" :  Empldetails.Registered_State ,
                        "RegisteredCountry" :  Empldetails.Registered_Country ,
                        "CommunicationCity" :  Empldetails.Communication_City ,
                        "CommunicationZipPostal" :  Empldetails.Communication_Zip ,
                        "CommunicationStreet" :  Empldetails.Communication_Street ,
                        "CommunicationStateProvince" :  Empldetails.Communication_State ,
                        "CommunicationCountry" :  Empldetails.Communication_Country
                        }
                    };
                    var msg = UpdateSFDC(JSON.stringify(body));
                    if(msg[0] = "Successfully Update")
                    {
                        showMsg("Email updated succesfully.");
                    }
                    else
                    {
                        showMsg("Error while updating SFDC. Please contact the support team.");
                    }
                }
                if(response.data == "INVALID_LOGIN") {
                    showMsg("Invalid Login Details.");
                }
                if(response.data == "DUPLICATE_EMAIL") {
                    showMsg("Email already in use. Please try again using a different email.");
                }
            },
            error: DefaultAjaxError
    });
}

function sendChgReqOTP(){
    var newContact = getValue("#new_ContactNumber");
    if(invalidate("#new_ContactNumber", "Please enter new contact number")) {
        return false
    }
    hide("#btnSendOtp");
    show("#verifyotp")
    show("#txtVerifyOtp");
    show("#txtVerifyUser_Contact");
    show("#txtVerifyPass_Contact");
    $("#new_ContactNumber").prop('disabled', true);

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetProfileData.ashx',
        data: 'userid=' + localStorage.getItem("emplcontid") + '&module=send_otp&new_contact=' + newContact,
        async: false,
        dataType: "json",
        success: function (response) {
            //console.log(response);
            if (response.status == "Success") {
                if(response.data.length > 0) {
                    profile = response.data[0];
                }
            }
        },
        error: DefaultAjaxError
    });
}

function getProfileInfo(flag){
    var profile;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetProfileData.ashx?userid=' + localStorage.getItem("emplcontid") + '&module=base',
        //data:{"userid" : localStorage.getItem("emplcontid")},
        async: false,
        dataType: "json",
        success: function (response) {
            console.log("GetProfileData :", response);
            if (response.status == "Success") {
                if(response.data.length > 0) {
                    if(flag == "COMMON"){
                        profile = response.data[0];
                    }
                }
            }
        },
        error: DefaultAjaxError
    });
    return profile;
}

function getUpdateEmplDD(selected){
    switch (selected.value) {
      case "1":

      break;
      case "2":
        data = getProfileInfo("COMMON");
        setValue("#old_ContactNumber", data.ContactNo);
        hideIds("#reqChg_Email", "#reqChg_RegAddress", "#reqChg_CommAddress", "#verifyotp", "#txtVerifyOtp", ".loadForm", "#reqChg_Rera");
        show("#reqChg_ContactNo");
      break;
      case "3":
        data = getProfileInfo("COMMON");
        setValue("#old_Email", data.Email);
        hideIds("#reqChg_ContactNo", "#reqChg_RegAddress", "#reqChg_CommAddress", "#reqChg_Rera");
        show("#reqChg_Email");
        show("#txtVerifyUser_Email");
        show("#txtVerifyPass_Email");
      break;
      case "4":
        data = getProfileInfo("COMMON");
        setValue("#old_RegAddress", data.RegisteredAddress);
        hideIds("#reqChg_ContactNo", "#reqChg_Email", "#reqChg_CommAddress", "#reqChg_Rera", "#reqChg_Gst");
        fillRegcountry();
        fillRegState();
        show("#reqChg_RegAddress");
      break;
      case "5":
        data = getProfileInfo("COMMON");
        debugger
        setValue("#old_CommCountry", data.CommunicationCountry);
        setValue("#old_CommCity", data.CommunicationCity);
        setValue("#old_CommState", data.CommunicationState);
        setValue("#old_CommZipPostal", data.CommunicationZip);
        setValue("#old_CommAddress", data.CommunicationAddress);
        hideIds("#reqChg_ContactNo", "#reqChg_Email", "#reqChg_RegAddress", "#reqChg_Rera", "#reqChg_Gst");
        fillCommcountry();
        fillCommState();
        show("#reqChg_CommAddress");
        show("#txtVerifyUser_CommAddr");
        show("#txtVerifyPass_CommAddr");
      break;
      case "6":
        hideIds("#reqChg_ContactNo", "#reqChg_Email", "#reqChg_RegAddress", "#reqChg_CommAddress", "#reqChg_Gst");
        show("#reqChg_Rera");
        hide("#rera_block");
        fillState();
        fillDocDetails("GET_RERA");
      break;
      case "7":
        hideIds("#reqChg_ContactNo", "#reqChg_Email", "#reqChg_RegAddress", "#reqChg_CommAddress", "#reqChg_Rera");
        show("#reqChg_Gst");
        hide("#gst_block");
        fillState();
        fillDocDetails("GET_GST");
      break;
    }
}

function submitReqContactNo(){
    console.log(value);
}

function displayCPProperties(quarter){
    debugger
    var userid = localStorage.getItem("emplcontid");
    if (quarter == "") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a quarter & financial year.');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + 'GetBookingData.ashx',
        data:{"userid" : userid, "quarter": quarter},
        dataType: "json",
        async: false,
        success: function (response) {
            if (response.status == "Success") {
                setTimeout(function () {
                    $('.ui-loader').hide();
                }, 500);
                if(response.data.length>0){
                    $("#noDataFound").hide();
                    //document.getElementById("noDataFound").style.display = "none"; // to hide
                    $(".tblBookingStatusRow").remove();
                    var BookingStatusTable = document.getElementById("BookingStatus");
                    $.each(response.data, function (index, value) {
                        var tableRow = BookingStatusTable.insertRow(index+1);
                        $(tableRow).css({"border-bottom": "1px solid grey"});
                        tableRow.className="tblBookingStatusRow";
                        var PrimaryApplicantName = tableRow.insertCell(0);
                        var project_Name = tableRow.insertCell(1);
                        var registration_Date = tableRow.insertCell(2);
                        var AgreementAmount = tableRow.insertCell(3);
                        var paymentRealized = tableRow.insertCell(4);
                        $(registration_Date).css({"text-align": "center"});
                        $(AgreementAmount).css({"text-align": "center"});
                        $(paymentRealized).css({"text-align": "center"});

                        PrimaryApplicantName.innerHTML = value.PrimaryApplicantName;
                        project_Name.innerHTML = value.ProjectName;
                        registration_Date.innerHTML = value.RegistrationDate;
                        AgreementAmount.innerHTML =  (parseFloat(value.AgreementAmount)/10000000).toFixed(2);
                        paymentRealized.innerHTML = value.paymentRealized + "%";
                   });
                }
                else{
                    $('.ui-loader').hide();
                    $(".tblBookingStatusRow").remove();
                    $("#noDataFound").show();
                }
            }
            else{
                $('.ui-loader').hide();
            }
        },
        error: DefaultAjaxError
    });
}

function displayCancelCPProperties(quarter){
    debugger
    var userid = localStorage.getItem("emplcontid");
    if (quarter == "") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a quarter & financial year.');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + 'GetBookingCancelData.ashx',
        data:{"userid" : userid, "quarter": quarter},
        dataType: "json",
        async: false,
        success: function (response) {
            if (response.status == "Success") {
                setTimeout(function () {
                    $('.ui-loader').hide();
                }, 500);
                if(response.data.length>0){
                    $("#noDataFound").hide();
                    //document.getElementById("noDataFound").style.display = "none"; // to hide
                    $(".tblBookingStatusRow").remove();
                    var BookingStatusTable = document.getElementById("BookingStatus");
                    $.each(response.data, function (index, value) {
                        var tableRow = BookingStatusTable.insertRow(index+1);
                        $(tableRow).css({"border-bottom": "1px solid grey"});
                        tableRow.className="tblBookingStatusRow";
                        var PrimaryApplicantName = tableRow.insertCell(0);
                        var project_Name = tableRow.insertCell(1);
                        var registration_Date = tableRow.insertCell(2);
                        var AgreementAmount = tableRow.insertCell(3);
                        var paymentRealized = tableRow.insertCell(4);
                        $(registration_Date).css({"text-align": "center"});
                        $(AgreementAmount).css({"text-align": "center"});
                        $(paymentRealized).css({"text-align": "center"});

                        PrimaryApplicantName.innerHTML = value.PrimaryApplicantName;
                        project_Name.innerHTML = value.ProjectName;
                        registration_Date.innerHTML = value.RegistrationDate;
                        AgreementAmount.innerHTML =  (parseFloat(value.AgreementAmount)/10000000).toFixed(2);
                        paymentRealized.innerHTML = value.paymentRealized + "%";
                   });
                }
                else{
                    $('.ui-loader').hide();
                    $(".tblBookingStatusRow").remove();
                    $("#noDataFound").show();
                }
            }
            else{
                $('.ui-loader').hide();
            }
        },
        error: DefaultAjaxError
    });
}

function displayBookingSummaryStatus(quarter, projectId){
    var userid = localStorage.getItem("emplcontid");
    if (quarter == "SELECT QUARTER & FINANCIAL YEAR") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a quarter & financial year.');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + 'GetBookingData.ashx',
        data:{"userid" : userid, "quarter": quarter, "sfdcprojectid": projectId},
        dataType: "json",
        async: false,
        success: function (response) {
            if (response.status == "Success") {
                setTimeout(function () {
                    $('.ui-loader').hide();
                }, 500);
                if(response.data.length>0){
                    $("#noDataFound").hide();
                    //document.getElementById("noDataFound").style.display = "none"; // to hide
                    $(".tblBookingStatusRow").remove();
                    var BookingStatusTable = document.getElementById("BookingStatus");
                    $.each(response.data, function (index, value) {
                        var tableRow = BookingStatusTable.insertRow(index+1);
                        $(tableRow).css({"border-bottom": "1px solid grey"});
                        tableRow.className="tblBookingStatusRow";
                        var PrimaryApplicantName = tableRow.insertCell(0);
                        var project_Name = tableRow.insertCell(1);
                        var registration_Date = tableRow.insertCell(2);
                        var AgreementAmount = tableRow.insertCell(3);
                        var paymentRealized = tableRow.insertCell(4);
                        $(registration_Date).css({"text-align": "center"});
                        $(AgreementAmount).css({"text-align": "center"});
                        $(paymentRealized).css({"text-align": "center"});
                        PrimaryApplicantName.innerHTML = value.PrimaryApplicantName;
                        project_Name.innerHTML = value.ProjectName;
                        registration_Date.innerHTML = value.RegistrationDate;
                        AgreementAmount.innerHTML =  (parseFloat(value.AgreementAmount)/10000000).toFixed(2);
                        paymentRealized.innerHTML = value.paymentRealized + "%";
                        //invoiceDownload.innerHTML = "<img src='http://cp.godrejproperties.com/images/app/download.png' />";
                   });
                }
                else{
                    $('.ui-loader').hide();
                    $(".tblBookingStatusRow").remove();
                    $("#noDataFound").show();
                }
            }
            else{
                $('.ui-loader').hide();
            }
        },
        error: DefaultAjaxError
    });
}

function displayBookingCancelSummaryStatus(quarter, projectId){
    var userid = localStorage.getItem("emplcontid");
    if (quarter == "SELECT QUARTER & FINANCIAL YEAR") {
        $(".tblBookingStatusRow").remove();
        $("#noDataFound").show();
        //document.getElementById("noDataFound").style.display = "none"; // to hide
        showMsg('Please select a quarter & financial year.');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getCPProperties/'+ quarter + "/" + panno,
        url: baseAPI + 'GetBookingCancelData.ashx',
        data:{"userid" : userid, "quarter": quarter, "sfdcprojectid": projectId},
        dataType: "json",
        async: false,
        success: function (response) {
            if (response.status == "Success") {
                setTimeout(function () {
                    $('.ui-loader').hide();
                }, 500);
                if(response.data.length>0){
                    $("#noDataFound").hide();
                    //document.getElementById("noDataFound").style.display = "none"; // to hide
                    $(".tblBookingStatusRow").remove();
                    var BookingStatusTable = document.getElementById("BookingStatus");
                    $.each(response.data, function (index, value) {
                        var tableRow = BookingStatusTable.insertRow(index+1);
                        $(tableRow).css({"border-bottom": "1px solid grey"});
                        tableRow.className="tblBookingStatusRow";
                        var PrimaryApplicantName = tableRow.insertCell(0);
                        var project_Name = tableRow.insertCell(1);
                        var registration_Date = tableRow.insertCell(2);
                        var AgreementAmount = tableRow.insertCell(3);
                        var paymentRealized = tableRow.insertCell(4);
                        $(registration_Date).css({"text-align": "center"});
                        $(AgreementAmount).css({"text-align": "center"});
                        $(paymentRealized).css({"text-align": "center"});
                        PrimaryApplicantName.innerHTML = value.PrimaryApplicantName;
                        project_Name.innerHTML = value.ProjectName;
                        registration_Date.innerHTML = value.RegistrationDate;
                        AgreementAmount.innerHTML =  (parseFloat(value.AgreementAmount)/10000000).toFixed(2);
                        paymentRealized.innerHTML = value.paymentRealized + "%";
                        //invoiceDownload.innerHTML = "<img src='http://cp.godrejproperties.com/images/app/download.png' />";
                   });
                }
                else{
                    $('.ui-loader').hide();
                    $(".tblBookingStatusRow").remove();
                    $("#noDataFound").show();
                }
            }
            else{
                $('.ui-loader').hide();
            }
        },
        error: DefaultAjaxError
    });
}

function displayWalkInSummaryStatus(quarter, projectId){
    var userid = localStorage.getItem("emplcontid");
    if (quarter == "") {
        $(".tblWalkInStatusRow").remove();
        $("#noDataFound").hide(); // to hide
        showMsg('Please select a quarter & financial year.');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseAPI + 'GetEnquiryData.ashx',
        data:{"userid" : userid, "quarter": quarter, "sfdcprojectid": projectId},
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            setTimeout(function () {
                $('.ui-loader').hide();
            }, 500);
            if (response.status == "Success") {
                if(response.data.length>0){
                    $("#noDataFound").hide(); // to hide
                    $(".tblWalkInStatusRow").remove();
                    var BookingStatusTable = document.getElementById("WalkInStatus");
                    $.each(response.data, function (index, value) {
                        var tableRow = BookingStatusTable.insertRow(index+1);
                        $(tableRow).css({"border-bottom": "1px solid grey"});
                        tableRow.className="tblWalkInStatusRow";
                        var customer_Name = tableRow.insertCell(0);
                        var project_Name = tableRow.insertCell(1);
                        var mobile_Number = tableRow.insertCell(2);
                        var walkIn_Status = tableRow.insertCell(3);

                        $(mobile_Number).css({"text-align": "center"});
                        $(walkIn_Status).css({"text-align": "center"});

                        //var comments_Status = tableRow.insertCell(4);

                        customer_Name.innerHTML = value.customerName;
                        project_Name.innerHTML = value.ProjectName;
                        mobile_Number.innerHTML = value.mobileNo;
                        walkIn_Status.innerHTML = value.enquiryStatus;
                        //comments_Status.innerHTML = "<span style='color:#007fc8'>Click Here</span>";
                        //comments_Status.value = value.comments;
                        //comments_Status.className="comments_StatusClick"
                    });
                }
                else{
                    $('.ui-loader').hide();
                    $(".tblWalkInStatusRow").remove();
                    $("#noDataFound").show(); // to show
                }
            }
            else{
                $('.ui-loader').hide();
            }
        },
        error: DefaultAjaxError
    });
}


function displayWalkInDetails(quarter){
    var userid = localStorage.getItem("emplcontid");
    if (quarter == "") {
        $(".tblWalkInStatusRow").remove();
        $("#noDataFound").hide(); // to hide
        showMsg('Please select a quarter & financial year.');
        return false;
    }
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        //url: baseURLEOI + 'CPProperties.svc/getWalkInData/'+ quarter + "/" + panno,
        url: baseAPI + 'GetEnquiryData.ashx',
        data:{"userid" : userid, "quarter": quarter},
        async: false,
        dataType: "json",
        success: function (response) {
            console.log(response);
            setTimeout(function () {
                $('.ui-loader').hide();
            }, 500);
            if (response.status == "Success") {
                if(response.data.length>0){
                    $("#noDataFound").hide(); // to hide
                    $(".tblWalkInStatusRow").remove();
                    var BookingStatusTable = document.getElementById("WalkInStatus");
                    $.each(response.data, function (index, value) {
                        var tableRow = BookingStatusTable.insertRow(index+1);
                        $(tableRow).css({"border-bottom": "1px solid grey"});
                        tableRow.className="tblWalkInStatusRow";
                        var customer_Name = tableRow.insertCell(0);
                        var project_Name = tableRow.insertCell(1);
                        var mobile_Number = tableRow.insertCell(2);
                        var walkIn_Status = tableRow.insertCell(3);

                        $(mobile_Number).css({"text-align": "center"});
                        $(walkIn_Status).css({"text-align": "center"});

                        //var comments_Status = tableRow.insertCell(4);

                        customer_Name.innerHTML = value.customerName;
                        project_Name.innerHTML = value.ProjectName;
                        mobile_Number.innerHTML = value.mobileNo;
                        walkIn_Status.innerHTML = value.enquiryStatus;
                        //comments_Status.innerHTML = "<span style='color:#007fc8'>Click Here</span>";
                        //comments_Status.value = value.comments;
                        //comments_Status.className="comments_StatusClick"
                    });
                }
                else{
                    $('.ui-loader').hide();
                    $(".tblWalkInStatusRow").remove();
                    $("#noDataFound").show(); // to show
                }
                /*
                // Show comment data when click on particular table cell
                $(".comments_StatusClick").on("click",function(){
                    showMsg(this.value);
                    /*
                    $('#error_content').empty().html(this.value);
                    setTimeout(function () {
                        $('#enquiry_inline').simpledialog2({
                            callbackClose: function () {
                                var me = this;
                                setTimeout(function () { me.close(); }, 2000);
                            }
                        });
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 500);
                    /
                    return false;
                });
                */
            }
            else{
                $('.ui-loader').hide();
            }
        },
        error: DefaultAjaxError
    });
}

function sharingVerification(){
    var email = localStorage.getItem("email");
    var mobile = localStorage.getItem("mobileno");
    var isOTPGenerated = localStorage.getItem("isOTPGenerated");
    var showOTPDialog = localStorage.getItem("showOTPDialog");

    if(showOTPDialog == 1){
        var userid = localStorage.getItem("emplcontid");
        $.ajax({
             type: "POST",
             //url: baseURLEOI + 'EOIService.svc/getOTPForMenu',
             url: baseAPI + 'GetOTPForMenu.ashx',
             data: {"userid":userid},
             //contentType: "application/json; charset=utf-8",
             dataType: "json",
             processData: true,
             async: false,
             success: function (response) {
                if(response.status == "Success"){
                    localStorage.setItem("isOTPGenerated", "1");
                    var verificationMessage = "A verification code has been sent to your registered email ID "+ email + " with an SMS to " + mobile;
                    alert(verificationMessage);
                    $('#message_content').empty().html('Enter Verification Code:');
                    setTimeout(function () {
                        $('#otp_inline').simpledialog2({
                            callbackClose: function () {
                                var me = this;
                            }
                        });
                        $('.ui-simpledialog-screen').css('height', $(document).height());
                    }, 500);
                }
                else if(response.status == "Fail"){
                    alert('Something went wrong, Please try again later.');
                }
           },
           error: DefaultAjaxError
        });
        return false;
    }
}

/*
function getOTPAPICall(){
var panno = localStorage.getItem("panno");
var brokerid = localStorage.getItem("brokerid");
var email = localStorage.getItem("email");
var mobile = localStorage.getItem("mobileno");

var otpData = {
"brokerpanno":panno, "brokerId":brokerid, "emailId":email, "mobileNo":mobile
}
         $.ajax({
                 type: "POST",
                 url: baseURLEOI + 'EOIService.svc/getOTPForMenu',
                 data: JSON.stringify(otpData),
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 processData: true,
                 async: false,
                 success: function (response) {
                 if(response.status == "Success"){
                 localStorage.setItem("isOTPGenerated", "1");
                 }else if(response.status == "Fail"){
                 alert('Something went wrong, Please try again later.');
                 }
               },
                 failure: function (response) {
                     alert('Fail');
                 }
         });
}

*/
function submitOTP(){
    var userid = localStorage.getItem("emplcontid");
    var otp = $('#otp').val();

// Current below validation didn't work.
    if (otp == null || otp == "") {
        console.log("Inside OTP");
        showMsg('Please enter OTP.')
        return false;
    }
    if (otp.length < 6 || otp.length > 6) {
        showMsg('OTP number should be 6 digits.')
        return false;
    }
    var otpData = {
        "userid":userid,
        "otp":otp
    }
    $.ajax({
         type: "POST",
         //url: baseURLEOI + 'EOIService.svc/verifyOtp',
         url: baseAPI + 'verifyOtp.ashx',
         data: otpData,
         //contentType: "application/json; charset=utf-8",
         dataType: "json",
         processData: true,
         async: false,
         success: function (response) {
             if(response.statusCode == 200){
                console.log("Inside Success OTP");
                localStorage.setItem("showOTPDialog", 0);
             }
             else if(response.statusCode == 400){
                showMsg('Invalid OTP.');
             }
         },
         error: DefaultAjaxError
    });
}

// To display project list
function DisplayAllKitProjects(cityid) {
    $("#ul_region").html("");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc" + '/DisplayAllCPKitProj/'+cityid,
        async: false,
        success: function (response) {
            var ul = $("#ul_region");
            $.each(response.DisplayAllCPKitProjResult, function (index, value) {
                if (value.Status == "Success") {
                    ul.append('<li><a href="cp_kit.html?id=' + value.projectid + '" data-transition="slide">' + value.title + '<br><span>' + value.cityname + '</span></a></li>');
                    //ul.append('<li><a href="my-inbox.html?projectid=' + value.projectid + '" data-transition="slide">' + value.title + '<br><span>' + value.cityname + '</span></a></li>');
                }
                else
                    ul.append('<li style="text-align:center;">No properties found</li>');
            });
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}



// To Get Cities for inbox
function GetCityKit() {
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'CPService.svc/DisplayAllKitCity',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="0">  CHOOSE BY CITY </option>';

            $.each(response.DisplayAllKitCityResult, function (index, value) {
                if (value.Status == "Success") {

                    optionsValues += '<option value="' + value.CityId + '">' + value.CityName + '</option>';
                }
            });
            var options = $('#ddlCity');
            options.html(optionsValues);
            $('.ui-loader').hide();
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

// To display project list
// function DisplayAllRegions() {
//     $.ajax({
//         type: "GET",
//         contentType: "application/json; charset=utf-8",
//         url: baseURL + "CPService.svc" + '/DisplayAllRegion',
//         async: false,
//         success: function (response) {
//             var ul = $("#ul_region");
//             $.each(response.DisplayAllRegionResult, function (index, value) {
//                 if (value.Status == "Success") {
//                     ul.append('<li><a href="cp_kit.html?id=' + value.ID + '" data-transition="slide">' + value.Region + '</span></a></li>');
//                 }
//                 else
//                     ul.append('<li style="text-align:center;">No properties found</li>');
//             });
//         },
//         failure: function (response) {
//             alert('Fail');
//         }
//     });
// }


//for inbox
function getCPkitdoc(projectid) {
    $("#cpkitdoc").html("");
    var div = $("#cpkitdoc");
    var divse = '';
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc/GetCPKit/" + projectid,
        success: function (response) {
            console.log(response.GetCPKitResult);
            $.each(response.GetCPKitResult, function (index, value) {
                if (value.Status == "success") {
                    divse += '<h1><img class="img doc_img" src="images/Document_Gray.png"><span class="title1_1"><span id"desc1">' + value.Title + '</span></span><span class="date1"><strong>Date:</strong> ' + value.Date + '</span><br><a rel="' + value.Image + '" class="view1 ui-link" href="javascript:void(0);">View document</a></h1>';

                }
                else {
                    divse += '<span style="text-align: center; text-transform: none; display: block; color: #333333;">No Documents</span>';
                }
            });
            div.append(divse);
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

// To Get Cities for inbox
function GetCityDoc() {
    $('.ui-loader').show();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + 'CPService.svc/DisplayAllDocCity',
        processData: false,
        async: false,
        success: function (response) {
            var optionsValues = '<select>';
            optionsValues += '<option value="0">  CHOOSE BY CITY </option>';

            $.each(response.DisplayAllDocCityResult, function (index, value) {
                if (value.Status == "Success") {

                    optionsValues += '<option value="' + value.CityId + '">' + value.CityName + '</option>';
                }
            });
            var options = $('#ddlCity');
            options.html(optionsValues);
            $('.ui-loader').hide();
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}


// To display project list
function GetCampaign() {
    $("#ul_campaign").html("");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc" + '/DisplayAllCampaign',
        async: false,
        success: function (response) {
            var ul = $("#ul_campaign");
            $.each(response.DisplayAllCampaignResult, function (index, value) {
                if (value.Status == "Success") {
                    ul.append('<li><a href="my-campaign.html?campaignid=' + value.CampaignId + '" data-transition="slide">' + value.CampaignName + '</span></a></li>');
                }
                //else
                    //ul.append('<li style="text-align:center;">No properties found</li>');
            });
            if($("ul#ul_campaign").has("li").length==0)
            {
                $("#div_campaign").hide();
            }
            else{
                $("#div_campaign").show();
            }
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

//for inbox
function getCampaigndoc(projectid) {
    $("#campaigndoc").html("");
    var div = $("#campaigndoc");
    var divse = '';
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc/DisplayAllCampaignDocs/" + projectid,
        success: function (response) {
            $.each(response.DisplayAllCampaignDocsResult, function (index, value) {
                if (value.Status == "Success") {
                    divse += '<h1><img class="img doc_img" src="images/Document_Gray.png"><span class="title1_1"><span id"desc1">' + value.title + '</span></span><span class="date1"><strong>Date:</strong> ' + value.date + '</span><br><a rel="' + value.Image + '" class="view1 ui-link" href="javascript:void(0);">View document</a></h1>';

                }
                else {
                    divse += '<span style="text-align: center; text-transform: none; display: block; color: #333333;">No Documents</span>';
                }
            });
            div.append(divse);
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

//for gpl policy
function getAllPolicy() {
debugger
    $("#div_policy").html("");
    var div = $("#div_policy");
    var divse = '';
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: baseURLEOI + "CPService.svc/DisplayAllPolicy",
        success: function (response) {
            $.each(response.DisplayAllPolicyResult, function (index, value) {
                if (value.Status == "Success") {
                    divse += '<h1><img class="img doc_img" src="images/Document_Gray.png"><span class="title1_1"><span id"desc1">' + value.title + '</span></span><br><a rel="' + value.image + '" class="view1 ui-link" href="javascript:void(0);">View document</a></h1>';

                }
                else {
                    divse += '<span style="text-align: center; text-transform: none; display: block; color: #333333;">No Documents</span>';
                }
            });
            div.append(divse);
        },
        failure: function (response) {
            alert('Fail');
        }
    });
}

function showMsg(msg, callback){
    $('#error_content').empty().html(msg);
    setTimeout(function () {
        $('#enquiry_inline').simpledialog2({
            callbackClose: function (event, ui) {
                $('.ui-loader').hide();
                if(callback != undefined){
                    callback(event, ui);
                }
                //var me = this;
                //setTimeout(function () { me.close(); }, 2000);
            }
        });
        $('.ui-simpledialog-screen').css('height', $(document).height());
    }, 500);
}